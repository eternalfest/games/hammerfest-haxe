package hf.entity;

import hf.Entity;
import hf.Pos;
import hf.entity.Animator;
import hf.levels.TeleporterData;
import hf.mode.GameMode;

typedef MvtSteps = {
  var total: Float;
  var dx: Float;
  var dy: Float;
}

class Physics extends Animator {
  public var gravityFactor: Float;
  public var fallFactor: Float;
  public var shockResistance: Float;
  public var fl_stable: Bool;
  public var fl_physics: Bool;
  public var fl_friction: Bool;
  public var fl_gravity: Bool;
  public var fl_strictGravity: Bool;
  public var fl_hitGround: Bool;
  public var fl_hitCeil: Bool;
  public var fl_hitWall: Bool;
  public var fl_hitBorder: Bool;
  public var fl_slide: Bool;
  public var fl_teleport: Bool;
  public var fl_portal: Bool;
  public var fl_bump: Bool;
  public var fl_moveable: Bool;
  public var fl_wind: Bool;
  public var fl_skipNextGravity: Bool;
  public var fl_skipNextGround: Bool;
  public var fl_stopStepping: Bool;
  public var lastTeleporter: TeleporterData;
  public var fallStart: Float;
  public var slideFriction: Float;

  // Used by entity.bad.Flyer and entity.bad.WallWalker
  public var fl_intercept: Null<Bool>;

  public function new(): Void {
    super();
    this.dx = 0;
    this.dy = 0;
    this.gravityFactor = 1.0;
    this.fallFactor = 1.0;
    this.shockResistance = 1.0;
    this.fl_stable = false;
    this.fl_physics = true;
    this.fl_friction = true;
    this.fl_gravity = true;
    this.fl_strictGravity = true;
    this.fl_hitGround = true;
    this.fl_hitCeil = false;
    this.fl_hitWall = true;
    this.fl_hitBorder = true;
    this.fl_slide = true;
    this.fl_teleport = false;
    this.fl_portal = false;
    this.fl_wind = false;
    this.fl_moveable = true;
    this.fl_bump = false;
    this.fl_stopStepping = false;
    this.fl_skipNextGravity = true;
    this.fl_skipNextGround = false;
  }

  public function enablePhysics(): Void {
    this.fl_physics = true;
  }

  public function disablePhysics(): Void {
    this.fl_physics = false;
  }

  public function shockWave(e: Physics, radius: Float, power: Float): Void {
    if (!e.fl_moveable) {
      return;
    }
    power /= e.shockResistance;
    var v5 = e.distance(this.x, this.y);
    var v6 = 1 - v5 / radius;
    var v7 = Math.atan2(e.y - this.y, e.x - this.x);
    e.dx = Math.cos(v7) * v6 * power;
    if (e.fl_stable) {
      e.dy = -5;
    } else {
      if (e.fl_intercept) {
        e.dy = 0;
        e.dx *= 2;
      } else {
        e.dy += Math.sin(v7) * v6 * power;
      }
      e.dy = Math.max(e.dy, -10);
    }
    e.fl_stable = false;
  }

  public function killHit(dx: Null<Float>): Void {
    if (this.fl_kill) {
      return;
    }
    this.dx = dx;
    this.dy = -10;
    this.fl_hitGround = false;
    this.fl_hitWall = false;
    this.fl_kill = true;
    this.fallFactor = Data.FALL_FACTOR_DEAD;
    this.onKill();
  }

  public function resurrect(): Void {
    this.fl_hitGround = true;
    this.fl_hitWall = true;
    this.fl_kill = false;
    this.fallFactor = 1.0;
    this.fl_stopStepping = true;
  }

  public function moveToAng(angDeg: Float, speed: Float): Void {
    var v4 = Math.PI * angDeg / 180;
    this.dx = Math.cos(v4) * speed;
    this.dy = Math.sin(v4) * speed;
  }

  public function moveToTarget(e: Pos<Float>, speed: Float): Void {
    var v4 = Math.atan2(e.y - this.y, e.x - this.x);
    this.dx = Math.cos(v4) * speed;
    this.dy = Math.sin(v4) * speed;
  }

  public function moveToPoint(x: Float, y: Float, speed: Float): Void {
    var v5 = Math.atan2(y - this.y, x - this.x);
    this.dx = Math.cos(v5) * speed;
    this.dy = Math.sin(v5) * speed;
  }

  public function moveUp(speed: Float): Void {
    this.moveToAng(-90, speed);
  }

  public function moveDown(speed: Float): Void {
    this.moveToAng(90, speed);
  }

  public function moveLeft(speed: Float): Void {
    this.moveToAng(180, speed);
  }

  public function moveRight(speed: Float): Void {
    this.moveToAng(0, speed);
  }

  public function moveFrom(e: Pos<Float>, speed: Float): Void {
    this.x = e.x;
    this.y = e.y;
    var v4 = -HfStd.random(Math.round(100 * Math.PI)) / 100;
    this.x = e.x + Math.cos(v4) * Data.CASE_WIDTH * 2;
    this.y = e.y + Math.sin(v4) * Data.CASE_HEIGHT * 2;
    this.moveToTarget(e, speed);
    this.dx = -this.dx;
    this.dy = -this.dy;
  }

  public function moveToGround(): Void {
    if (this.fl_stable) {
      return;
    }
    var v2 = this.world.getGround(this.cx, this.cy);
    this.moveToCase(v2.x, v2.y);
  }

  public function checkHits(): Void {
    var v2 = this.getByType(Data.ENTITY);
    for (v3 in 0...v2.length) {
      if (!v2[v3].fl_kill && !this.fl_kill && this.hitBound(v2[v3]) && v2[v3].uniqId != this.uniqId) {
        this.hit(v2[v3]);
        v2[v3].hit(this);
      }
    }
  }

  public function prefix(): Void {
  }

  public function infix(): Void {
    this.checkHits();
    var v2 = this.world.getCase({x: this.cx, y: this.cy});
    if (this.fl_teleport && !this.fl_kill) {
      if (v2 == Data.FIELD_TELEPORT) {
        var v3 = this.world.getTeleporter(this, this.cx, this.cy);
        if (v3 != null) {
          var v4 = this.world.getNextTeleporter(v3);
          this.game.fxMan.attachFx(this.x, this.y - Data.CASE_HEIGHT, "hammer_fx_pop");
          if (v4.td.dir == Data.HORIZONTAL) {
            if (v4.fl_rand) {
              this.moveTo(v4.td.centerX - Data.CASE_WIDTH * 0.5, v4.td.centerY);
            } else {
              this.moveTo(v4.td.centerX + Entity.x_ctr(this.cx) - v3.centerX, v4.td.centerY);
            }
          } else {
            if (v4.fl_rand) {
              this.moveTo(v4.td.centerX, v4.td.centerY);
            } else {
              this.moveTo(v4.td.centerX, v4.td.centerY + Entity.y_ctr(this.cy) - v3.centerY);
            }
          }
          this.game.fxMan.attachFx(this.x, this.y - Data.CASE_HEIGHT, "hammer_fx_shine");
          this.game.soundMan.playSound("sound_teleport", Data.CHAN_FIELD);
          this.fl_stopStepping = true;
          this.lastTeleporter = v4.td;
          this.onTeleport();
        }
      } else {
        this.lastTeleporter = null;
      }
    }
    if (this.fl_portal && !this.fl_kill) {
      if (v2 == Data.FIELD_PORTAL) {
        if (this.game.fl_clear) {
          var v5 = this.cx;
          var v6 = this.cy;
          while (this.world.getCase({x: v5 - 1, y: v6}) == Data.FIELD_PORTAL) {
            --v5;
          }
          while (this.world.getCase({x: v5, y: v6 - 1}) == Data.FIELD_PORTAL) {
            --v6;
          }
          var v7 = null;
          for (v8 in 0...this.world.portalList.length) {
            if (this.world.portalList[v8].cx == v5 && this.world.portalList[v8].cy == v6) {
              v7 = v8;
            }
          }
          this.onPortal(v7);
        } else {
          this.onPortalRefusal();
        }
      }
    }
    if (this.fl_bump && !this.fl_kill && v2 == Data.FIELD_BUMPER) {
      var v9 = Data.VERTICAL;
      if (this.world.getCase({x: this.cx - 1, y: this.cy}) == v2 || this.world.getCase({x: this.cx + 1, y: this.cy}) == v2) {
        v9 = Data.HORIZONTAL;
      }
      if (v9 == Data.HORIZONTAL && Math.abs(this.dy) < 20) {
        this.dx = 0;
        this.dy *= 5;
      }
      if (v9 == Data.VERTICAL && Math.abs(this.dx) < 20) {
        this.dx *= 7;
        this.dy = 0;
      }
      this.onBump();
    }
  }

  public function postfix(): Void {
  }

  public function recal(): Void {
    this.y = Entity.y_ctr(Entity.y_rtc(this.y));
    this.updateCoords();
    while (this.world.getCase({x: this.cx, y: this.cy}) == Data.GROUND) {
      this.y -= Data.CASE_HEIGHT;
      this.updateCoords();
    }
  }

  public function calcSteps(dxStep: Float, dyStep: Float): MvtSteps {
    var v4 = dxStep * Timer.tmod;
    var v5 = dyStep * Timer.tmod;
    var v6: Float = Math.ceil(Math.abs(v4) / Data.STEP_MAX);
    v6 = Math.max(v6, Math.ceil(Math.abs(v5) / Data.STEP_MAX));
    return {total: v6, dx: v4 / v6, dy: v5 / v6};
  }

  public function needsPatch(): Bool {
    return false;
  }

  public function onKill(): Void {
  }

  public function onDeathLine(): Void {
  }

  public function onHitWall(): Void {
    this.dx = 0;
  }

  public function onHitGround(f: Float): Void {
    this.fl_stable = true;
    this.dy = 0;
    this.recal();
  }

  public function onHitCeil(): Void {
    this.dy = 0;
  }

  public function onTeleport(): Void {
  }

  public function onPortal(pid: Int): Void {
  }

  public function onPortalRefusal(): Void {
  }

  public function onBump(): Void {
  }

  override public function endUpdate(): Void {
    if (this.fl_hitBorder) {
      if (this.x < Data.BORDER_MARGIN) {
        this.x = Data.BORDER_MARGIN;
      }
      if (this.x >= Data.GAME_WIDTH - Data.BORDER_MARGIN) {
        this.x = Data.GAME_WIDTH - Data.BORDER_MARGIN - 1;
      }
    }
    if (this.game.fl_aqua) {
      if (!this.isType(Data.FX) && (this.dx != 0 || this.dy != 0)) {
        if (HfStd.random(100) <= 10) {
          this.game.fxMan.inGameParticles(Data.PARTICLE_BUBBLE, this.x + HfStd.random(Data.CASE_WIDTH) * (HfStd.random(2) * 2 - 1), this.y - HfStd.random(Data.CASE_HEIGHT), 1);
        }
      }
    }
    super.endUpdate();
  }

  override public function init(g: GameMode): Void {
    super.init(g);
    this.register(Data.PHYSICS);
  }

  override public function update(): Void {
    super.update();
    if (!this.fl_physics) {
      return;
    }
    this.updateCoords();
    if (this.fl_wind) {
      if (this.fl_stable && this.game.fl_wind) {
        this.dx += this.game.windSpeed * Timer.tmod;
      }
    }
    if (this.fl_stable) {
      if (this.dy != 0 || this.world.getCase({x: this.fcx, y: this.fcy}) != Data.GROUND) {
        this.fl_stable = false;
      }
    }
    if (this.dx != 0 || this.dy != 0 || !this.fl_stable) {
      if (!this.fl_skipNextGravity && !this.fl_stable && this.fl_gravity) {
        var v4 = 1.0;
        if (Timer.tmod >= 2) {
          v4 = 1.1;
        }
        if (this.dy < 0) {
          this.dy += this.gravityFactor * Data.GRAVITY * Timer.tmod * v4;
        } else {
          if (this.fallStart == null) {
            this.fallStart = this.y;
          }
          if (this.game.fl_aqua && !this.fl_strictGravity) {
            this.dy += 0.3 * this.fallFactor * Data.FALL_SPEED * Timer.tmod * v4;
          } else {
            this.dy += this.fallFactor * Data.FALL_SPEED * Timer.tmod * v4;
          }
        }
      }
      this.fl_skipNextGravity = false;
      this.prefix();
      var v5 = this.calcSteps(this.dx, this.dy);
      var v3 = 0;
      while (true) {
        if (!(!this.fl_stopStepping && v3 < v5.total)) break;
        var v6 = this.cx;
        var v7 = this.cy;
        var v8 = this.fcx;
        var v9 = this.fcy;
        var v10 = this.x;
        var v11 = this.y;
        this.oldX = this.x;
        this.oldY = this.y;
        this.x += v5.dx;
        this.y += v5.dy;
        this.updateCoords();
        if (this.fl_hitWall) {
          var v12 = false;
          if (this.dx > 0 && this.world.getCase({x: Entity.x_rtc(v10 + Data.CASE_WIDTH * 0.5), y: Entity.y_rtc(v11)}) > 0) {
            v12 = true;
          }
          if (this.dx < 0 && this.world.getCase({x: Entity.x_rtc(v10 - Data.CASE_WIDTH * 0.5), y: Entity.y_rtc(v11)}) > 0) {
            v12 = true;
          }
          if (v12) {
            this.x = v10;
            v5.dx = 0;
            this.updateCoords();
            this.onHitWall();
          }
        }
        if (this.fl_hitBorder && (this.x < Data.BORDER_MARGIN || this.x >= Data.GAME_WIDTH - Data.BORDER_MARGIN) || this.fl_hitWall && this.world.getCase({x: this.cx, y: Entity.y_rtc(v11)}) > 0) {
          this.x = v10;
          v5.dx = 0;
          this.updateCoords();
          this.onHitWall();
        }
        if (this.fl_hitWall && v5.dy > 0 && !this.fl_kill) {
          if (this.world.getCase(Entity.rtc(v10, v11 + Math.floor(Data.CASE_HEIGHT / 2))) != Data.WALL && this.world.getCase({x: this.fcx, y: this.fcy}) == Data.WALL) {
            this.x = v10;
            v5.dx = 0;
            this.updateCoords();
            this.onHitWall();
          }
        }
        if (this.fl_hitGround && v5.dy >= 0) {
          if (this.world.getCase(Entity.rtc(v10, v11 + Math.floor(Data.CASE_HEIGHT / 2))) != Data.GROUND && this.world.getCase({x: this.fcx, y: this.fcy}) == Data.GROUND) {
            if (this.world.checkFlag({x: this.fcx, y: this.fcy}, Data.IA_TILE)) {
              if (this.fl_skipNextGround) {
                this.fl_skipNextGround = false;
              } else {
                v5.dy = 0;
                this.onHitGround(this.y - this.fallStart);
                this.fallStart = null;
                this.updateCoords();
              }
            }
          }
        }
        if (this.fl_hitCeil && v5.dy <= 0) {
          if (this.world.getCase(Entity.rtc(v10, v11 - Math.floor(Data.CASE_HEIGHT / 2))) <= 0 && this.world.getCase(Entity.rtc(this.x, this.y - Math.floor(Data.CASE_HEIGHT / 2))) > 0) {
            v5.dy = 0;
            this.onHitCeil();
            this.updateCoords();
          }
        }
        if (v6 != this.cx || v7 != this.cy) {
          var v13 = false;
          if (this.fl_hitGround && v7 < this.cy) {
            if (this.needsPatch()) {
              if (this.world.getCase({x: v6, y: v7}) <= 0 && this.dy > 0 && this.world.getCase({x: this.cx, y: this.cy}) > 0 && this.cy < Data.LEVEL_HEIGHT) {
                this.x = Entity.x_ctr(v6);
                this.y = Entity.y_ctr(v7);
                v5.dy = 0;
                this.updateCoords();
                this.onHitGround(this.y - this.fallStart);
                v13 = true;
              }
            }
          }
          if (this.fl_hitGround && this.dy >= 0 && v6 != this.cx && v7 != this.cy) {
            if (this.world.getCase({x: v6, y: v7}) <= 0 && this.world.getCase({x: this.cx, y: this.cy}) == Data.GROUND) {
              this.x = Entity.x_ctr(v6);
              this.y = Entity.y_ctr(v7);
              v5.dx = 0;
              this.updateCoords();
              this.onHitWall();
              v13 = true;
            }
          }
          if (!v13) {
            this.tRem(v6, v7);
            this.infix();
            this.updateCoords();
            this.tAdd(this.cx, this.cy);
          }
        }
        ++v3;
      }
    }
    this.fl_stopStepping = false;
    this.postfix();
    if (this.fl_friction) {
      if ((this.game.fl_ice || this.fl_slide) && this.fl_stable) {
        if (this.slideFriction == null) {
          this.dx *= this.game.sFriction;
        } else {
          this.dx *= Math.pow(this.slideFriction, Timer.tmod);
        }
      } else {
        this.dx *= this.game.xFriction;
        this.dy *= this.game.yFriction;
      }
    }
    if (Math.abs(this.dx) <= 0.2) {
      this.dx = 0;
    }
    if (Math.abs(this.dy) <= 0.2) {
      this.dy = 0;
    }
    if (this.y >= Data.DEATH_LINE) {
      this.onDeathLine();
    }
  }
}
