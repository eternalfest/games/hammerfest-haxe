package hf.entity.bad.flyer;

import hf.entity.bad.Flyer;
import hf.Entity;
import hf.mode.GameMode;

class Tzongre extends Flyer {
  public function new(): Void {
    super();
  }

  public static function attach(g: GameMode, x: Float, y: Float): Tzongre {
    var v5 = Data.LINKAGES[Data.BAD_TZONGRE];
    var v6: Tzongre = cast g.depthMan.attach(v5, Data.DP_BADS);
    v6.initBad(g, x, y);
    v6.setLifeTimer(Data.SECOND * 60);
    return v6;
  }

  override public function freeze(d: Float): Void {
  }

  override public function hit(e: Entity): Void {
    var v3 = Data.PLAYER.downcast(e);
    if (v3 != null) {
      this.game.fxMan.attachFx(this.x, this.y - Data.CASE_HEIGHT, "hammer_fx_shine");
      v3.getScore(this, 50000);
      this.destroy();
    }
    var v4 = Data.BAD.downcast(e);
    if (v4 != null) {
      v4.knock(Data.KNOCK_DURATION);
    }
  }

  override public function init(g: GameMode): Void {
    super.init(g);
    this.unregister(Data.BAD_CLEAR);
  }

  override public function killHit(dx: Null<Float>): Void {
  }

  override public function knock(d: Float): Void {
  }
}
