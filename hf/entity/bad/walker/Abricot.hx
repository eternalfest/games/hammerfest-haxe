package hf.entity.bad.walker;

import hf.entity.bad.Jumper;
import hf.mode.GameMode;

class Abricot extends Jumper {
  public var fl_spawner: Bool;

  public function new(): Void {
    super();
    this.animFactor = 0.65;
    this.setJumpUp(5);
    this.setJumpH(100);
    this.setClimb(100, 3);
    this.setFall(20);
  }

  public static function attach(g: GameMode, x: Float, y: Float, spawner: Bool): Abricot {
    var v6 = Data.LINKAGES[Data.BAD_ABRICOT];
    var v7: Abricot = cast g.depthMan.attach(v6, Data.DP_BADS);
    v7.fl_spawner = spawner;
    v7.initBad(g, x, y);
    return v7;
  }

  override public function init(g: GameMode): Void {
    super.init(g);
    if (!this.fl_spawner) {
      this.scale(75);
    }
    this.dir = -1;
  }

  override public function onDeathLine(): Void {
    if (this.fl_spawner) {
      this.game.attachBad(Data.BAD_ABRICOT2, this.x - Data.CASE_WIDTH, -30);
      this.game.attachBad(Data.BAD_ABRICOT2, this.x + Data.CASE_WIDTH, -30);
    }
    super.onDeathLine();
  }
}
