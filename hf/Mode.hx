package hf;

import etwin.flash.MovieClip;
import hf.DepthManager;
import hf.GameManager;
import hf.Hf;
import hf.SoundManager;
import etwin.flash.Key;

class Mode {
  public var _name: String;
  public var manager: GameManager;
  public var root: Hf;
  public var mc: MovieClip;
  public var depthMan: DepthManager;
  public var soundMan: SoundManager;
  public var fl_switch: Bool;
  public var fl_hide: Bool;
  public var fl_lock: Bool;
  public var fl_music: Bool;
  public var fl_mute: Bool;
  public var fl_runAsChild: Bool;
  public var currentTrack: Null<Int>;
  public var xOffset: Float;
  public var yOffset: Float;
  public var uniqId: Int;
  public var cycle: Float;
  public var xFriction: Null<Float>;
  public var yFriction: Null<Float>;

  public function new(m: GameManager): Void {
    this.manager = m;
    this.root = this.manager.root;
    this.mc = HfStd.createEmptyMC(this.root, this.manager.uniq++);
    this.depthMan = new DepthManager(this.mc);
    this.soundMan = this.manager.soundMan;
    this.lock();
    this.fl_switch = false;
    this.fl_music = false;
    this.fl_mute = false;
    this.fl_runAsChild = false;
    this.currentTrack = null;
    this.xOffset = 0;
    this.yOffset = 0;
    this.uniqId = 1;
    this.cycle = 0;
    this._name = "$abstractMode";
    this.show();
  }

  public function show(): Void {
    this.mc._visible = true;
    this.fl_hide = false;
  }

  public function hide(): Void {
    this.mc._visible = false;
    this.fl_hide = true;
  }

  public function init(): Void {
    this.mc._x = this.xOffset;
    this.mc._y = this.yOffset;
  }

  public function getUniqId(): Int {
    return this.uniqId++;
  }

  public function lock(): Void {
    this.fl_lock = true;
  }

  public function unlock(): Void {
    this.fl_lock = false;
  }

  public function short(): String {
    return this._name;
  }

  public function destroy(): Void {
    this.depthMan.destroy();
    this.lock();
  }

  public function updateConstants(): Void {
    if (this.fl_lock) {
      return;
    }
    this.xFriction = Math.pow(Data.FRICTION_X, Timer.tmod);
    this.yFriction = Math.pow(Data.FRICTION_Y, Timer.tmod);
    this.cycle += Timer.tmod;
  }

  public function getDebugControls(): Void {
    if (Key.isDown(Key.BACKSPACE)) {
      Log.clear();
    }
  }

  public function getControls(): Void {
  }

  public function onSleep(): Void {
  }

  public function onWakeUp(modeName: String, data: Dynamic /*TODO*/): Void {
  }

  public function playMusic(id: Int): Void {
    if (!GameManager.CONFIG.hasMusic()) {
      return;
    }
    this.playMusicAt(id, 0);
  }

  public function playMusicAt(id: Int, pos: Float): Void {
    if (!GameManager.CONFIG.hasMusic()) {
      return;
    }
    if (this.fl_music) {
      this.stopMusic();
    }
    this.currentTrack = id;
    this.manager.musics[this.currentTrack].start(pos / 1000, 99999);
    this.fl_music = true;
    if (this.fl_mute) {
      this.setMusicVolume(0);
    } else {
      this.setMusicVolume(1);
    }
  }

  public function stopMusic(): Void {
    if (!GameManager.CONFIG.hasMusic()) {
      return;
    }
    this.manager.musics[this.currentTrack].stop();
    this.fl_music = false;
  }

  public function setMusicVolume(n: Float): Void {
    if (!this.fl_music || !GameManager.CONFIG.hasMusic()) {
      return;
    }
    n *= GameManager.CONFIG.musicVolume * 100;
    this.manager.musics[this.currentTrack].setVolume(Math.round(n));
  }

  public function endMode(): Void {
    this.stopMusic();
  }

  public function main(): Void {
    if (this.manager.fl_debug) {
      this.getDebugControls();
    }
    this.getControls();
    this.updateConstants();
  }
}
