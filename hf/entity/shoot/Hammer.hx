package hf.entity.shoot;

import hf.entity.Player;
import hf.entity.Shoot;
import hf.mode.GameMode;

class Hammer extends Shoot {
  public var player: Null<Player>;

  public function new(): Void {
    super();
    this.shootSpeed = 0;
  }

  public static function attach(g: GameMode, x: Float, y: Float): Hammer {
    var v5 = "hammer_shoot_hammer";
    var v6: Hammer = cast g.depthMan.attach(v5, Data.DP_SPEAR);
    v6.initShoot(g, x, y - 10);
    return v6;
  }

  public function setOwner(p: Player): Void {
    this.player = p;
  }

  override public function destroy(): Void {
    this.player.specialMan.interrupt(85);
    this.game.fxMan.attachFx(this.x, this.y - Data.CASE_HEIGHT / 2, "hammer_fx_pop");
    super.destroy();
  }

  override public function endUpdate(): Void {
    super.endUpdate();
    if (this.player.dir > 0 && this._xscale < 0) {
      this._xscale = Math.abs(this._xscale);
    }
    if (this.player.dir < 0 && this._xscale > 0) {
      this._xscale = -Math.abs(this._xscale);
    }
  }

  override public function init(g: GameMode): Void {
    super.init(g);
    this.playAnim(Data.ANIM_SHOOT_LOOP);
  }

  override public function update(): Void {
    var v3 = this.player.x + this.player.dir * Data.CASE_WIDTH * 0.7;
    v3 = Math.min(Data.GAME_WIDTH - 1, v3);
    v3 = Math.max(1, v3);
    var v4 = this.player.y - Data.CASE_HEIGHT * 0.7;
    v3 = Math.max(1, Math.min(Data.GAME_WIDTH - 1, v3));
    v4 = Math.max(20, Math.min(Data.GAME_HEIGHT - 1, v4));
    this.moveTo(v3, v4);
    super.update();
    var v5 = this.game.getClose(Data.BAD, this.x, this.y, Data.CASE_WIDTH * 2, false);
    for (v6 in 0...v5.length) {
      v5[v6].killHit(this.player.dir * 9);
    }
    if (this.player.fl_kill || this.game.fl_clear || !this.player.specialMan.actives[85]) {
      this.destroy();
    }
  }
}
