package hf.entity.bad.walker;

import hf.entity.bad.Jumper;
import hf.mode.GameMode;

class LitchiWeak extends Jumper {
  public function new(): Void {
    super();
    this.setJumpH(100);
    this.setJumpUp(10);
    this.setJumpDown(6);
    this.setClimb(25, 3);
    this.setFall(25);
  }

  public static function attach(g: GameMode, x: Float, y: Float): LitchiWeak {
    var v5 = Data.LINKAGES[Data.BAD_LITCHI_WEAK];
    var v6: LitchiWeak = cast g.depthMan.attach(v5, Data.DP_BADS);
    v6.initBad(g, x, y);
    return v6;
  }

  override public function onEndAnim(id: Int): Void {
    super.onEndAnim(id);
    if (id == Data.ANIM_BAD_SHOOT_START.id) {
      this.playAnim(Data.ANIM_BAD_WALK);
      this.walk();
    }
  }

  override public function walk(): Void {
    if (this.animId != Data.ANIM_BAD_SHOOT_START.id) {
      super.walk();
    }
  }
}
