package hf;

/**
 * Class providing global time measurement that can be accessed from various parts of the engine.
 *
 * The values are smoothed across frames to improve support for browser/flash platforms where FPS
 * can't be fixed using `vsync`. This reduces the sensibility to sudden spikes, for example when
 * spawning many particles.
 *
 * @see https://github.com/HeapsIO/heaps/blob/master/hxd/Timer.hx
 *
 * The Timer class acts as a global time measurement that can be accessed from various parts of the engine.
 * These three values are representation of the same underlying calculus: tmod, dt, fps
 */
class Timer {
  /**
   * The FPS on which `tmod` values are based on.
   *
   * Default: `32`
   * @final
   */
  public static var wantedFPS: Float = 32;

  /**
   * Maximum duration between two frames (in seconds).
   *
   * If the duration exceeds this amount, the timer will not treat it as a lag
   * but as if the engine itself was frozen. A non-lag cause for this may be
   * when the computer moves to sleep and supsends all its active programs.
   *
   * Default: `0.5`
   * @final
   */
  public static var maxDeltaTime: Float = 0.5;

  /**
   * The last timestamp when `Timer.update` was called.
   */
  public static var oldTime: Int = HfStd.getTimer();

  /**
   * The smoothing done between frames.
   *
   * A smoothing of 0 gives "real time" values, higher values will smooth the results for `tmod`
   * over frames using the following formula:
   *
   * ```
   * tmod = lerp(tmod, deltaT, tmod_factor)
   * ```
   *
   * Default: `0.95`
   * @final
   */
  public static var tmod_factor: Float = 0.95;

  /**
   * Internal temporary value used to compute frame smoothing.
   */
  public static var calc_tmod: Float = 1;

  /**
   * Smoothed duration between the current and previous frames (in seconds).
   */
  public static var tmod: Float = 1;

  /**
   * Real duration between the current and previous frames (in seconds).
   */
  public static var deltaT: Float = 1;

  /**
   * A frame counter, increases on each call to `Timer.update`.
   */
  public static var frameCount: Int = 0;

  /**
   * Update the timer calculus on each frame.
   *
   * It is automatically called by `GameManager.main`
   */
  public static function update(): Void {
    ++Timer.frameCount;
    var v2 = HfStd.getTimer();
    Timer.deltaT = (v2 - Timer.oldTime) / 1000.0;
    Timer.oldTime = v2;
    if (Timer.deltaT < Timer.maxDeltaTime) {
      Timer.calc_tmod = Timer.calc_tmod * Timer.tmod_factor + (1 - Timer.tmod_factor) * Timer.deltaT * Timer.wantedFPS;
    } else {
      Timer.deltaT = 1 / Timer.wantedFPS;
    }
    Timer.tmod = Timer.calc_tmod;
  }

  /**
   * Returns the current smoothed FPS.
   */
  public static function fps(): Float {
    return Timer.wantedFPS / Timer.tmod;
  }
}
