package hf.gui;

import hf.gui.Container;
import hf.gui.Item;
import etwin.flash.Key;

class SimpleButton extends Item {
  public var event: Dynamic /*TODO*/;
  public var key: Dynamic /*TODO*/;
  public var body: Dynamic /*TODO*/;
  public var left: Dynamic /*TODO*/;
  public var right: Dynamic /*TODO*/;
  public var toggle: Null<Dynamic /*TODO*/>;
  public var fl_lock: Null<Bool>;
  public var fl_keyLock: Null<Bool>;

  public function new(): Void {
    super();
  }

  public function initButton(c: Container, l: Dynamic /*TODO*/, key: Dynamic /*TODO*/, func: Dynamic /*TODO*/): Void {
    this.init(c, l);
    this.event = func;
    this.key = key;
    this.body.onRelease = function () {
      this.release();
    };
    this.body.onRollOut = function () {
      this.rollOut();
    };
    this.body.onRollOver = function () {
      this.rollOver();
    };
    this.left.onRelease = function () {
      this.event();
    };
    this.left.onRollOut = function () {
      this.rollOut();
    };
    this.left.onRollOver = function () {
      this.rollOver();
    };
    this.right.onRelease = function () {
      this.event();
    };
    this.right.onRollOut = function () {
      this.rollOut();
    };
    this.right.onRollOver = function () {
      this.rollOver();
    };
    this.rollOut();
  }

  public function setToggleKey(k: Dynamic /*TODO*/): Void {
    this.toggle = k;
  }

  public function release(): Void {
    if (!this.container.fl_lock) {
      this.event();
    }
  }

  public function rollOut(): Void {
    var v2 = "1";
    this.left.gotoAndStop(v2);
    this.body.gotoAndStop(v2);
    this.right.gotoAndStop(v2);
  }

  public function rollOver(): Void {
    var v2 = "2";
    this.left.gotoAndStop(v2);
    this.body.gotoAndStop(v2);
    this.right.gotoAndStop(v2);
  }

  public function shortcut(): Bool {
    return Key.isDown(this.key) && (this.toggle == null && !Key.isDown(Key.CONTROL) && !Key.isDown(Key.SHIFT) || Key.isDown(this.toggle));
  }

  public static function attach(c: Container, l: Dynamic /*TODO*/, k: Dynamic /*TODO*/, func: Dynamic /*TODO*/): SimpleButton {
    var v6: SimpleButton = cast c.depthMan.attach("hammer_editor_button", Data.DP_INTERF);
    v6.initButton(c, l, k, func);
    return v6;
  }

  override public function setLabel(l: String): Void {
    this.field.text = l;
    this.body._width = this.field.textWidth + 5;
    this.right._x = this.body._width;
    this.width = this.left._width + this.body._width + this.right._width;
  }

  override public function update(): Void {
    if (this.container.fl_lock) {
      return;
    }
    if (!this.shortcut()) {
      this.fl_keyLock = false;
    }
    if (this.shortcut() && !this.fl_keyLock) {
      this.event();
      this.fl_keyLock = true;
    }
  }
}
