package hf.mode;

import etwin.flash.MovieClip;
import hf.GameManager;
import hf.mode.GameMode;
import etwin.flash.MovieClip;

class Tutorial extends GameMode {
  public function new(m: GameManager): Void {
    super(m);
    this._name = "$tutorial";
  }

  public function darknessManager(): Void {
  }

  override public function endMode(): Void {
    this.stopMusic();
    this.manager.startGameMode(new hf.mode.Shareware(this.manager, 0));
  }

  override public function initGame(): Void {
    super.initGame();
    this.world.goto(0);
    var v3 = this.insertPlayer(this.world.current.__dollar__playerX, this.world.current.__dollar__playerY);
    v3.lives = 99999;
  }

  override public function init(): Void {
    super.init();
    this.initGame();
    this.initInterface();
  }

  override public function initInterface(): Void {
    super.initInterface();
    this.gi.lightMode();
  }

  override public function initWorld(): Void {
    super.initWorld();
    this.world = new hf.levels.GameMechanics(this.manager, "xml_tutorial");
    this.world.setDepthMan(this.depthMan);
    this.world.setGame(this);
    this.world.fl_shadow = false;
    if (this.world.setName != "xml_tutorial") {
      return;
    }
  }

  override public function onEndOfSet(): Void {
    super.onEndOfSet();
    this.manager.startGameMode(new hf.mode.Shareware(this.manager, 0));
  }

  override public function onGameOver(): Void {
    this.fl_gameOver = true;
    this.lock();
    var v2 = HfStd.getVar(HfStd.getRoot(), "$out");
    this.manager.redirect(v2, null);
  }

  override public function onHurryUp(): Null<MovieClip> {
    return null;
  }

  override public function onLevelReady(): Void {
    super.onLevelReady();
    if (this.world.currentId == 0) {
      this.fxMan.attachLevelPop(Lang.get(16), false);
    }
  }

  override public function onPause(): Void {
    super.onPause();
    this.pauseMC.sector.text = Lang.get(14) + " «" + Lang.get(16) + "»";
  }
}
