package hf.entity.bad.walker;

import hf.entity.bad.Shooter;
import hf.entity.shoot.FramBall;
import hf.mode.GameMode;

class Framboise extends Shooter {
  public static var FRAGS: Int = 6;
  public static var MAX_TRIES: Int = 1000;

  public var white: Float;
  public var fl_phased: Bool;
  public var tx: Null<Float>;
  public var ty: Null<Float>;
  public var arrived: Null<Int>;

  public function new(): Void {
    super();
    this.setJumpUp(3);
    this.setJumpDown(6);
    this.setJumpH(100);
    this.setClimb(100, 3);
    this.setFall(20);
    this.setShoot(0.7);
    this.initShooter(20, 12);
    this.white = 0;
    this.fl_phased = false;
  }

  public static function attach(g: GameMode, x: Float, y: Float): Framboise {
    var v5 = Data.LINKAGES[Data.BAD_FRAMBOISE];
    var v6: Framboise = cast g.depthMan.attach(v5, Data.DP_BADS);
    v6.initBad(g, x, y);
    return v6;
  }

  public function aroundX(base: Float): Float {
    return base + HfStd.random(Data.CASE_WIDTH) * (HfStd.random(2) * 2 - 1);
  }

  public function aroundY(base: Float): Float {
    return base - HfStd.random(Data.CASE_HEIGHT);
  }

  public function onArrived(fb: FramBall): Void {
    this.show();
    this.moveTo(this.tx, this.ty);
    fb.destroy();
    if (fb._currentframe >= 5) {
      if (fb._currentframe == 5) {
        this.sub.o1._visible = true;
      } else {
        this.sub.o2._visible = true;
      }
    } else {
      this.sub.nextFrame();
    }
    ++this.arrived;
    if (this.arrived >= hf.entity.bad.walker.Framboise.FRAGS) {
      this.phaseIn();
    }
  }

  public function clearFrags(): Void {
    var v2 = this.game.getList(Data.SHOOT);
    for (v3 in 0...v2.length) {
      var v4: FramBall = cast v2[v3];
      if (v4.owner == this) {
        v4.destroy();
      }
    }
  }

  public function phaseOut(): Void {
    this.game.fxMan.inGameParticles(Data.PARTICLE_FRAMB_SMALL, this.x, this.y, HfStd.random(3) + 2);
    this.game.fxMan.attachExplosion(this.x, this.y, 40);
    this.fl_phased = true;
    this.dx = 0;
    this.dy = 0;
    this.disableShooter();
    this.disableAnimator();
    this.gotoAndStop("15");
    this.sub.stop();
    this.sub.o1._visible = false;
    this.sub.o2._visible = false;
    this.hide();
  }

  public function phaseIn(): Void {
    this.clearFrags();
    this.fl_phased = false;
    this.enableAnimator();
    this.enableShooter();
    var v2 = this.game.fxMan.attachExplosion(this.x, this.y, 40);
  }

  override public function destroy(): Void {
    this.clearFrags();
    super.destroy();
  }

  override public function endUpdate(): Void {
    super.endUpdate();
    if (this.white > 0) {
      this.setColorHex(Math.round(100 * this.white), 16777215);
      var v3 = new etwin.flash.filters.GlowFilter();
      v3.color = 16777215;
      v3.strength = this.white * 2;
      v3.blurX = 4;
      v3.blurY = v3.blurX;
      this.filters = [v3];
      this.white -= Timer.tmod * 0.1;
      if (this.white <= 0) {
        this.filters = [];
      }
    }
  }

  override public function freeze(d: Float): Void {
    this.phaseIn();
    super.freeze(d);
  }

  override public function init(g: GameMode): Void {
    super.init(g);
  }

  override public function isReady(): Bool {
    return super.isReady() && !this.fl_phased;
  }

  override public function killHit(dx: Null<Float>): Void {
    this.phaseIn();
    super.killHit(dx);
  }

  override public function knock(d: Float): Void {
    this.phaseIn();
    super.knock(d);
  }

  override public function onHitGround(h: Float): Void {
    super.onHitGround(h);
    if (h >= Data.CASE_HEIGHT * 2) {
      this.game.fxMan.inGameParticles(Data.PARTICLE_FRAMB_SMALL, this.x, this.y, HfStd.random(4) + 2);
    }
  }

  override public function onShoot(): Void {
    var v4;
    var v3;
    var v6;
    var v2 = 0;
    do {
      v6 = false;
      v3 = HfStd.random(Data.LEVEL_WIDTH);
      v4 = HfStd.random(Data.LEVEL_HEIGHT);
      ++v2;
      var v5 = this.distanceCase(v3, v4);
      if (v5 <= 7) {
        v6 = true;
      }
      if (!this.game.world.checkFlag({x: v3, y: v4}, Data.IA_TILE_TOP)) {
        v6 = true;
      }
      if (this.game.world.checkFlag({x: v3, y: v4}, Data.IA_SMALL_SPOT)) {
        v6 = true;
      }
      if ((this.game.getListAt(Data.SPEAR, v3, v4)).length > 0) {
        v6 = true;
      }
    } while(v2 < hf.entity.bad.walker.Framboise.MAX_TRIES && v6);
    if (v2 >= hf.entity.bad.walker.Framboise.MAX_TRIES) {
      return;
    }
    this.tx = Entity.x_ctr(v3);
    this.ty = Entity.y_ctr(v4);
    this.arrived = 0;
    this.phaseOut();
    for (v8 in 0...hf.entity.bad.walker.Framboise.FRAGS) {
      var v7 = hf.entity.shoot.FramBall.attach(this.game, this.aroundX(this.x), this.aroundY(this.y));
      v7.setOwner(this);
      v7.gotoAndStop("" + (v8 + 1));
    }
  }

  override public function update(): Void {
    if (!this._visible) {
      this.moveTo(100, -200);
      if (!this.isHealthy()) {
        this.show();
        this.moveTo(this.tx, this.ty);
        this.phaseIn();
      }
    } else {
      var v3 = this.game.getClose(Data.PLAYER_BOMB, this.x, this.y, 90, false);
      if (this.isReady() && v3.length >= 1) {
        this.startShoot();
      }
    }
    super.update();
  }

  override public function walk(): Void {
    if (!this.fl_phased) {
      super.walk();
    }
  }
}
