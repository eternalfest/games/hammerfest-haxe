package hf.entity;

import hf.entity.Physics;
import hf.mode.GameMode;

typedef NextAction = {
  var dx: Float;
  var dy: Float;
  var delay: Float;
  var action: Int;
}

class Mover extends Physics {
  public var fl_bounce: Bool;
  public var bounceFactor: Float;
  public var next: NextAction;

  public function new(): Void {
    super();
    this.fl_bounce = false;
    this.bounceFactor = 0.5;
  }

  public function setNext(dx: Float, dy: Float, delay: Float, action: Int): Void {
    this.next = {dx: dx, dy: dy, delay: delay, action: action};
  }

  public function onNext(): Void {
    if (this.next.action == Data.ACTION_MOVE) {
      this.dx = this.next.dx;
      this.dy = this.next.dy;
      if (this.dy != 0) {
        this.fl_stable = false;
      }
      this.next = null;
    }
  }

  public function isReady(): Bool {
    return this.fl_stable && this.next == null;
  }

  override public function init(g: GameMode): Void {
    super.init(g);
  }

  override public function onHitGround(h: Float): Void {
    if (this.fl_bounce) {
      var v4 = this.bounceFactor * Math.abs(this.dy);
      if (v4 >= 2) {
        this.setNext(this.dx, -v4, 0, Data.ACTION_MOVE);
        this.fl_skipNextGravity = true;
      }
    }
    super.onHitGround(h);
  }

  override public function onKill(): Void {
    super.onKill();
    this.next = null;
  }

  override public function update(): Void {
    if (this.next != null) {
      this.next.delay -= Timer.tmod;
      if (this.next.delay <= 0) {
        this.onNext();
      }
    }
    super.update();
  }
}
