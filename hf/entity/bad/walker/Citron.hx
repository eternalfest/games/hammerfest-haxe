package hf.entity.bad.walker;

import hf.entity.bad.Shooter;
import hf.mode.GameMode;

class Citron extends Shooter {
  public function new(): Void {
    super();
    this.setJumpH(50);
    this.setShoot(3);
    this.initShooter(50, 20);
  }

  public static function attach(g: GameMode, x: Float, y: Float): Citron {
    var v5 = Data.LINKAGES[Data.BAD_CITRON];
    var v6: Citron = cast g.depthMan.attach(v5, Data.DP_BADS);
    v6.initBad(g, x, y);
    return v6;
  }

  override public function onShoot(): Void {
    var v2 = hf.entity.shoot.Zeste.attach(this.game, this.x, this.y);
    var v3 = this.game.getOne(Data.PLAYER);
    if (v3.y < this.y) {
      v2.moveUp(v2.shootSpeed);
    } else {
      v2.moveDown(v2.shootSpeed);
    }
  }
}
