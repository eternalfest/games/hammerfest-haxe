package hf.mode;

import etwin.flash.MovieClip;
import etwin.flash.DynamicMovieClip;
import hf.GameManager;
import hf.mode.Adventure;
import etwin.flash.Date;

class TimeAttack extends Adventure {
  public static var INFINITY: Int = 59 * 60 * 1000 + 59 * 1000 + 999;

  public var runId: Null<Int>;
  public var suspendTimer: Null<Int>;
  public var times: Array<Int>;
  public var frameTimer: Int;
  public var fl_alerts: Array<Bool>;
  public var prevFrameTimer: Int;
  public var gameTimer: Int;
  public var starter: Float;
  public var mcStarter: DynamicMovieClip /* TODO */;
  public var last: Int;
  public var fl_chronoStop: Bool;

  public function new(m: GameManager, id: Int): Void {
    super(m, id);
    this.runId = null;
    this.initRunId();
    if (this.runId == null) {
      GameManager.fatal("unknown run ID");
      return;
    }
    this.firstLevel = this.runId * 9;
    this._name = "$time";
    this.suspendTimer = null;
    this.times = new Array();
    this.frameTimer = HfStd.getTimer();
    this.fl_alerts = new Array();
    this.fl_static = true;
    this.fl_bullet = false;
    this.fl_mirror = false;
    this.fl_nightmare = false;
  }

  public function initRunId(): Void {
    if (GameManager.CONFIG.hasOption(Data.OPT_SET_TA_0)) {
      this.runId = 0;
    }
    if (GameManager.CONFIG.hasOption(Data.OPT_SET_TA_1)) {
      this.runId = 1;
    }
    if (GameManager.CONFIG.hasOption(Data.OPT_SET_TA_2)) {
      this.runId = 2;
    }
  }

  public function initializePlayers(): Void {
    var v2 = this.getPlayerList();
    for (v3 in 0...v2.length) {
      var v4 = v2[v3];
      v4.getScore = null;
      v4.getScoreHidden = null;
      v4.lives = 3;
      this.gi.setLives(v4.pid, v4.lives);
    }
  }

  public function getChrono(): Int {
    return Math.floor(this.frameTimer - this.gameTimer);
  }

  public function display(str: String): Void {
    var v3 = this.getPlayerList();
    for (v4 in 0...v3.length) {
      var v5 = v3[v4];
      this.gi.print(0, str);
      this.gi.print(1, str);
    }
  }

  public function displayLastTime(): Void {
    var v2 = this.times[this.times.length - 1];
    if (v2 != null) {
      var v3: DynamicMovieClip = cast this.depthMan.attach("hammer_interf_time", Data.DP_INTERF);
      v3._x = 20;
      v3._y = Data.DOC_HEIGHT - 50;
      v3.sub.field.text = "+ " + this.formatTime(v2);
      FxManager.addGlow(v3, Data.DARK_COLORS[0], 2);
    }
  }

  public function formatTime(t: Int): String {
    var v3 = new Date();
    v3.setTime(t);
    return Data.leadingZeros(v3.getMinutes() + (v3.getHours() - 1) * 60, 2) + "\" " + Data.leadingZeros(v3.getSeconds(), 2) + "' " + Data.leadingZeros(v3.getMilliseconds(), 3);
  }

  public function resetChrono(): Void {
    this.last = this.getChrono();
    this.fl_chronoStop = false;
    this.suspendTimer = null;
    this.gameTimer = this.frameTimer;
  }

  public function startChrono(): Void {
    if (this.suspendTimer != null) {
      var v2 = this.frameTimer - this.suspendTimer;
      this.gameTimer += v2;
    }
    this.fl_chronoStop = false;
    this.suspendTimer = null;
  }

  public function stopChrono(): Void {
    if (this.fl_chronoStop) {
      return;
    }
    this.fl_chronoStop = true;
    this.suspendTimer = this.frameTimer;
  }

  public function timeShift(n: Float): Void {
    this.gameTimer = Std.int(Math.min(this.frameTimer, this.gameTimer + n * 1000));
  }

  override public function addLevelItems(): Void {
  }

  override public function goto(id: Int): Void {
    if (this.world.isEmptyLevel(id, this)) {
      this.onEndOfSet();
      return;
    }
    this.stopChrono();
    var v4 = Math.floor(this.prevFrameTimer - this.gameTimer);
    var v5 = v4;
    for (v6 in 0...this.times.length) {
      v5 -= this.times[v6];
    }
    this.times.push(v5);
    this.display(this.formatTime(v4));
    super.goto(id);
  }

  override public function initGame(): Void {
    super.initGame();
    this.initializePlayers();
    this.resetChrono();
  }

  override public function initWorld(): Void {
    this.addWorld("xml_time");
  }

  override public function lock(): Void {
    super.lock();
    this.stopChrono();
  }

  override public function main(): Void {
    var v6;
    this.huTimer = 0;
    if (this.starter > 0) {
      var v3 = Math.ceil(this.starter / 32);
      this.starter -= Timer.tmod;
      var v4 = Math.ceil(this.starter / 32);
      this.mcStarter._xscale *= 0.99;
      this.mcStarter._yscale = this.mcStarter._xscale;
      this.mcStarter._alpha *= 0.95;
      if (v3 != v4) {
        this.mcStarter.field.text = "" + v4;
        this.mcStarter._alpha = 100;
        this.mcStarter._xscale = 70 + 120 - v4 * 40;
        this.mcStarter._yscale = this.mcStarter._xscale;
      }
      if (this.starter <= 0) {
        this.mcStarter.removeMovieClip();
        this.fxMan.attachAlert(Lang.get(20));
        this.unlock();
        this.resetChrono();
      }
    }
    this.prevFrameTimer = this.frameTimer;
    this.frameTimer = HfStd.getTimer();
    super.main();
    if (!this.fl_chronoStop) {
      if (Math.floor(this.getChrono() / 60000) != Math.floor(this.last / 60000)) {
        var v5 = Math.floor(this.getChrono() / 60000);
        if (v5 > 0 && this.fl_alerts[v5] != true) {
          this.fl_alerts[v5] = true;
          if (v5 > 1) {
            v6 = this.fxMan.attachAlert(v5 + " " + Lang.get(19));
          } else {
            v6 = this.fxMan.attachAlert(v5 + " " + Lang.get(18));
          }
          v6._y += 40;
        }
      }
      this.last = this.getChrono();
      if (!this.fl_lock && this.starter <= 0) {
        this.display(this.formatTime(this.getChrono()));
      }
    }
  }

  override public function onEndOfSet(): Void {
    this.saveScore();
  }

  override public function onGameOver(): Void {
    this.times = [3599999];
    this.saveScore();
  }

  override public function onLevelClear(): Void {
    this.perfectOrder = null;
    super.onLevelClear();
  }

  override public function onLevelReady(): Void {
    super.onLevelReady();
    this.fxMan.levelName.removeMovieClip();
    if (this.world.currentId != this.firstLevel) {
      this.displayLastTime();
    }
    if (this.world.currentId == this.firstLevel) {
      this.lock();
      this.display(this.formatTime(0));
      this.starter = Data.SECOND * 3.5;
      this.mcStarter = cast this.depthMan.attach("hammer_interf_starter", Data.DP_INTERF);
      this.mcStarter._x = Data.DOC_WIDTH * 0.5;
      this.mcStarter._y = Data.DOC_HEIGHT * 0.5;
      this.mcStarter.field.text = "";
      this.resetChrono();
    }
  }

  override public function onResurrect(): Void {
    super.onResurrect();
  }

  override public function pickUpScore(id: Int, sid: Int): Int {
    var v5 = super.pickUpScore(id, sid);
    if (id == 0) {
      var v6 = Data.getCrystalTime(sid);
      this.timeShift(v6);
      var v7 = this.getPlayerList();
      for (v8 in 0...v7.length) {
        var v9 = v7[v8];
        this.fxMan.attachScorePop(v9.baseColor, v9.darkColor, v9.x, v9.y, "-" + v6 + " sec");
      }
    }
    return v5;
  }

  override public function saveScore(): Void {
    var v2 = 0;
    for (v3 in 0...this.times.length) {
      v2 += this.times[v3];
    }
    (HfStd.getGlobal("gameOver"))(v2, this.runId, {__dollar__reachedLevel: 0, __dollar__item2: null, __dollar__data: null});
  }

  override public function switchDimensionById(id: Int, lid: Int, pid: Int): Void {
    this.stopChrono();
    super.switchDimensionById(id, lid, pid);
  }

  override public function unlock(): Void {
    super.unlock();
    this.startChrono();
  }
}
