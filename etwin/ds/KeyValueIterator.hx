package etwin.ds;

/**
 * A `KeyValueIterator` is an `Iterator` that has a key and a value.
 */
typedef KeyValueIterator<K, V> = Iterator<{key: K, value: V}>;
