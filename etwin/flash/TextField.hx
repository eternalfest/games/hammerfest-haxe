package etwin.flash;

import etwin.flash.StyleSheet;
import etwin.flash.ContextMenu;

@:native("TextField")
extern class TextField {
  /**
    Returns the names of fonts on the player's host system as an array.
  **/
  public static function getFontList(): Array<Dynamic>;

  /**
    Sets or retrieves the alpha transparency value of the text field.
  **/
  public var _alpha: Float;

  /**
    The height of the text field in pixels.
  **/
  public var _height: Float;

  /**
  The rendering quality used for a SWF file.

  **Deprecated** since Flash Player 7 — This property was deprecated in favor of `TextField._quality`.
  **/
  public var _highquality: Float;

  /**
    The instance name of the text field.
  **/
  public var _name: String;

  /**
    A reference to the movie clip or object that contains the current text field or object.
  **/
  public var _parent: MovieClip;

  /**
    The rendering quality used for a SWF file.
  **/
  public var _quality: String;

  /**
    The rotation of the text field, in degrees, from its original orientation.
  **/
  public var _rotation: Float;

  /**
    The number of seconds a sound prebuffers before it starts to stream.
  **/
  public var _soundbuftime: Float;

  /**
    Retrieves the URL of the SWF file that created the text field.
  **/
  public var _url(default, null): String;

  /**
    A Boolean value that indicates whether the text field `my_txt` is visible.
  **/
  public var _visible: Bool;

  /**
    The width of the text field, in pixels.
  **/
  public var _width: Float;

  /**
    An integer that sets the x coordinate of a text field relative to the local coordinates of the parent movie clip.
  **/
  public var _x: Float;

  /**
    Returns the x coordinate of the mouse position relative to the text field.
  **/
  public var _xmouse(default, null): Float;

  /**
    Determines the horizontal scale of the text field as applied from the registration point of the text field, expressed as a percentage.
  **/
  public var _xscale: Float;

  /**
    The y coordinate of a text field relative to the local coordinates of the parent movie clip.
  **/
  public var _y: Float;

  /**
    Indicates the y coordinate of the mouse position relative to the text field.
  **/
  public var _ymouse(default, null): Float;

  /**
    The vertical scale of the text field as applied from the registration point of the text field, expressed as a percentage.
  **/
  public var _yscale: Float;

  /**
    The type of anti-aliasing used for this TextField instance.
  **/
  public var antiAliasType: String;

  /**
    Controls automatic sizing and alignment of text fields.
  **/
  public var autoSize: Dynamic;

  /**
    Specifies if the text field has a background fill.
  **/
  public var background: Bool;

  /**
    The color of the text field background.
  **/
  public var backgroundColor: Int;

  /**
    Specifies if the text field has a border.
  **/
  public var border: Bool;

  /**
    The color of the text field border.
  **/
  public var borderColor: Int;

  /**
    An integer (one-based index) that indicates the bottommost line that is currently visible the text field.
  **/
  public var bottomScroll(default, null): Float;

  /**
    A Boolean value that specifies whether extra white space (spaces, line breaks, and so on) in an HTML text field should be removed.
  **/
  public var condenseWhite: Bool;

  /**
    Specifies whether to render using embedded font outlines.
  **/
  public var embedFonts: Bool;

  /**
    An indexed array containing each filter object currently associated with the text field.
  **/
  public var filters: Array<Dynamic>;

  /**
    The type of grid fitting used for this TextField instance.
  **/
  public var gridFitType: String;

  /**
    Indicates the current horizontal scrolling position.
  **/
  public var hscroll: Float;

  /**
    A flag that indicates whether the text field contains an HTML representation.
  **/
  public var html: Bool;

  /**
    If the text field is an HTML text field, this property contains the HTML representation of the text field's contents.
  **/
  public var htmlText: String;

  /**
    Indicates the number of characters in a text field.
  **/
  public var length(default, null): Int;

  /**
    Indicates the maximum number of characters that the text field can contain.
  **/
  public var maxChars: Int;

  /**
    Indicates the maximum value of `TextField.hscroll`.
  **/
  public var maxhscroll(default, null): Float;

  /**
    Indicates the maximum value of `TextField.scroll`.
  **/
  public var maxscroll(default, null): Float;

  /**
    Associates the ContextMenu object `contextMenu` with the text field `my_txt`.
  **/
  public var menu: ContextMenu;

  /**
    A Boolean value that indicates whether Flash Player should automatically scroll multiline text fields when the mouse pointer clicks a text field and the user rolls the mouse wheel.
  **/
  public var mouseWheelEnabled: Bool;

  /**
    Indicates whether the text field is a multiline text field.
  **/
  public var multiline: Bool;

  /**
    Specifies whether the text field is a password text field.
  **/
  public var password: Bool;

  /**
    Indicates the set of characters that a user may enter into the text field.
  **/
  public var restrict: String;

  /**
    The vertical position of text in a text field.
  **/
  public var scroll: Float;

  /**
    A Boolean value that indicates whether the text field is selectable.
  **/
  public var selectable: Bool;

  /**
    The sharpness of the glyph edges in this TextField instance.
  **/
  public var sharpness: Float;

  /**
    Attaches a style sheet to the text field.
  **/
  public var styleSheet: StyleSheet;

  /**
    Specifies whether the text field is included in automatic tab ordering.
  **/
  public var tabEnabled: Bool;

  /**
    Lets you customize the tab ordering of objects in a SWF file.
  **/
  public var tabIndex: Int;

  /**
    The target path of the text field instance.
  **/
  public var _target(default, null): String;

  /**
    Indicates the current text in the text field.
  **/
  public var text: String;

  /**
    Indicates the color of the text in a text field.
  **/
  public var textColor: Int;

  /**
    Indicates the height of the text, in pixels.
  **/
  public var textHeight: Float;

  /**
    Indicates the width of the text, in pixels.
  **/
  public var textWidth: Float;

  /**
    The thickness of the glyph edges in this TextField instance.
  **/
  public var thickness: Float;

  /**
    Specifies the type of text field.
  **/
  public var type: String;

  /**
    The name of the variable that the text field is associated with.
  **/
  public var variable: String;

  /**
    A Boolean value that indicates if the text field has word wrap.
  **/
  public var wordWrap: Bool;

  /**
    Replaces a range of characters, specified by the `beginIndex` and `endIndex` parameters, in the specified text field with the contents of the `newText` parameter.
  **/
  public function replaceText(beginIndex: Int, endIndex: Int, newText: String): Void;

  /**
    Replaces the current selection with the contents of the `newText` parameter.
  **/
  public function replaceSel(newText: String): Void;

  /**
    Returns a TextFormat object for a character, a range of characters, or an entire TextField object.
  **/
  public function getTextFormat(?beginIndex: Int, ?endIndex: Int): TextFormat;

  /**
    Applies the text formatting specified by the textFormat parameter to some or all of the text in a text field.
  **/
  @:overload(function(textFormat: TextFormat): Void {})
  @:overload(function(beginIndex: Int, textFormat: TextFormat): Void {})
  public function setTextFormat(beginIndex: Int, endIndex: Int, textFormat: TextFormat): Void;

  /**
    Removes the text field.
  **/
  public function removeTextField(): Void;

  /**
    Returns a TextFormat object containing a copy of the text field's text format object.
  **/
  public function getNewTextFormat(): TextFormat;

  /**
    Sets the default new text format of a text field.
  **/
  public function setNewTextFormat(tf: TextFormat): Void;

  /**
    Returns the depth of a text field.
  **/
  public function getDepth(): Int;

  /**
    Registers an object to receive TextField event notifications.
  **/
  public function addListener(listener: Dynamic): Bool;

  /**
    Removes a listener object previously registered to a text field instance with `TextField.addListener()`.
  **/
  public function removeListener(listener: Dynamic): Bool;

  /**
    Event handler/listener; invoked when the content of a text field changes.
  **/
  public dynamic function onChanged(changedField: TextField): Void;

  /**
    Invoked when a text field loses keyboard focus.
  **/
  public dynamic function onKillFocus(newFocus: Dynamic): Void;

  /**
    Event handler/listener; invoked when one of the text field scroll properties changes.
  **/
  public dynamic function onScroller(scrolledField: TextField): Void;

  /**
    Invoked when a text field receives keyboard focus.
  **/
  public dynamic function onSetFocus(oldFocus: Dynamic): Void;
}
