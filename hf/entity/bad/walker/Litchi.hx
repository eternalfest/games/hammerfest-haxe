package hf.entity.bad.walker;

import hf.entity.bad.Jumper;
import hf.entity.bad.walker.LitchiWeak;
import hf.mode.GameMode;

class Litchi extends Jumper {
  public var child: Null<LitchiWeak>;

  public function new(): Void {
    super();
    this.speed *= 0.8;
    this.setJumpH(100);
    this.setJumpUp(10);
    this.setJumpDown(6);
    this.setClimb(25, 3);
    this.setFall(25);
  }

  public static function attach(g: GameMode, x: Float, y: Float): Litchi {
    var v5 = Data.LINKAGES[Data.BAD_LITCHI];
    var v6: Litchi = cast g.depthMan.attach(v5, Data.DP_BADS);
    v6.initBad(g, x, y);
    return v6;
  }

  public function weaken(): Void {
    if (this.child._name != null) {
      return;
    }
    this.child = hf.entity.bad.walker.LitchiWeak.attach(this.game, this.x, this.y - Data.CASE_HEIGHT);
    this.child.angerMore();
    this.child.updateSpeed();
    this.child.halt();
    this.child.dir = this.dir;
    this.child.fl_ninFoe = this.fl_ninFoe;
    this.child.fl_ninFriend = this.fl_ninFriend;
    this.game.fxMan.inGameParticles(Data.PARTICLE_LITCHI, this.x + HfStd.random(20) * (HfStd.random(2) * 2 - 1), this.y - HfStd.random(20), HfStd.random(3) + 5);
    this.child.playAnim(Data.ANIM_BAD_SHOOT_START);
  }

  override public function endUpdate(): Void {
    if (this.child != null) {
      this.child.dy = -7;
      this.destroy();
    } else {
      super.endUpdate();
    }
  }

  override public function forceKill(dx: Null<Float>): Void {
    super.killHit(dx);
  }

  override public function freeze(d: Float): Void {
    this.weaken();
  }

  override public function killHit(dx: Null<Float>): Void {
    if (!this.fl_knock) {
      this.knock(Data.KNOCK_DURATION);
    }
  }

  override public function update(): Void {
    super.update();
    if (!this.fl_kill && this.fl_knock && this.child == null && this.dy <= -Data.BAD_VJUMP_Y * 0.6) {
      this.dy = -Data.BAD_VJUMP_Y * 0.6;
    }
  }
}
