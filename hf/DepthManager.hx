package hf;

import etwin.flash.MovieClip;

typedef Plan = {
  var tbl: Array<MovieClip>;
  var cur: Int;
}

class DepthManager {
  public var root_mc: MovieClip;
  public var plans: Array<Plan>;

  public function new(mc: MovieClip): Void {
    this.root_mc = mc;
    this.plans = new Array();
  }

  public function getMC(): MovieClip {
    return this.root_mc;
  }

  public function getPlan(pnb: Int): Plan {
    var v3 = this.plans[pnb];
    if (v3 == null) {
      v3 = {tbl: new Array(), cur: 0};
      this.plans[pnb] = v3;
    }
    return v3;
  }

  public function compact(plan: Int): Void {
    var v3 = this.plans[plan];
    var v4 = v3.tbl;
    var v5 = v3.cur;
    var v7 = 0;
    var v8 = plan * 1000;
    for (v6 in 0...v5) {
      if (v4[v6]._name != null) {
        v4[v6].swapDepths(v8 + v7);
        v4[v7] = v4[v6];
        ++v7;
      }
    }
    v3.cur = v7;
  }

  public function attach(inst: String, plan: Int): MovieClip {
    var v4 = this.getPlan(plan);
    var v5 = v4.tbl;
    var v6 = v4.cur;
    if (v6 == 1000) {
      this.compact(plan);
      return this.attach(inst, plan);
    }
    var v7 = HfStd.attachMC(this.root_mc, inst, v6 + plan * 1000);
    v5[v6] = v7;
    ++v4.cur;
    return v7;
  }

  public function empty(plan: Int): MovieClip {
    var v3 = this.getPlan(plan);
    var v4 = v3.tbl;
    var v5 = v3.cur;
    if (v5 == 1000) {
      this.compact(plan);
      return this.empty(plan);
    }
    var v6 = HfStd.createEmptyMC(this.root_mc, v5 + plan * 1000);
    v4[v5] = v6;
    ++v3.cur;
    return v6;
  }

  public function reserve(mc: MovieClip, plan: Int): Int {
    var v4 = this.getPlan(plan);
    var v5 = v4.tbl;
    var v6 = v4.cur;
    if (v6 == 1000) {
      this.compact(plan);
      return this.reserve(mc, plan);
    }
    v5[v6] = mc;
    ++v4.cur;
    return v6 + plan * 1000;
  }

  public function swap(mc: MovieClip, plan: Int): Void {
    var v4 = Math.floor(mc.getDepth() / 1000);
    if (v4 == plan) {
      return;
    }
    var v5 = this.getPlan(v4);
    var v6 = v5.tbl;
    var v7 = v5.cur;
    for (v8 in 0...v7) {
      if (v6[v8] == mc) {
        v6[v8] = null;
      } else {
      }
    }
    mc.swapDepths(this.reserve(mc, plan));
  }

  public function under(mc: MovieClip): Void {
    var v3 = mc.getDepth();
    var v4 = Math.floor(v3 / 1000);
    var v5 = this.getPlan(v4);
    var v6 = v5.tbl;
    var v7 = v3 % 1000;
    if (v6[v7] == mc) {
      v6[v7] = null;
      v6.unshift(mc);
      ++v5.cur;
      this.compact(v4);
    }
  }

  public function over(mc: MovieClip): Void {
    var v3 = mc.getDepth();
    var v4 = Math.floor(v3 / 1000);
    var v5 = this.getPlan(v4);
    var v6 = v5.tbl;
    var v7 = v3 % 1000;
    if (v6[v7] == mc) {
      v6[v7] = null;
      if (v5.cur == 1000) {
        this.compact(v4);
      }
      v3 = v5.cur;
      ++v5.cur;
      mc.swapDepths(v3 + v4 * 1000);
      v6[v3] = mc;
    }
  }

  public function ysort(plan: Int): Void {
    var v7;
    var v3 = this.getPlan(plan);
    var v4 = v3.tbl;
    var v5 = v3.cur;
    var v8 = -99999999.0;
    for (v6 in 0...v5) {
      var v9 = v4[v6];
      var v10 = v9._y;
      if (v10 >= v8) {
        v8 = v10;
      } else {
        v7 = v6;
        while (v7 > 0) {
          var v11 = v4[v7 - 1];
          if (v11._y > v10) {
            v4[v7] = v11;
            v9.swapDepths(v11);
            continue;
          }
          v4[v7] = v9;
          break;
          --v7;
        }
        if (v7 == 0) {
          v4[0] = v9;
        }
      }
    }
  }

  public function clear(plan: Int): Void {
    var v3 = this.getPlan(plan);
    var v5 = v3.tbl;
    for (v4 in 0...v3.cur) {
      v5[v4].removeMovieClip();
    }
    v3.cur = 0;
  }

  public function destroy(): Void {
    for (v2 in 0...this.plans.length) {
      this.clear(v2);
    }
  }
}
