package hf.entity.bad.walker;

import hf.entity.bad.Shooter;
import hf.mode.GameMode;

class Poire extends Shooter {
  public function new(): Void {
    super();
    this.setJumpH(100);
    this.setClimb(100, 1);
    this.setShoot(4);
    this.initShooter(50, 8);
  }

  public static function attach(g: GameMode, x: Float, y: Float): Poire {
    var v5 = Data.LINKAGES[Data.BAD_POIRE];
    var v6: Poire = cast g.depthMan.attach(v5, Data.DP_BADS);
    v6.initBad(g, x, y);
    return v6;
  }

  override public function onShoot(): Void {
    var v2 = hf.entity.bomb.bad.PoireBomb.attach(this.game, this.x, this.y);
    var v3 = 10;
    if (this.dir < 0) {
      v2.moveToAng(-135, v3);
    } else {
      v2.moveToAng(-45, v3);
    }
    this.setNext(null, null, this.shootDuration, Data.ACTION_FALLBACK);
  }
}
