package hf.entity.shoot;

import hf.entity.bad.walker.Fraise;
import hf.entity.Shoot;
import hf.Entity;
import hf.mode.GameMode;

class Ball extends Shoot {
  public var targetCatcher: Null<Fraise>;

  public function new(): Void {
    super();
    this.shootSpeed = 8.5;
    this._yOffset = 0;
    this.setLifeTimer(Data.BALL_TIMEOUT);
    this.fl_alphaBlink = false;
    this.fl_borderBounce = true;
  }

  public static function attach(g: GameMode, x: Float, y: Float): Ball {
    var v5 = "hammer_shoot_ball";
    var v6: Ball = cast g.depthMan.attach(v5, Data.DP_SHOTS);
    v6.initShoot(g, x, y);
    return v6;
  }

  override public function hit(e: Entity): Void {
    var v3 = Data.PLAYER.downcast(e);
    if (v3 != null) {
      v3.killHit(this.dx);
    }
    var c = Data.CATCHER.downcast(e);
    if (c != null && this.targetCatcher == c) {
      c.catchBall(this);
    }
  }

  override public function init(g: GameMode): Void {
    super.init(g);
    this.register(Data.BALL);
  }

  override public function onLifeTimer(): Void {
    this.game.fxMan.attachFx(this.x, this.y - Data.CASE_HEIGHT / 2, "hammer_fx_pop");
    var v3 = this.game.getOne(Data.CATCHER);
    v3.assignBall();
    super.onLifeTimer();
  }
}
