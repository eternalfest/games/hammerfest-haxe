package hf.entity.supa;

import hf.entity.Supa;
import hf.mode.GameMode;

class Bubble extends Supa {
  public function new(): Void {
    super();
    this.fl_alphaBlink = true;
    this.blinkAlpha = 25;
  }

  public static function attach(g: GameMode): Bubble {
    var v3 = "hammer_supa_bubble";
    var v4: Bubble = cast g.depthMan.attach(v3, Data.DP_SUPA);
    v4.initSupa(g, Data.GAME_WIDTH, 0);
    return v4;
  }

  override public function initSupa(g: GameMode, x: Float, y: Float): Void {
    super.initSupa(g, x, y);
    this.speed = 8;
    this.radius = 80;
    this.moveTo(Data.GAME_WIDTH / 2, Data.GAME_HEIGHT / 2);
    this.moveToAng(45 + HfStd.random(30) * (HfStd.random(2) * 2 - 1) + (HfStd.random(4) + 1) * 90, this.speed + HfStd.random(5));
    this.setLifeTimer(Data.SECOND * 10);
  }

  override public function postfix(): Void {
    super.postfix();
    if (this.y <= 0) {
      this.dy = Math.abs(this.dy);
    }
    if (this.y >= Data.GAME_HEIGHT) {
      this.dy = -Math.abs(this.dy);
    }
    if (this.x <= 0) {
      this.dx = Math.abs(this.dx);
    }
    if (this.x >= Data.GAME_WIDTH) {
      this.dx = -Math.abs(this.dx);
    }
  }

  override public function prefix(): Void {
    super.prefix();
    var v3 = this.game.getClose(Data.BAD, this.x, this.y, this.radius, false);
    for (v4 in 0...v3.length) {
      var v5 = v3[v4];
      if (!v5.fl_knock) {
        v5.knock(Data.KNOCK_DURATION * 2);
        v5.dx = this.dx * 2;
        v5.dy = -5;
        this.game.fxMan.inGameParticles(Data.PARTICLE_ICE, v5.x, v5.y - Data.CASE_HEIGHT, HfStd.random(5));
      }
    }
  }
}
