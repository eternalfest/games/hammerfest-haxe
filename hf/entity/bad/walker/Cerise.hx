package hf.entity.bad.walker;

import hf.entity.bad.Walker;
import hf.mode.GameMode;

class Cerise extends Walker {
  public function new(): Void {
    super();
    this.animFactor = 0.65;
  }

  public static function attach(g: GameMode, x: Float, y: Float): Cerise {
    var v5 = Data.LINKAGES[Data.BAD_CERISE];
    var v6: Cerise = cast g.depthMan.attach(v5, Data.DP_BADS);
    v6.initBad(g, x, y);
    return v6;
  }
}
