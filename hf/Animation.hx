package hf;

import etwin.flash.DynamicMovieClip;
import hf.mode.GameMode;

class Animation {
  public var mc: Null<DynamicMovieClip /* TODO */>;
  public var game: GameMode;
  public var frame: Float;
  public var lifeTimer: Float;
  public var blinkTimer: Float;
  public var fl_kill: Bool;
  public var fl_loop: Bool;
  public var fl_loopDone: Bool;
  public var fl_blink: Bool;

  public function new(g: GameMode): Void {
    this.game = g;
    this.frame = 0;
    this.lifeTimer = 0;
    this.blinkTimer = 0;
    this.fl_kill = false;
    this.fl_loop = false;
    this.fl_loopDone = false;
    this.fl_blink = false;
  }

  public function attach(x: Float, y: Float, link: String, depth: Int): Void {
    this.mc = ((cast this.game.depthMan.attach(link, depth)): DynamicMovieClip);
    this.mc._x = x;
    this.mc._y = y;
  }

  public function init(g: GameMode): Void {
    this.game = g;
  }

  public function destroy(): Void {
    this.mc.removeMovieClip();
    this.fl_kill = true;
  }

  public function blink(): Void {
    this.fl_blink = true;
  }

  public function stopBlink(): Void {
    this.fl_blink = false;
    this.mc._alpha = 100;
  }

  public function short(): String {
    return this.mc._name + " @" + this.mc._x + "," + this.mc._y;
  }

  public function update(): Void {
    if (this.fl_loopDone) {
      this.lifeTimer -= Timer.tmod;
      if (this.lifeTimer <= 0) {
        this.destroy();
      }
    }
    if (this.fl_blink) {
      this.blinkTimer -= Timer.tmod;
      if (this.blinkTimer <= 0) {
        this.mc._alpha = (this.mc._alpha != 100) ? 100 : 30;
        this.blinkTimer = Data.BLINK_DURATION_FAST;
      }
    }
    this.frame += Timer.tmod;
    while (this.frame >= 1) {
      this.mc.nextFrame();
      if (this.mc._currentframe == this.mc._totalframes) {
        if (this.fl_loop) {
          this.mc.gotoAndStop("1");
        }
        this.fl_loopDone = true;
      }
      --this.frame;
    }
  }
}
