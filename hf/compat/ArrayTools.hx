package hf.compat;

class ArrayTools {

  public static inline function duplicate<T>(array: Array<T>): Array<T> {
    return untyped array.duplicate();
  }

}