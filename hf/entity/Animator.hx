package hf.entity;

import etwin.flash.DynamicMovieClip;
import hf.entity.Trigger;
import hf.mode.GameMode;

typedef Anim = {
  var id: Int;
  var loop: Bool;
}

class Animator extends Trigger {
  public var sub: Null<DynamicMovieClip>;
  public var frame: Float;
  public var animId: Int;
  public var fl_anim: Bool;
  public var animFactor: Float;
  public var fl_loop: Bool;
  public var fl_blinking: Bool;
  public var fl_blinked: Bool;
  public var fl_stickyAnim: Bool;
  public var fl_alphaBlink: Bool;
  public var fl_blink: Bool;
  public var blinkTimer: Float;
  public var blinkColor: Int;
  public var blinkAlpha: Float;
  public var blinkColorAlpha: Float;
  public var fadeStep: Float;

  public function new(): Void {
    super();
    this.frame = 0;
    this.fadeStep = 0;
    this.animFactor = 1.0;
    this.fl_loop = false;
    this.fl_blinking = false;
    this.fl_blinked = true;
    this.fl_stickyAnim = false;
    this.fl_alphaBlink = true;
    this.fl_blink = true;
    this.blinkTimer = 0;
    this.blinkColor = 16777215;
    this.blinkAlpha = 20;
    this.blinkColorAlpha = 30;
    this.enableAnimator();
  }

  public function enableAnimator(): Void {
    this.fl_anim = true;
    this.stop();
  }

  public function disableAnimator(): Void {
    this.fl_anim = false;
    this.play();
  }

  public function blink(duration: Float): Void {
    if (!this.fl_blink) {
      return;
    }
    this.fl_blinking = true;
    this.blinkTimer = duration;
  }

  public function stopBlink(): Void {
    this.fl_blinking = false;
    if (this.fl_alphaBlink) {
      this.alpha = 100;
    } else {
      this.resetColor();
    }
  }

  public function blinkLife(): Void {
    if (this.lifeTimer / this.totalLife <= 0.1) {
      this.blink(Data.BLINK_DURATION_FAST);
    } else {
      if (this.lifeTimer / this.totalLife <= 0.3) {
        this.blink(Data.BLINK_DURATION);
      }
    }
  }

  public function setSub(mc: DynamicMovieClip): Void {
    this.sub = mc;
  }

  public function onEndAnim(id: Int): Void {
    this.unstickAnim();
  }

  public function stickAnim(): Void {
    this.fl_stickyAnim = true;
  }

  public function unstickAnim(): Void {
    this.fl_stickyAnim = false;
  }

  public function playAnim(animObject: Anim): Void {
    if (this.fl_stickyAnim || this.fl_kill || !this.fl_anim) {
      return;
    }
    if (this.animId == animObject.id && this.fl_loop == animObject.loop) {
      return;
    }
    this.animId = animObject.id;
    this.gotoAndStop("" + (this.animId + 1));
    this.sub.gotoAndStop("1");
    this.fl_loop = animObject.loop;
    this.frame = 0;
  }

  public function forceLoop(flag: Bool): Void {
    this.fl_loop = flag;
  }

  public function replayAnim(): Void {
    var v2 = this.animId;
    var v3 = (v2 != 1) ? 1 : 2;
    this.playAnim({id: v3, loop: this.fl_loop});
    this.playAnim({id: v2, loop: this.fl_loop});
  }

  override public function init(g: GameMode): Void {
    super.init(g);
    this.gotoAndStop("1");
    this.sub.stop();
  }

  override public function update(): Void {
    super.update();
    if (this.fl_blink) {
      if (!this.fl_blinking && this.lifeTimer > 0) {
        this.blinkLife();
      }
      if (this.fl_blinking) {
        this.blinkTimer -= Timer.tmod;
        if (this.blinkTimer <= 0) {
          if (this.fl_blinked) {
            if (this.fl_alphaBlink) {
              this.alpha = 100;
            } else {
              this.resetColor();
            }
            this.fl_blinked = false;
          } else {
            if (this.fl_alphaBlink) {
              this.alpha = this.blinkAlpha;
            } else {
              this.setColorHex(this.blinkColorAlpha, this.blinkColor);
            }
            this.fl_blinked = true;
          }
          this.blinkLife();
        }
      }
    }
    if (!this.fl_anim) {
      return;
    }
    if (this.frame >= 0) {
      var v3 = false;
      this.frame += this.animFactor * Timer.tmod;
      while (true) {
        if (!(!v3 && this.frame >= 1)) break;
        if (this.sub._currentframe == this.sub._totalframes) {
          if (this.fl_loop) {
            this.sub.gotoAndStop("1");
          } else {
            this.frame = -1;
            this.onEndAnim(this.animId);
            v3 = true;
          }
        }
        if (!v3) {
          this.sub.nextFrame();
          --this.frame;
        }
      }
    }
  }
}
