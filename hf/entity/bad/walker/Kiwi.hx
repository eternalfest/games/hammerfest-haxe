package hf.entity.bad.walker;

import hf.entity.bad.Shooter;
import hf.entity.bomb.bad.Mine;
import hf.mode.GameMode;

class Kiwi extends Shooter {
  public var mineList: Array<Mine>;

  public function new(): Void {
    super();
    this.setJumpUp(10);
    this.setJumpDown(20);
    this.setJumpH(50);
    this.setClimb(100, 3);
    this.setShoot(3);
    this.initShooter(Data.SECOND * 1, Data.SECOND * 0.6);
    this.speed *= 0.7;
    this.angerFactor *= 2.5;
    this.calcSpeed();
    this.shootCD = 0;
    this.mineList = new Array();
  }

  public static function attach(g: GameMode, x: Float, y: Float): Kiwi {
    var v5 = Data.LINKAGES[Data.BAD_KIWI];
    var v6: Kiwi = cast g.depthMan.attach(v5, Data.DP_BADS);
    v6.initBad(g, x, y);
    return v6;
  }

  public function clearMines(): Void {
    for (v2 in 0...this.mineList.length) {
      var v3 = this.mineList[v2];
      if (!v3.fl_trigger) {
        v3.setLifeTimer(HfStd.random(Math.round(Data.SECOND * 0.7)));
      }
    }
    this.mineList = new Array();
  }

  override public function onShoot(): Void {
    var v2 = hf.entity.bomb.bad.Mine.attach(this.game, this.x + this.dir * Data.CASE_WIDTH * 1.1, this.y);
    this.mineList.push(v2);
  }
}
