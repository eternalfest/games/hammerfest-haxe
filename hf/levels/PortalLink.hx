package hf.levels;

class PortalLink {
  public var from_did: Null<Int>;
  public var from_lid: Null<Int>;
  public var from_pid: Null<Int>;
  public var to_did: Null<Int>;
  public var to_lid: Null<Int>;
  public var to_pid: Null<Int>;

  public function new(): Void {
  }

  public function cleanUp(): Void {
    if (HfStd.isNaN(this.from_pid)) {
      this.from_pid = 0;
    }
    if (HfStd.isNaN(this.to_pid)) {
      this.to_pid = 0;
    }
  }

  public function trace(): Void {
    Log.trace("link: " + this.from_did + "," + this.from_lid + "(" + this.from_pid + ")  > " + this.to_did + "," + this.to_lid + "(" + this.to_pid + ")");
  }
}
