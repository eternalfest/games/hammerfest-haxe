package hf.entity.bomb.bad;

import hf.entity.bomb.BadBomb;
import hf.entity.Player;
import hf.mode.GameMode;

class PoireBomb extends BadBomb {
  public function new(): Void {
    super();
    this.duration = 45;
    this.power = 30;
    this.radius = Data.CASE_WIDTH * 4;
  }

  public static function attach(g: GameMode, x: Float, y: Float): PoireBomb {
    var v5 = "hammer_bomb_poire";
    var v6: PoireBomb = cast g.depthMan.attach(v5, Data.DP_BOMBS);
    v6.initBomb(g, x, y);
    return v6;
  }

  override public function duplicate(): PoireBomb {
    return hf.entity.bomb.bad.PoireBomb.attach(this.game, this.x, this.y);
  }

  override public function onExplode(): Void {
    super.onExplode();
    this.game.fxMan.attachExplodeZone(this.x, this.y, this.radius);
    var v3 = this.game.getClose(Data.PLAYER, this.x, this.y, this.radius, false);
    for (v4 in 0...v3.length) {
      var v5 = v3[v4];
      v5.killHit(0);
      this.shockWave(v5, this.radius, this.power);
      if (!v5.fl_shield) {
        v5.dy = -10 - HfStd.random(20);
      }
    }
  }

  override public function onKick(p: Player): Void {
    super.onKick(p);
    this.setLifeTimer(this.lifeTimer + Data.SECOND * 0.5);
  }
}
