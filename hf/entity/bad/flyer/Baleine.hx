package hf.entity.bad.flyer;

import hf.entity.bad.Flyer;
import hf.mode.GameMode;

/**
 * https://i.imgur.com/lzb51Dn.jpg
 */
class Baleine extends Flyer {
  public function new(): Void {
    super();
  }

  public static function attach(g: GameMode, x: Float, y: Float): Baleine {
    var v5 = Data.LINKAGES[Data.BAD_BALEINE];
    var v6: Baleine = cast g.depthMan.attach(v5, Data.DP_BADS);
    v6.initBad(g, x, y);
    return v6;
  }
}
