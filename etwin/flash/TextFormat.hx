package etwin.flash;

@:native("TextFormat")
extern class TextFormat {
  var align: String;
  var blockIndent: Float;
  var bold: Bool;
  var bullet: Bool;
  var color: Int;
  var font: String;
  var indent: Float;
  var italic: Bool;
  var kerning: Bool;
  var leading: Float;
  var leftMargin: Float;
  var letterSpacing: Float;
  var rightMargin: Float;
  var size: Float;
  var tabStops: Array<Int>;
  var target: String;
  var underline: Bool;
  var url: String;

  function new(
    ?font: String, ?size: Float, ?color: Int,
    ?bold: Bool, ?italic: Bool, ?underline: Bool,
    ?url: String, ?target: String, ?align: String,
    ?leftMargin: Float, ?rightMargin: Float, ?indent: Float,
    ?leading: Float
  ): Void;

  function getTextExtent(text: String, ?width: Float): Dynamic;
}
