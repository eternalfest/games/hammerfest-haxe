package hf.entity.bomb.player;

import hf.entity.bomb.PlayerBomb;
import hf.mode.GameMode;

class Red extends PlayerBomb {
  public var JUMP_POWER: Float;

  public function new(): Void {
    super();
    this.duration = 38;
    this.power = 30;
    this.JUMP_POWER = 32;
  }

  public static function attach(g: GameMode, x: Float, y: Float): Red {
    var v5 = "hammer_bomb_red";
    var v6: Red = cast g.depthMan.attach(v5, Data.DP_BOMBS);
    v6.initBomb(g, x, y);
    return v6;
  }

  override public function duplicate(): Red {
    return hf.entity.bomb.player.Red.attach(this.game, this.x, this.y);
  }

  override public function onExplode(): Void {
    super.onExplode();
    var v3 = this.bombGetClose(Data.BAD);
    for (v4 in 0...v3.length) {
      var v5 = v3[v4];
      v5.setCombo(this.uniqId);
      v5.freeze(Data.FREEZE_DURATION);
      this.shockWave(v5, this.radius, this.power);
      if (v5.dy < 0) {
        v5.dy *= 3;
        if (this.distance(v5.x, v5.y) <= this.radius * 0.5) {
          v5.dx *= 0.5;
          v5.dy *= 2;
        }
      }
    }
    this.game.fxMan.inGameParticles(Data.PARTICLE_ICE, this.x, this.y, HfStd.random(2) + 2);
    this.game.fxMan.attachExplodeZone(this.x, this.y, this.radius);
    var v3 = this.game.getList(Data.PLAYER);
    for (v6 in 0...v3.length) {
      var v7 = v3[v6];
      var v8 = v7.x - this.x;
      var v9 = v7.y - this.y;
      if (this.fl_stable) {
        v8 *= 1.5;
        v9 *= 0.5;
      } else {
        v8 *= 0.9;
        v9 *= 0.35;
      }
      var v10 = Math.sqrt(v8 * v8 + v9 * v9);
      if (v10 <= 40) {
        if (v7.dy > 0) {
          v7.dy = 0;
        }
        v7.dy -= this.JUMP_POWER;
        if (v7.dy <= -35) {
          this.game.shake(10, 3);
          this.game.fxMan.attachExplodeZone(v7.x, v7.y - 40, 50);
          this.game.fxMan.attachExplodeZone(v7.x, v7.y - 80, 40);
          this.game.fxMan.attachExplodeZone(v7.x, v7.y - 120, 30);
        }
      } else {
        if (v7.distance(this.x, this.y) <= this.radius) {
          this.shockWave(v7, this.radius, this.power);
        }
      }
    }
  }
}
