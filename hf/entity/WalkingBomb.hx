package hf.entity;

import hf.entity.Physics;
import hf.entity.Player;
import hf.entity.bomb.PlayerBomb;
import hf.mode.GameMode;
import etwin.flash.Key;

class WalkingBomb extends Physics {
  public var left: Int;
  public var right: Int;
  public var fl_unstable: Bool;
  public var fl_knock: Bool;
  public var knockTimer: Float;
  public var realBomb: PlayerBomb;

  public function new(): Void {
    super();
    this.left = Key.PGUP;
    this.right = Key.PGDN;
    this.dir = 1;
    this.fl_slide = false;
    this.fl_teleport = true;
    this.fl_wind = true;
    this.fl_blink = false;
    this.fl_portal = true;
    this.fl_unstable = false;
    this.fl_knock = false;
  }

  public static function attach(g: GameMode, b: PlayerBomb): WalkingBomb {
    var v4 = "hammer_player_wbomb";
    var v5: WalkingBomb = cast g.depthMan.attach(v4, Data.DP_BOMBS);
    v5.initBomb(g, b);
    return v5;
  }

  public function initBomb(g: GameMode, b: PlayerBomb): Void {
    this.init(g);
    this.register(Data.BOMB);
    this.realBomb = b;
    this.moveTo(this.realBomb.x, this.realBomb.y);
    this.setLifeTimer(b.lifeTimer);
    this.dx = this.realBomb.dx;
    this.dy = this.realBomb.dy;
    this.fl_unstable = this.realBomb.fl_unstable;
    this.updateCoords();
    this.playAnim(Data.ANIM_WBOMB_STOP);
  }

  public function destroyBoth(): Void {
    this.realBomb.destroy();
    this.destroy();
  }

  public function onKick(p: Player): Void {
    this.fl_stable = false;
  }

  public function onExplode(): Void {
    this.realBomb.moveTo(this.x, this.y);
    this.realBomb.updateCoords();
    this.realBomb.onExplode();
    this.destroy();
  }

  public function getControls(): Void {
    if (Key.isDown(this.left)) {
      this.dx = -Data.WBOMB_SPEED;
      this.dir = -1;
      if (this.fl_stable) {
        this.playAnim(Data.ANIM_WBOMB_WALK);
      }
    }
    if (Key.isDown(this.right)) {
      this.dx = Data.WBOMB_SPEED;
      this.dir = 1;
      if (this.fl_stable) {
        this.playAnim(Data.ANIM_WBOMB_WALK);
      }
    }
    if (!Key.isDown(this.left) && !Key.isDown(this.right)) {
      this.dx *= this.game.gFriction * 0.9;
      if (this.animId == Data.ANIM_WBOMB_WALK.id) {
        this.playAnim(Data.ANIM_WBOMB_STOP);
      }
    }
  }

  public function knock(d: Float): Void {
    this.fl_knock = true;
    this.knockTimer = d;
    this.playAnim(Data.ANIM_WBOMB_STOP);
  }

  public function checkClimb(): Void {
  }

  public function jump(jx: Float, jy: Float): Void {
    this.dx = this.dir * jx;
    this.dy = -jy;
    this.fl_stable = false;
  }

  override public function endUpdate(): Void {
    super.endUpdate();
    this._xscale = this.dir * Math.abs(this._xscale);
  }

  override public function hit(e: Entity): Void {
    super.hit(e);
    if (this.fl_unstable && Data.BAD.check(e)) {
      this.onExplode();
    }
  }

  override public function infix(): Void {
    super.infix();
    if (this.fl_stable) {
      if (this.world.checkFlag({x: this.cx, y: this.cy}, Data.IA_JUMP_LEFT) && this.dx < 0) {
        this.jump(Data.BAD_HJUMP_X, Data.BAD_HJUMP_Y);
      }
      if (this.world.checkFlag({x: this.cx, y: this.cy}, Data.IA_JUMP_RIGHT) && this.dx > 0) {
        this.jump(Data.BAD_HJUMP_X, Data.BAD_HJUMP_Y);
      }
    }
  }

  override public function onDeathLine(): Void {
    super.onDeathLine();
    this.destroyBoth();
  }

  override public function onHitGround(h: Float): Void {
    super.onHitGround(h);
    if (this.fl_unstable && h >= 10) {
      this.onExplode();
    }
  }

  override public function onHitWall(): Void {
    if (this.fl_stable) {
      if (Key.isDown(this.left) && this.world.checkFlag({x: this.cx, y: this.cy}, Data.IA_CLIMB_LEFT)) {
        var v2 = this.world.getWallHeight(this.cx - 1, this.cy, Data.IA_CLIMB);
        if (v2 <= 1) {
          this.jump(Data.BAD_VJUMP_X_CLIFF, Data.BAD_VJUMP_Y_LIST[0]);
          this.centerInCase();
        }
      }
      if (Key.isDown(this.right) && this.world.checkFlag({x: this.cx, y: this.cy}, Data.IA_CLIMB_RIGHT)) {
        var v3 = this.world.getWallHeight(this.cx + 1, this.cy, Data.IA_CLIMB);
        if (v3 <= 1) {
          this.jump(Data.BAD_VJUMP_X_CLIFF, Data.BAD_VJUMP_Y_LIST[0]);
          this.centerInCase();
        }
      }
    }
  }

  override public function onLifeTimer(): Void {
    this.onExplode();
    super.onLifeTimer();
  }

  override public function onPortal(pid: Int): Void {
    super.onPortal(pid);
    this.game.fxMan.attachFx(this.x, this.y - Data.CASE_HEIGHT * 0.5, "hammer_fx_shine");
    this.destroyBoth();
  }

  override public function onPortalRefusal(): Void {
    super.onPortalRefusal();
    this.knock(Data.SECOND);
    this.dx = -this.dx * 3;
    this.dy = -5;
    this.game.fxMan.inGameParticles(Data.PARTICLE_PORTAL, this.x, this.y, 5);
  }

  override public function needsPatch(): Bool {
    return true;
  }

  override public function update(): Void {
    this.realBomb.y = -1500;
    this.realBomb.lifeTimer = Data.SECOND * 10;
    if (this.fl_knock && this.knockTimer > 0) {
      this.knockTimer -= Timer.tmod;
      if (this.knockTimer <= 0) {
        this.fl_knock = false;
      }
    }
    if (this.fl_stable && !this.fl_knock) {
      this.getControls();
    } else {
      if (this.animId == Data.ANIM_WBOMB_WALK.id) {
        this.playAnim(Data.ANIM_WBOMB_STOP);
      }
    }
    super.update();
    if (this.realBomb._name == null) {
      this.destroy();
    }
  }
}
