package hf.compat;

import etwin.flash.XMLNode;

class XMLNodeTools {

  public static inline function get(node: XMLNode, attr: String): Null<String> {
    return untyped node.get(attr);
  }

  public static inline function set(node: XMLNode, attr: String, value: Null<String>) {
    return untyped node.set(attr, value);
  }

}