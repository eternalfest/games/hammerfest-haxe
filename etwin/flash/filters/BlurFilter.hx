package etwin.flash.filters;

import etwin.flash.filters.BitmapFilter;

@:native("flash.filters.BlurFilter")
@:final
extern class BlurFilter extends BitmapFilter {
	var quality: Float;
	var blurX: Float;
	var blurY: Float;

	function new(?blurX: Float, ?blurY: Float, ?quality:  Float): Void;
	override function clone(): BlurFilter;
}
