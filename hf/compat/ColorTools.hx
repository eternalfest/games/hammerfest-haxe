package hf.compat;

import etwin.flash.Color;

class ColorTools {

  public static inline function reset(color: Color): Void {
    return untyped color.reset();
  }

}