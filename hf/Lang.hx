package hf;

import etwin.flash.XMLNode;
import etwin.flash.XML;
using hf.compat.XMLNodeTools;

class Lang {
  public static var strList: Array<String> = [];
  public static var itemNames: Array<String> = [];
  public static var familyNames: Array<String> = [];
  public static var questNames: Array<String> = [];
  public static var questDesc: Array<String> = [];
  public static var levelNames: Array<Array<String>> = [];
  public static var keyNames: Array<String> = [];
  public static var lang: String = "ERR ";
  public static var fl_debug: Bool = false;
  public static var doc: Null<XMLNode>;

  public static function init(raw: String): Void {
    var v3 = new XML(raw);
    v3.ignoreWhite = true;
    v3.parseXML(raw);
    Lang.doc = v3.firstChild;
    Lang.lang = Lang.doc.get("$id".substring(1));
    Lang.fl_debug = Lang.doc.get("$debug".substring(1)) == "1";
    Lang.strList = new Array();
    Lang.strList = Lang._getStringData("$statics", "$v");
    Lang.itemNames = Lang._getStringData("$items", "$name");
    Lang.familyNames = Lang._getStringData("$families", "$name");
    Lang.questNames = Lang._getStringData("$quests", "$title");
    Lang.keyNames = Lang._getStringData("$keys", "$name");
    var v4 = Lang._find(Lang.doc, "$quests".substring(1));
    while (v4 != null) {
      var v5 = HfStd.parseInt(v4.get("$id".substring(1)), 10);
      Lang.questDesc[v5] = Data.cleanString(v4.firstChild.nodeValue);
      v4 = v4.nextSibling;
    }
    v4 = Lang._find(Lang.doc, "$dimensions".substring(1));
    while (v4 != null) {
      var v6 = HfStd.parseInt(v4.get("$id".substring(1)), 10);
      Lang.levelNames[v6] = new Array();
      var v7 = v4.firstChild;
      while (v7 != null) {
        var v8 = HfStd.parseInt(v7.get("$id".substring(1)), 10);
        Lang.levelNames[v6][v8] = v7.get("$name".substring(1));
        v7 = v7.nextSibling;
      }
      v4 = v4.nextSibling;
    }
  }

  public static function _getStringData(parentNode: String, attrName: String): Array<String> {
    var v4 = new Array();
    var v5 = Lang._find(Lang.doc, parentNode.substring(1));
    while (v5 != null) {
      var v6 = HfStd.parseInt(v5.get("$id".substring(1)), 10);
      var v7 = v5.get(attrName.substring(1));
      if (Lang.fl_debug) {
        v7 = "[" + Lang.lang.toLowerCase() + "]" + v7;
      }
      v4[v6] = v7;
      v5 = v5.nextSibling;
    }
    return v4;
  }

  public static function _find(doc: XMLNode, name: String): Null<XMLNode> {
    var v4 = doc.firstChild;
    while (v4.nodeName != name) {
      v4 = v4.nextSibling;
      if (v4 == null) {
        GameManager.fatal("node '" + name + "' not found !");
        return null;
      }
    }
    return v4.firstChild;
  }

  public static function get(id: Int): String {
    if (Lang.fl_debug) {
      return "[" + Lang.lang.toLowerCase() + "]" + Lang.strList[id];
    }
    return Lang.strList[id];
  }

  public static function getItemName(id: Int): String {
    if (Lang.itemNames[id] == null) {
    }
    if (Lang.fl_debug) {
      return "[" + Lang.lang.toLowerCase() + "]" + Lang.itemNames[id];
    }
    return Lang.itemNames[id];
  }

  public static function getFamilyName(id: Int): String {
    return Lang.familyNames[id];
  }

  public static function getQuestName(id: Int): String {
    return Lang.questNames[id];
  }

  public static function getQuestDesc(id: Int): String {
    return Lang.questDesc[id];
  }

  public static function getLevelName(did: Int, lid: Int): String {
    return Lang.levelNames[did][lid];
  }

  public static function getSectorName(did: Int, lid: Int): String {
    var v4 = Lang.getLevelName(did, lid);
    while (true) {
      if (!(lid > 0 && v4 == null)) break;
      --lid;
      v4 = Lang.getLevelName(did, lid);
    }
    if (v4 == null) {
      v4 = "";
    }
    return v4;
  }

  public static function getKeyName(kid: Int): String {
    return Lang.keyNames[kid];
  }
}
