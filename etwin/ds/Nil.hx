package etwin.ds;

import haxe.macro.Expr;

/**
 * Typed `Null<T>`, or lightweight alternative to `haxe.ds.Option<T>`.
 *
 * It internally use a sentinel value to represent the `none` case;
 * this implies that:
 *   - Like `Option<T>`, `Nil.some(null) != Nil.none()`;
 *   - Like `Null<T>`, `Nil<Nil<T>>` is equivalent to `Nil<T>`.
 *   - `Nil<T>` values can be compared directly without unwrapping them.
 */
abstract Nil<T>(Dynamic) {

  @:keep
  private static var NONE(default, null): Dynamic = untyped {
    #if flash8
    // To improve performance of the `isNone()` test on
    // Flash, we need to limit the number of field accesses
    // necessary to get the NONE sentinel value.
    // As such, we put it directly in each movie's root scope,
    // so that a single field access is needed.
    // To avoid having a separate NONE value for each movie, we
    // additionally keep a 'master copy' in the _global scope.
    //
    // Quick benchmarking shows a ~35% perf increase for `isNone()`.
    if (_global.__etwin_ds_Nil_None == null) {
      _global.__etwin_ds_Nil_None = {};
    }
    __etwin_ds_Nil_None = _global.__etwin_ds_Nil_None;
    __etwin_ds_Nil_None;
    #else
    {};
    #end
  };

  private inline function new(val: Dynamic) {
    this = val;
  }

  /**
    Create a `Nil` containing no value.
  **/
  public static inline function none<T>(): Nil<T> {
    #if flash8
    return untyped __etwin_ds_Nil_None;
    #else
    return NONE;
    #end
  }

  /**
    Create a `Nil` containing the given value.
  **/
  @:from public static inline function some<T>(val: T): Nil<T> {
    return new Nil(val);
  }

  /**
    Convert a nullable value into a `Nil<T>`, treating `null` as `Nil.none()`.
  **/
  public static inline function fromNullable<T>(val: Null<T>): Nil<T> {
    return val == null ? Nil.none() : Nil.some(val);
  }

  /**
    Convert a `Nil<T>` into a `Null<T>`, treating `Nil.none()` as `null`.

    **Note:**: Both `Nil.some(null)` and `Nil.none()` will be converted to `null`.
  **/
  public inline function toNullable(): Null<T> {
    return isNone() ? null : this;
  }

  /**
    Convert a `Option<T>` into a `Nil<T>`:
    - `Some(val)` becomes `Nil.some(val)`;
    - `None` becomes `Nil.none()`.
  **/
  public static function fromOption<T>(opt: haxe.ds.Option<T>): Nil<T> {
    return switch (opt) {
      case haxe.ds.Option.Some(val): Nil.some(val);
      case haxe.ds.Option.None: Nil.none();
    }
  }

  /**
    Convert a `Nil<T>` into a `Option<T>`:
    - `Nil.some(val)` becomes `Some(val)`;
    - `Nil.none()` becomes `None`.
  **/
  @:to public function toOption(): haxe.ds.Option<T> {
    if (isNone()) {
      return haxe.ds.Option.None;
    } else {
      return haxe.ds.Option.Some(this);
    }
  }

  /**
    Return `true` if this `Nil` doesn't contain a value.
  **/
  public inline function isNone(): Bool {
    return this == Nil.none();
  }

  /**
    Return `true` if this `Nil` contains a value.
  **/
  public inline function isSome(): Bool {
    return this != Nil.none();
  }

  /**
    Unwrap this `Nil<T>`, throwing an error if it was `Nil.none()`.
  **/
  public inline function unwrap(): T {
    return (this: Nil<T>).or(_ => {
      throw new etwin.Error("NilError: unwrapped a none() value");
    });
  }

  private inline function unwrapUnchecked(): T {
    return this;
  }

  /**
    Unwrap this `Nil<T>`, returning `val` if it was `Nil.none()`.

    If evaluating the default value is expensive or involve side-effects (`throw`ing
    an exception, `break`ing from a loop, etc), the lazy variant `.or(_ => <expr>)`
    can be used to only evaluate the default value if `this` was `Nil.none()`.

    **Usage:**
    ```hx
    var some = Nil.some(42);
    var none = Nil.none();

    var val = some.or(0); // 42
    var zero = none.or(0); // 0

    var reachable = some.or(_ => return); // 42
    var unreachable = some.or(return); // exits the function
    ```
  **/
  macro public function or(self: ExprOf<Nil<T>>, val: ExprOf<T>): ExprOf<T> {
    var stmts = [];
    // Don't use another variable if self is already a variable.
    var self = switch (self.expr) {
      case EConst(CIdent(_)): self;
      default:
        stmts.push(macro @:pos(self.pos) var __self = $self);
        macro @:pos(self.pos) __self;
    };

    var val = switch (val.expr) {
      // Lazy version.
      case EBinop(OpArrow, { expr: EConst(CIdent(left)) }, valExpr):
        if (left != "_")
          haxe.macro.Context.error("Expected lazy block: _ => <expr>", val.pos);
        valExpr;
      // Eager version, but is it a constant so we can use it directly.
      case EConst(_): val;
      // Eager version with a complex expression, introduce a tmp variable.
      default:
        stmts.push(macro @:pos(val.pos) var __val = $val);
        macro @:pos(val.pos) __val;
    };

    stmts.push(macro $self.isNone() ? $val : $self.unwrapUnchecked());
    return macro $b{stmts};
  }

  /**
    Map the value inside this `Nil<T>`. Control-flow such as
    `return`, `break` and `continue` will see through this call.

    **Usage:**
    ```hx
    var some = Nil.some(42);
    var none = Nil.none();

    // Nil.some("42")
    var str1 = some.map(v => Std.string(v));

    // Nil.none()
    var str2 = none.map(v => Std.string(v));

    // Nil.none()
    var negative = some.map(v => v < 0 ? v : Nil.none());
    ```
  **/
  macro public function map<U>(self: ExprOf<Nil<T>>, map: Expr): ExprOf<Nil<U>> {
    var stmts = [];
    // Don't use another variable if self is already a variable.
    var self = switch (self.expr) {
      case EConst(CIdent(_)): self;
      default:
        stmts.push(macro var __self = $self);
        macro __self;
    };

    return switch (map.expr) {
      case EBinop(OpArrow, { expr: EConst(CIdent(ident)) }, mapExpr):
        stmts.push(macro if ($self.isNone()) {
          // Cast the `Nil.none<T>()` to a `Nil.none<_>()`.
          // The `else` branch will infer the actual type.
          new etwin.ds.Nil(cast $self);
        } else {
          var $ident = $self.unwrapUnchecked();
          $mapExpr;
        });
        macro $b{stmts};
      default:
        haxe.macro.Context.error("Expected lazy block: <ident> => <expr>: " + map, map.pos);
    }
  }

  /**
    Return a string representation of this `Nil<T>`.
  **/
  public function toString(): String {
    return isNone() ? "None[]" : ("Some[" + Std.string(unwrapUnchecked()) + "]");
  }
}
