package hf.mode;

import etwin.flash.MovieClip;
import hf.Chrono;
import hf.entity.bomb.player.SoccerBall;
import hf.entity.Player;
import hf.GameManager;
import hf.mode.MultiCoop;
import etwin.flash.MovieClip;
import etwin.flash.Key;

class Soccer extends MultiCoop {
  public static var BOOST_DISTANCE: Float = 180;
  public static var MAX_BOOST: Float = 1.5;
  public static var MATCH_DURATION: Int = 5 * (1000 * 60);
  public static var WARNING_TIME: Int = 1 * (1000 * 60);
  public static var BLINK_COLOR: Int = 16759722;
  public static var BLINK_GLOWCOLOR: Int = 10027008;
  public static var GOAL_OFFTIME: Float = 5 * Data.SECOND;
  public static var START_OFFTIME: Float = 3 * Data.SECOND;
  public static var END_TIMER: Float = 13 * Data.SECOND;

  public var fl_help: Bool;
  public var fl_party: Bool;
  public var fl_match: Bool;
  public var fl_end: Bool;
  public var chrono: Chrono;
  public var blinkTimer: Float;
  public var scores: Array<Int>;
  public var teams: Array<Array<Player>>;
  public var ball: Null<SoccerBall>;
  public var offPlayTimer: Null<Float>;
  public var winners: Int;
  public var finalTimer: Float;

  public function new(m: GameManager, id: Dynamic): Void {
    id = null;
    if (GameManager.CONFIG.hasOption(Data.OPT_SET_SOC_0)) {
      id = 0;
    }
    if (GameManager.CONFIG.hasOption(Data.OPT_SET_SOC_1)) {
      id = 1;
    }
    if (GameManager.CONFIG.hasOption(Data.OPT_SET_SOC_2)) {
      id = 2;
    }
    if (GameManager.CONFIG.hasOption(Data.OPT_SET_SOC_3)) {
      id = 3;
    }
    if (id == null) {
      GameManager.fatal("unknown soccermap ID");
      return;
    }
    super(m, id);
    this._name = "$soccer";
    this.fl_disguise = false;
    this.fl_map = false;
    this.fl_help = true;
    this.fl_party = false;
    this.fl_match = false;
    this.fl_end = false;
    this.fl_nightmare = false;
    this.blinkTimer = 0;
    this.chrono = new Chrono();
    this.scores = new Array();
    this.teams = new Array();
    this.manager.fl_debug = false;
  }

  public function getTeam(p: Player): Int {
    var v3 = 0;
    for (v4 in 0...this.teams[1].length) {
      if (this.teams[1][v4].pid == p.pid) {
        v3 = 1;
      }
    }
    return v3;
  }

  public function getTeamPos(p: Player): Int {
    var v3 = this.getTeam(p);
    var v4 = 0;
    for (v5 in 0...this.teams[v3].length) {
      if (p.pid == this.teams[v3][v5].pid) {
        v4 = v5;
      }
    }
    return v4;
  }

  public function getTeamScore(tid: Int): Int {
    return this.scores[tid];
  }

  public function goal(tid: Int): Void {
    var v3 = this.ball.lastPlayer;
    var v4 = this.getPlayerList();
    ++this.scores[tid];
    v4[tid].getScoreHidden(1);
    for (v5 in 0...v4.length) {
      var v6 = v4[v5];
      v6.head = v6.defaultHead;
      v6.speedFactor = 1;
      if (this.getTeam(v6) == tid) {
        v6.setBaseAnims(Data.ANIM_PLAYER_WALK_V, Data.ANIM_PLAYER_STOP_V);
      } else {
        v6.setBaseAnims(Data.ANIM_PLAYER_SOCCER, Data.ANIM_PLAYER_STOP);
      }
    }
    if (tid == this.getTeam(v3)) {
      this.soundMan.playSound("sound_item_score", Data.CHAN_FIELD);
      this.fxMan.attachAlert(Lang.get(22) + " (" + this.getTeamScore(0) + " - " + this.getTeamScore(1) + ")");
      if (v3.skin == 4) {
        v3.head = Data.HEAD_SANDY_CROWN;
      } else {
        v3.head = Data.HEAD_CROWN;
      }
    } else {
      this.soundMan.playSound("sound_player_death", Data.CHAN_FIELD);
      this.fxMan.attachAlert(Lang.get(23));
      if (v3.skin == 4) {
        v3.head = Data.HEAD_SANDY_LOSE;
      } else {
        v3.head = Data.HEAD_LOSE;
      }
    }
    v3.replayAnim();
    this.fxMan.inGameParticlesDir(Data.PARTICLE_CLASSIC_BOMB, this.ball.x, this.ball.y, 10, -this.ball.dx);
    this.fxMan.attachFx(this.ball.x, this.ball.y - Data.CASE_HEIGHT, "hammer_fx_shine");
    this.offPlayTimer = hf.mode.Soccer.GOAL_OFFTIME;
  }

  public function endMatch(): Void {
    this.fxMan.clear();
    this.fxMan.inGameParticles(Data.PARTICLE_CLASSIC_BOMB, this.ball.x, this.ball.y, 10);
    this.ball.destroy();
    this.fxMan.attachAlert(Lang.get(25));
    this.offPlayTimer = 99999;
    this.fl_end = true;
    if (this.getTeamScore(0) > this.getTeamScore(1)) {
      this.winners = 0;
    }
    if (this.getTeamScore(0) < this.getTeamScore(1)) {
      this.winners = 1;
    }
    var v2 = this.getPlayerList();
    for (v3 in 0...v2.length) {
      var v4 = v2[v3];
      v4.speedFactor = 1;
      v4.setBaseAnims(Data.ANIM_PLAYER_SOCCER, Data.ANIM_PLAYER_STOP);
    }
  }

  public function showResults(): Void {
    if (this.winners == 0) {
      this.fxMan.attachAlert(Lang.get(26));
    }
    if (this.winners == 1) {
      this.fxMan.attachAlert(Lang.get(27));
    }
    if (this.winners == null) {
      this.fxMan.attachAlert(Lang.get(28));
    }
    this.shake(Data.SECOND, 5);
    var v2 = this.getPlayerList();
    for (v3 in 0...v2.length) {
      var v4 = v2[v3];
      if (this.winners != null && this.getTeam(v4) == this.winners) {
        v4.speedFactor = 1;
        v4.setBaseAnims(Data.ANIM_PLAYER_WALK_V, Data.ANIM_PLAYER_STOP_V);
      } else {
        v4.speedFactor = 0.3;
        v4.setBaseAnims(Data.ANIM_PLAYER_WALK_L, Data.ANIM_PLAYER_STOP_L);
      }
    }
  }

  public function onEndMode(): Void {
    this.endMode();
  }

  public function insertBall(): Void {
    var v2 = this.world.current.__dollar__specialSlots[HfStd.random(this.world.current.__dollar__specialSlots.length)];
    this.ball = hf.entity.bomb.player.SoccerBall.attach(this, Entity.x_ctr(v2.__dollar__x), Entity.y_ctr(v2.__dollar__y));
    this.fxMan.inGameParticles(Data.PARTICLE_CLASSIC_BOMB, this.ball.x, this.ball.y, 9);
    this.fxMan.inGameParticles(Data.PARTICLE_ICE, this.ball.x, this.ball.y, 5);
    var v3 = this.getPlayerList();
    for (v4 in 0...v3.length) {
      v3[v4].setBaseAnims(Data.ANIM_PLAYER_SOCCER, Data.ANIM_PLAYER_STOP);
    }
    this.soundMan.playSound("sound_pop", Data.CHAN_FIELD);
  }

  public function darknessManager(): Void {
  }

  override public function addLevelItems(): Void {
  }

  override public function endMode(): Void {
    var v2 = HfStd.getVar(HfStd.getRoot(), "$out");
    this.manager.redirect(v2, null);
  }

  override public function forcedGoto(id: Int): Void {
    super.forcedGoto(id);
    this.cleanKills();
    var v4 = this.getPlayerList();
    for (v5 in 0...v4.length) {
      this.initPlayer(v4[v5]);
    }
  }

  override public function getDebugControls(): Void {
  }

  override public function initGame(): Void {
    super.initGame();
    if (this.fl_party) {
      var v3 = this.insertPlayer(6, 10);
      v3.ctrl.setKeys(79, 76, 75, 77, 73);
      var v4 = this.insertPlayer(14, 10);
      v4.ctrl.setKeys(101, 98, 97, 99, 100);
    }
    var v5 = this.getPlayerList();
    this.teams = [[], []];
    for (v6 in 0...v5.length) {
      if (v6 < v5.length / 2) {
        this.teams[0].push(v5[v6]);
      } else {
        this.teams[1].push(v5[v6]);
      }
    }
    this.scores = [0, 0];
    var v7 = this.depthMan.empty(Data.DP_INTERF);
    var v8 = HfStd.createTextField(v7, 1);
    v8._width = 300;
    v8._height = 50;
    v8._x = Data.GAME_WIDTH * 0.5 - v8._width * 0.5;
    v8._y = Data.GAME_HEIGHT - 20;
    v8.textColor = 16777215;
    v8.html = true;
    v8.htmlText = "<P ALIGN=\"CENTER\"><FONT COLOR=\"#FFFFFF\">" + Lang.get(33) + "</FONT></P>";
    v8.wordWrap = false;
    v8.selectable = false;
  }

  override public function initInterface(): Void {
    (this.gi: hf.gui.SoccerInterface).destroy();
    this.gi = new hf.gui.SoccerInterface(this);
  }

  override public function initPlayer(p: Player): Void {
    super.initPlayer(p);
    p.baseColor = Data.BASE_COLORS[this.getTeam(p) + 1];
    p.skin = p.pid + 3;
    if (p.skin == 4) {
      p.defaultHead = Data.HEAD_SANDY;
      p.head = p.defaultHead;
    }
    p.baseWalkAnim = Data.ANIM_PLAYER_SOCCER;
    p.ctrl.fl_upKick = true;
    p.ctrl.fl_powerControl = GameManager.CONFIG.hasOption(Data.OPT_KICK_CONTROL);
    p.lives = 0;
    p.dx = 0;
    p.dy = 0;
    p.unshield();
    if (p.fl_knock) {
      p.onWakeUp();
    }
    if (this.getTeam(p) == 0) {
      p.moveTo(Entity.x_ctr(this.world.current.__dollar__playerX + this.getTeamPos(p)), Entity.y_ctr(this.world.current.__dollar__playerY));
    } else {
      p.moveTo(Entity.x_ctr(Data.LEVEL_WIDTH - this.world.current.__dollar__playerX - 1 - this.getTeamPos(p)), Entity.y_ctr(this.world.current.__dollar__playerY));
    }
    if (GameManager.CONFIG.hasOption(Data.OPT_SOCCER_BOMBS)) {
      p.changeWeapon(Data.WEAPON_B_REPEL);
    } else {
      p.changeWeapon(Data.WEAPON_NONE);
    }
  }

  override public function initWorld(): Void {
    this.addWorld("xml_soccer");
  }

  override public function lock(): Void {
    super.lock();
    this.chrono.stop();
  }

  override public function main(): Void {
    var v3: Int;
    this.chrono.update();
    if (!this.fl_match) {
      this.chrono.reset();
    }
    if (this.manager.isDev() && Key.isDown(Key.SHIFT)) {
      this.chrono.timeShift(-1);
    }
    if (this.chrono.fl_stop) {
      v3 = hf.mode.Soccer.MATCH_DURATION - (this.chrono.suspendTimer - this.chrono.gameTimer);
    } else {
      v3 = hf.mode.Soccer.MATCH_DURATION - this.chrono.get();
    }
    v3 = Std.int(Math.max(0, v3));
    var gi: hf.gui.SoccerInterface = this.gi;
    gi.setTime(this.chrono.formatTimeShort(v3));
    if (v3 < hf.mode.Soccer.WARNING_TIME) {
      this.blinkTimer -= Timer.tmod;
      if (this.blinkTimer <= 0) {
        var v4 = gi.time;
        if (v4.textColor == hf.mode.Soccer.BLINK_COLOR || v3 == 0) {
          v4.textColor = 16777215;
          FxManager.addGlow(v4, hf.gui.SoccerInterface.GLOW_COLOR, 2);
        } else {
          v4.textColor = hf.mode.Soccer.BLINK_COLOR;
          FxManager.addGlow(v4, hf.mode.Soccer.BLINK_GLOWCOLOR, 2);
        }
        this.blinkTimer = 15 * (v3 / hf.mode.Soccer.WARNING_TIME);
      }
    }
    if (!this.fl_lock) {
      if (this.offPlayTimer > 0) {
        this.offPlayTimer -= Timer.tmod;
      }
      if (this.ball._name == null && this.offPlayTimer <= 0) {
        this.fl_match = true;
        this.fxMan.attachAlert(Lang.get(24));
        this.insertBall();
        this.chrono.start();
      }
      if (this.ball._name != null) {
        var v5 = this.getPlayerList();
        for (v6 in 0...v5.length) {
          var v7 = v5[v6];
          var v8 = v7.distance(this.ball.x, this.ball.y);
          if (v8 > hf.mode.Soccer.BOOST_DISTANCE) {
            var v9 = Math.min(hf.mode.Soccer.MAX_BOOST, v8 / hf.mode.Soccer.BOOST_DISTANCE);
            v7.speedFactor = v9;
          } else {
            v7.speedFactor = 1;
          }
        }
      }
    }
    super.main();
    this.fxMan.detachExit();
    if (!this.fl_lock) {
      if (this.chrono.get() >= hf.mode.Soccer.MATCH_DURATION && !this.fl_end) {
        this.endMatch();
        this.finalTimer = Data.SECOND * 3;
      }
      if (this.finalTimer > 0) {
        this.finalTimer -= Timer.tmod;
        if (this.finalTimer <= 0) {
          this.showResults();
          this.endModeTimer = hf.mode.Soccer.END_TIMER;
        }
      }
      if (this.endModeTimer > 0) {
        var v10 = this.getPlayerList();
        for (v11 in 0...v10.length) {
          var v12 = v10[v11];
          if (this.getTeam(v12) == this.winners) {
            if (v12.dx != 0) {
              this.fxMan.inGameParticlesDir(Data.PARTICLE_SPARK, v12.x, v12.y - HfStd.random(40), HfStd.random(3), -v12.dir);
            } else {
              this.fxMan.inGameParticles(Data.PARTICLE_SPARK, v12.x, v12.y - HfStd.random(40), HfStd.random(2));
            }
          }
        }
      }
    }
  }

  override public function onGameOver(): Void {
    this.endMode();
  }

  override public function onHurryUp(): Null<MovieClip> {
    return null;
  }

  override public function onLevelReady(): Void {
    super.onLevelReady();
    this.fl_match = false;
    this.offPlayTimer = hf.mode.Soccer.START_OFFTIME;
    this.onPause();
    this.pauseMC.onRelease = HfStd.callback(this, "onUnpause");
    this.pauseMC.click.text = Lang.get(6);
    this.fxMan.levelName.removeMovieClip();
  }

  override public function onPause(): Void {
    super.onPause();
    this.pauseMC.gotoAndStop("2");
    this.pauseMC.move.text = Lang.get(29);
    this.pauseMC.attack.text = Lang.get(30);
    this.pauseMC.up.text = Lang.get(31);
    this.pauseMC.tip.text = Lang.get(32);
    this.pauseMC.click.text = "";
  }

  override public function onUnpause(): Void {
    super.onUnpause();
    if (this.fl_help) {
      this.fxMan.attachAlert(Lang.get(21));
      this.fl_help = false;
    }
  }

  override public function unlock(): Void {
    super.unlock();
    this.chrono.start();
  }
}
