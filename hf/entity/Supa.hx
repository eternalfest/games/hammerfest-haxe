package hf.entity;

import hf.entity.Mover;
import hf.mode.GameMode;

class Supa extends Mover {
  public var speed: Float;
  public var radius: Float;

  public function new(): Void {
    super();
    this.fl_hitGround = false;
    this.fl_hitWall = false;
    this.fl_gravity = false;
    this.fl_friction = false;
    this.fl_hitBorder = false;
    this.fl_alphaBlink = false;
    this.blinkColorAlpha = 80;
    this.minAlpha = 0;
    this.speed = 0;
    this.radius = 0;
  }

  public function initSupa(g: GameMode, x: Float, y: Float): Void {
    this.init(g);
    this.moveTo(x, y);
    this.endUpdate();
  }

  override public function infix(): Void {
  }

  override public function init(g: GameMode): Void {
    super.init(g);
    this.register(Data.SUPA);
    this.disableAnimator();
  }

  override public function tAdd(cx: Int, cy: Int): Void {
  }

  override public function tRem(cx: Int, cy: Int): Void {
  }
}
