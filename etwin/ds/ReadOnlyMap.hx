package etwin.ds;

typedef ReadOnlyMap<K, V> = {
  function exists(k: K): Bool;

  function get(k: K): Null<V>;

  function keys(): Iterator<K>;

  function iterator(): Iterator<V>;
}
