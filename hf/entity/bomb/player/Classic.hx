package hf.entity.bomb.player;

import hf.entity.bomb.PlayerBomb;
import hf.mode.GameMode;

class Classic extends PlayerBomb {
  public function new(): Void {
    super();
    this.duration = 45;
    this.power = 30;
  }

  public static function attach(g: GameMode, x: Float, y: Float): Classic {
    var v5 = "hammer_bomb_classic";
    var v6: Classic = cast g.depthMan.attach(v5, Data.DP_BOMBS);
    v6.initBomb(g, x, y);
    return v6;
  }

  override public function duplicate(): Classic {
    return hf.entity.bomb.player.Classic.attach(this.game, this.x, this.y);
  }

  override public function onExplode(): Void {
    super.onExplode();
    if (GameManager.CONFIG.fl_shaky) {
      this.game.shake(Data.SECOND * 0.35, 1.5);
    }
    var v3 = this.bombGetClose(Data.BAD);
    for (v4 in 0...v3.length) {
      var v5 = v3[v4];
      v5.setCombo(this.uniqId);
      v5.freeze(Data.FREEZE_DURATION);
      this.shockWave(v5, this.radius, this.power);
    }
    this.game.fxMan.inGameParticles(Data.PARTICLE_ICE, this.x, this.y, HfStd.random(2) + 2);
    var v3 = this.bombGetClose(Data.BAD_BOMB);
    for (v6 in 0...v3.length) {
      var v7 = v3[v6];
      if (!v7.fl_explode) {
        var v8 = v7.getFrozen(this.uniqId);
        if (v8 != null) {
          this.shockWave(v8, this.radius, this.power);
          v7.destroy();
        }
      }
    }
  }
}
