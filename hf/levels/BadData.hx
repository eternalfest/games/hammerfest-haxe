package hf.levels;

class BadData {
  public var __dollar__id: Int;
  public var __dollar__x: Float;
  public var __dollar__y: Float;

  public function new(x: Float, y: Float, id: Int): Void {
    this.__dollar__id = id;
    this.__dollar__x = x;
    this.__dollar__y = y;
  }
}
