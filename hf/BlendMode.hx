package hf;

class BlendMode {
  public static var NORMAL: Int = 1;
  public static var LAYER: Int = 2;
  public static var MULTIPLY: Int = 3;
  public static var SCREEN: Int = 4;
  public static var LIGHTEN: Int = 5;
  public static var DARKEN: Int = 6;
  public static var DIFFERENCE: Int = 7;
  public static var ADD: Int = 8;
  public static var SUBSTRACT: Int = 9;
  public static var INVERT: Int = 10;
  public static var ALPHA: Int = 11;
  public static var ERASE: Int = 12;
  public static var OVERLAY: Int = 13;
  public static var HARDLIGHT: Int = 14;
}
