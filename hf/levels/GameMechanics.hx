package hf.levels;

import hf.Entity;
import hf.entity.Physics;
import hf.GameManager;
import hf.levels.Data in LevelData;
import hf.levels.ScriptEngine;
import hf.levels.TeleporterData;
import hf.levels.ViewManager;
import hf.mode.GameMode;
import hf.Case;
import hf.Pos;
import hf.Data in Data;

typedef NextTeleporter = {
  var fl_rand: Bool;
  var td: TeleporterData;
}

class GameMechanics extends ViewManager {
  public var fl_parsing: Bool;
  public var fl_lock: Bool;
  public var fl_visited: Array<Bool>;
  public var fl_mainWorld: Bool;
  /**
   * triggers[x][y]
   */
  public var triggers: Array<Array<Array<Entity>>>;
  public var scriptEngine: Null<ScriptEngine>;
  public var game: Null<GameMode>;
  public var flcurrentIA: Null<Bool>;
  public var _iteration: Case;
  public var flagMap: Array<Array<Int>>;
  public var fallMap: Array<Array<Int>>;

  public function new(m: GameManager, s: String): Void {
    super(m, s);
    this.fl_parsing = false;
    this.fl_lock = true;
    this.fl_visited = new Array();
    this.fl_mainWorld = true;
    this.resetIA();
    this.triggers = new Array();
    for (v5 in 0...Data.LEVEL_WIDTH) {
      this.triggers[v5] = new Array();
      for (v6 in 0...Data.LEVEL_HEIGHT) {
        this.triggers[v5][v6] = new Array();
      }
    }
  }

  public function setGame(g: GameMode): Void {
    this.game = g;
  }

  public function lock(): Void {
    this.fl_lock = true;
  }

  public function unlock(): Void {
    this.fl_lock = false;
  }

  public function setVisited(): Void {
    this.fl_visited[this.currentId] = true;
  }

  public function isVisited(): Bool {
    return this.fl_visited[this.currentId] == true;
  }

  public function resetIA(): Void {
    this.flcurrentIA = false;
    this._iteration = {cx: 0, cy: 0};
    this.flagMap = new Array();
    this.fallMap = new Array();
    for (v2 in 0...Data.LEVEL_WIDTH) {
      this.flagMap[v2] = new Array();
      this.fallMap[v2] = new Array();
      for (v3 in 0...Data.LEVEL_HEIGHT) {
        this.flagMap[v2][v3] = 0;
        this.fallMap[v2][v3] = -1;
      }
    }
  }

  public function checkFlag(pt: Pos<Int>, flag: Int): Bool {
    var v4 = pt.x;
    var v5 = pt.y;
    if (v4 >= 0 && v4 < Data.LEVEL_WIDTH && v5 >= 0 && v5 < Data.LEVEL_HEIGHT) {
      return (this.flagMap[v4][v5] & flag) > 0;
    }
    return false;
  }

  public function forceFlag(pt: Pos<Int>, flag: Int, value: Bool): Void {
    if (value) {
      this.flagMap[pt.x][pt.y] |= flag;
    } else {
      this.flagMap[pt.x][pt.y] &= flag ^ -1;
    }
  }

  public function parseCurrentIA(it: Case): Void {
    var v3 = 0;
    var v4 = Data.LEVEL_WIDTH * Data.LEVEL_HEIGHT;
    var v5 = it.cx;
    var v6 = it.cy;
    while (true) {
      if (!(v3 < Data.MAX_ITERATION && v6 < Data.LEVEL_HEIGHT)) break;
      var v7 = 0;
      if (this.getCase({x: v5, y: v6 + 1}) == Data.GROUND) {
        v7 |= Data.IA_TILE_TOP;
        if (this.getCase({x: v5, y: v6 - Data.IA_VJUMP}) == Data.GROUND && this.getCase({x: v5, y: v6 - Data.IA_VJUMP - 1}) <= 0) {
          v7 |= Data.IA_JUMP_UP;
        }
      }
      if (this.getCase({x: v5, y: v6}) == Data.GROUND && this.getCase({x: v5, y: v6 - 1}) <= 0) {
        v7 |= Data.IA_TILE;
      }
      var v8 = this._checkSecureFall(v5, v6);
      this.fallMap[v5][v6] = v8;
      if (v8 >= 0) {
        v7 |= Data.IA_ALLOW_FALL;
      }
      if (this.getCase({x: v5, y: v6}) == 0 && this.getCase({x: v5, y: v6 + 1}) == Data.GROUND) {
        v8 = this._checkSecureFall(v5, v6 + 2);
        if (v8 >= 0) {
          v7 |= Data.IA_JUMP_DOWN;
        }
      }
      if (this.getCase({x: v5, y: v6 + 1}) == Data.GROUND && (this.getCase({x: v5 - 1, y: v6 + 1}) <= 0 || this.getCase({x: v5 + 1, y: v6 + 1}) <= 0)) {
        v7 |= Data.IA_BORDER;
      }
      if (this.getCase({x: v5, y: v6 + 1}) <= 0 && (this.getCase({x: v5 - 1, y: v6 + 1}) == Data.GROUND || this.getCase({x: v5 + 1, y: v6 + 1}) == Data.GROUND)) {
        v7 |= Data.IA_FALL_SPOT;
      }
      if ((v7 & Data.IA_BORDER) > 0) {
        if (this.getCase({x: v5 - 1, y: v6 + 1}) != Data.GROUND && this.getCase({x: v5 + 1, y: v6 + 1}) != Data.GROUND) {
          v7 |= Data.IA_SMALL_SPOT;
        }
      }
      if ((v7 & Data.IA_TILE_TOP) > 0) {
        var v9 = 1;
        var v10 = 1;
        while (v10 <= 5) {
          if (this.getCase({x: v5, y: v6 - v10}) <= 0) {
            ++v9;
          } else {
            v10 = 999;
          }
          ++v10;
        }
        if (v9 > 0) {
          if (this.getCase({x: v5 - 1, y: v6}) > 0) {
            var v11 = this.getWallHeight(v5 - 1, v6, Data.IA_CLIMB);
            if (v11 != null && v11 < v9 && v6 - v11 >= 0) {
              v7 |= Data.IA_CLIMB_LEFT;
            }
          }
          if (this.getCase({x: v5 + 1, y: v6}) > 0) {
            var v12 = this.getWallHeight(v5 + 1, v6, Data.IA_CLIMB);
            if (v12 != null && v12 < v9 && v6 - v12 >= 0) {
              v7 |= Data.IA_CLIMB_RIGHT;
            }
          }
        }
      }
      if ((v7 & Data.IA_FALL_SPOT) > 0) {
        var v13 = 1;
        var v14 = 1;
        while (v14 <= 5) {
          if (this.getCase({x: v5, y: v6 - v14}) <= 0) {
            ++v13;
          } else {
            v13 += 2;
            v14 = 999;
          }
          ++v14;
        }
        if (v13 > 0) {
          if (this.getCase({x: v5 + 1, y: v6 + 1}) == Data.GROUND) {
            var v15 = this.getStepHeight(v5, v6, Data.IA_CLIMB);
            if (v15 != null && v15 < v13) {
              if (this.checkFlag({x: v5, y: v6 - v15}, Data.IA_BORDER) && this.checkFlag({x: v5 + 1, y: v6 - v15}, Data.IA_FALL_SPOT)) {
                v7 |= Data.IA_CLIMB_LEFT;
              }
            }
          }
          if (this.getCase({x: v5 - 1, y: v6 + 1}) == Data.GROUND) {
            var v16 = this.getStepHeight(v5, v6, Data.IA_CLIMB);
            if (v16 != null && v16 < v13) {
              if (this.checkFlag({x: v5, y: v6 - v16}, Data.IA_BORDER) && this.checkFlag({x: v5 - 1, y: v6 - v16}, Data.IA_FALL_SPOT)) {
                v7 |= Data.IA_CLIMB_RIGHT;
              }
            }
          }
        }
      }
      if ((v7 & Data.IA_FALL_SPOT) > 0) {
        if (this.getCase({x: v5 + 1, y: v6 + 1}) == Data.GROUND && this.getCase({x: v5 - Data.IA_HJUMP, y: v6 + 1}) == Data.GROUND) {
          if (this.getCase({x: v5 - 1, y: v6}) <= 0) {
            v7 |= Data.IA_JUMP_LEFT;
          }
        }
        if (this.getCase({x: v5 - 1, y: v6 + 1}) == Data.GROUND && this.getCase({x: v5 + Data.IA_HJUMP, y: v6 + 1}) == Data.GROUND) {
          if (this.getCase({x: v5 + 1, y: v6}) <= 0) {
            v7 |= Data.IA_JUMP_RIGHT;
          }
        }
      }
      this.flagMap[v5][v6] = v7;
      ++v5;
      if (v5 >= Data.LEVEL_WIDTH) {
        v5 = 0;
        ++v6;
      }
      ++v3;
    }
    this.manager.progress(v6 / Data.LEVEL_HEIGHT);
    if (v3 != Data.MAX_ITERATION) {
      this.onParseIAComplete();
    }
    it.cx = v5;
    it.cy = v6;
  }

  public function _checkSecureFall(cx: Int, cy: Int): Int {
    if (this.current.__dollar__map[cx][cy] == Data.GROUND) {
      return -1;
    }
    if (this.current.__dollar__map[cx][cy] == Data.WALL) {
      return -1;
    }
    if ((this.flagMap[cx][cy - 1] & Data.IA_ALLOW_FALL) > 0) {
      return this.fallMap[cx][cy - 1] - 1;
    }
    var v4 = false;
    var v5 = cy + 1;
    var v6 = 0;
    while (true) {
      if (!(!v4 && v5 < Data.LEVEL_HEIGHT && cx >= 0 && cx < Data.LEVEL_WIDTH)) break;
      if (this.current.__dollar__map[cx][v5] == Data.GROUND) {
        v4 = true;
      } else {
        ++v5;
        ++v6;
      }
    }
    if (v4) {
      return v6;
    }
    return -1;
  }

  public function getWallHeight(cx: Int, cy: Int, max: Int): Null<Int> {
    var v5 = 0;
    while (true) {
      if (!(this.getCase({x: cx, y: cy - v5}) > 0 && v5 < max)) break;
      ++v5;
    }
    if (v5 >= max) {
      v5 = null;
    }
    return v5;
  }

  public function getStepHeight(cx: Int, cy: Int, max: Int): Null<Int> {
    var v5 = 0;
    while (true) {
      if (!(this.getCase({x: cx, y: cy - v5}) <= 0 && v5 < max)) break;
      ++v5;
    }
    ++v5;
    if (v5 >= max) {
      v5 = null;
    }
    return v5;
  }

  public function onParseIAComplete(): Void {
    this.fl_parsing = false;
    this.flcurrentIA = true;
    this.checkDataReady();
  }

  public function parseWalls(l: LevelData): Void {
    var v3 = l.__dollar__map;
    var v4 = 0;
    for (v5 in 0...Data.LEVEL_HEIGHT) {
      for (v6 in 0...Data.LEVEL_WIDTH) {
        if (v3[v6][v5] > 0) {
          if (v3[v6][v5 - 1] > 0) {
            v3[v6][v5] = Data.WALL;
            ++v4;
          }
        }
      }
    }
  }

  public function getTeleporter(e: Physics, cx: Int, cy: Int): Null<TeleporterData> {
    var v5 = null;
    if (this.getCase({x: cx, y: cy}) != Data.FIELD_TELEPORT) {
      return null;
    }
    var v6 = false;
    for (v7 in 0...this.teleporterList.length) {
      if (v6) break;
      var v8 = this.teleporterList[v7];
      if (v8.dir == Data.HORIZONTAL && cx >= v8.cx && cx < v8.cx + v8.length && cy == v8.cy) {
        v5 = v8;
        v6 = true;
      }
      if (!v6) {
        if (v8.dir == Data.VERTICAL && cy >= v8.cy && cy < v8.cy + v8.length && cx == v8.cx) {
          v5 = v8;
          v6 = true;
        }
      }
    }
    if (v5 == e.lastTeleporter) {
      return null;
    }
    if (v5 == null) {
      GameManager.fatal("teleporter not found in level " + this.currentId);
    }
    return v5;
  }

  public function getNextTeleporter(start: TeleporterData): NextTeleporter {
    var v3 = null;
    var v4 = false;
    var v5 = false;
    for (v6 in 0...this.teleporterList.length) {
      if (v4) break;
      var v7 = this.teleporterList[v6];
      if (v7.cx != start.cx || v7.cy != start.cy) {
        if (start.dir == Data.HORIZONTAL) {
          if (start.dir == v7.dir && start.cx == v7.cx && start.length == v7.length) {
            v3 = v7;
            v4 = true;
          }
        }
        if (!v4 && start.dir == Data.VERTICAL) {
          if (start.dir == v7.dir && start.cy == v7.cy && start.length == v7.length) {
            v3 = v7;
            v4 = true;
          }
        }
      }
    }
    if (v3 == null) {
      v5 = true;
      if (this.teleporterList.length > 1) {
        var v8 = new Array();
        for (v9 in 0...this.teleporterList.length) {
          var v10 = this.teleporterList[v9];
          if (v10.cx != start.cx || v10.cy != start.cy) {
            if (v10.dir == start.dir && v10.length == start.length) {
              v8.push(this.teleporterList[v9]);
            }
          }
        }
        if (v8.length > 0) {
          v3 = v8[HfStd.random(v8.length)];
          if (v8.length == 1) {
            v5 = false;
          }
        }
      }
    }
    if (v3 == null) {
      v5 = true;
      if (this.teleporterList.length > 1) {
        var v11 = new Array();
        for (v12 in 0...this.teleporterList.length) {
          if (this.teleporterList[v12].cx != start.cx || this.teleporterList[v12].cy != start.cy) {
            v11.push(this.teleporterList[v12]);
          }
        }
        v3 = v11[HfStd.random(v11.length)];
      }
    }
    if (v3 == null) {
      GameManager.fatal("target teleporter not found in level " + this.currentId);
    }
    return {fl_rand: v5, td: v3};
  }

  override public function createView(id: Int): View {
    this.scriptEngine.onLevelAttach();
    return super.createView(id);
  }

  override public function destroy(): Void {
    super.destroy();
    this.scriptEngine.destroy();
  }

  override public function forceCase(cx: Int, cy: Int, t: Int): Void {
    super.forceCase(cx, cy, t);
    if (this.inBound(cx, cy)) {
      if (t == Data.GROUND) {
        this.forceFlag({x: cx, y: cy}, Data.IA_TILE, true);
      } else {
        this.forceFlag({x: cx, y: cy}, Data.IA_TILE, false);
      }
    }
  }

  override public function goto(id: Int): Void {
    this.setVisited();
    this.resetIA();
    super.goto(id);
  }

  override public function isDataReady(): Bool {
    return super.isDataReady() && this.flcurrentIA;
  }

  override public function onDataReady(): Void {
    super.onDataReady();
    this.scriptEngine.compile();
  }

  override public function onEndOfSet(): Void {
    super.onEndOfSet();
    this.game.onEndOfSet();
  }

  override public function onFadeDone(): Void {
    super.onFadeDone();
    this.game.onRestore();
    this.onViewReady();
  }

  override public function onHScrollDone(): Void {
    super.onHScrollDone();
    this.game.onRestore();
    this.onViewReady();
  }

  override public function onReadComplete(): Void {
    super.onReadComplete();
    this.scriptEngine = new hf.levels.ScriptEngine(this.game, this.current);
    this.fl_parsing = true;
  }

  override public function onRestoreReady(): Void {
    super.onRestoreReady();
    this.scrollDir = !this.game.fl_rightPortal ? -1 : 1;
  }

  override public function onViewReady(): Void {
    super.onViewReady();
    this.game.onLevelReady();
  }

  override public function restore(lid: Int): Void {
    super.restore(lid);
  }

  override public function suspend(): Void {
    super.suspend();
    this.lock();
    var v3 = Data.cleanString(this.scriptEngine.script.toString());
    if (v3 != null) {
      this.current.__dollar__script = v3;
    }
    this.setVisited();
  }

  override public function unserialize(id: Int): LevelData {
    var v4 = super.unserialize(id);
    this.parseWalls(v4);
    return v4;
  }

  override public function update(): Void {
    super.update();
    if (this.fl_parsing) {
      this.parseCurrentIA(this._iteration);
    }
    if (!this.fl_lock) {
      this.scriptEngine.update();
    }
    for (v3 in 0...this.portalList.length) {
      var v4 = this.portalList[v3];
      v4.mc._y = v4.y + 3 * Math.sin(v4.cpt);
      v4.cpt += Timer.tmod * 0.1;
      if (HfStd.random(5) == 0) {
        var v5 = this.game.fxMan.attachFx(v4.x + Data.CASE_WIDTH * 0.5 + HfStd.random(15) * (HfStd.random(2) * 2 - 1), v4.y + Data.CASE_WIDTH * 0.5 + HfStd.random(15) * (HfStd.random(2) * 2 - 1), "hammer_fx_star");
        v5.mc._xscale = HfStd.random(70) + 30;
        v5.mc._yscale = v5.mc._xscale;
      }
    }
  }
}
