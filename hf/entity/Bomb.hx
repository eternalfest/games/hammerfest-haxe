package hf.entity;

import hf.entity.Mover;
import hf.entity.Player;
import hf.Entity;
import hf.mode.GameMode;

class Bomb extends Mover {
  public var explodeSound: String;
  public var radius: Float;
  public var power: Float;
  public var duration: Float;
  public var fl_explode: Bool;
  public var fl_airKick: Bool;
  public var fl_bumped: Bool;

  public function new(): Void {
    super();
    this.explodeSound = "sound_bomb";
    this.radius = Data.CASE_WIDTH * 3;
    this.power = 0;
    this.duration = 0;
    this.fl_slide = false;
    this.fl_bounce = false;
    this.fl_teleport = true;
    this.fl_wind = true;
    this.fl_blink = false;
    this.fl_explode = false;
    this.fl_airKick = false;
    this.fl_portal = true;
    this.fl_bump = true;
    this.fl_bumped = false;
    this.fl_strictGravity = false;
    this.slideFriction = 0.98;
  }

  public function initBomb(g: GameMode, x: Float, y: Float): Void {
    this.init(g);
    this.register(Data.BOMB);
    this.moveTo(x, y);
    this.setLifeTimer(this.duration);
    this.updateCoords();
    this.playAnim(Data.ANIM_BOMB_DROP);
  }

  public function bombGetClose<E: Entity>(type: EntityType<E>): Array<E> {
    return this.game.getClose(type, this.x, this.y, this.radius, this.fl_stable);
  }

  public function onExplode(): Void {
    this.playAnim(Data.ANIM_BOMB_EXPLODE);
    if (this.explodeSound != null) {
      this.game.soundMan.playSound(this.explodeSound, Data.CHAN_BOMB);
    }
    this.rotation = 0;
    this.fl_physics = false;
    this.fl_explode = true;
  }

  public function onKick(p: Player): Void {
  }

  public function duplicate(): Bomb {
    return null;
  }

  override public function endUpdate(): Void {
    if (!this.fl_stable) {
      var v3 = 30;
      if (this.dx > 0) {
        this.rotation += 0.02 * (v3 - this.rotation);
      } else {
        this.rotation -= 0.02 * (v3 - this.rotation);
      }
      this.rotation = Math.max(-v3, Math.min(v3, this.rotation));
    }
    super.endUpdate();
  }

  override public function onBump(): Void {
    this.fl_bumped = true;
  }

  override public function onDeathLine(): Void {
    super.onDeathLine();
    this.destroy();
  }

  override public function onEndAnim(id: Int): Void {
    super.onEndAnim(id);
    if (id == Data.ANIM_BOMB_DROP.id) {
      this.playAnim(Data.ANIM_BOMB_LOOP);
    }
    if (id == Data.ANIM_BOMB_EXPLODE.id) {
      this.destroy();
    }
  }

  override public function onHitWall(): Void {
    if (this.fl_bumped) {
      this.dx = -this.dx * 0.7;
    } else {
      super.onHitWall();
    }
  }

  override public function onLifeTimer(): Void {
    this.stopBlink();
    this.onExplode();
  }

  override public function onPortal(pid: Int): Void {
    super.onPortal(pid);
    this.game.fxMan.attachFx(this.x, this.y - Data.CASE_HEIGHT * 0.5, "hammer_fx_shine");
    this.destroy();
  }

  override public function onPortalRefusal(): Void {
    super.onPortalRefusal();
    this.dx = -this.dx * 3;
    this.dy = -5;
    this.game.fxMan.inGameParticles(Data.PARTICLE_PORTAL, this.x, this.y, 5);
  }

  override public function needsPatch(): Bool {
    return true;
  }
}
