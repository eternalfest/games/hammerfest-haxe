package hf.entity;

import hf.entity.Physics;
import hf.entity.Player;
import hf.mode.GameMode;

class Item extends Physics {
  public var id: Int;
  public var subId: Int;

  public function new(): Void {
    super();
    this.disableAnimator();
    this.fl_alphaBlink = true;
    this.fl_largeTrigger = true;
    this.fl_strictGravity = false;
    this.minAlpha = 0;
  }

  public function initItem(g: GameMode, x: Float, y: Float, id: Int, subId: Null<Int>): Void {
    if (HfStd.isNaN(id)) {
      id = null;
    }
    if (HfStd.isNaN(subId)) {
      subId = null;
    }
    if (id == null) {
      GameManager.fatal("null item ID !");
    }
    this.init(g);
    this.moveTo(x, y);
    this.id = id;
    this.subId = subId;
    if (id >= 1000) {
      this.gotoAndStop("" + (id - 1000 + 1));
    } else {
      this.gotoAndStop("" + (id + 1));
    }
    this.sub.gotoAndStop("" + (subId + 1));
    this.game.fxMan.attachFx(x, y - Data.CASE_HEIGHT, "hammer_fx_shine");
    this.endUpdate();
  }

  public function execute(p: Player): Void {
    this.destroy();
  }

  override public function init(g: GameMode): Void {
    super.init(g);
    this.setLifeTimer(Data.ITEM_LIFE_TIME);
    this.register(Data.ITEM);
  }

  override public function onDeathLine(): Void {
    super.onDeathLine();
    this.moveTo(this.x, -30);
    this.dy = 0;
  }

  override public function onLifeTimer(): Void {
    super.onLifeTimer();
    this.game.fxMan.attachFx(this.x, this.y - Data.CASE_HEIGHT / 2, "hammer_fx_pop");
    this.game.soundMan.playSound("sound_pop", Data.CHAN_ITEM);
  }

  override public function update(): Void {
    super.update();
  }
}
