package hf.entity.shoot;

import hf.entity.Shoot;
import hf.Entity;
import hf.mode.GameMode;

class FireBall extends Shoot {
  public function new(): Void {
    super();
    this.fl_largeTrigger = true;
    this.shootSpeed = 7;
  }

  public static function attach(g: GameMode, x: Float, y: Float): FireBall {
    var v5 = "hammer_shoot_fireball";
    var v6: FireBall = cast g.depthMan.attach(v5, Data.DP_SHOTS);
    v6.initShoot(g, x, y - 10);
    return v6;
  }

  override public function destroy(): Void {
    this.game.fxMan.attachFx(this.x, this.y - Data.CASE_HEIGHT / 2, "hammer_fx_pop");
    super.destroy();
  }

  override public function hit(e: Entity): Void {
    var v3 = Data.PLAYER.downcast(e);
    if (v3 != null) {
      var v4 = this.distance(v3.x, v3.y);
      if (v4 <= Data.CASE_WIDTH * 1.2) {
        this.game.fxMan.attachExplosion(this.x, this.y, 25);
        v3.killHit(this.dx);
      }
    }
  }

  override public function init(g: GameMode): Void {
    super.init(g);
    this.playAnim(Data.ANIM_SHOOT_LOOP);
  }
}
