package etwin.ds;

typedef ReadOnlySet<T> = {
  function exists(value: T): Bool;

  function iterator(): Iterator<T>;
}
