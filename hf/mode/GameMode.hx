package hf.mode;

import etwin.flash.Color;
import etwin.flash.MovieClip;
import etwin.flash.DynamicMovieClip;
import hf.Chrono;
import hf.Data.ItemInfo;
import hf.entity.Bad;
import hf.entity.Player;
import hf.Entity;
import hf.fx.Darkness;
import hf.FxManager;
import hf.GameManager;
import hf.interf.ItemName;
import hf.levels.GameMechanics;
import hf.levels.PortalLink;
import hf.Mode;
import hf.RandomManager;
import hf.StatsManager;
import etwin.flash.Key;
using hf.compat.ColorTools;

typedef ExtraHole = {
  var x: Float;
  var y: Float;
  var d: Float;
  var mc: MovieClip;
}

typedef MapEvent = {
  var lid: Int;
  var eid: Int;
  var misc: String;
}

typedef PlayerSpawn = {
  var x: Float;
  var y: Float;
  var fl_unstable: Bool;
}

typedef PortalMc = {
  var x: Float;
  var y: Float;
  var cpt: Float;
  var mc: MovieClip;
}

typedef Unreg<E: Entity> = {
  var type: EntityType<E>;
  var ent: E;
}

class GameMode extends Mode {
  public static var WATER_COLOR: Int = 255;

  public var duration: Float;
  public var lagCpt: Float;
  public var fl_static: Bool;
  public var fl_bullet: Bool;
  public var fl_disguise: Bool;
  public var fl_map: Bool;
  public var fl_mirror: Bool;
  public var fl_nightmare: Bool;
  public var fl_ninja: Bool;
  public var fl_bombExpert: Bool;
  public var fl_bombControl: Bool;
  public var savedScores: Array<Int>;
  public var fxMan: FxManager;
  public var statsMan: StatsManager;
  public var randMan: RandomManager;
  public var speedFactor: Float;
  public var diffFactor: Float;
  public var comboList: Array<Int>;
  public var killList: Array<Entity>;
  // Actual type: Unreg<? extends Entity>
  // @:deprecated("TODO unsafe explanation")
  public var unregList: Array<Unreg<Entity>>;
  // Actual type: Array<Array<? extends Entity>>
  // @:deprecated("TODO unsafe explanation")
  public var lists: Array<Array<Entity>>;
  public var globalActives: Array<Bool>;
  public var endLevelStack: Array<Void -> Void>;
  public var portalMcList: Array<PortalMc>;
  public var extraHoles: Array<ExtraHole>;
  public var dfactor: Float;
  public var forcedDarkness: Null<Float>;
  public var targetDark: Null<Float>;
  public var darknessMC: Null<Darkness>;
  public var aquaTimer: Float;
  public var bulletTimer: Float;
  public var fl_flipX: Bool;
  public var fl_flipY: Bool;
  public var fl_clear: Bool;
  public var fl_gameOver: Bool;
  public var huTimer: Float;
  public var huState: Int;
  public var tipId: Int;
  public var specialPicks: Array<Int>;
  public var scorePicks: Array<Int>;
  public var currentDim: Int;
  public var dimensions: Array<GameMechanics>;
  public var latePlayers: Array<Player>;
  public var mapIcons: Array<MovieClip>;
  public var mapEvents: Array<MapEvent>;
  public var mapTravels: Array<MapEvent>;
  public var gameChrono: Chrono;
  public var endModeTimer: Float;
  public var worldKeys: Array<Bool>;
  public var gi: Dynamic;
  public var world: GameMechanics;
  public var shakeTimer: Float;
  public var shakeTotal: Float;
  public var shakePower: Float;
  public var fl_wind: Null<Bool>;
  public var windSpeed: Float;
  public var windTimer: Float;
  public var badCount: Int;
  public var keyLock: Int;
  public var fl_badsCallback: Null<Bool>;
  public var fakeLevelId: Int;
  public var gFriction: Float;
  public var sFriction: Float;
  public var dvars: Hash;
  public var color: Null<Color>;
  public var colorHex: Null<Int>;
  public var portalId: Int;
  public var nextLink: PortalLink;
  public var fl_rightPortal: Null<Bool>;
  public var popMC: DynamicMovieClip /* TODO */;
  public var pointerMC: MovieClip;
  public var radiusMC: MovieClip;
  public var itemNameMC: Null<ItemName>;
  public var friendsLimit: Null<Int>;
  public var fl_pause: Null<Bool>;
  public var pauseMC: Null<DynamicMovieClip /* TODO */>;
  public var mapMC: DynamicMovieClip /* TODO */;
  public var fl_ice: Null<Bool>;
  public var fl_aqua: Null<Bool>;

  /**
   * Will do nothing except in `hf.mode.Adventure`, but `hf.levels.ScriptEngine` uses it on
   * `hf.mode.GameMode`.
   *
   * TODO: Move to `hf.mode.Adventure`?
   */
  public var fl_warpStart: Null<Bool>;
  // Used by SpecialManager
  public var perfectItemCpt: Null<Int>;

  public function new(m: GameManager): Void {
    super(m);
    this.duration = 0;
    this.lagCpt = 0;
    this.fl_static = false;
    this.fl_bullet = true;
    this.fl_disguise = true;
    this.fl_map = false;
    this.fl_mirror = GameManager.CONFIG.hasOption(Data.OPT_MIRROR);
    this.fl_nightmare = GameManager.CONFIG.hasOption(Data.OPT_NIGHTMARE);
    this.fl_ninja = GameManager.CONFIG.hasOption(Data.OPT_NINJA);
    this.fl_bombExpert = GameManager.CONFIG.hasOption(Data.OPT_BOMB_EXPERT);
    this.fl_bombControl = false;
    this.savedScores = new Array();
    this.xOffset = 10;
    this.fxMan = new FxManager(this);
    this.statsMan = new StatsManager(this);
    this.randMan = new RandomManager();
    var v4 = HfStd.getTimer();
    this.randMan.register(Data.RAND_EXTENDS_ID, Data.RAND_EXTENDS);
    this.randMan.register(Data.RAND_ITEMS_ID, Data.getRandFromFamilies(Data.SPECIAL_ITEM_FAMILIES, GameManager.CONFIG.specialItemFamilies));
    this.randMan.register(Data.RAND_SCORES_ID, Data.getRandFromFamilies(Data.SCORE_ITEM_FAMILIES, GameManager.CONFIG.scoreItemFamilies));
    this.speedFactor = 1.0;
    this.diffFactor = 1.0;
    this.endModeTimer = 0;
    this.comboList = new Array();
    this.killList = new Array();
    this.unregList = new Array();
    this.lists = new Array();
    for (v5 in 0...200) {
      this.lists[v5] = new Array();
    }
    this.globalActives = new Array();
    this.endLevelStack = new Array();
    this.portalMcList = new Array();
    this.extraHoles = new Array();
    this.dfactor = 0;
    this.shake(null, null);
    this.wind(null, null);
    this.aquaTimer = 0;
    this.bulletTimer = 0;
    this.fl_flipX = false;
    this.fl_flipY = false;
    this.fl_clear = false;
    this.fl_gameOver = false;
    this.huTimer = 0;
    this.huState = 0;
    this.tipId = 0;
    this._name = "$abstractGameMode";
    this.specialPicks = new Array();
    this.scorePicks = new Array();
    this.currentDim = 0;
    this.dimensions = new Array();
    this.latePlayers = new Array();
    this.mapIcons = new Array();
    this.mapEvents = new Array();
    this.mapTravels = new Array();
    this.gameChrono = new Chrono();
    this.clearDynamicVars();
    this.worldKeys = new Array();
    for (v6 in 5000...5100) {
      if (GameManager.CONFIG.hasFamily(v6)) {
        this.giveKey(v6 - 5000);
      }
    }
  }

  public function initWorld(): Void {
  }

  public function initGame(): Void {
    this.initWorld();
  }

  public function initInterface(): Void {
    (this.gi: hf.gui.GameInterface).destroy();
    this.gi = new hf.gui.GameInterface(this);
  }

  public function initLevel(): Void {
    if (!this.world.isVisited()) {
      this.resetHurry();
    }
    this.badCount = 0;
    this.forcedDarkness = null;
    this.fl_clear = false;
    var v2 = this.getPlayerList();
    for (v3 in 0...v2.length) {
      v2[v3].onStartLevel();
      if (v2[v3].y < 0) {
        this.fxMan.attachEnter(v2[v3].x, 0);
      }
    }
  }

  public function initPlayer(p: Player): Void {
  }

  public function shake(duration: Null<Float>, power: Null<Float>): Void {
    if (duration == null) {
      this.shakeTimer = 0;
      this.shakeTimer = 0;
    } else {
      if (duration < this.shakeTimer || power < this.shakePower) {
        return;
      }
      this.shakeTotal = duration;
      this.shakeTimer = duration;
      this.shakePower = power;
    }
  }

  public function wind(speed: Null<Float>, duration: Null<Float>): Void {
    if (speed == null) {
      this.fl_wind = false;
    } else {
      this.fl_wind = true;
      this.windSpeed = speed;
      this.windTimer = duration;
    }
  }

  public function flipX(fl: Bool): Void {
    this.fl_flipX = fl;
    if (fl) {
      this.mc._xscale = -100;
      this.mc._x = Data.GAME_WIDTH + this.xOffset;
    } else {
      this.mc._xscale = 100;
      this.mc._x = this.xOffset;
    }
  }

  public function flipY(fl: Bool): Void {
    this.fl_flipY = fl;
    if (fl) {
      this.mc._yscale = -100;
      this.mc._y = Data.GAME_HEIGHT + this.yOffset + 20;
    } else {
      this.mc._yscale = 100;
      this.mc._y = this.yOffset;
    }
  }

  public function resetHurry(): Void {
    this.huTimer = 0;
    this.huState = 0;
    var v2 = 0;
    var v3 = this.getPlayerList();
    for (v4 in 0...v3.length) {
      v2 = v3[v4].lives <= v2 ? v2 : v3[v4].lives;
    }
    if (v2 < 4) {
      this.diffFactor = 1.0;
    } else {
      this.diffFactor = 1.0 + 0.05 * (v2 - 3);
    }
    if (this.fl_nightmare) {
      this.diffFactor *= 1.4;
    }
    this.destroyList(Data.HU_BAD);
    var v5 = this.getBadList();
    for (v6 in 0...v5.length) {
      v5[v6].calmDown();
    }
    var v7: Dynamic /* TODO */ = cast this.getOne(Data.BOSS);
    if (v7 != null) {
      v7.onPlayerDeath();
    }
  }

  public function checkLevelClear(): Bool {
    if (this.fl_clear) {
      return true;
    }
    if (this.countList(Data.BAD_CLEAR) > 0) {
      return false;
    }
    if (this.world.fl_mainWorld) {
      if (this.world.currentId == Data.BAT_LEVEL || this.world.currentId == Data.TUBERCULOZ_LEVEL) {
        return false;
      }
    }
    this.onLevelClear();
    return true;
  }

  public function printDebugInfos(): Void {
    Log.print("build:" + "01-11 11:48:51");
    Log.print("lang=" + Lang.lang + " debug=" + Lang.fl_debug);
    Log.print("debug=" + this.manager.fl_debug + " local=" + this.manager.fl_local);
    Log.print("cookie=" + this.manager.fl_cookie);
    Log.print("--- DATA ---");
    Log.print(GameManager.CONFIG.toString());
    Log.print("fl8 = " + this.manager.fl_flash8);
    Log.print("ent = " + this.countList(Data.ENTITY));
    Log.print("bad = " + this.countList(Data.BAD));
    var v2 = new Array();
    for (v3 in 0...this.worldKeys.length) {
      if (this.worldKeys[v3]) {
        v2.push(v3);
      }
    }
    Log.print("keys= " + v2.join(", "));
  }

  public function printBetaInfos(): Void {
    Log.print("appuyez sur 'C' pour\ncopier ce texte dans le\npresse-papier");
    Log.print("--- VERSION ---");
    Log.print("beta 6.1+");
    Log.print("build:" + "01-11 11:48:51");
    Log.print("f:" + GameManager.CONFIG.families.join("."));
    Log.print("--- DATA ---");
    Log.print("lvl= " + this.world.currentId);
    Log.print("bads= " + this.countList(Data.BAD));
    Log.print("bads_c= " + this.countList(Data.BAD_CLEAR));
    Log.print("clear= " + this.fl_clear);
    Log.print("cycle= " + Math.round(this.world.scriptEngine.cycle));
    Log.print("compile= " + this.world.scriptEngine.fl_compile);
    Log.print("--- SCRIPT ---");
    var v2 = this.world.scriptEngine.script.firstChild;
    while (v2 != null) {
      if (v2.nodeName != null) {
        Log.print(v2.nodeName);
      }
      v2 = v2.nextSibling;
    }
    Log.print("--- HIST ---");
    Log.print("\n" + this.world.scriptEngine.history.join("\n*"));
    Log.print("------");
  }

  public function addLevelItems(): Void {
  }

  public function startLevel(): Void {
    var v2 = 0;
    v2 = this.world.scriptEngine.insertBads();
    if (v2 == 0) {
      this.fxMan.attachExit();
      this.fl_clear = true;
    }
    this.updateDarkness();
    if (!this.world.isVisited()) {
      this.addLevelItems();
    }
    this.statsMan.reset();
    this.world.scriptEngine.onPlayerBirth();
    this.world.unlock();
    this.fl_badsCallback = false;
    if (this.isBossLevel(this.world.currentId)) {
      this.fxMan.attachWarning();
    }
  }

  public function nextLevel(): Void {
    ++this.fakeLevelId;
    this.goto(this.world.currentId + 1);
  }

  public function forcedGoto(id: Int): Void {
    this.fakeLevelId += id - this.world.currentId;
    this.world.fl_hideBorders = false;
    this.world.fl_hideTiles = false;
    this.goto(id);
    var v3 = this.getPlayerList();
    for (v4 in 0...v3.length) {
      v3[v4].moveTo(Entity.x_ctr(this.world.current.__dollar__playerX), Entity.y_ctr(this.world.current.__dollar__playerY - 1));
      v3[v4].shield(Data.SHIELD_DURATION * 1.3);
      v3[v4].hide();
    }
  }

  public function clearLevel(): Void {
    this.killPop();
    this.killPointer();
    this.killItemName();
    this.destroyList(Data.BAD);
    this.destroyList(Data.ITEM);
    this.destroyList(Data.BOMB);
    this.destroyList(Data.SUPA);
    this.destroyList(Data.SHOOT);
    this.destroyList(Data.FX);
    this.destroyList(Data.BOSS);
    this.clearDynamicVars();
  }

  public function goto(id: Int): Void {
    if (this.fl_lock || this.fl_gameOver) {
      return;
    }
    if (id >= this.world.levels.length) {
      this.world.onEndOfSet();
      return;
    }
    this.clearExtraHoles();
    this.world.lock();
    this.killPortals();
    this.clearLevel();
    this.lock();
    this.fl_clear = false;
    this.world.goto(id);
    var v3 = this.getList(Data.ENTITY);
    for (v5 in 0...v3.length) {
      v3[v5].world = this.world;
    }
    var v4 = this.getPlayerList();
    var v6: Player = null;
    for (v7 in 0...v4.length) {
      if (v6 == null || v4[v7].y > v6.y) {
        v6 = v4[v7];
      }
    }
    for (v8 in 0...v4.length) {
      v4[v8].moveTo(v6.x, -600);
      v4[v8].hide();
      v4[v8].onNextLevel();
    }
    v6.moveTo(v6.x, -200);
    this.fxMan.onNextLevel();
    this.soundMan.playSound("sound_level_clear", Data.CHAN_INTERF);
  }

  public function countCombo(id: Int): Int {
    if (this.comboList[id] == null) {
      this.comboList[id] = 0;
    }
    return ++this.comboList[id];
  }

  public function canAddItem(): Bool {
    return !this.fl_clear || this.world.current.__dollar__badList.length == 0;
  }

  public function bulletTime(d: Float): Void {
    if (!this.fl_bullet) {
      return;
    }
    this.bulletTimer = d;
  }

  public function updateGroundFrictions(): Void {
    this.gFriction = Math.pow(Data.FRICTION_GROUND, Timer.tmod);
    this.sFriction = Math.pow(Data.FRICTION_SLIDE, Timer.tmod);
  }

  public function pickUpSpecial(id: Int): Int {
    if (this.specialPicks[id] == null) {
      this.specialPicks[id] = 0;
    }
    return ++this.specialPicks[id];
  }

  public function pickUpScore(id: Int, subId: Int): Int {
    if (this.scorePicks[id] == null) {
      this.scorePicks[id] = 0;
    }
    return ++this.scorePicks[id];
  }

  public function getPicks(): String {
    var v2 = "";
    for (v3 in 0...this.specialPicks.length) {
      if (this.specialPicks[v3] != null) {
        v2 += v3 + "=" + this.specialPicks[v3] + "|";
      }
    }
    for (v4 in 0...this.scorePicks.length) {
      if (this.scorePicks[v4] != null) {
        v2 += v4 + 1000 + "=" + this.scorePicks[v4] + "|";
      }
    }
    if (v2.length > 0) {
      v2 = v2.substr(0, v2.length - 1);
    }
    return v2;
  }

  public function getPicks2(): Array<Int> {
    var v2 = new Array();
    for (v3 in 0...this.specialPicks.length) {
      if (this.specialPicks[v3] != null) {
        v2[v3] = this.specialPicks[v3];
      }
    }
    for (v4 in 0...this.scorePicks.length) {
      if (this.scorePicks[v4] != null) {
        v2[v4 + 1000] = this.scorePicks[v4];
      }
    }
    return v2;
  }

  public function isBossLevel(id: Int): Bool {
    return false;
  }

  public function flipCoordReal(x: Float): Float {
    if (this.fl_mirror) {
      return Data.GAME_WIDTH - x - 1;
    }
    return x;
  }

  public function flipCoordCase<T: Float>(cx: T): T {
    if (this.fl_mirror) {
      return Data.LEVEL_WIDTH - cx - 1;
    }
    return cx;
  }

  public function registerMapEvent(eid: Int, misc: Dynamic /*TODO*/): Void {
    var v4 = this.dimensions[0].currentId;
    if (!this.world.fl_mainWorld) {
      return;
    }
    for (v5 in 0...this.mapEvents.length) {
      var v6 = this.mapEvents[v5];
      if (v6.lid == v4 && v6.eid == Data.EVENT_EXIT_RIGHT && eid == Data.EVENT_BACK_RIGHT) {
        return;
      }
      if (v6.lid == v4 && v6.eid == Data.EVENT_EXIT_LEFT && eid == Data.EVENT_BACK_LEFT) {
        return;
      }
      if (v6.lid == v4 && v6.eid == Data.EVENT_EXIT_RIGHT && eid == Data.EVENT_EXIT_RIGHT) {
        return;
      }
      if (v6.lid == v4 && v6.eid == Data.EVENT_EXIT_LEFT && eid == Data.EVENT_EXIT_LEFT) {
        return;
      }
    }
    var v7 = {lid: v4, eid: eid, misc: misc};
    if (eid == Data.EVENT_EXIT_LEFT || eid == Data.EVENT_EXIT_RIGHT || eid == Data.EVENT_BACK_LEFT || eid == Data.EVENT_BACK_RIGHT || eid == Data.EVENT_TIME) {
      this.mapTravels.push(v7);
    } else {
      this.mapEvents.push(v7);
    }
  }

  public function getMapY(lid: Int): Float {
    return 84 + Math.min(350, lid * 3.5);
  }

  public function setDynamicVar(name: String, value: Dynamic /*TODO*/): Void {
    this.dvars.set(name.toLowerCase(), value);
  }

  public function getDynamicVar(name: String): Dynamic /*TODO*/ {
    return this.dvars.get((name.substring(1)).toLowerCase());
  }

  public function getDynamicInt(name: String): Int {
    return HfStd.parseInt(this.getDynamicVar(name), 10);
  }

  public function clearDynamicVars(): Void {
    this.dvars = new Hash();
  }

  public function saveScore(): Void {
  }

  public function registerScore(pid: Int, score: Int): Void {
    if (this.savedScores[pid] == null || this.savedScores[pid] <= 0) {
      this.savedScores[pid] = score;
    }
  }

  public function setColorHex(a: Float, col: Int): Void {
    var v4 = {r: col >> 16, g: col >> 8 & 255, b: col & 255};
    var v5 = a / 100;
    var v6: etwin.flash.ColorTransform = {ra: Std.int(100 - a), ga: Std.int(100 - a), ba: Std.int(100 - a), aa: 100, rb: Std.int(v5 * v4.r), gb: Std.int(v5 * v4.g), bb: Std.int(v5 * v4.b), ab: 0};
    this.color = new Color(this.root);
    this.color.setTransform(v6);
    this.colorHex = col;
  }

  public function resetCol(): Void {
    if (this.color != null) {
      this.colorHex = null;
      this.color.reset();
      this.color = null;
    }
  }

  public function giveKey(id: Int): Void {
    this.worldKeys[id] = true;
  }

  public function hasKey(id: Int): Bool {
    return this.worldKeys[id] == true || HfStd.isNaN(id);
  }

  public function switchDimensionById(id: Int, lid: Int, pid: Int): Void {
    if (!this.fl_clear) {
      return;
    }
    this.resetHurry();
    this.latePlayers = new Array();
    var v5 = this.getPlayerList();
    for (v6 in 0...v5.length) {
      var v7 = v5[v6];
      v7.specialMan.clearTemp();
      v7.specialMan.clearRec();
      v7._xscale = v7.scaleFactor * 100;
      v7._yscale = v7._xscale;
      v7.lockTimer = 0;
      v7.fl_lockControls = false;
      v7.fl_gravity = true;
      v7.fl_friction = true;
      if (!v7.fl_kill) {
        v7.fl_hitWall = true;
        v7.fl_hitGround = true;
      }
      v7.changeWeapon(Data.WEAPON_B_CLASSIC);
      if (this.world.getCase({x: v7.cx, y: v7.cy}) != Data.FIELD_PORTAL) {
        this.latePlayers.push(v7);
      }
      v7.hide();
    }
    this.holeUpdate();
    this.clearExtraHoles();
    this.clearLevel();
    this.killPortals();
    this.fxMan.clear();
    this.cleanKills();
    this.currentDim = id;
    var v8 = this.world.getSnapShot();
    this.world.suspend();
    this.world = this.dimensions[this.currentDim];
    this.world.darknessFactor = this.dfactor;
    this.lock();
    this.fl_clear = false;
    if (pid < 0) {
      this.world.fl_fadeNextTransition = true;
    }
    this.world.restoreFrom(v8, lid);
    this.updateEntitiesWorld();
    if (!this.world.fl_mainWorld) {
      this.fakeLevelId = 0;
    } else {
      this.fakeLevelId = null;
    }
    this.portalId = pid;
    this.nextLink = null;
  }

  public function addWorld(name: String): GameMechanics {
    var v3 = new hf.levels.GameMechanics(this.manager, name);
    v3.fl_mirror = this.fl_mirror;
    v3.setDepthMan(this.depthMan);
    v3.setGame(this);
    if (this.dimensions.length > 0) {
      v3.suspend();
      v3.fl_mainWorld = false;
      this.dimensions.push(v3);
      return v3;
    }
    this.world = v3;
    this.dimensions.push(v3);
    return v3;
  }

  public function getPortalEntrance(pid: Int): PlayerSpawn {
    var v3 = this.world.current.__dollar__playerX;
    var v4 = this.world.current.__dollar__playerY;
    var v5 = false;
    if (pid >= 0 && this.world.portalList[pid] != null) {
      v3 = this.world.portalList[pid].cx;
      v4 = this.world.portalList[pid].cy;
      if (this.world.getCase({x: v3, y: v4 + 1}) == Data.FIELD_PORTAL) {
        while (this.world.getCase({x: v3, y: v4 + 1}) == Data.FIELD_PORTAL) {
          ++v4;
        }
        if (this.world.getCase({x: v3 - 1, y: v4}) <= 0) {
          v3 -= 1;
        } else {
          if (this.world.getCase({x: v3 + 1, y: v4}) <= 0) {
            v3 += 1;
          }
        }
      } else {
        if (this.world.getCase({x: v3 + 1, y: v4}) == Data.FIELD_PORTAL) {
          v4 += 2;
        }
        v5 = true;
      }
      return {x: v3, y: v4, fl_unstable: v5};
    }
    v5 = true;
    return {x: v3, y: v4, fl_unstable: v5};
  }

  public function usePortal(pid: Int, e: Entity): Bool {
    if (this.nextLink != null) {
      return false;
    }
    var v4 = Data.getLink(this.currentDim, this.world.currentId, pid);
    if (v4 == null) {
      return false;
    }
    var v5 = Lang.getLevelName(v4.to_did, v4.to_lid);
    if (this.flipCoordReal(e.x) >= Data.GAME_WIDTH * 0.5) {
      this.fl_rightPortal = true;
      this.registerMapEvent(Data.EVENT_EXIT_RIGHT, this.world.currentId + 1 + ". " + v5);
    } else {
      this.fl_rightPortal = false;
      this.registerMapEvent(Data.EVENT_EXIT_LEFT, this.world.currentId + 1 + ". " + v5);
    }
    if (e == null) {
      var v6 = this.getPlayerList();
      for (v7 in 0...v6.length) {
        var v8 = v6[v7];
        v8.dx = (this.portalMcList[pid].x - v8.x) * 0.018;
        v8.dy = (this.portalMcList[pid].y - v8.y) * 0.018;
        v8.fl_hitWall = false;
        v8.fl_hitGround = false;
        v8.fl_gravity = false;
        v8.fl_friction = false;
        v8.specialMan.clearTemp();
        v8.unshield();
        v8.lockControls(Data.SECOND * 9999);
        v8.playAnim(Data.ANIM_PLAYER_DIE);
      }
      this.nextLink = v4;
      return true;
    }
    this.switchDimensionById(v4.to_did, v4.to_lid, v4.to_pid);
    return true;
  }

  public function openPortal(cx: Float, cy: Float, pid: Int): Bool {
    if (this.portalMcList[pid] != null) {
      return false;
    }
    this.world.scriptEngine.insertPortal(cx, cy, pid);
    var v5 = Entity.x_ctr(this.flipCoordCase(cx));
    var v6 = Entity.y_ctr(cy) - Data.CASE_HEIGHT * 0.5;
    var v7 = this.depthMan.attach("hammer_portal", Data.DP_SPRITE_BACK_LAYER);
    v7._x = v5;
    v7._y = v6;
    this.fxMan.attachExplosion(v5, v6, 40);
    this.fxMan.inGameParticles(Data.PARTICLE_PORTAL, v5, v6, 5);
    this.fxMan.attachShine(v5, v6);
    this.portalMcList[pid] = {x: v5, y: v6, mc: v7, cpt: 0};
    return true;
  }

  public function getListId<E: Entity>(type: EntityType<E>): Int {
    var v3 = 0;
    var v4 = 1 << v3;
    while (type.asInt() != v4) {
      ++v3;
      v4 = 1 << v3;
    }
    return v3;
  }

  // TODO: return ReadOnlyArray here?
  public function getList<E: Entity>(type: EntityType<E>): Array<E> {
    return cast this.lists[this.getListId(type)];
  }

  public function getListAt<E: Entity>(type: EntityType<E>, cx: Int, cy: Int): Array<E> {
    var v5 = this.getList(type);
    var v6 = new Array();
    for (v7 in 0...v5.length) {
      if (v5[v7].cx == cx && v5[v7].cy == cy) {
        v6.push(v5[v7]);
      }
    }
    return v6;
  }

  public function countList<E: Entity>(type: EntityType<E>): Int {
    return this.lists[this.getListId(type)].length;
  }

  // TODO: return ReadOnlyArray here?
  public function getBadList(): Array<Bad> {
    return this.getList(Data.BAD);
  }

  // TODO: return ReadOnlyArray here?
  public function getBadClearList(): Array<Bad> {
    return this.getList(Data.BAD_CLEAR);
  }

  // TODO: return ReadOnlyArray here?
  public function getPlayerList(): Array<Player> {
    return this.getList(Data.PLAYER);
  }

  public function getListCopy<E: Entity>(type: EntityType<E>): Array<E> {
    var v3 = this.getList(type);
    var v4 = new Array();
    for (v5 in 0...v3.length) {
      v4[v5] = v3[v5];
    }
    return v4;
  }

  public function getClose<E: Entity>(type: EntityType<E>, x: Float, y: Float, radius: Float, fl_onGround: Bool): Array<E> {
    var v7 = this.getList(type);
    var v8 = new Array();
    var v9 = Math.pow(radius, 2);
    for (v10 in 0...v7.length) {
      var v11 = v7[v10];
      var v12 = Math.pow(v11.x - x, 2) + Math.pow(v11.y - y, 2);
      if (v12 <= v9) {
        if (Math.sqrt(v12) <= radius) {
          if (!fl_onGround || fl_onGround && v11.y <= y + Data.CASE_HEIGHT) {
            v8.push(v11);
          }
        }
      }
    }
    return v8;
  }

  public function getOne<E: Entity>(type: EntityType<E>): Null<E> {
    var v3 = this.getList(type);
    return v3[HfStd.random(v3.length)];
  }

  public function getAnotherOne<E: Entity>(type: EntityType<E>, e: Entity): Null<E> {
    var v5;
    var v4 = this.getList(type);
    if (v4.length <= 1) {
      return null;
    }
    do {
      v5 = HfStd.random(v4.length);
    } while (v4[v5] == e);
    return v4[v5];
  }

  public function addToList<E: Entity>(type: EntityType<E>, e: E): Void {
    this.lists[this.getListId(type)].push(e);
  }

  public function removeFromList<E: Entity>(type: EntityType<E>, e: Entity): Void {
    this.lists[this.getListId(type)].remove(e);
  }

  public function destroyList<E: Entity>(type: EntityType<E>): Void {
    var v3 = this.getList(type);
    for (v4 in 0...v3.length) {
      v3[v4].destroy();
    }
  }

  public function destroySome<E: Entity>(type: EntityType<E>, n: Int): Void {
    var v4 = this.getListCopy(type);
    while (true) {
      if (!(v4.length > 0 && n > 0)) break;
      var v5 = HfStd.random(v4.length);
      v4[v5].destroy();
      v4.splice(v5, 1);
      --n;
    }
  }

  public function cleanKills(): Void {
    for (v2 in 0...this.unregList.length) {
      this.removeFromList(this.unregList[v2].type, this.unregList[v2].ent);
    }
    this.unregList = new Array();
    for (v3 in 0...this.killList.length) {
      var v4 = this.killList[v3];
      v4.removeMovieClip();
    }
    this.killList = new Array();
  }

  public function exitGame(): Void {
    var v2 = new PersistCodec();
    var v3 = v2.encode(v2.encode(this.specialPicks) + ":" + v2.encode(this.scorePicks));
    this.manager.redirect("endGame.html", v3);
  }

  public function insertPlayer(cx: Float, cy: Float): Player {
    var v4 = 0;
    var v5 = this.getPlayerList();
    for (v6 in 0...v5.length) {
      if (!v5[v6].fl_destroy) {
        ++v4;
      }
    }
    var v7 = hf.entity.Player.attach(this, Entity.x_ctr(cx), Entity.y_ctr(cy));
    v7.hide();
    v7.pid = v4;
    return v7;
  }

  public function attachBad(id: Int, x: Float, y: Float): Bad {
    var v5: Null<Bad> = null;
    switch (id) {
      case Data.BAD_POMME:
        v5 = hf.entity.bad.walker.Pomme.attach(this, x, y);
      case Data.BAD_CERISE:
        v5 = hf.entity.bad.walker.Cerise.attach(this, x, y);
      case Data.BAD_BANANE:
        v5 = hf.entity.bad.walker.Banane.attach(this, x, y);
      case Data.BAD_ANANAS:
        v5 = hf.entity.bad.walker.Ananas.attach(this, x, y);
      case Data.BAD_ABRICOT:
        v5 = hf.entity.bad.walker.Abricot.attach(this, x, y, true);
      case Data.BAD_ABRICOT2:
        v5 = hf.entity.bad.walker.Abricot.attach(this, x, y, false);
      case Data.BAD_POIRE:
        v5 = hf.entity.bad.walker.Poire.attach(this, x, y);
      case Data.BAD_BOMBE:
        v5 = hf.entity.bad.walker.Bombe.attach(this, x, y);
      case Data.BAD_ORANGE:
        v5 = hf.entity.bad.walker.Orange.attach(this, x, y);
      case Data.BAD_FRAISE:
        v5 = hf.entity.bad.walker.Fraise.attach(this, x, y);
      case Data.BAD_CITRON:
        v5 = hf.entity.bad.walker.Citron.attach(this, x, y);
      case Data.BAD_BALEINE:
        v5 = hf.entity.bad.flyer.Baleine.attach(this, x, y);
      case Data.BAD_SPEAR:
        v5 = hf.entity.bad.Spear.attach(this, x, y);
      case Data.BAD_CRAWLER:
        v5 = hf.entity.bad.ww.Crawler.attach(this, x, y);
      case Data.BAD_TZONGRE:
        v5 = hf.entity.bad.flyer.Tzongre.attach(this, x, y);
      case Data.BAD_SAW:
        v5 = hf.entity.bad.ww.Saw.attach(this, x, y);
      case Data.BAD_LITCHI:
        v5 = hf.entity.bad.walker.Litchi.attach(this, x, y);
      case Data.BAD_KIWI:
        v5 = hf.entity.bad.walker.Kiwi.attach(this, x, y);
      case Data.BAD_LITCHI_WEAK:
        v5 = hf.entity.bad.walker.LitchiWeak.attach(this, x, y);
      case Data.BAD_FRAMBOISE:
        v5 = hf.entity.bad.walker.Framboise.attach(this, x, y);
      default:
        GameManager.fatal("(attachBad) unknown bad " + id);
    }
    if (v5.isType(Data.BAD_CLEAR)) {
      ++this.badCount;
    }
    return v5;
  }

  public function attachPop(msg: String, fl_tuto: Bool): Void {
    this.killPop();
    this.popMC = ((cast this.depthMan.attach("hammer_interf_pop", Data.DP_INTERF)): etwin.flash.DynamicMovieClip);
    this.popMC._x = Data.GAME_WIDTH * 0.5;
    if (fl_tuto) {
      this.popMC.sub.gotoAndStop("2");
      this.popMC.sub.header.text = Lang.get(2);
    } else {
      this.popMC.sub.gotoAndStop("1");
    }
    while (true) {
      if (!(msg.charCodeAt(0) == 10 || msg.charCodeAt(0) == 13)) break;
      msg = msg.substring(1);
    }
    this.popMC.sub.field.html = true;
    this.popMC.sub.field.htmlText = Data.replaceTag(msg, "*", "<font color=\"#f7e8d5\">", "</font>");
    var v4 = this.popMC.sub.field.textHeight;
    this.popMC.sub.field._y = -v4 * 0.5 - 2;
  }

  public function attachPointer(cx: Float, cy: Float, ocx: Float, ocy: Float): Void {
    this.killPointer();
    var v6 = Entity.x_ctr(cx);
    var v7 = Entity.y_ctr(cy);
    var v8 = Entity.x_ctr(ocx);
    var v9 = Entity.y_ctr(ocy);
    this.pointerMC = this.depthMan.attach("hammer_fx_pointer", Data.DP_INTERF);
    this.pointerMC._x = v6;
    this.pointerMC._y = v7;
    var v10 = Math.atan2(v9 - v7, v8 - v6) * 180 / Math.PI;
    this.pointerMC._rotation = v10 - 90;
  }

  public function attachRadius(x: Float, y: Float, r: Float): Void {
    this.killRadius();
    this.radiusMC = this.depthMan.attach("debug_radius", Data.DP_INTERF);
    this.radiusMC._x = x;
    this.radiusMC._y = y;
    this.radiusMC._width = r * 2;
    this.radiusMC._height = this.radiusMC._width;
  }

  public function attachItemName(family: Array<Array<ItemInfo>>, id: Int): Void {
    var v7;
    if (id == null) {
      return;
    }
    var v4 = null;
    var v5 = 0;
    while (true) {
      if (!(v4 == null && v5 < family.length)) break;
      var v6 = 0;
      while (true) {
        if (!(v4 == null && v6 < family[v5].length)) break;
        if (family[v5][v6].id == id) {
          v4 = family[v5][v6].name;
        }
        ++v6;
      }
      ++v5;
    }
    if (v4 != "" && v4 != null) {
      this.killItemName();
      this.itemNameMC = cast this.depthMan.attach("hammer_interf_item_name", Data.DP_TOP);
      this.itemNameMC._x = Data.GAME_WIDTH * 0.5 + 20;
      this.itemNameMC._y = 15;
      this.itemNameMC.field.text = v4;
      if (id < 1000) {
        v7 = ((cast HfStd.attachMC(this.itemNameMC, "hammer_item_special", this.manager.uniq++)): etwin.flash.DynamicMovieClip);
        v7.gotoAndStop("" + (id + 1));
      } else {
        v7 = ((cast HfStd.attachMC(this.itemNameMC, "hammer_item_score", this.manager.uniq++)): etwin.flash.DynamicMovieClip);
        v7.gotoAndStop("" + (id - 1000 + 1));
      }
      v7._x -= this.itemNameMC.field.textWidth * 0.5 + 20;
      v7._y = 10;
      v7._xscale = 75;
      v7._yscale = v7._xscale;
      v7.sub.stop();
    }
  }

  public function killPop(): Void {
    this.popMC.removeMovieClip();
  }

  public function killPointer(): Void {
    this.pointerMC.removeMovieClip();
  }

  public function killRadius(): Void {
    this.radiusMC.removeMovieClip();
  }

  public function killItemName(): Void {
    this.itemNameMC.removeMovieClip();
  }

  public function killPortals(): Void {
    for (v2 in 0...this.portalMcList.length) {
      this.portalMcList[v2].mc.removeMovieClip();
    }
    this.portalMcList = new Array();
  }

  public function attachMapIcon(eid: Int, lid: Int, txt: String, offset: Float, offsetTotal: Int): Void {
    var v7 = Data.GAME_WIDTH * 0.5;
    var v8 = this.getMapY(lid);
    if (offset != null) {
      var v9 = 8;
      v7 += offset * v9 - 0.5 * (offsetTotal - 1) * v9;
    }
    if (eid == Data.EVENT_EXIT_LEFT || eid == Data.EVENT_BACK_LEFT) {
      v7 = Data.GAME_WIDTH * 0.5 - 5;
    }
    if (eid == Data.EVENT_EXIT_RIGHT || eid == Data.EVENT_BACK_RIGHT) {
      v7 = Data.GAME_WIDTH * 0.5 + 5;
    }
    var v10 = ((cast this.depthMan.attach("hammer_interf_mapIcon", Data.DP_INTERF)): etwin.flash.DynamicMovieClip);
    v10.gotoAndStop("" + eid);
    v10._x = Math.floor(v7);
    v10._y = Math.floor(v8);
    if (txt == null) {
      txt = "?";
    }
    v10.field.text = txt;
    this.mapIcons.push(v10);
  }

  public function callEvilOne(baseAnger: Int): Void {
    var v3 = this.getPlayerList();
    for (v4 in 0...v3.length) {
      if (!v3[v4].fl_kill) {
        var v5 = hf.entity.bad.FireBall.attach(this, v3[v4]);
        v5.anger = baseAnger - 1;
        if (baseAnger > 0) {
          v5.fl_summon = false;
          v5.stopBlink();
        }
        v5.angerMore();
      }
    }
  }

  public function onLevelReady(): Void {
    this.unlock();
    this.initLevel();
    this.startLevel();
    this.updateEntitiesWorld();
    var v2 = this.getList(Data.PLAYER);
    for (v3 in 0...v2.length) {
      v2[v3].show();
    }
  }

  public function onBadsReady(): Void {
    if (this.fl_ninja && (this.getBadClearList()).length > 1) {
      var v2 = this.getOne(Data.BAD_CLEAR);
      v2.fl_ninFoe = true;
      if ((this.fl_nightmare || !this.world.fl_mainWorld || this.world.fl_mainWorld) && this.world.currentId >= 20) {
        var v3 = this.dimensions[0].currentId;
        this.friendsLimit = Math.floor(Math.max(2, Math.floor((v3 - 20) / 10)));
      } else {
        this.friendsLimit = 1;
      }
      if (this.fl_nightmare) {
        ++this.friendsLimit;
      }
      this.friendsLimit = Math.round(Math.min((this.getBadClearList()).length - 1, this.friendsLimit));
      while (this.friendsLimit > 0) {
        var v4 = this.getAnotherOne(Data.BAD_CLEAR, v2);
        if (!v4.fl_ninFriend) {
          v4.fl_ninFriend = true;
          --this.friendsLimit;
        }
      }
    }
  }

  public function onRestore(): Void {
    this.unlock();
    var v2 = this.getPortalEntrance(this.portalId);
    var v3 = this.getPlayerList();
    for (v4 in 0...v3.length) {
      var v5 = v3[v4];
      v5.moveToCase(v2.x, v2.y);
      v5.show();
      v5.unshield();
      if (v2.fl_unstable) {
        v5.knock(Data.SECOND * 0.6);
      }
    }
    for (v6 in 0...this.latePlayers.length) {
      var v7 = this.latePlayers[v6];
      v7.knock(Data.SECOND * 1.3);
      v7.dx = 0;
    }
    if (this.world.fl_mainWorld) {
      if (v2.x >= Data.LEVEL_WIDTH * 0.5) {
        this.registerMapEvent(Data.EVENT_BACK_RIGHT, null);
      } else {
        this.registerMapEvent(Data.EVENT_BACK_LEFT, null);
      }
    }
  }

  public function updateEntitiesWorld(): Void {
    var v2 = this.getList(Data.ENTITY);
    for (v3 in 0...v2.length) {
      v2[v3].world = this.world;
    }
  }

  public function onLevelClear(): Void {
    if (this.fl_clear) {
      return;
    }
    this.world.scriptEngine.clearEndTriggers();
    var v2 = this.getList(Data.SPECIAL_ITEM);
    for (v3 in 0...v2.length) {
      var v4 = v2[v3];
      if (v4.id == 0) {
        v4.destroy();
      }
    }
    this.fl_clear = true;
    this.fxMan.attachExit();
    for (v5 in 0...this.endLevelStack.length) {
      this.endLevelStack[v5]();
    }
    this.endLevelStack = new Array();
  }

  public function onHurryUp(): Null<MovieClip> {
    ++this.huState;
    this.huTimer = 0;
    var v2 = this.getBadList();
    for (v3 in 0...v2.length) {
      v2[v3].onHurryUp();
    }
    var v4 = this.fxMan.attachHurryUp();
    if (this.huState == 1) {
      this.soundMan.playSound("sound_hurry", Data.CHAN_INTERF);
    }
    if (this.huState == 2) {
      this.callEvilOne(0);
    }
    return v4;
  }

  public function onGameOver(): Void {
    this.fl_gameOver = true;
  }

  public function onKillBad(b: Bad): Void {
  }

  public function onPause(): Void {
    if (this.fl_lock) {
      return;
    }
    this.fl_pause = true;
    this.lock();
    this.world.lock();
    this.pauseMC.removeMovieClip();
    this.pauseMC = cast this.depthMan.attach("hammer_interf_instructions", Data.DP_INTERF);
    this.pauseMC.gotoAndStop("1");
    this.pauseMC._x = Data.GAME_WIDTH * 0.5;
    this.pauseMC._y = Data.GAME_HEIGHT * 0.5;
    this.pauseMC.click.text = "";
    this.pauseMC.title.text = Lang.get(5);
    this.pauseMC.move.text = Lang.get(7);
    this.pauseMC.attack.text = Lang.get(8);
    this.pauseMC.pause.text = Lang.get(9);
    this.pauseMC.space.text = Lang.get(10);
    this.pauseMC.sector.text = Lang.get(14) + "«" + Lang.getSectorName(this.currentDim, this.world.currentId) + "»";
    if (!this.fl_mute) {
      this.setMusicVolume(0.5);
    }
    var v2 = Lang.get(301 + this.tipId++);
    if (v2 == null) {
      this.tipId = 0;
      v2 = Lang.get(301 + this.tipId++);
    }
    this.pauseMC.tip.html = true;
    this.pauseMC.tip.htmlText = "<b>" + Lang.get(300) + "</b>" + v2;
  }

  public function onMap(): Void {
    this.fl_pause = true;
    this.lock();
    this.world.lock();
    if (!this.fl_mute) {
      this.setMusicVolume(0.5);
    }
    this.mapMC.removeMovieClip();
    this.mapMC = cast this.depthMan.attach("hammer_map", Data.DP_INTERF);
    this.mapMC.field.text = Lang.getSectorName(this.currentDim, this.world.currentId);
    this.mapMC._x = -this.xOffset;
    var v2 = this.dimensions[0].currentId;
    if (v2 == 0) {
      this.mapMC.ptr._y = 70;
      this.mapMC.pit._visible = false;
    } else {
      this.mapMC.ptr._y = this.getMapY(v2);
      this.mapMC.pit.blendMode = BlendMode.OVERLAY;
      this.mapMC.pit._alpha = 75;
      this.mapMC.pit._yscale = Math.min(100, 100 * (v2 / 100));
    }
    for (v3 in 0...this.mapTravels.length) {
      var v4 = this.mapTravels[v3];
      this.attachMapIcon(v4.eid, v4.lid, v4.misc, null, null);
    }
    for (v5 in 0...this.mapEvents.length) {
      var v6 = this.mapEvents[v5];
      var v7 = new Array();
      for (v8 in 0...this.mapEvents.length) {
        if (this.mapEvents[v8].lid == v6.lid) {
          v7.push(this.mapEvents[v8]);
        }
      }
      for (v9 in 0...v7.length) {
        this.attachMapIcon(v7[v9].eid, v7[v9].lid, v7[v9].misc, v9, v7.length);
      }
    }
  }

  public function onUnpause(): Void {
    if (!this.fl_pause) {
      return;
    }
    this.fl_pause = false;
    this.unlock();
    this.world.unlock();
    this.pauseMC.removeMovieClip();
    this.mapMC.removeMovieClip();
    if (!this.fl_mute) {
      this.setMusicVolume(1);
    }
    for (v2 in 0...this.mapIcons.length) {
      this.mapIcons[v2].removeMovieClip();
    }
    this.mapIcons = new Array();
  }

  public function onResurrect(): Void {
    this.registerMapEvent(Data.EVENT_DEATH, null);
    this.destroyList(Data.SUPA);
    this.resetHurry();
    this.updateDarkness();
    this.world.scriptEngine.onPlayerBirth();
    this.world.scriptEngine.onPlayerDeath();
  }

  public function onExplode(x: Float, y: Float, radius: Float): Void {
    this.world.scriptEngine.onExplode(x, y, radius);
  }

  public function onEndOfSet(): Void {
  }

  public function updateDarkness(): Void {
    if (this.forcedDarkness != null) {
      this.targetDark = this.forcedDarkness;
    } else {
      if (this.world.fl_mainWorld) {
        if (this.world.currentId < Data.MIN_DARKNESS_LEVEL || this.world.currentId > 101) {
          this.darknessMC.removeMovieClip();
          this.world.darknessFactor = 0;
          this.dfactor = 0;
          return;
        }
        this.targetDark = this.world.currentId;
      } else {
        this.targetDark = this.world.darknessFactor;
      }
    }
    if (this.darknessMC._name == null) {
      this.darknessMC = cast this.depthMan.attach("hammer_fx_darkness", Data.DP_BORDERS);
      this.darknessMC._x -= this.xOffset;
      this.darknessMC.blendMode = BlendMode.LAYER;
      this.darknessMC.holes = new Array();
      this.darknessMC.holes.push(this.darknessMC.hole1);
      this.darknessMC.holes.push(this.darknessMC.hole2);
      for (v2 in 0...2) {
        this.darknessMC.holes[v2].blendMode = BlendMode.ERASE;
        this.darknessMC.holes[v2]._y = -500;
      }
    }
    for (v3 in 0...2) {
      this.darknessMC.holes[v3]._xscale = 100;
      this.darknessMC.holes[v3]._yscale = this.darknessMC.holes[v3]._xscale;
    }
    this.detachExtraHoles();
    for (v4 in 0...this.extraHoles.length) {
      var v5 = HfStd.attachMC(this.darknessMC, "hammer_fx_darknessHole", this.manager.uniq++);
      v5._x = this.extraHoles[v4].x;
      v5._y = this.extraHoles[v4].y;
      v5._width = this.extraHoles[v4].d;
      v5._height = this.extraHoles[v4].d;
      v5.blendMode = BlendMode.ERASE;
      this.darknessMC.holes.push(v5);
      this.extraHoles[v4].mc = v5;
    }
    var v6 = this.getPlayerList();
    for (v7 in 0...v6.length) {
      if (v6[v7].fl_candle || v6[v7].specialMan.actives[68]) {
        this.darknessMC.holes[v7]._xscale = 150;
        this.darknessMC.holes[v7]._yscale = this.darknessMC.holes[v7]._xscale;
      }
      if (v6[v7].fl_torch || v6[v7].specialMan.actives[26]) {
        if (this.forcedDarkness == null) {
          this.targetDark *= 0.5;
        } else {
          this.targetDark *= 0.75;
        }
      }
    }
    this.holeUpdate();
  }

  public function addHole(x: Float, y: Float, diameter: Float): Void {
    this.extraHoles.push({x: x, y: y, d: diameter, mc: null});
  }

  public function detachExtraHoles(): Void {
    for (v2 in 0...this.extraHoles.length) {
      this.extraHoles[v2].mc.removeMovieClip();
    }
    this.darknessMC.holes.splice(2, 9999);
  }

  public function clearExtraHoles(): Void {
    this.detachExtraHoles();
    this.extraHoles = new Array();
  }

  public function holeUpdate(): Void {
    if (this.darknessMC._name == null) {
      return;
    }
    var v2 = this.getPlayerList();
    for (v3 in 0...v2.length) {
      var v4 = v2[v3];
      var v5 = v4.x + Data.CASE_WIDTH * 0.5;
      var v6 = v4.y - Data.CASE_HEIGHT;
      var v7: MovieClip = this.darknessMC.holes[v3];
      if (v4._visible) {
        v7._visible = true;
        v7._x += (v5 - v7._x) * 0.5;
        v7._y += (v6 - v7._y) * 0.5;
      } else {
        v7._visible = false;
        v7._x = v5;
        v7._y = v6;
      }
    }
    this.dfactor = Math.round(this.dfactor);
    this.targetDark = Math.round(this.targetDark);
    if (this.dfactor == null) {
      this.dfactor = 0;
    }
    if (this.dfactor < this.targetDark) {
      this.dfactor += 2;
      if (this.dfactor > this.targetDark) {
        this.dfactor = this.targetDark;
      }
    }
    if (this.dfactor > this.targetDark) {
      this.dfactor -= 2;
      if (this.dfactor < this.targetDark) {
        this.dfactor = this.targetDark;
      }
    }
    this.world.darknessFactor = this.dfactor;
    this.darknessMC._alpha = this.dfactor;
  }

  override public function destroy(): Void {
    this.resetCol();
    this.killPortals();
    super.destroy();
  }

  override public function getControls(): Void {
    super.getControls();
    if (this.keyLock != null && !Key.isDown(this.keyLock)) {
      this.keyLock = null;
    }
    if (Key.isDown(80) && this.keyLock != 80) {
      if (this.fl_lock) {
        this.onUnpause();
      } else {
        this.onPause();
      }
      this.keyLock = 80;
    }
    if (this.fl_map && Key.isDown(67) && this.keyLock != 67) {
      if (this.fl_lock) {
        this.onUnpause();
      } else {
        this.onMap();
      }
      this.keyLock = 67;
    }
    if (!this.fl_lock && this.fl_music && Key.isDown(77) && this.keyLock != 77) {
      this.fl_mute = !this.fl_mute;
      if (this.fl_mute) {
        this.setMusicVolume(0);
      } else {
        this.setMusicVolume(1);
      }
      this.keyLock = 77;
    }
    if (Key.isDown(Key.SHIFT) && Key.isDown(Key.CONTROL) && Key.isDown(75)) {
      if (!this.fl_switch && !this.fl_lock && this.countList(Data.PLAYER) > 0) {
        this.destroyList(Data.PLAYER);
        this.onGameOver();
      }
    }
    if (!this.fl_switch && Key.isDown(Key.ESCAPE) && !this.fl_gameOver && this.keyLock != Key.ESCAPE) {
      if (this.manager.fl_local && !this.fl_lock) {
        this.endMode();
      } else {
        if (this.manager.fl_debug) {
          if (!this.fl_lock) {
            this.destroyList(Data.PLAYER);
            this.onGameOver();
          }
        } else {
          if (this.fl_lock) {
            this.onUnpause();
          } else {
            this.onPause();
          }
        }
      }
      this.keyLock = Key.ESCAPE;
    }
    if (this.fl_disguise && Key.isDown(68) && this.keyLock != 68) {
      var v3 = (this.getPlayerList())[0];
      var v4 = v3.head;
      ++v3.head;
      if (v3.head == Data.HEAD_AFRO && !GameManager.CONFIG.hasFamily(109)) {
        ++v3.head;
      }
      if (v3.head == Data.HEAD_CERBERE && !GameManager.CONFIG.hasFamily(110)) {
        ++v3.head;
      }
      if (v3.head == Data.HEAD_PIOU && !GameManager.CONFIG.hasFamily(111)) {
        ++v3.head;
      }
      if (v3.head == Data.HEAD_MARIO && !GameManager.CONFIG.hasFamily(112)) {
        ++v3.head;
      }
      if (v3.head == Data.HEAD_TUB && !GameManager.CONFIG.hasFamily(113)) {
        ++v3.head;
      }
      if (v3.head > 6) {
        v3.head = v3.defaultHead;
      }
      if (v4 != v3.head) {
        v3.replayAnim();
      }
      this.keyLock = 68;
    }
    if (Key.isDown(70)) {
      Log.print(Math.round(Timer.fps()) + " FPS");
      Log.print("Performances: " + Math.min(100, Math.round(100 * Timer.fps() / 30)) + "%");
    }
    if (Key.isDown(66)) {
      Log.print("Dernière mise à jour:");
      Log.print("01-11 11:48:51");
      Log.print("Version de Flash: " + this.manager.fVersion);
    }
  }

  override public function getDebugControls(): Void {
    super.getDebugControls();
    if (Key.isDown(Key.SHIFT)) {
      Timer.tmod = 0.3;
    }
    if (Key.isDown(Key.HOME)) {
      Timer.tmod = 1.0;
    }
  }

  override public function lock(): Void {
    super.lock();
    this.gameChrono.stop();
  }

  override public function main(): Void {
    if (GameManager.CONFIG.fl_detail) {
      if (Timer.fps() <= 16) {
        this.lagCpt += Timer.tmod;
        if (this.lagCpt >= Data.SECOND * 30) {
          GameManager.CONFIG.setLowDetails();
          GameManager.CONFIG.fl_shaky = false;
        }
      } else {
        this.lagCpt = 0;
      }
    }
    this.gameChrono.update();
    if (this.fl_pause) {
      if (this.manager.fl_debug) {
        this.printDebugInfos();
      }
    }
    if (this.fl_bullet && this.bulletTimer > 0) {
      this.bulletTimer -= Timer.tmod;
      if (this.bulletTimer > 0) {
        Timer.tmod = 0.3;
      }
    }
    if (this.itemNameMC._name != null) {
      this.itemNameMC._alpha -= (105 - this.itemNameMC._alpha) * 0.01;
      if (this.itemNameMC._alpha <= 5) {
        this.itemNameMC.removeMovieClip();
      }
    }
    this.updateGroundFrictions();
    this.fl_ice = this.getDynamicVar("$ICE") != null;
    this.fl_aqua = this.getDynamicVar("$AQUA") != null;
    super.main();
    this.world.update();
    (this.gi: hf.gui.GameInterface).update();
    if (this.fl_lock) {
      return;
    }
    if ((this.getBadList()).length > 0 && this.fl_badsCallback == false) {
      this.onBadsReady();
      this.fl_badsCallback = true;
    }
    for (v3 in 0...this.portalMcList.length) {
      var v4 = this.portalMcList[v3];
      if (v4 != null) {
        v4.mc._y = v4.y + 2 * Math.sin(v4.cpt);
        v4.cpt += Timer.tmod * 0.1;
        if (HfStd.random(5) == 0) {
          var v5 = this.fxMan.attachFx(v4.x + HfStd.random(25) * (HfStd.random(2) * 2 - 1), v4.y + HfStd.random(25) * (HfStd.random(2) * 2 - 1), "hammer_fx_star");
          v5.mc._xscale = HfStd.random(70) + 30;
          v5.mc._yscale = v5.mc._xscale;
        }
      }
    }
    this.duration += Timer.tmod;
    if (this.endModeTimer > 0) {
      this.endModeTimer -= Timer.tmod;
      if (this.endModeTimer <= 0) {
        var v6 = this.getPlayerList();
        for (v7 in 0...v6.length) {
          this.registerScore(v6[v7].pid, v6[v7].score);
        }
        this.onGameOver();
      }
    }
    this.fxMan.main();
    this.huTimer += Timer.tmod;
    if (Key.isDown(72) && this.manager.fl_debug) {
      this.huTimer += Timer.tmod * 20;
    }
    if (this.huState < Data.HU_STEPS.length && this.huTimer >= Data.HU_STEPS[this.huState] / this.diffFactor) {
      this.onHurryUp();
    }
    if (this.huState >= Data.HU_STEPS.length) {
      if (this.countList(Data.HU_BAD) == 0) {
        this.callEvilOne(Math.round(this.huTimer / Data.AUTO_ANGER));
      }
    }
    var v8 = this.getList(Data.ENTITY);
    for (v9 in 0...v8.length) {
      v8[v9].update();
      v8[v9].endUpdate();
    }
    this.holeUpdate();
    this.cleanKills();
    if (!this.world.fl_lock) {
      this.checkLevelClear();
    }
    if (this.nextLink != null) {
      var v10 = this.getPlayerList();
      for (v11 in 0...v10.length) {
        var v12 = v10[v11];
        v12._xscale *= 0.85;
        v12._yscale = Math.abs(v12._xscale);
        if (Math.abs(v12._xscale) <= 2) {
          this.switchDimensionById(this.nextLink.to_did, this.nextLink.to_lid, this.nextLink.to_pid);
          this.nextLink = null;
          break;
        }
      }
    }
    if (this.shakeTimer > 0) {
      this.shakeTimer -= Timer.tmod;
      if (this.shakeTimer <= 0) {
        this.shakeTimer = 0;
        this.shakePower = 0;
      }
      if (this.fl_flipX) {
        this.mc._x = Data.GAME_WIDTH + this.xOffset - Math.round((HfStd.random(2) * 2 - 1) * (HfStd.random(Math.round(this.shakePower * 10)) / 10) * this.shakeTimer / this.shakeTotal);
      } else {
        this.mc._x = Math.round(this.xOffset + (HfStd.random(2) * 2 - 1) * (HfStd.random(Math.round(this.shakePower * 10)) / 10) * this.shakeTimer / this.shakeTotal);
      }
      if (this.fl_flipY) {
        this.mc._y = Data.GAME_HEIGHT + 20 + Math.round(this.yOffset + (HfStd.random(2) * 2 - 1) * (HfStd.random(Math.round(this.shakePower * 10)) / 10) * this.shakeTimer / this.shakeTotal);
      } else {
        this.mc._y = Math.round(this.yOffset + (HfStd.random(2) * 2 - 1) * (HfStd.random(Math.round(this.shakePower * 10)) / 10) * this.shakeTimer / this.shakeTotal);
      }
    }
    if (this.fl_aqua) {
      this.aquaTimer += 0.03 * Timer.tmod;
      if (!this.fl_flipY) {
        this.mc._y = -7 + 7 * Math.cos(this.aquaTimer);
        this.mc._yscale = 102 - 2 * Math.cos(this.aquaTimer);
      }
    } else {
      if (this.aquaTimer != 0) {
        this.aquaTimer = 0;
        this.flipY(this.fl_flipY);
      }
    }
    if (this.windTimer > 0) {
      this.windTimer -= Timer.tmod;
      if (this.windTimer <= 0) {
        this.wind(null, null);
      }
    }
    if (this.countList(Data.BAD_CLEAR) > 0) {
      if (this.fxMan.mc_exitArrow._name != null) {
        this.fxMan.detachExit();
      }
    }
    if (this.fxMan.mc_exitArrow._name != null) {
      if (this.manager.isTutorial()) {
        this.fxMan.detachExit();
      }
    }
    if (this.fl_nightmare) {
      var v13 = this.getBadList();
      for (v14 in 0...v13.length) {
        var v15 = v13[v14];
        if (v15.isType(Data.BAD_CLEAR) && v15.anger == 0) {
          v15.angerMore();
        }
      }
    }
  }

  override public function unlock(): Void {
    super.unlock();
    this.gameChrono.start();
  }
}
