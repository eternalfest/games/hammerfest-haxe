package hf;

import etwin.flash.MovieClip;
import etwin.flash.TextField;

@:native("Log") // Class provided by the loader
extern class Log {
  public static var std: Dynamic /*TODO*/;
  public static var initdone: Bool;
  public static var maxHeight: Null<Float>;
  public static var ttrace: Null<TextField>;
  public static var tprint: Null<TextField>;
  public static var tid: Null<Int>;


  public static function clear(): Void;

  public static function init(mc: MovieClip, w: Float, h: Float): Void;

  public static function traceString(s: String): Void;

  public static function setColor(c: Int): Void;

  public static function print(x: Dynamic /*TODO*/): Void;

  public static function destroy(): Void;

  public static function toString(o: Dynamic /*TODO*/, ?s: String): String;

  public static function trace(o: Dynamic /*TODO*/): Void;
}
