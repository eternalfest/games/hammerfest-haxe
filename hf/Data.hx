package hf;

import hf.GameManager;
import hf.Hash;
import hf.levels.PortalLink;

import hf.entity.Animator.Anim;
import hf.entity.Bad;
import hf.entity.Bomb;
import hf.entity.Item;
import hf.entity.Physics;
import hf.entity.Player;
import hf.entity.Shoot;
import hf.entity.Supa;
import hf.entity.bad.FireBall;
import hf.entity.bad.Spear;
import hf.entity.bad.walker.Fraise;
import hf.entity.bomb.BadBomb;
import hf.entity.bomb.PlayerBomb;
import hf.entity.bomb.player.SoccerBall;
import hf.entity.fx.Particle;
import hf.entity.item.SpecialItem;
import hf.entity.shoot.Ball;
import etwin.flash.XML;
using hf.compat.XMLNodeTools;

typedef ItemInfo = {
  var id: Int;
  var r: Int;
  var br: Int;
  var v: Int;
  var name: String;
}

typedef LevelRef = {
  var did: Int;
  var lid: Int;
}

typedef LevelTag = {
  var name: String;
  var did: Int;
  var lid: Int;
}

class Data {
  /**
   * Current game manager.
   */
  public static var manager: Null<GameManager> = null;
  // TODO: ArrayMap<Array<ItemInfo>>
  public static var SPECIAL_ITEM_FAMILIES: Null<Array<Array<ItemInfo>>>;
  // TODO: ArrayMap<Array<ItemInfo>>
  public static var SCORE_ITEM_FAMILIES: Null<Array<Array<ItemInfo>>>;
  public static var FAMILY_CACHE: Null<Array<Int>>;
  public static var ITEM_VALUES: Null<Dynamic>;
  public static var LINKS: Null<Array<PortalLink>>;
  public static var LEVEL_TAG_LIST: Null<Array<LevelTag>>;

  public static function initLinkages(): Array<String> {
    var v2 = new Array();
    v2[Data.BAD_POMME] = "hammer_bad_pomme";
    v2[Data.BAD_CERISE] = "hammer_bad_cerise";
    v2[Data.BAD_BANANE] = "hammer_bad_banane";
    v2[Data.BAD_FIREBALL] = "hammer_bad_fireball";
    v2[Data.BAD_ANANAS] = "hammer_bad_ananas";
    v2[Data.BAD_ABRICOT] = "hammer_bad_abricot";
    v2[Data.BAD_ABRICOT2] = "hammer_bad_abricot";
    v2[Data.BAD_POIRE] = "hammer_bad_poire";
    v2[Data.BAD_BOMBE] = "hammer_bad_bombe";
    v2[Data.BAD_ORANGE] = "hammer_bad_orange";
    v2[Data.BAD_FRAISE] = "hammer_bad_fraise";
    v2[Data.BAD_CITRON] = "hammer_bad_citron";
    v2[Data.BAD_BALEINE] = "hammer_bad_baleine";
    v2[Data.BAD_SPEAR] = "hammer_bad_spear";
    v2[Data.BAD_CRAWLER] = "hammer_bad_crawler";
    v2[Data.BAD_TZONGRE] = "hammer_bad_tzongre";
    v2[Data.BAD_SAW] = "hammer_bad_saw";
    v2[Data.BAD_LITCHI] = "hammer_bad_litchi";
    v2[Data.BAD_KIWI] = "hammer_bad_kiwi";
    v2[Data.BAD_LITCHI_WEAK] = "hammer_bad_litchi_weak";
    v2[Data.BAD_FRAMBOISE] = "hammer_bad_framboise";
    return v2;
  }

  public static function init(m: GameManager): Void {
    Data.manager = m;
    Data.SPECIAL_ITEM_FAMILIES = Data.xml_readSpecialItems();
    Data.SCORE_ITEM_FAMILIES = Data.xml_readScoreItems();
    Data.FAMILY_CACHE = Data.cacheFamilies();
    Data.ITEM_VALUES = Data.getScoreItemValues();
    Data.LINKS = Data.xml_readPortalLinks();
  }

  public static function initItemsRaw(): Array<Int> {
    var v2 = new Array();
    var v3 = HfStd.getVar(Data.manager.root, "xml_items");
    var v4 = (new XML(v3)).firstChild;
    if (v4.nodeName != "$items".substring(1)) {
      GameManager.fatal("XML error: invalid node '" + v4.nodeName + "'");
      return null;
    }
    var v5 = v4.firstChild;
    while (v5 != null) {
      v4 = v5.firstChild;
      while (v4 != null) {
        var v6 = (cast v4.get("$rarity".substring(1))) | 0;
        var v7 = (cast v4.get("$id".substring(1))) | 0;
        var v8 = Data.__NA;
        switch (v6) {
          case 1:
            v8 = Data.COMM;
          case 2:
            v8 = Data.UNCO;
          case 3:
            v8 = Data.RARE;
          case 4:
            v8 = Data.UNIQ;
          case 5:
            v8 = Data.MYTH;
          case 6:
            v8 = Data.__NA;
          case 7:
            v8 = Data.CANE;
        }
        v2[v7] = v8;
        v4 = v4.nextSibling;
      }
      v5 = v5.nextSibling;
    }
    return v2;
  }

  // TODO: ArrayMap<Array<ItemInfo>>
  public static function xml_readFamily(xmlName: String): Array<Array<ItemInfo>> {
    var v3 = new Array();
    var v4 = HfStd.getVar(Data.manager.root, xmlName);
    var v5 = (new XML(v4)).firstChild;
    if (v5.nodeName != "$items".substring(1)) {
      GameManager.fatal("XML error (" + xmlName + " @ " + Data.manager.root._name + "): invalid node '" + v5.nodeName + "'");
      return null;
    }
    var v6 = v5.firstChild;
    while (v6 != null) {
      v5 = v6.firstChild;
      var v7 = (cast v6.get("$id".substring(1))) | 0;
      v3[v7] = new Array();
      while (v5 != null) {
        var v8 = (cast v5.get("$id".substring(1))) | 0;
        var v9 = (cast v5.get("$rarity".substring(1))) | 0;
        var v10 = (cast v5.get("$value".substring(1))) | 0;
        if (v10 == null) {
          v10 = 0;
        }
        v3[v7].push({id: v8, r: Data.RARITY[v9], br: v9, v: v10, name: Lang.getItemName(v8)});
        v5 = v5.nextSibling;
      }
      v6 = v6.nextSibling;
    }
    return v3;
  }

  // TODO: ArrayMap<Array<ItemInfo>>
  public static function xml_readSpecialItems(): Array<Array<ItemInfo>> {
    return Data.xml_readFamily("xml_specialItems");
  }

  // TODO: ArrayMap<Array<ItemInfo>>
  public static function xml_readScoreItems(): Array<Array<ItemInfo>> {
    return Data.xml_readFamily("xml_scoreItems");
  }

  public static function getRandFromFamilies(familySet: Array<Array<ItemInfo>>, familiesId: Array<Int>): Array<Int> {
    var v4 = new Array();
    for (v5 in 0...familiesId.length) {
      var v6 = familySet[familiesId[v5]];
      var v7 = 0;
      while (v7 < v6.length) {
        v4[v6[v7].id] = v6[v7].r;
        ++v7;
      }
    }
    return v4;
  }

  public static function getScoreItemValues(): Array<Int> {
    var v2 = new Array();
    for (v3 in 0...Data.SCORE_ITEM_FAMILIES.length) {
      var v4 = Data.SCORE_ITEM_FAMILIES[v3];
      var v5 = 0;
      while (v5 < v4.length) {
        v2[v4[v5].id] = v4[v5].v;
        ++v5;
      }
    }
    return v2;
  }

  public static function cacheFamilies(): Array<Int> {
    var v2 = new Array();
    for (v3 in 0...Data.SPECIAL_ITEM_FAMILIES.length) {
      var v4 = Data.SPECIAL_ITEM_FAMILIES[v3];
      for (v5 in 0...v4.length) {
        v2[v4[v5].id] = v3;
      }
    }
    for (v6 in 0...Data.SCORE_ITEM_FAMILIES.length) {
      var v7 = Data.SCORE_ITEM_FAMILIES[v6];
      for (v8 in 0...v7.length) {
        v2[v7[v8].id] = v6;
      }
    }
    return v2;
  }

  public static function getTagFromLevel(did: Int, lid: Int): Null<String> {
    var v4 = null;
    for (v5 in 0...Data.LEVEL_TAG_LIST.length) {
      var v6 = Data.LEVEL_TAG_LIST[v5];
      if (v6.did == did && v6.lid == lid) {
        v4 = v6.name;
      }
    }
    return v4;
  }

  public static function getLevelFromTag(code: String): LevelRef {
    code = code.toLowerCase();
    var v3 = null;
    var v4 = (code.split("+"))[0];
    var v5 = HfStd.parseInt((code.split("+"))[1], 10);
    if (HfStd.isNaN(v5)) {
      v5 = 0;
    }
    for (v6 in 0...Data.LEVEL_TAG_LIST.length) {
      var v7 = Data.LEVEL_TAG_LIST[v6];
      if (v7.name == v4) {
        v3 = {did: v7.did, lid: v7.lid + v5};
      }
    }
    return v3;
  }

  public static function xml_readPortalLinks(): Array<PortalLink> {
    var v2 = new Array();
    var v3 = HfStd.getVar(Data.manager.root, "xml_portalLinks");
    var v4 = new XML(null);
    v4.ignoreWhite = true;
    v4.parseXML(v3);
    var v5 = v4.firstChild;
    if (v5.nodeName != "$links".substring(1)) {
      GameManager.fatal("XML error (xml_portals @ " + Data.manager.root._name + "): invalid node '" + v5.nodeName + "'");
      return null;
    }
    v5 = v5.firstChild;
    if (v5.nodeName != "$tags".substring(1)) {
      GameManager.fatal("XML error (xml_portals @ " + Data.manager.root._name + "): invalid node '" + v5.nodeName + "'");
    }
    var v6 = v5.firstChild;
    Data.LEVEL_TAG_LIST = new Array();
    while (v6 != null) {
      Data.LEVEL_TAG_LIST.push({name: (v6.get("$name".substring(1))).toLowerCase(), did: HfStd.parseInt(v6.get("$did".substring(1)), 10), lid: HfStd.parseInt(v6.get("$lid".substring(1)), 10)});
      v6 = v6.nextSibling;
    }
    v5 = v5.nextSibling;
    if (v5.nodeName != "$ways".substring(1)) {
      GameManager.fatal("xml_readPortalLinks: unknown node " + v5.nodeName);
      return null;
    }
    v5 = v5.firstChild;
    while (v5 != null) {
      var v7 = v5.get("$from".substring(1));
      v7 = Tools.replace(v7, "(", ",");
      v7 = Tools.replace(v7, ")", "");
      v7 = Tools.replace(v7, " ", "");
      var v8 = v7.split(",");
      v7 = v5.get("$to".substring(1));
      v7 = Tools.replace(v7, "(", ",");
      v7 = Tools.replace(v7, ")", "");
      v7 = Tools.replace(v7, " ", "");
      var v9 = v7.split(",");
      var v10 = new hf.levels.PortalLink();
      var v11 = Data.getLevelFromTag(v8[0]);
      v10.from_did = v11.did;
      v10.from_lid = v11.lid;
      v10.from_pid = HfStd.parseInt(v8[1], 10);
      v11 = Data.getLevelFromTag(v9[0]);
      v10.to_did = v11.did;
      v10.to_lid = v11.lid;
      v10.to_pid = HfStd.parseInt(v9[1], 10);
      v10.cleanUp();
      v2.push(v10);
      if (v5.nodeName == "$twoway".substring(1)) {
        var v12 = new hf.levels.PortalLink();
        v12.from_did = v10.to_did;
        v12.from_lid = v10.to_lid;
        v12.from_pid = v10.to_pid;
        v12.to_did = v10.from_did;
        v12.to_lid = v10.from_lid;
        v12.to_pid = v10.from_pid;
        v2.push(v12);
      }
      v5 = v5.nextSibling;
    }
    return v2;
  }

  public static function computeMd5(fam: Array<Dynamic /*TODO*/>): String {
    var v3 = 0.0;
    for (v4 in 0...fam.length) {
      if (fam[v4] != null) {
        for (v5 in 0...fam[v4].length) {
          var v6 = fam[v4][v5];
          if (v6.v > 0) {
            v3 += (v4 / 1000) * v6.id * v6.br * v6.v / 1000;
          } else {
            v3 += (v4 / 1000) * v6.id * v6.br;
          }
        }
      }
    }
    return Md5.encode("" + v3);
  }

  public static function serializeHash(h: Hash): String {
    var str = "";
    h.iter(function (k, e) {
      if (str.length > 0) {
        str += "#";
      }
      str += k + ";" + e.join(":");
    });
    return str;
  }

  public static function unserializeHash(str: String): Hash {
    var v3 = new Hash();
    if (str == null) {
      return v3;
    }
    var v4 = str.split("#");
    for (v5 in 0...v4.length) {
      var v6 = v4[v5].split(";");
      var v7 = v6[0];
      var v8 = v6[1];
      if (v8.length != null) {
        v3.set(v7, v8.split(":"));
      }
    }
    return v3;
  }

  public static function duplicate<T>(o: T): T {
    var v3 = new PersistCodec();
    var v4 = v3.encode(o);
    return v3.decode(v4);
  }

  public static function getCrystalValue(id: Int): Int {
    return Math.round(Math.min(50000, 5 * 100 * Math.round(Math.pow(id + 1, 2))));
  }

  public static function getCrystalTime(id: Int): Int {
    var v3 = [1, 3, 5, 7, 9, 10];
    return v3[Std.int(Math.min(id, v3.length - 1))];
  }

  public static function cleanLeading(s: String): String {
    while (s.substr(0, 1) == " ") {
      s = s.substr(1, s.length);
    }
    while (s.substr(s.length - 1, 1) == " ") {
      s = s.substr(0, s.length - 1);
    }
    return s;
  }

  public static function cleanString(s: String): String {
    s = Data.cleanLeading(s);
    s = Tools.replace(s, String.fromCharCode(13), " ");
    return s;
  }

  public static function leadingZeros(n: Int, zeros: Int): String {
    var v4 = "" + n;
    while (v4.length < zeros) {
      v4 = "0" + v4;
    }
    return v4;
  }

  public static function replaceTag(str: String, char: String, start: String, end: String): String {
    var v6 = str.split(char);
    if (v6.length % 2 == 0) {
      GameManager.warning("invalid string (splitter " + char + "): " + str);
      return str;
    }
    var v7 = "";
    for (v8 in 0...v6.length) {
      if (v8 % 2 != 0) {
        v7 += start + v6[v8] + end;
      } else {
        v7 += v6[v8];
      }
    }
    return v7;
  }

  public static function stime(): Void {
    Data.WATCH = HfStd.getTimer();
  }

  public static function time(n: Int): Void {
    var v3 = HfStd.getTimer() - Data.WATCH;
    Log.trace(n + " : " + v3);
  }

  public static function formatNumber(n: Int): String {
    var v3 = n + "";
    if (v3.indexOf("-", 0) < 0) {
      var v4 = v3.length - 3;
      while (v4 > 0) {
        v3 = v3.substr(0, v4) + "." + v3.substr(v4, v3.length);
        v4 -= 3;
      }
    }
    return v3;
  }

  public static function formatNumberStr(txt: String): String {
    return Data.formatNumber(HfStd.parseInt(txt, 10));
  }

  public static function getLink(did: Int, lid: Int, pid: Int): PortalLink {
    var v5 = null;
    var v6 = 0;
    while (true) {
      if (!(v6 < Data.LINKS.length && v5 == null)) break;
      var v7 = Data.LINKS[v6];
      if (v7.from_did == did && v7.from_lid == lid && v7.from_pid == pid) {
        v5 = v7;
      }
      ++v6;
    }
    return v5;
  }

  /**
   * Width of the Flash document, in pixels.
   */
  public static var DOC_WIDTH: Int = 420;

  /**
   * Height of the Flash document, in pixels.
   */
  public static var DOC_HEIGHT: Int = 520;

  /**
   * Width of the game area, in pixels.
   */
  public static var GAME_WIDTH: Int = 400;

  /**
   * Height of the game area, in pixels.
   */
  public static var GAME_HEIGHT: Int = 500;

  /**
   * Level width, in cases.
   */
  public static var LEVEL_WIDTH: Int = 20;

  /**
   * Level height, in cases.
   */
  public static var LEVEL_HEIGHT: Int = 25;

  /**
   * Case width, in pixels.
   */
  public static var CASE_WIDTH: Int = 20;

  /**
   * Case height. in pixels.
   */
  public static var CASE_HEIGHT: Int = 20;

  /**
   * Duration of a second, in frames.
   */
  public static var SECOND: Int = 32;

  public static var auto_inc: Int = 0;
  public static var DP_SPECIAL_BG: Int = Data.auto_inc++;
  public static var DP_BACK_LAYER: Int = Data.auto_inc++;
  public static var DP_SPRITE_BACK_LAYER: Int = Data.auto_inc++;
  public static var DP_FIELD_LAYER: Int = Data.auto_inc++;
  public static var DP_SPEAR: Int = Data.auto_inc++;
  public static var DP_PLAYER: Int = Data.auto_inc++;
  public static var DP_ITEMS: Int = Data.auto_inc++;
  public static var DP_SHOTS: Int = Data.auto_inc++;
  public static var DP_BADS: Int = Data.auto_inc++;
  public static var DP_BOMBS: Int = Data.auto_inc++;
  public static var DP_FX: Int = Data.auto_inc++;
  public static var DP_SUPA: Int = Data.auto_inc++;
  public static var DP_TOP_LAYER: Int = Data.auto_inc++;
  public static var DP_SPRITE_TOP_LAYER: Int = Data.auto_inc++;
  public static var DP_BORDERS: Int = Data.auto_inc++;
  public static var DP_SCROLLER: Int = Data.auto_inc++;
  public static var DP_INTERF: Int = Data.auto_inc++;
  public static var DP_TOP: Int = Data.auto_inc++;
  public static var DP_SOUNDS: Int = Data.auto_inc++;
  public static var CHAN_MUSIC: Int = 0;
  public static var CHAN_BOMB: Int = 1;
  public static var CHAN_PLAYER: Int = 2;
  public static var CHAN_BAD: Int = 3;
  public static var CHAN_ITEM: Int = 4;
  public static var CHAN_FIELD: Int = 5;
  public static var CHAN_INTERF: Int = 6;
  public static var TRACKS: Array<String> = ["music_ingame", "music_boss"];
  public static var type_bit: Int = 0;
  // TODO: these types may be too restrictive for modders, relax them?
  public static var ENTITY: EntityType<Entity> = EntityType.fromUnchecked(1 << Data.type_bit++);
  public static var PHYSICS: EntityType<Physics> = EntityType.fromUnchecked(1 << Data.type_bit++);
  public static var ITEM: EntityType<Item> = EntityType.fromUnchecked(1 << Data.type_bit++);
  public static var SPECIAL_ITEM: EntityType<SpecialItem> = EntityType.fromUnchecked(1 << Data.type_bit++);
  public static var PLAYER: EntityType<Player> = EntityType.fromUnchecked(1 << Data.type_bit++);
  public static var PLAYER_BOMB: EntityType<PlayerBomb> = EntityType.fromUnchecked(1 << Data.type_bit++);
  public static var BAD: EntityType<Bad> = EntityType.fromUnchecked(1 << Data.type_bit++);
  public static var SHOOT: EntityType<Shoot> = EntityType.fromUnchecked(1 << Data.type_bit++);
  public static var BOMB: EntityType<Bomb> = EntityType.fromUnchecked(1 << Data.type_bit++);
  public static var FX: EntityType<Particle> = EntityType.fromUnchecked(1 << Data.type_bit++);
  public static var SUPA: EntityType<Supa> = EntityType.fromUnchecked(1 << Data.type_bit++);
  public static var CATCHER: EntityType<Fraise> = EntityType.fromUnchecked(1 << Data.type_bit++);
  public static var BALL: EntityType<Ball> = EntityType.fromUnchecked(1 << Data.type_bit++);
  public static var HU_BAD: EntityType<FireBall> = EntityType.fromUnchecked(1 << Data.type_bit++);
  // TODO: introduce a "fake type" for representing Tuberculoz and Bat?
  public static var BOSS: EntityType<Entity> = EntityType.fromUnchecked(1 << Data.type_bit++);
  public static var BAD_BOMB: EntityType<BadBomb> = EntityType.fromUnchecked(1 << Data.type_bit++);
  public static var BAD_CLEAR: EntityType<Bad> = EntityType.fromUnchecked(1 << Data.type_bit++);
  public static var SOCCERBALL: EntityType<SoccerBall> = EntityType.fromUnchecked(1 << Data.type_bit++);
  public static var PLAYER_SHOOT: EntityType<Shoot> = EntityType.fromUnchecked(1 << Data.type_bit++);
  public static var PERFECT_ITEM: EntityType<Item> = EntityType.fromUnchecked(1 << Data.type_bit++);
  public static var SPEAR: EntityType<Spear> = EntityType.fromUnchecked(1 << Data.type_bit++);
  public static var GROUND: Int = 1;
  public static var WALL: Int = 2;
  public static var OUT_WALL: Int = 3;
  public static var HORIZONTAL: Int = 1;
  public static var VERTICAL: Int = 2;
  public static var SCROLL_SPEED: Float = 0.04;
  public static var FADE_SPEED: Float = 8;
  public static var FIELD_TELEPORT: Int = -6;
  public static var FIELD_PORTAL: Int = -7;
  public static var FIELD_GOAL_1: Int = -8;
  public static var FIELD_GOAL_2: Int = -9;
  public static var FIELD_BUMPER: Int = -10;
  public static var FIELD_PEACE: Int = -11;
  public static var HU_STEPS: Array<Float> = [35 * Data.SECOND, 25 * Data.SECOND];
  public static var LEVEL_READ_LENGTH: Int = 1;
  public static var MIN_DARKNESS_LEVEL: Int = 16;
  public static var stat_inc: Int = 0;
  public static var STAT_MAX_COMBO: Int = Data.stat_inc++;
  public static var STAT_SUPAITEM: Int = Data.stat_inc++;
  public static var STAT_KICK: Int = Data.stat_inc++;
  public static var STAT_BOMB: Int = Data.stat_inc++;
  public static var STAT_SHOT: Int = Data.stat_inc++;
  public static var STAT_JUMP: Int = Data.stat_inc++;
  public static var STAT_ICEHIT: Int = Data.stat_inc++;
  public static var STAT_KNOCK: Int = Data.stat_inc++;
  public static var STAT_DEATH: Int = Data.stat_inc++;
  public static var EXTEND_TIMER: Float = 10 * Data.SECOND;
  public static var EXT_MIN_COMBO: Int = 3;
  public static var EXT_MAX_BOMBS: Int = 3;
  public static var EXT_MAX_KICKS: Int = 0;
  public static var EXT_MAX_JUMPS: Int = 10;
  public static var BLINK_DURATION: Float = 2.5;
  public static var BLINK_DURATION_FAST: Float = 1;
  public static var ANIM_PLAYER_STOP: Anim = {id: 0, loop: true};
  public static var ANIM_PLAYER_WALK: Anim = {id: 1, loop: true};
  public static var ANIM_PLAYER_JUMP_UP: Anim = {id: 2, loop: false};
  public static var ANIM_PLAYER_JUMP_DOWN: Anim = {id: 3, loop: false};
  public static var ANIM_PLAYER_JUMP_LAND: Anim = {id: 4, loop: false};
  public static var ANIM_PLAYER_DIE: Anim = {id: 5, loop: true};
  public static var ANIM_PLAYER_KICK: Anim = {id: 6, loop: false};
  public static var ANIM_PLAYER_ATTACK: Anim = {id: 7, loop: false};
  public static var ANIM_PLAYER_EDGE: Anim = {id: 8, loop: true};
  public static var ANIM_PLAYER_WAIT1: Anim = {id: 9, loop: false};
  public static var ANIM_PLAYER_WAIT2: Anim = {id: 10, loop: false};
  public static var ANIM_PLAYER_KNOCK_IN: Anim = {id: 12, loop: false};
  public static var ANIM_PLAYER_KNOCK_OUT: Anim = {id: 13, loop: false};
  public static var ANIM_PLAYER_RESURRECT: Anim = {id: 14, loop: false};
  public static var ANIM_PLAYER_CARROT: Anim = {id: 15, loop: true};
  public static var ANIM_PLAYER_RUN: Anim = {id: 16, loop: true};
  public static var ANIM_PLAYER_SOCCER: Anim = {id: 17, loop: true};
  public static var ANIM_PLAYER_AIRKICK: Anim = {id: 18, loop: false};
  public static var ANIM_PLAYER_STOP_V: Anim = {id: 19, loop: true};
  public static var ANIM_PLAYER_WALK_V: Anim = {id: 20, loop: true};
  public static var ANIM_PLAYER_STOP_L: Anim = {id: 21, loop: true};
  public static var ANIM_PLAYER_WALK_L: Anim = {id: 22, loop: true};
  public static var ANIM_BAD_WALK: Anim = {id: 0, loop: true};
  public static var ANIM_BAD_ANGER: Anim = {id: 1, loop: true};
  public static var ANIM_BAD_FREEZE: Anim = {id: 2, loop: false};
  public static var ANIM_BAD_KNOCK: Anim = {id: 3, loop: true};
  public static var ANIM_BAD_DIE: Anim = {id: 4, loop: true};
  public static var ANIM_BAD_SHOOT_START: Anim = {id: 5, loop: false};
  public static var ANIM_BAD_SHOOT_END: Anim = {id: 6, loop: false};
  public static var ANIM_BAD_THINK: Anim = {id: 7, loop: false};
  public static var ANIM_BAD_JUMP: Anim = {id: 8, loop: true};
  public static var ANIM_BAD_SHOOT_LOOP: Anim = {id: 9, loop: true};
  public static var ANIM_BAT_WAIT: Anim = {id: 0, loop: true};
  public static var ANIM_BAT_MOVE: Anim = {id: 1, loop: true};
  public static var ANIM_BAT_SWITCH: Anim = {id: 2, loop: false};
  public static var ANIM_BAT_DIVE: Anim = {id: 3, loop: false};
  public static var ANIM_BAT_INTRO: Anim = {id: 4, loop: false};
  public static var ANIM_BAT_KNOCK: Anim = {id: 5, loop: true};
  public static var ANIM_BAT_FINAL_DIVE: Anim = {id: 6, loop: true};
  public static var ANIM_BAT_ANGER: Anim = {id: 7, loop: true};
  public static var ANIM_BOSS_WAIT: Anim = {id: 0, loop: true};
  public static var ANIM_BOSS_SWITCH: Anim = {id: 1, loop: false};
  public static var ANIM_BOSS_JUMP_UP: Anim = {id: 2, loop: false};
  public static var ANIM_BOSS_JUMP_DOWN: Anim = {id: 3, loop: false};
  public static var ANIM_BOSS_JUMP_LAND: Anim = {id: 4, loop: false};
  public static var ANIM_BOSS_TORNADO_START: Anim = {id: 5, loop: false};
  public static var ANIM_BOSS_TORNADO_END: Anim = {id: 6, loop: false};
  public static var ANIM_BOSS_BAT_FORM: Anim = {id: 7, loop: false};
  public static var ANIM_BOSS_BURN_START: Anim = {id: 8, loop: false};
  public static var ANIM_BOSS_DEATH: Anim = {id: 9, loop: false};
  public static var ANIM_BOSS_DASH_START: Anim = {id: 10, loop: false};
  public static var ANIM_BOSS_DASH: Anim = {id: 11, loop: false};
  public static var ANIM_BOSS_BOMB: Anim = {id: 12, loop: false};
  public static var ANIM_BOSS_HIT: Anim = {id: 13, loop: false};
  public static var ANIM_BOSS_DASH_BUILD: Anim = {id: 14, loop: true};
  public static var ANIM_BOSS_BURN_LOOP: Anim = {id: 15, loop: true};
  public static var ANIM_BOSS_TORNADO_LOOP: Anim = {id: 16, loop: true};
  public static var ANIM_BOSS_DASH_LOOP: Anim = {id: 17, loop: true};
  public static var ANIM_BOMB_DROP: Anim = {id: 0, loop: false};
  public static var ANIM_BOMB_LOOP: Anim = {id: 1, loop: true};
  public static var ANIM_BOMB_EXPLODE: Anim = {id: 2, loop: false};
  public static var ANIM_WBOMB_STOP: Anim = {id: 0, loop: true};
  public static var ANIM_WBOMB_WALK: Anim = {id: 1, loop: true};
  public static var ANIM_SHOOT: Anim = {id: 0, loop: false};
  public static var ANIM_SHOOT_LOOP: Anim = {id: 0, loop: true};
  public static var MAX_ITERATION: Int = 30;
  public static var flag_bit: Int = 0;
  public static var GRID_NAMES: Array<String> = ["$IA_TILE_TOP", "$IA_ALLOW_FALL", "$IA_BORDER", "$IA_SMALLSPOT", "$IA_FALL_SPOT", "$IA_JUMP_UP", "$IA_JUMP_DOWN", "$IA_JUMP_LEFT", "$IA_JUMP_RIGHT", "$IA_TILE", "$IA_CLIMB_LEFT", "$IA_CLIMB_RIGHT", "$FL_TELEPORTER"];
  public static var IA_TILE_TOP: Int = 1 << Data.flag_bit++;
  public static var IA_ALLOW_FALL: Int = 1 << Data.flag_bit++;
  public static var IA_BORDER: Int = 1 << Data.flag_bit++;
  public static var IA_SMALL_SPOT: Int = 1 << Data.flag_bit++;
  public static var IA_FALL_SPOT: Int = 1 << Data.flag_bit++;
  public static var IA_JUMP_UP: Int = 1 << Data.flag_bit++;
  public static var IA_JUMP_DOWN: Int = 1 << Data.flag_bit++;
  public static var IA_JUMP_LEFT: Int = 1 << Data.flag_bit++;
  public static var IA_JUMP_RIGHT: Int = 1 << Data.flag_bit++;
  public static var IA_TILE: Int = 1 << Data.flag_bit++;
  public static var IA_CLIMB_LEFT: Int = 1 << Data.flag_bit++;
  public static var IA_CLIMB_RIGHT: Int = 1 << Data.flag_bit++;
  public static var FL_TELEPORTER: Int = 1 << Data.flag_bit++;
  public static var IA_HJUMP: Int = 2;
  public static var IA_VJUMP: Int = 2;
  public static var IA_CLIMB: Int = 4;
  public static var IA_CLOSE_DISTANCE: Float = 110;
  public static var ACTION_MOVE: Int = Data.auto_inc++;
  public static var ACTION_WALK: Int = Data.auto_inc++;
  public static var ACTION_SHOOT: Int = Data.auto_inc++;
  public static var ACTION_FALLBACK: Int = Data.auto_inc++;
  public static var EVENT_EXIT_RIGHT: Int = 1;
  public static var EVENT_BACK_RIGHT: Int = 2;
  public static var EVENT_EXIT_LEFT: Int = 3;
  public static var EVENT_BACK_LEFT: Int = 4;
  public static var EVENT_DEATH: Int = 5;
  public static var EVENT_EXTEND: Int = 6;
  public static var EVENT_TIME: Int = 7;
  public static var BORDER_MARGIN: Float = Data.CASE_WIDTH / 2;
  public static var FRICTION_X: Float = 0.93;
  public static var FRICTION_Y: Float = 0.86;
  public static var FRICTION_GROUND: Float = 0.7;
  public static var FRICTION_SLIDE: Float = 0.97;
  public static var GRAVITY: Float = 1.0;
  public static var FALL_FACTOR_FROZEN: Float = 2.3;
  public static var FALL_FACTOR_KNOCK: Float = 1.5;
  public static var FALL_FACTOR_DEAD: Float = 1.75;
  public static var FALL_SPEED: Float = 0.9;
  public static var STEP_MAX: Float = Data.CASE_WIDTH;
  public static var DEATH_LINE: Float = Data.GAME_HEIGHT + 50;
  public static var MAX_FX: Int = 16;
  public static var DUST_FALL_HEIGHT: Float = Data.CASE_HEIGHT * 4;
  public static var PARTICLE_ICE: Int = 1;
  public static var PARTICLE_CLASSIC_BOMB: Int = 2;
  public static var PARTICLE_STONE: Int = 3;
  public static var PARTICLE_SPARK: Int = 4;
  public static var PARTICLE_DUST: Int = 5;
  public static var PARTICLE_ORANGE: Int = 6;
  public static var PARTICLE_METAL: Int = 7;
  public static var PARTICLE_TUBERCULOZ: Int = 8;
  public static var PARTICLE_RAIN: Int = 9;
  public static var PARTICLE_LITCHI: Int = 10;
  public static var PARTICLE_PORTAL: Int = Data.PARTICLE_SPARK;
  public static var PARTICLE_BUBBLE: Int = 11;
  public static var PARTICLE_ICE_BAD: Int = 12;
  public static var PARTICLE_BLOB: Int = 13;
  public static var PARTICLE_FRAMB: Int = 14;
  public static var PARTICLE_FRAMB_SMALL: Int = 14;
  public static var BG_STAR: Int = 0;
  public static var BG_FLASH: Int = 1;
  public static var BG_ORANGE: Int = 2;
  public static var BG_FIREBALL: Int = 3;
  public static var BG_HYPNO: Int = 4;
  public static var BG_CONSTEL: Int = 5;
  public static var BG_JAP: Int = 6;
  public static var BG_GHOSTS: Int = 7;
  public static var BG_FIRE: Int = 8;
  public static var BG_PYRAMID: Int = 9;
  public static var BG_SINGER: Int = 10;
  public static var BG_STORM: Int = 11;
  public static var BG_GUU: Int = 12;
  public static var BG_SOCCER: Int = 13;
  public static var PLAYER_SPEED: Float = 4.3;
  public static var PLAYER_JUMP: Float = 18.7;
  public static var PLAYER_HKICK_X: Float = 3.5;
  public static var PLAYER_HKICK_Y: Float = 7.4;
  public static var PLAYER_VKICK: Float = 18;
  public static var PLAYER_AIR_JUMP: Float = 7;
  public static var WBOMB_SPEED: Float = Data.PLAYER_SPEED * 1.5;
  public static var KICK_DISTANCE: Float = Data.CASE_WIDTH;
  public static var AIR_KICK_DISTANCE: Float = Data.CASE_WIDTH * 1.5;
  public static var SHIELD_DURATION: Float = Data.SECOND * 5;
  public static var WEAPON_DURATION: Float = Data.SECOND * 30;
  public static var SUPA_DURATION: Float = Data.SECOND * 30;
  public static var EXTRA_LIFE_STEPS: Array<Int> = [100000, 500000, 1000000, 2000000, 3000000, 4000000];
  public static var TELEPORTER_DISTANCE: Float = Data.CASE_WIDTH * 4;
  public static var BASE_COLORS: Array<Int> = [16777215, 16048275, 5592575, 16496207];
  public static var DARK_COLORS: Array<Int> = [7366029, 13975552, 0, 0];
  public static var CURSE_PEACE: Int = 1;
  public static var CURSE_SHRINK: Int = 2;
  public static var CURSE_SLOW: Int = 3;
  public static var CURSE_TAUNT: Int = 4;
  public static var CURSE_MULTIPLY: Int = 5;
  public static var CURSE_FALL: Int = 6;
  public static var CURSE_MARIO: Int = 7;
  public static var CURSE_TRAITOR: Int = 8;
  public static var CURSE_GOAL: Int = 9;
  public static var EDGE_TIMER: Float = Data.SECOND * 0.2;
  public static var WAIT_TIMER: Float = Data.SECOND * 8;
  public static var WEAPON_B_CLASSIC: Int = 1;
  public static var WEAPON_B_BLACK: Int = 2;
  public static var WEAPON_B_BLUE: Int = 3;
  public static var WEAPON_B_GREEN: Int = 4;
  public static var WEAPON_B_RED: Int = 5;
  public static var WEAPON_B_REPEL: Int = 9;
  public static var WEAPON_NONE: Int = -1;
  public static var WEAPON_S_ARROW: Int = 6;
  public static var WEAPON_S_FIRE: Int = 7;
  public static var WEAPON_S_ICE: Int = 8;
  public static var HEAD_NORMAL: Int = 1;
  public static var HEAD_AFRO: Int = 2;
  public static var HEAD_CERBERE: Int = 3;
  public static var HEAD_PIOU: Int = 4;
  public static var HEAD_MARIO: Int = 5;
  public static var HEAD_TUB: Int = 6;
  public static var HEAD_IGORETTE: Int = 7;
  public static var HEAD_LOSE: Int = 8;
  public static var HEAD_CROWN: Int = 9;
  public static var HEAD_SANDY: Int = 10;
  public static var HEAD_SANDY_LOSE: Int = 11;
  public static var HEAD_SANDY_CROWN: Int = 12;
  public static var PEACE_COOLDOWN: Float = Data.SECOND * 3;
  public static var AUTO_ANGER: Float = Data.SECOND * 4;
  public static var MAX_ANGER: Int = 3;
  public static var BALL_TIMEOUT: Float = 2.3 * Data.SECOND;
  public static var BAD_HJUMP_X: Float = 5.5;
  public static var BAD_HJUMP_Y: Float = 8.5;
  public static var BAD_VJUMP_X_CLIFF: Float = 2.2;
  public static var BAD_VJUMP_X: Float = 1.3;
  public static var BAD_VJUMP_Y: Float = 19;
  public static var BAD_VJUMP_Y_LIST: Array<Float> = [11, 14, 19];
  public static var BAD_VDJUMP_Y: Float = 6.5;
  public static var FREEZE_DURATION: Float = Data.SECOND * 5;
  public static var KNOCK_DURATION: Float = Data.SECOND * 3.75;
  public static var PLAYER_KNOCK_DURATION: Float = Data.SECOND * 2.5;
  public static var ICE_HIT_MIN_SPEED: Float = 4;
  public static var ICE_KNOCK_MIN_SPEED: Float = 2;
  public static var BAD_POMME: Int = 0;
  public static var BAD_CERISE: Int = 1;
  public static var BAD_BANANE: Int = 2;
  public static var BAD_FIREBALL: Int = 3;
  public static var BAD_ANANAS: Int = 4;
  public static var BAD_ABRICOT: Int = 5;
  public static var BAD_ABRICOT2: Int = 6;
  public static var BAD_POIRE: Int = 7;
  public static var BAD_BOMBE: Int = 8;
  public static var BAD_ORANGE: Int = 9;
  public static var BAD_FRAISE: Int = 10;
  public static var BAD_CITRON: Int = 11;
  public static var BAD_BALEINE: Int = 12;
  public static var BAD_SPEAR: Int = 13;
  public static var BAD_CRAWLER: Int = 14;
  public static var BAD_TZONGRE: Int = 15;
  public static var BAD_SAW: Int = 16;
  public static var BAD_LITCHI: Int = 17;
  public static var BAD_KIWI: Int = 18;
  public static var BAD_LITCHI_WEAK: Int = 19;
  public static var BAD_FRAMBOISE: Int = 20;
  public static var LINKAGES: Array<String> = Data.initLinkages();
  public static var BAT_LEVEL: Int = 100;
  public static var TUBERCULOZ_LEVEL: Int = 101;
  public static var BOSS_BAT_MIN_DIST: Float = Data.CASE_WIDTH * 7;
  public static var BOSS_BAT_MIN_X_DIST: Float = Data.CASE_WIDTH * 3;
  public static var MAX_ITEMS: Int = 300;
  public static var ITEM_LIFE_TIME: Float = 8 * Data.SECOND;
  public static var DIAMANT: Int = 8;
  public static var CONVERT_DIAMANT: Int = 17;
  public static var EXTENDS: Array<String> = ["C", "R", "Y", "S", "T", "A", "L"];
  public static var SPECIAL_ITEM_TIMER: Float = 8 * Data.SECOND;
  public static var SCORE_ITEM_TIMER: Float = 12 * Data.SECOND;
  public static var __NA: Int = 0;
  public static var COMM: Int = 2000;
  public static var UNCO: Int = 1000;
  public static var RARE: Int = 300;
  public static var UNIQ: Int = 100;
  public static var MYTH: Int = 10;
  public static var CANE: Int = 60;
  public static var LEGEND: Int = 1;
  public static var RARITY: Array<Int> = [0, Data.COMM, Data.UNCO, Data.RARE, Data.UNIQ, Data.MYTH, Data.LEGEND, Data.CANE];
  public static var RAND_EXTENDS_ID: Int = Data.auto_inc++;
  public static var RAND_ITEMS_ID: Int = Data.auto_inc++;
  public static var RAND_SCORES_ID: Int = Data.auto_inc++;
  public static var RAND_EXTENDS: Array<Int> = [10, 10, 6, 5, 5, 10, 4];
  public static var OPT_MIRROR: String = "$mirror".substring(1);
  public static var OPT_MIRROR_MULTI: String = "$mirrormulti".substring(1);
  public static var OPT_NIGHTMARE_MULTI: String = "$nightmaremulti".substring(1);
  public static var OPT_NIGHTMARE: String = "$nightmare".substring(1);
  public static var OPT_LIFE_SHARING: String = "$lifesharing".substring(1);
  public static var OPT_SOCCER_BOMBS: String = "$soccerbomb".substring(1);
  public static var OPT_KICK_CONTROL: String = "$kickcontrol".substring(1);
  public static var OPT_BOMB_CONTROL: String = "$bombcontrol".substring(1);
  public static var OPT_NINJA: String = "$ninja".substring(1);
  public static var OPT_BOMB_EXPERT: String = "$bombexpert".substring(1);
  public static var OPT_BOOST: String = "$boost".substring(1);
  public static var OPT_SET_TA_0: String = "$set_ta_0".substring(1);
  public static var OPT_SET_TA_1: String = "$set_ta_1".substring(1);
  public static var OPT_SET_TA_2: String = "$set_ta_2".substring(1);
  public static var OPT_SET_MTA_0: String = "$set_mta_0".substring(1);
  public static var OPT_SET_MTA_1: String = "$set_mta_1".substring(1);
  public static var OPT_SET_MTA_2: String = "$set_mta_2".substring(1);
  public static var OPT_SET_SOC_0: String = "$set_soc_0".substring(1);
  public static var OPT_SET_SOC_1: String = "$set_soc_1".substring(1);
  public static var OPT_SET_SOC_2: String = "$set_soc_2".substring(1);
  public static var OPT_SET_SOC_3: String = "$set_soc_3".substring(1);
  public static var FIELDS: Array<Null<String>> = [null, "BASE ", "noir ", "bleu ", "vert ", "rouge ", "warp ", "portal ", "goal 1", "goal 2", "bumper ", "peace "];
  public static var MAX_TILES: Int = 53;
  public static var MAX_BG: Int = 30;
  public static var MAX_BADS: Int = 20;
  public static var MAX_FIELDS: Int = 11;
  public static var TOOL_TILE: Int = Data.auto_inc++;
  public static var TOOL_BAD: Int = Data.auto_inc++;
  public static var TOOL_FIELD: Int = Data.auto_inc++;
  public static var TOOL_START: Int = Data.auto_inc++;
  public static var TOOL_SPECIAL: Int = Data.auto_inc++;
  public static var TOOL_SCORE: Int = Data.auto_inc++;
  public static var WATCH: Int = 0;
}
