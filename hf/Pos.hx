package hf;

typedef Pos<T> = {
  var x: T;
  var y: T;
}
