package hf.levels;

import hf.Data in HfData;

class Data {
  public var __dollar__map: Array<Array<Int>>;
  public var __dollar__playerX: Int;
  public var __dollar__playerY: Int;
  public var __dollar__skinBg: Int;
  public var __dollar__skinTiles: Int;
  public var __dollar__badList: Array<Dynamic /*TODO*/>;
  public var __dollar__specialSlots: Array<Dynamic /*TODO*/>;
  public var __dollar__scoreSlots: Array<Dynamic /*TODO*/>;
  public var __dollar__script: String;

  public function new(): Void {
    this.__dollar__map = new Array();
    for (v2 in 0...HfData.LEVEL_WIDTH) {
      this.__dollar__map[v2] = new Array();
      for (v3 in 0...HfData.LEVEL_HEIGHT) {
        this.__dollar__map[v2][v3] = 0;
      }
    }
    this.__dollar__playerX = 0;
    this.__dollar__playerY = 0;
    this.__dollar__skinBg = 1;
    this.__dollar__skinTiles = 1;
    this.__dollar__badList = new Array();
    this.__dollar__specialSlots = new Array();
    this.__dollar__scoreSlots = new Array();
    this.__dollar__script = "";
  }
}
