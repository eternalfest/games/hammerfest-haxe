package hf.entity.bad;

import hf.entity.Bad;
import hf.Entity;
import hf.mode.GameMode;

class Spear extends Bad {
  public var skin: Int;

  public function new(): Void {
    super();
    this.disablePhysics();
    this.disableAnimator();
    this.skin = 2;
    this.realRadius = Data.CASE_WIDTH * 0.7;
  }

  public static function attach(g: GameMode, x: Float, y: Float): Spear {
    var v5 = Data.LINKAGES[Data.BAD_SPEAR];
    var v6: Spear = cast g.depthMan.attach(v5, Data.DP_SPEAR);
    v6.initBad(g, x, y);
    return v6;
  }

  override public function burn(): Void {
    var v2 = this.game.fxMan.attachFx(this.x, this.y, "hammer_fx_pop");
  }

  override public function freeze(d: Float): Void {
  }

  override public function hit(e: Entity): Void {
    super.hit(e);
    var v4 = Data.BAD_CLEAR.downcast(e);
    if (v4 != null) {
      if (v4.fl_physics && v4.fl_trap) {
        v4.killHit(null);
      }
    }
  }

  override public function initBad(g: GameMode, x: Float, y: Float): Void {
    super.initBad(g, x, y);
    if (this.world.getCase({x: this.cx, y: this.cy + 1}) > 0) {
      this.gotoAndStop("1");
    } else {
      if (this.world.getCase({x: this.cx, y: this.cy - 1}) > 0) {
        this.gotoAndStop("3");
      } else {
        if (this.world.getCase({x: this.cx - 1, y: this.cy}) > 0) {
          this.gotoAndStop("2");
        } else {
          if (this.world.getCase({x: this.cx + 1, y: this.cy}) > 0) {
            this.gotoAndStop("4");
          }
        }
      }
    }
    var v6 = this.game.getDynamicVar("$SPEAR_SKIN");
    if (v6 == null) {
      this.sub.gotoAndStop("1");
    } else {
      this.sub.gotoAndStop(v6);
    }
    if (this.game.world.scriptEngine.cycle > Data.SECOND) {
      this.game.fxMan.attachFx(x + Data.CASE_WIDTH * 0.5, y + Data.CASE_HEIGHT * 0.5, "hammer_fx_pop");
    }
  }

  override public function init(g: GameMode): Void {
    super.init(g);
    this.unregister(Data.BAD_CLEAR);
    this.register(Data.SPEAR);
  }

  override public function killHit(dx: Null<Float>): Void {
  }

  override public function knock(d: Float): Void {
  }
}
