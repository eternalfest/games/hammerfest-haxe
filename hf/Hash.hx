package hf;

@:native("Hash") // Class provided by the loader
extern class Hash {
  public function new(): Void;

  public function get(k: String): Dynamic;

  public function set(k: String, i: Dynamic): Void;

  public function remove(k: String): Bool;

  public function exists(k: String): Bool;

  public function iter(f: String -> Dynamic -> Void): Void;
}
