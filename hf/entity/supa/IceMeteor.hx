package hf.entity.supa;

import hf.entity.Supa;
import hf.mode.GameMode;

class IceMeteor extends Supa {
  public function new(): Void {
    super();
  }

  public static function attach(g: GameMode): IceMeteor {
    var v3 = "hammer_supa_icemeteor";
    var v4: IceMeteor = cast g.depthMan.attach(v3, Data.DP_SUPA);
    v4.initSupa(g, Data.GAME_WIDTH, 0);
    return v4;
  }

  override public function initSupa(g: GameMode, x: Float, y: Float): Void {
    super.initSupa(g, x, y);
    this.speed = 10;
    this.radius = 50;
    this.moveToAng(130, this.speed);
    this.setLifeTimer(Data.SUPA_DURATION);
  }

  override public function postfix(): Void {
    super.postfix();
    this.rotation -= 7 * Timer.tmod;
    if (!this.world.shapeInBound(this)) {
      this.moveTo(Data.GAME_WIDTH, 0);
    }
  }

  override public function prefix(): Void {
    super.prefix();
    var v3 = this.game.getClose(Data.BAD, this.x, this.y, this.radius, false);
    for (v4 in 0...v3.length) {
      var v5 = v3[v4];
      if (!v5.fl_freeze) {
        v5.freeze(Data.FREEZE_DURATION);
        v5.dx = this.dx * 2;
        v5.dy = -5;
      }
    }
  }
}
