package hf.mode;

import hf.levels.SetManager;
import hf.Mode;
import etwin.flash.System;
import etwin.flash.Key;
import etwin.flash.XML;

class Converter extends Mode {
  public var currentSet: Int;
  public var world: Null<SetManager>;

  public static var SETS: Array<String> = ["$tutorial", "$adventure"];

  public function new(m: Dynamic /*TODO*/): Void {
    super(m);
    this._name = "$converter";
    Log.trace("Hit ENTER to start.");
    Log.trace("ESCAPE to quit.");
    this.currentSet = 0;
  }

  public function readNext(): Void {
    this.world = new hf.levels.SetManager(this.manager, hf.mode.Converter.SETS[this.currentSet]);
    this.world.goto(0);
    Log.trace("Reading set " + this.currentSet + " : " + this.world.setName);
    Log.trace("Content : " + this.world.levels.length + " levels");
    ++this.currentSet;
  }

  public function convert(): Void {
    var v2 = 100;
    var v3 = new Array();
    Log.trace("------");
    for (v4 in 0...this.world.levels.length) {
      this.world.goto(v4);
      var v5 = this.world.levels[v4];
      Log.trace("> Processing " + v4 + "...");
      var v6 = new XML(v5.__dollar__script);
      var v7 = v6.firstChild;
      while (v7 != null) {
        var v8 = v7.firstChild;
        while (v8 != null) {
          if (v8.nodeName == "$e_msg" || v8.nodeName == "$e_tuto") {
            var v9 = v8.firstChild.nodeValue;
            if (v9 != null) {
              v9 = Tools.replace(v9, String.fromCharCode(13), "");
              v9 = Tools.replace(v9, String.fromCharCode(10), "");
              Log.trace("FOUND: " + v9);
              v3.push("<t id=\"" + v2 + "\" v=\"" + v9 + "\"/>");
              ++v2;
            }
          }
          v8 = v8.nextSibling;
        }
        v7 = v7.nextSibling;
      }
    }
    Log.trace("Done, copied to system clipboard.");
    Log.trace("Hit ENTER to start next one.");
    System.setClipboard(v3.join("\n"));
  }

  public function serialize(d: Dynamic /*TODO*/): String {
    return (new PersistCodec()).encode(d);
  }

  public function unserialize(s: String): Dynamic /*TODO*/ {
    return (new PersistCodec()).decode(s);
  }

  override public function main(): Void {
    super.main();
    if (Key.isDown(Key.ENTER) || Key.isDown(Key.SPACE)) {
      if (hf.mode.Converter.SETS[this.currentSet] == null) {
        Log.trace("All done.");
      } else {
        this.readNext();
        this.convert();
      }
    }
    if (Key.isDown(Key.ESCAPE)) {
      Log.clear();
      this.endMode();
    }
  }
}
