package etwin.ds;

import haxe.ds.EnumValueMap;
import haxe.ds.IntMap;
import haxe.ds.ObjectMap;
import haxe.ds.StringMap;
import haxe.macro.Expr;
import Map.IMap;

/**
 * An unordered collection of unique elements.
 */
@:multiType(T)
@:forward(exists, iterator, remove, toString)
abstract Set<T>(IMap<T, T>) {
  public function new();

  public inline function add(item: T): Void {
    this.set(item, item);
  }

  public inline function addAll(items: Iterable<T>): Void {
    for (item in items) {
      add(item);
    }
  }

  /**
   * Creates a new `Set` containing the elements of the given iterable.
   * Example: `Set.from(["foo", "bar"])`
   */
  macro public static function from<T>(expr: ExprOf<Iterable<T>>): ExprOf<Set<T>> {
    return macro etwin.ds.Set.fromMap([for (item in $expr) item => item]);
  }

  /**
   * Creates a new `Set` containing the given elements.
   * Example: `Set.of("foo", "bar")`
   */
  macro public static function of<T>(items: Array<ExprOf<T>>): ExprOf<Set<T>> {
    var stmts = [];
    var exprs = [];
    for (item in items) {
      var isLiteral = item.expr.match(EConst(_)) && !item.expr.match(EConst(CRegexp(_)));
      if (isLiteral) {
        exprs.push(macro $item => $item);
      } else {
        var name = "__item" + exprs.length;
        stmts.push(macro var $name = $item);
        exprs.push(macro $i{name} => $i{name});
      }
    }
    stmts.push(macro etwin.ds.Set.fromMap([$a{exprs}]));
    return macro $b{stmts};
  }

  @:to static inline function toStringMap(t: IMap<String, String>): IMap<String, String> {
    return new StringMap();
  }

  @:to static inline function toIntMap(t: IMap<Int, Int>): IMap<Int, Int> {
    return new IntMap();
  }

  @:to static inline function toEnumValueMap<T: EnumValue>(t: IMap<T, T>): IMap<T, T> {
    return new EnumValueMap();
  }

  @:to static inline function toObjectMap<T: {}>(t: IMap<T, T>): IMap<T, T> {
    return new ObjectMap();
  }

  @:from static inline function fromMap<T>(map: Map<T, T>): Set<T> {
    return cast map;
  }
}
