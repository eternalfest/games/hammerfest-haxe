package hf.entity.shoot;

import hf.entity.Shoot;
import hf.Entity;
import hf.mode.GameMode;

class Zeste extends Shoot {
  public function new(): Void {
    super();
    this.shootSpeed = 6;
  }

  public static function attach(g: GameMode, x: Float, y: Float): Zeste {
    var v5 = "hammer_shoot_zest";
    var v6: Zeste = cast g.depthMan.attach(v5, Data.DP_SHOTS);
    v6.initShoot(g, x, y);
    return v6;
  }

  override public function hit(e: Entity): Void {
    var v3 = Data.PLAYER.downcast(e);
    if (v3 != null) {
      if (Math.abs(e.x - this.x) <= Data.CASE_WIDTH * 0.65) {
        v3.killHit((e.x - this.x) * 1.5);
        this.game.fxMan.attachExplodeZone(this.x, this.y, 15);
      }
    }
  }

  override public function moveDown(s: Float): Void {
    super.moveDown(s);
    this._yscale = -this._yscale;
    this._yOffset = -Data.CASE_HEIGHT;
  }
}
