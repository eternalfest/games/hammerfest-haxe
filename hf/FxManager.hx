package hf;

import etwin.flash.MovieClip;
import etwin.flash.DynamicMovieClip;
import hf.Animation;
import hf.mode.GameMode;

typedef DelayedFx = {
  var t: Float;
  var x: Float;
  var y: Float;
  var link: String;
}

typedef SpecialBg = {
  var id: Int;
  var subId: Int;
  var timer: Float;
}

class FxManager {
  public var game: GameMode;
  public var animList: Array<Animation>;
  public var bgList: Array<SpecialBg>;
  public var mcList: Array<MovieClip>;
  public var stack: Array<DelayedFx>;
  public var fl_bg: Bool;
  public var levelName: DynamicMovieClip /* TODO */;
  public var nameTimer: Float;
  public var lastAlert: MovieClip;
  public var mc_exitArrow: MovieClip;
  public var igMsg: DynamicMovieClip /* TODO */;

  public function new(g: GameMode): Void {
    this.game = g;
    this.animList = new Array();
    this.bgList = new Array();
    this.mcList = new Array();
    this.stack = new Array();
    this.fl_bg = false;
  }

  public function attachLevelPop(name: String, fl_label: Bool): Void {
    if (name != null) {
      this.levelName.removeMovieClip();
      this.levelName = ((cast this.game.depthMan.attach("hammer_interf_zone", Data.DP_INTERF)): etwin.flash.DynamicMovieClip);
      this.levelName._x = -10;
      this.levelName._y = Data.GAME_HEIGHT - 1;
      this.levelName.field.text = name;
      FxManager.addGlow(this.levelName, 0, 2);
      if (fl_label) {
        this.levelName.label.text = Lang.get(13);
      } else {
        this.levelName.label.text = "";
      }
      this.nameTimer = Data.SECOND * 5;
    }
  }

  public function attachAlert(str: String): MovieClip {
    var v3: etwin.flash.DynamicMovieClip = cast this.game.depthMan.attach("hurryUp", Data.DP_INTERF);
    v3._x = Data.GAME_WIDTH / 2;
    v3._y = Data.GAME_HEIGHT / 2;
    v3.label = str;
    this.mcList.push(v3);
    this.lastAlert = v3;
    return v3;
  }

  public function detachLastAlert(): Void {
    var v2 = 0;
    while (v2 < this.mcList.length) {
      if (this.mcList[v2]._name == this.lastAlert._name) {
        this.mcList.splice(v2, 1);
        --v2;
      }
      ++v2;
    }
    this.lastAlert.removeMovieClip();
  }

  public function attachHurryUp(): MovieClip {
    return this.attachAlert(Lang.get(4));
  }

  public function attachWarning(): MovieClip {
    return this.attachAlert(Lang.get(12));
  }

  public function attachExit(): Void {
    this.detachExit();
    var v2: etwin.flash.DynamicMovieClip = cast this.game.depthMan.attach("hammer_fx_exit", Data.DP_INTERF);
    v2._x = Data.GAME_WIDTH / 2;
    v2._y = Data.GAME_HEIGHT;
    v2.label = Lang.get(3);
    this.mc_exitArrow = v2;
  }

  public function detachExit(): Void {
    this.mc_exitArrow.removeMovieClip();
  }

  public function attachEnter(x: Float, pid: Int): Void {
    var v4: etwin.flash.DynamicMovieClip = cast this.game.depthMan.attach("hammer_fx_enter", Data.DP_INTERF);
    v4._x = x;
    v4._y = 0;
    var v5 = v4.field;
    if (pid == 0) {
      v5.text = "";
    } else {
      v5.text = "Player " + pid;
      v5.textColor = Data.BASE_COLORS[pid - 1];
    }
    this.mcList.push(v4);
  }

  public function attachScorePop(color: Int, glowColor: Int, x: Float, y: Float, txt: String): Void {
    var v7 = this.attachFx(x, y, "popScore");
    v7.fl_loop = false;
    txt = Data.formatNumberStr(txt);
    FxManager.addGlow(v7.mc, glowColor, 2);
    v7.mc.label.field.textColor = color;
    v7.mc.value = txt;
  }

  public function attachExplodeZone(x: Float, y: Float, radius: Float): Animation {
    if (this.game.fl_lock) {
      return null;
    }
    var v5 = this.attachFx(x, y, "explodeZone");
    v5.mc._width = radius * 2;
    v5.mc._height = v5.mc._width;
    return v5;
  }

  public function attachExplosion(x: Float, y: Float, radius: Float): Animation {
    if (this.game.fl_lock) {
      return null;
    }
    var v5 = this.attachFx(x, y, "explodeZone");
    v5.mc._width = radius * 2;
    v5.mc._height = v5.mc._width;
    v5.mc.blendMode = BlendMode.OVERLAY;
    return v5;
  }

  public function attachShine(x: Float, y: Float): Animation {
    if (this.game.fl_lock) {
      return null;
    }
    var v4 = this.attachFx(x, y, "shine");
    v4.mc._xscale *= 1.5;
    v4.mc._yscale = v4.mc._xscale;
    v4.mc._xscale *= HfStd.random(2) * 2 - 1;
    return v4;
  }

  public function keyRequired(kid: Int): Void {
    this.igMsg.removeMovieClip();
    this.igMsg = ((cast this.game.depthMan.attach("hammer_interf_inGameMsg", Data.DP_TOP)): etwin.flash.DynamicMovieClip);
    this.igMsg.label.text = Lang.get(40);
    this.igMsg.field.text = Lang.getKeyName(kid);
    FxManager.addGlow(this.igMsg, 0, 2);
    this.igMsg.timer = Data.SECOND * 2;
  }

  public function keyUsed(kid: Int): Void {
    this.igMsg.removeMovieClip();
    this.igMsg = ((cast this.game.depthMan.attach("hammer_interf_inGameMsg", Data.DP_TOP)): etwin.flash.DynamicMovieClip);
    this.igMsg.label.text = Lang.get(41);
    this.igMsg.field.text = Lang.getKeyName(kid);
    FxManager.addGlow(this.igMsg, 0, 2);
    this.igMsg.timer = Data.SECOND * 3;
  }

  public function attachFx(x: Float, y: Float, link: String): Animation {
    if (this.game.fl_lock) {
      return null;
    }
    var v5 = new Animation(this.game);
    v5.attach(x, y, link, Data.DP_FX);
    this.animList.push(v5);
    return v5;
  }

  public function dust(cx: Int, cy: Int): Void {
    if (!GameManager.CONFIG.fl_detail) {
      return;
    }
    var v4 = Entity.x_ctr(cx);
    var v5 = Entity.y_ctr(cy);
    var v6 = 7;
    var v7 = v4 - Data.CASE_WIDTH * 0.5;
    var v8 = v4 + Data.CASE_WIDTH * 0.5;
    if (this.game.world.getCase({x: cx - 1, y: cy}) == Data.GROUND) {
      v7 -= Data.CASE_WIDTH;
    }
    if (this.game.world.getCase({x: cx + 1, y: cy}) == Data.GROUND) {
      v8 += Data.CASE_WIDTH;
    }
    var v9 = Math.round(v8 - v7);
    for (v10 in 0...v6) {
      var v11 = this.attachFx(v7 + HfStd.random(v9), v5, "hammer_fx_dust");
      v11.mc._xscale = HfStd.random(50) + 50 * (HfStd.random(2) * 2 - 1);
      v11.mc._yscale = HfStd.random(80) + 10;
      v11.mc._alpha = HfStd.random(50) + 50;
      v11.mc.gotoAndStop("" + (HfStd.random(5) + 5));
    }
  }

  public function delayFx(t: Float, x: Float, y: Float, link: String): Void {
    this.stack.push({t: t, x: x, y: y, link: link});
  }

  public function inGameParticles(id: Int, x: Float, y: Float, n: Int): Void {
    this.inGameParticlesDir(id, x, y, n, null);
  }

  public function inGameParticlesDir(id: Int, x: Float, y: Float, n: Int, dir: Null<Float>): Void {
    if (this.game.fl_lock) {
      return;
    }
    if (!GameManager.CONFIG.fl_detail) {
      return;
    }
    var v7 = this.game.getList(Data.FX);
    if (v7.length + n > Data.MAX_FX) {
      n = Math.ceil(n * 0.5);
      this.game.destroySome(Data.FX, n + v7.length - Data.MAX_FX);
    }
    var v8 = (HfStd.random(2) != 0) ? false : true;
    for (v9 in 0...n) {
      var v10 = hf.entity.fx.Particle.attach(this.game, id, x, y);
      if (x <= Data.CASE_WIDTH) {
        v8 = false;
      }
      if (x >= Data.GAME_WIDTH - Data.CASE_WIDTH) {
        v8 = true;
      }
      v8 = (dir == null) ? v8 : dir < 0;
      if (v8) {
        v10.next.dx = -Math.abs(v10.next.dx);
      } else {
        v10.next.dx = Math.abs(v10.next.dx);
      }
      v8 = !v8;
    }
  }

  public function attachBg(id: Int, subId: Int, timer: Float): Void {
    if (timer == null) {
      timer = 15;
    }
    this.bgList.push({id: id, subId: subId, timer: timer});
  }

  public function detachBg(): Void {
    this.fl_bg = false;
    this.game.world.view.detachSpecialBg();
  }

  public function clearBg(): Void {
    this.bgList = new Array();
    this.detachBg();
  }

  public function clear(): Void {
    this.mc_exitArrow.removeMovieClip();
    this.levelName.removeMovieClip();
    this.clearBg();
    this.game.destroyList(Data.FX);
    for (v2 in 0...this.animList.length) {
      this.animList[v2].destroy();
    }
    this.animList = new Array();
    for (v3 in 0...this.mcList.length) {
      this.mcList[v3].removeMovieClip();
    }
    this.mcList = new Array();
    this.game.cleanKills();
  }

  public function onNextLevel(): Void {
    this.stack = new Array();
    this.clear();
    this.levelName.removeMovieClip();
    this.detachExit();
  }

  public static function addGlow(mc: MovieClip, color: Int, length: Float): Void {
    var v5 = new etwin.flash.filters.GlowFilter();
    v5.color = color;
    v5.quality = 1;
    v5.strength = 100;
    v5.blurX = length;
    v5.blurY = v5.blurX;
    mc.filters = [v5];
  }

  public function main(): Void {
    if (this.bgList.length > 0) {
      var v2 = this.bgList[0];
      if (!this.fl_bg) {
        this.fl_bg = true;
        this.game.world.view.attachSpecialBg(v2.id, v2.subId);
      }
      v2.timer -= Timer.tmod;
      if (v2.timer <= 0) {
        this.bgList.splice(0, 1);
        this.detachBg();
      }
    }
    if (this.levelName._name != null) {
      this.nameTimer -= Timer.tmod;
      if (this.nameTimer <= 0) {
        this.levelName._y += Timer.tmod * 0.7;
        if (this.levelName._y >= Data.GAME_HEIGHT + 30) {
          this.levelName.removeMovieClip();
        }
      }
    }
    var v3 = 0;
    while (v3 < this.stack.length) {
      this.stack[v3].t -= Timer.tmod;
      if (this.stack[v3].t <= 0) {
        this.attachFx(this.stack[v3].x, this.stack[v3].y, this.stack[v3].link);
        this.stack.splice(v3, 1);
        --v3;
      }
      ++v3;
    }
    var v4 = 0;
    while (v4 < this.animList.length) {
      var v5 = this.animList[v4];
      v5.update();
      if (v5.fl_kill) {
        this.animList[v4] = null;
        this.animList.splice(v4, 1);
        --v4;
      }
      ++v4;
    }
    if (this.igMsg._name != null) {
      this.igMsg.timer -= Timer.tmod;
      if (this.igMsg.timer <= 0) {
        this.igMsg._alpha -= Timer.tmod * 2;
      }
      if (this.igMsg._alpha <= 0) {
        this.igMsg.removeMovieClip();
      }
    }
  }
}
