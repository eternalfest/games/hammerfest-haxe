package hf.mode;

import hf.Chrono;
import hf.entity.Player;
import hf.GameManager;
import hf.mode.Adventure;

class BossRush extends Adventure {
  public var levelTimer: Int;
  public var levelAvg: Float;
  public var lastLevelTimer: Null<Int>;
  public var chrono: Chrono;
  public var levelCount: Dynamic /*TODO*/;

  public function new(m: GameManager, id: Int): Void {
    super(m, id);
    this._name = "$bossRush";
    this.fl_disguise = false;
    this.fl_map = true;
    this.fl_mirror = false;
    this.fl_nightmare = false;
    this.levelTimer = 0;
    this.levelAvg = 0;
    this.lastLevelTimer = null;
    this.chrono = new Chrono();
  }

  public function upgradePlayer(p: Player): Void {
    p.speedFactor = 1.5;
    p.maxBombs = 2;
    p.ctrl.fl_upKick = true;
  }

  public function registerLevelTime(): Void {
    this.lastLevelTimer = this.chrono.get() - this.levelTimer;
    if (this.levelAvg == 0) {
      this.levelAvg = this.lastLevelTimer;
    }
    this.levelAvg = 0.5 * (this.levelAvg + this.lastLevelTimer);
  }

  public function displayTime(t: Int): Void {
    var v3 = (this.fxMan.attachFx(5, Data.DOC_HEIGHT - 45, "hammer_interf_time")).mc;
    v3.sub.field.text = Lang.get(39) + " +" + this.chrono.formatTimeShort(t);
    FxManager.addGlow(v3, Data.DARK_COLORS[0], 2);
  }

  override public function goto(id: Int): Void {
    if ((id - 1) % 10 == 0 && id > 1) {
      this.registerMapEvent(Data.EVENT_TIME, this.chrono.getStrShort());
    }
    this.registerLevelTime();
    super.goto(id);
  }

  override public function initPlayer(p: Player): Void {
    super.initPlayer(p);
    this.upgradePlayer(p);
  }

  override public function lock(): Void {
    super.lock();
    this.chrono.stop();
  }

  override public function main(): Void {
    this.chrono.update();
    if (this.manager.isDev()) {
    }
    super.main();
  }

  override public function onPause(): Void {
    super.onPause();
    this.pauseMC.gotoAndStop("4");
    this.pauseMC.title.text = Lang.get(36);
    this.pauseMC.timeLabel.text = Lang.get(37);
    this.pauseMC.time.text = this.chrono.getStrShort();
    this.pauseMC.timeAvgLabel.text = Lang.get(38);
    if (this.levelAvg > 0) {
      this.pauseMC.timeAvg.text = this.chrono.formatTimeShort(Math.round(this.levelAvg));
    } else {
      this.pauseMC.timeAvg.text = "--";
    }
  }

  override public function onResurrect(): Void {
    super.onResurrect();
    var v3 = this.getPlayerList();
    for (v4 in 0...v3.length) {
      this.upgradePlayer(v3[v4]);
    }
  }

  override public function startLevel(): Void {
    super.startLevel();
    if (!this.chrono.fl_init) {
      this.chrono.reset();
    } else {
      this.displayTime(this.lastLevelTimer);
      this.lastLevelTimer = null;
    }
    this.levelTimer = this.chrono.get();
    ++this.levelCount;
  }

  override public function switchDimensionById(did: Int, lid: Int, pid: Int): Void {
    this.registerLevelTime();
    super.switchDimensionById(did, lid, pid);
  }

  override public function unlock(): Void {
    super.unlock();
    this.chrono.start();
  }
}
