package hf.entity.bomb;

import hf.entity.Bomb;
import hf.mode.GameMode;

class PlayerBomb extends Bomb {
  public static var UPGRADE_FACTOR: Float = 1.5;
  public static var MAX_UPGRADES: Int = 1;

  public var fl_unstable: Bool;
  public var upgrades: Int;
  public var owner: Null<Player>;

  public function new(): Void {
    super();
    this.fl_airKick = true;
    this.fl_unstable = false;
    this.upgrades = 0;
  }

  public function setOwner(p: Player): Void {
    this.owner = p;
  }

  public function upgradeBomb(p: Player): Void {
    this.game.fxMan.attachFx(this.x, this.y, "hammer_fx_pop");
    this.radius *= hf.entity.bomb.PlayerBomb.UPGRADE_FACTOR;
    this.power *= hf.entity.bomb.PlayerBomb.UPGRADE_FACTOR;
    this.setLifeTimer(this.duration * 0.7);
    this.dx *= 1.5;
    this.fl_blink = true;
    this.fl_alphaBlink = false;
    this.blinkColor = 16711680;
    this.scale(this.scaleFactor * hf.entity.bomb.PlayerBomb.UPGRADE_FACTOR * 100);
    this.owner = p;
    ++this.upgrades;
  }

  override public function hit(e: Entity): Void {
    super.hit(e);
    if (this.fl_unstable) {
      if (Data.BAD.check(e)) {
        this.onExplode();
      }
    }
  }

  override public function init(g: GameMode): Void {
    super.init(g);
    this.register(Data.PLAYER_BOMB);
  }

  override public function onExplode(): Void {
    super.onExplode();
    if (this.upgrades > 0) {
      this.game.fxMan.attachExplosion(this.x, this.y, this.radius);
    }
    if (this.game.fl_bombExpert) {
      var v3 = this.game.getPlayerList();
      for (v4 in 0...v3.length) {
        var v5 = v3[v4];
        var v6 = this.distance(v5.x, v5.y);
        if (v6 <= this.radius) {
          var v7 = (this.radius - v6) / this.radius;
          v5.knock(Data.SECOND + 2 * Data.SECOND * v7);
          var v8 = Math.atan2(v5.y - this.y, v5.x - this.x);
          v5.dx = Math.cos(v8) * this.power * 0.7 * v7;
          v5.dy = Math.sin(v8) * this.power * 0.7 * v7;
        }
      }
    }
    this.game.onExplode(this.x, this.y, this.radius);
    if (this.owner != null) {
      if (this.owner.specialMan.actives[14]) {
        hf.entity.item.ScoreItem.attach(this.game, this.x, this.y, 47, 0);
      }
      if (this.owner.specialMan.actives[15]) {
        hf.entity.item.ScoreItem.attach(this.game, this.x, this.y, 48, 0);
      }
      if (this.owner.specialMan.actives[16]) {
        hf.entity.item.ScoreItem.attach(this.game, this.x, this.y, 49, 0);
      }
      if (this.owner.specialMan.actives[17]) {
        hf.entity.item.ScoreItem.attach(this.game, this.x, this.y, 50, 0);
      }
    }
  }

  override public function onHitGround(h: Float): Void {
    super.onHitGround(h);
    if (this.fl_unstable && this.fl_bounce) {
      this.onExplode();
    }
  }

  override public function onKick(p: Player): Void {
    if (this.upgrades < hf.entity.bomb.PlayerBomb.MAX_UPGRADES) {
      if (p.pid != this.owner.pid) {
        this.upgradeBomb(p);
      }
    }
    super.onKick(p);
    if (!this.fl_stable) {
      this.fl_airKick = false;
    }
  }

  override public function onLifeTimer(): Void {
    var v3 = this.parent;
    super.onLifeTimer();
  }

  override public function update(): Void {
    super.update();
    if (!this.fl_blinking && this.upgrades > 0) {
      this.blink(Data.BLINK_DURATION_FAST);
    }
  }
}
