package etwin.flash;

@:native("Sound")
extern class Sound {
  public var checkPolicyFile: Bool;
  public var duration(default, null): Float;
  public var id3(default, null): Dynamic;
  public var position(default, null): Float;

  /**
    Creates a new Sound object for a specified movie clip.
  **/
  public function new(?target: Dynamic): Void;

  /**
    Returns the pan level set in the last `setPan()` call as an integer from -100 (left) to +100 (right).
  **/
  public function getPan(): Float;

  /**
    Determines how the sound is played in the left and right channels (speakers).
  **/
  public function setPan(value: Float): Void;

  /**
    Returns the sound transform information for the specified Sound object set with the last `Sound.setTransform()` call.
  **/
  public function getTransform(): Dynamic;

  /**
    Sets the sound transform (or balance) information, for a Sound object.
  **/
  public function setTransform(transformObject: Dynamic): Void;

  /**
    Returns the sound volume level as an integer from 0 to 100, where 0 is off and 100 is full volume.
  **/
  public function getVolume(): Float;

  /**
    Sets the volume for the Sound object.
  **/
  public function setVolume(value: Float): Void;

  /**
    Attaches the sound specified in the id parameter to the specified Sound object.
  **/
  public function attachSound(id: String): Void;

  /**
    Starts playing the last attached sound from the beginning if no parameter is specified, or starting at the point in the sound specified by the `secondOffset` parameter.
  **/
  public function start(?secondOffset: Float, ?loops: Float): Void;

  /**
    Stops all sounds currently playing if no parameter is specified, or just the sound specified in the `idName` parameter.
  **/
  public function stop(?linkageID: String): Void;

  /**
    Loads an MP3 file into a Sound object.
  **/
  public function loadSound(url: String, isStreaming: Bool): Void;

  /**
    Returns the number of bytes loaded (streamed) for the specified Sound object.
  **/
  public function getBytesLoaded(): Float;

  /**
    Returns the size, in bytes, of the specified Sound object.
  **/
  public function getBytesTotal(): Float;

  /**
    Invoked each time new ID3 data is available for an MP3 file that you load using `Sound.attachSound()` or `Sound.loadSound()`.
  **/
  public dynamic function onID3(): Void;

  /**
    Invoked automatically when a sound loads.
  **/
  public dynamic function onLoad(success: Bool): Void;

  /**
    Invoked automatically when a sound finishes playing.
  **/
  public dynamic function onSoundComplete(): Void;
}
