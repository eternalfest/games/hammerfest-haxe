package hf.entity.supa;

import hf.entity.Supa;
import hf.mode.GameMode;

class Arrow extends Supa {
  public function new(): Void {
    super();
  }

  public static function attach(g: GameMode): Arrow {
    var v3 = "hammer_supa_arrow";
    var v4: Arrow = cast g.depthMan.attach(v3, Data.DP_SUPA);
    v4.initSupa(g, Data.GAME_WIDTH, -50);
    return v4;
  }

  override public function initSupa(g: GameMode, x: Float, y: Float): Void {
    super.initSupa(g, x, y);
    this.speed = 10;
    this.radius = 50;
    this.moveLeft(this.speed);
  }

  override public function postfix(): Void {
    super.postfix();
    if (!this.world.shapeInBound(this)) {
      this.moveTo(Data.GAME_WIDTH, HfStd.random(Std.int(Data.GAME_HEIGHT * 0.7)) + 40);
    }
  }

  override public function prefix(): Void {
    super.prefix();
    var v3 = this.game.getClose(Data.BAD, this.x, this.y, this.radius, false);
    for (v4 in 0...v3.length) {
      var v5 = v3[v4];
      if (v5.y >= this.y - Data.CASE_HEIGHT && v5.y <= this.y + Data.CASE_HEIGHT * 2) {
        if (!v5.fl_kill) {
          v5.killHit(-this.speed * 1.5);
        }
      }
    }
  }
}
