package hf.entity.bad.ww;

import hf.entity.bad.WallWalker;
import hf.Entity;
import hf.mode.GameMode;

class Saw extends WallWalker {
  public static var ROTATION_RECAL: Float = 0.3;
  public static var STUN_DURATION: Float = Data.SECOND * 3;
  public static var BASE_SPEED: Float = 3;
  public static var ROTATION_SPEED: Float = 10;

  public var rotSpeed: Float;
  public var fl_stun: Bool;
  public var fl_stop: Bool;
  public var fl_updateSpeed: Bool;
  public var stunTimer: Float;

  public function new(): Void {
    super();
    this.speed = hf.entity.bad.ww.Saw.BASE_SPEED;
    this.angerFactor = 0;
    this.subOffset = 2;
    this.rotSpeed = 0;
    this.fl_stun = false;
    this.fl_stop = false;
    this.fl_updateSpeed = false;
  }

  public static function attach(g: GameMode, x: Float, y: Float): Saw {
    var v5 = Data.LINKAGES[Data.BAD_SAW];
    var v6: Saw = cast g.depthMan.attach(v5, Data.DP_BADS);
    v6.initBad(g, x, y);
    return v6;
  }

  public function stun(): Void {
    if (this.fl_stop) {
      return;
    }
    this.game.fxMan.attachExplosion(this.x, this.y - Data.CASE_HEIGHT * 0.5, 30);
    if (!this.fl_stun) {
      this.game.fxMan.inGameParticlesDir(Data.PARTICLE_METAL, this.x, this.y, 5, this.dx);
      this.game.fxMan.inGameParticlesDir(Data.PARTICLE_STONE, this.x, this.y, HfStd.random(4), this.dx);
    }
    this.fl_stun = true;
    this.fl_wallWalk = false;
    this.stunTimer = hf.entity.bad.ww.Saw.STUN_DURATION;
    this.dx = 0;
    this.dy = 0;
  }

  public function halt(): Void {
    if (this.fl_stop) {
      return;
    }
    this.fl_stop = true;
    this.fl_wallWalk = false;
    this.dx = 0;
    this.dy = 0;
  }

  public function run(): Void {
    if (!this.fl_stop) {
      return;
    }
    this.fl_stop = false;
    this.fl_wallWalk = true;
    this.updateSpeed();
  }

  override public function endUpdate(): Void {
    super.endUpdate();
    if (this.fl_stun) {
      var v3 = 1 - this.stunTimer / hf.entity.bad.ww.Saw.STUN_DURATION;
      this._x = this.x + v3 * (HfStd.random(20) / 10) * (HfStd.random(2) * 2 - 1);
      this._y = this.y + v3 * (HfStd.random(20) / 10) * (HfStd.random(2) * 2 - 1);
    }
    if (this.fl_wallWalk || this.fl_stop) {
      var v4 = Math.atan2(this.cp.y, this.cp.x);
      var v5 = 180 * v4 / Math.PI - 90;
      var v6 = v5 - this.sub._rotation;
      if (v6 < -180) {
        v6 += 360;
      }
      if (v6 > 180) {
        v6 -= 360;
      }
      this.sub._rotation += v6 * hf.entity.bad.ww.Saw.ROTATION_RECAL;
    } else {
      if (this.fl_kill) {
        this.sub._rotation += Timer.tmod * 14.5;
      } else {
        if (this.isHealthy()) {
          this.sub._rotation += -this.sub._rotation * (hf.entity.bad.ww.Saw.ROTATION_RECAL * 0.25);
        }
      }
    }
    if (this.fl_stop || this.fl_stun) {
      this.rotSpeed *= 0.9;
    } else {
      this.rotSpeed = Math.min(hf.entity.bad.ww.Saw.ROTATION_SPEED, this.rotSpeed + Timer.tmod);
    }
    this.sub.sub._rotation += this.rotSpeed;
  }

  override public function freeze(d: Float): Void {
    this.stun();
  }

  override public function hit(e: Entity): Void {
    if (this.isHealthy()) {
      if (e.isType(Data.PLAYER)) {
        this.game.fxMan.inGameParticles(Data.PARTICLE_CLASSIC_BOMB, this.x, this.y, HfStd.random(5) + 3);
      }
    }
    super.hit(e);
    if (this.isHealthy()) {
      if (e.isType(Data.BOMB) && !e.isType(Data.BAD_BOMB)) {
        var v4 = e;
        v4.setLifeTimer(Data.SECOND * 0.6);
        v4.dx = (this.dx == 0) ? -this.cp.x * 4 : -this.dx;
        v4.dy = (this.cp.y == 0) ? -8 : -this.cp.y * 13;
        this.game.fxMan.inGameParticlesDir(Data.PARTICLE_SPARK, v4.x, v4.y, 2, v4.dx);
      }
    }
  }

  override public function initBad(g: GameMode, x: Float, y: Float): Void {
    super.initBad(g, x, y);
    this.scale(80);
  }

  override public function init(g: GameMode): Void {
    super.init(g);
    this.unregister(Data.BAD_CLEAR);
  }

  override public function isHealthy(): Bool {
    return !this.fl_kill && !this.fl_stun && !this.fl_stop;
  }

  override public function killHit(dx: Null<Float>): Void {
    this.stun();
  }

  override public function knock(d: Float): Void {
    this.stun();
  }

  override public function land(): Void {
  }

  override public function onWakeUp(): Void {
    this.fl_wallWalk = true;
    this.moveToSafePos();
    this.updateSpeed();
    this.game.fxMan.inGameParticles(Data.PARTICLE_STONE, this.x, this.y, 3);
  }

  override public function update(): Void {
    var v3 = this.game.getDynamicInt("$SAW_SPEED");
    var v4 = this.speed;
    if (HfStd.isNaN(v3) || v3 < 0) {
      this.speed = hf.entity.bad.ww.Saw.BASE_SPEED;
      this.run();
    } else {
      if (v3 == 0) {
        this.halt();
      } else {
        this.speed = v3;
        this.run();
      }
    }
    if (v4 != this.speed) {
      this.fl_updateSpeed = true;
    }
    if (this.fl_updateSpeed && this.isHealthy()) {
      this.fl_updateSpeed = false;
      this.updateSpeed();
    }
    if (this.fl_stop || this.fl_stun) {
      this.dx = 0;
      this.dy = 0;
    }
    if (this.fl_stun) {
      if (HfStd.random(10) == 0) {
        this.game.fxMan.inGameParticles(Data.PARTICLE_SPARK, this.x, this.y, 1);
      }
      this.stunTimer -= Timer.tmod;
      if (this.stunTimer <= 0) {
        this.fl_stun = false;
        this.onWakeUp();
      }
    }
    super.update();
  }
}
