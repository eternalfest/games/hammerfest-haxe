package hf.entity.fx;

import hf.entity.Mover;
import hf.Entity;
import hf.mode.GameMode;

class Particle extends Mover {
  public var pid: Int;
  public var bounce: Float;
  public var subFrame: Int;
  public var skipWallsY: Float;

  public function new(): Void {
    super();
    this.setLifeTimer(Data.SECOND * 2 + Data.SECOND * 2 * HfStd.random(100) / 100);
    this.disableAnimator();
  }

  public function initParticle(g: GameMode, frame: Int, x: Float, y: Float): Void {
    this.init(g);
    this.pid = frame;
    if (this.pid != Data.PARTICLE_RAIN) {
      this.bounce = HfStd.random(65) / 10 + 1.5;
      this.setNext((1 + HfStd.random(50) / 10) * (HfStd.random(2) * 2 - 1), -this.bounce, 0, Data.ACTION_MOVE);
    }
    this.gotoAndStop("" + this.pid);
    this.subFrame = HfStd.random(this.sub._totalframes) + 1;
    this.sub.gotoAndStop("" + this.subFrame);
    this.scale(HfStd.random(50) + 50);
    this.rotation = HfStd.random(360);
    this.moveTo(x, y);
    this.skipWallsY = y;
    this.bounceFactor = 0.6;
    switch (this.pid) {
      case Data.PARTICLE_SPARK:
        this.scale(this.scaleFactor * 100 * 2);
        this.updateLifeTimer(Data.SECOND);
      case Data.PARTICLE_STONE:
        this.scale(this.scaleFactor * 150);
      case Data.PARTICLE_RAIN:
        this._yscale = 10;
        this.rotation = 5;
        this.alpha = HfStd.random(70) + 30;
      case Data.PARTICLE_METAL:
        this.scale(HfStd.random(40) + 80);
        this.bounceFactor = 0.3;
      case Data.PARTICLE_LITCHI:
        this.bounceFactor = 0.4;
      case Data.PARTICLE_BUBBLE:
        this.fl_gravity = false;
        this.fl_hitCeil = true;
        this.fl_blink = false;
        this._alpha = HfStd.random(80) + 20;
        this.moveUp(0.3 + HfStd.random(10) / 10);
        this.scale(HfStd.random(40) + 30);
        this.next = null;
        this.updateLifeTimer(Data.SECOND + HfStd.random(Data.SECOND * 20) / 10);
      case Data.PARTICLE_ICE_BAD:
        this.bounceFactor = 0.5;
        this.next.dx *= 0.3;
        this.next.dy = -Math.abs(this.next.dy);
      case Data.PARTICLE_BLOB:
        this.gravityFactor = 0.6;
        this.fallFactor = this.gravityFactor;
        this.updateLifeTimer(Data.SECOND * 3);
        this.fl_blink = false;
    }
    this.endUpdate();
  }

  public static function attach(g: GameMode, frame: Int, x: Float, y: Float): Particle {
    var v6 = "hammer_fx_particle";
    var v7: Particle = cast g.depthMan.attach(v6, Data.DP_BADS);
    v7.initParticle(g, frame, x, y);
    return v7;
  }

  override public function endUpdate(): Void {
    if (this.pid == Data.PARTICLE_RAIN) {
      this._yscale = Math.min(100, this._yscale + Timer.tmod * 4);
      this.rotation *= 0.93;
    } else {
      this.rotation += this.dx * 2;
    }
    super.endUpdate();
    if (this.pid == Data.PARTICLE_BUBBLE && this.totalLife > 0) {
      this._alpha = Math.min(100, 150 * this.lifeTimer / this.totalLife);
    }
    if (this.pid == Data.PARTICLE_BLOB && this.totalLife > 0) {
      this._xscale = 100 * this.scaleFactor * Math.min(1, 1.5 * this.lifeTimer / this.totalLife);
      this._yscale = this._xscale;
    }
  }

  override public function hit(e: Entity): Void {
    if (this.pid != Data.PARTICLE_RAIN) {
      return;
    }
    var player = Data.PLAYER.downcast(e);
    if (player != null) {
      if (!player.fl_shield) {
        e.scale(100 * e.scaleFactor - 1);
        this.destroy();
      }
    }
  }

  override public function init(g: GameMode): Void {
    super.init(g);
    this.register(Data.FX);
  }

  override public function onDeathLine(): Void {
    this.destroy();
  }

  override public function onHitCeil(): Void {
    this.destroy();
  }

  override public function onHitGround(h: Float): Void {
    if (this.pid == Data.PARTICLE_BLOB) {
      this.recal();
      this.dx = 0;
      this.dy = 0;
      this.fl_gravity = false;
    }
    if (this.pid == Data.PARTICLE_RAIN) {
      this.recal();
      var v4 = this.game.fxMan.attachFx(this.x, this.y, "hammer_fx_water_drop");
      v4.mc._alpha = HfStd.random(50) + 10;
      v4.mc._xscale = 100 * (HfStd.random(2) * 2 - 1);
      this.destroy();
      return;
    }
    this.skipWallsY = 0;
    this.fl_hitWall = true;
    this.setNext(this.dx, -this.dy * this.bounceFactor, 0, Data.ACTION_MOVE);
    if (Math.abs(this.next.dy) <= 1.5) {
      this.next.dx *= this.game.gFriction;
    }
    this.fl_skipNextGravity = true;
    super.onHitGround(h);
  }

  override public function onHitWall(): Void {
    if (this.fl_hitWall) {
      if (this.pid == Data.PARTICLE_BLOB) {
        this.dx = 0;
      } else {
        this.dx = -this.dx * 0.5;
      }
    }
  }

  override public function postfix(): Void {
    this.fl_friction = false;
  }

  override public function prefix(): Void {
    if (this.y > this.skipWallsY) {
      this.fl_hitWall = true;
    } else {
      this.fl_hitWall = false;
    }
    super.prefix();
  }

  override public function update(): Void {
    if (Math.abs(this.dx) <= 0.5 && this.pid != Data.PARTICLE_BLOB) {
      this.lifeTimer -= Timer.tmod;
      if (this.lifeTimer <= 0) {
        this.onLifeTimer();
      }
    }
    super.update();
  }
}
