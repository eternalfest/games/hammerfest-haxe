package hf.entity.item;

import hf.entity.Item;
import hf.entity.Player;
import hf.mode.GameMode;

class SpecialItem extends Item {
  public function new(): Void {
    super();
  }

  public static function attach(g: GameMode, x: Float, y: Float, id: Int, subId: Null<Int>): SpecialItem {
    if (g.fl_clear && id == 0) {
      return null;
    }
    var v7: SpecialItem = cast g.depthMan.attach("hammer_item_special", Data.DP_ITEMS);
    v7.initItem(g, x, y, id, subId);
    return v7;
  }

  override public function execute(p: Player): Void {
    if (this.id > 0) {
      this.game.manager.logAction("$S" + this.id);
    }
    this.game.pickUpSpecial(this.id);
    p.specialMan.execute(this);
    this.game.soundMan.playSound("sound_item_special", Data.CHAN_ITEM);
    if (this.id > 0) {
      this.game.attachItemName(Data.SPECIAL_ITEM_FAMILIES, this.id);
    }
    super.execute(p);
  }

  override public function init(g: GameMode): Void {
    super.init(g);
    this.register(Data.SPECIAL_ITEM);
  }

  override public function update(): Void {
    super.update();
    if (this.id != 0) {
      if (HfStd.random(4) == 0) {
        var v3 = this.game.fxMan.attachFx(this.x + HfStd.random(15) * (HfStd.random(2) * 2 - 1), this.y - HfStd.random(10), "hammer_fx_star");
        v3.mc._xscale = HfStd.random(70) + 30;
        v3.mc._yscale = v3.mc._xscale;
      }
    }
  }
}
