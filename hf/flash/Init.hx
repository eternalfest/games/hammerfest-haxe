package hf.flash;

import etwin.flash.geom.Matrix;
import etwin.flash.MovieClip;

class Init {
  public static var __m: Matrix = new etwin.flash.geom.Matrix();
  public static var __tx: String = "$tx".substring(1);
  public static var __ty: String = "$ty".substring(1);

  public function new(): Void {
  }

  public function drawMC(mc: MovieClip, x: Float, y: Float): Void {
    Reflect.setField(hf.flash.Init.__m, hf.flash.Init.__tx, x);
    Reflect.setField(hf.flash.Init.__m, hf.flash.Init.__ty, y);
    (untyped __this__).draw(mc, hf.flash.Init.__m);
  }

  public static function init(): Void {
    hf.flash.Init.__m.identity();
    Reflect.field(etwin.flash.display.BitmapData, "prototype").drawMC = Reflect.field(hf.flash.Init, "prototype").drawMC;
  }
}
