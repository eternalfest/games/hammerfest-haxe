package etwin;

/**
  The `Symbol` abstract type represents a unique String value.

  `Symbol` instances can be used as field names while greatly reducing the
  risks of conflict with existing names.

  You should avoid using `Symbol` directly and prefer the `WeakMap` and
  `WeakSet` classes built on top of it.

  The implementation is very careful to not rely on the initialization order of
  static values: you can safely use this class at any point in time.
**/
abstract Symbol(String) {
  private static var nextId: Int = getNextId();

  public inline function new(): Void {
    // The prefix acts as a unique key, it should never be used by other classes.
    // The `getNextId` functions guarantees that the next id is defined and unique.
    // The suffix is a rare symbol to further reduce collision risks.
    this = "__etwinSymbol" + getNextId() + "ÿ";
  }

  public inline function toString(): String {
    return this;
  }

  private static inline function getNextId(): Int {
    // Workaround for static initialization order issues
    if (nextId == null) {
      nextId = 0;
    }
    return nextId++;
  }
}
