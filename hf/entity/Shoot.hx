package hf.entity;

import hf.entity.Physics;
import hf.mode.GameMode;

class Shoot extends Physics {
  public var fl_checkBounds: Bool;
  public var fl_borderBounce: Bool;
  public var coolDown: Float;
  // TODO: Remove `shootSpeed` from this class and define it only on classes using it
  public var shootSpeed: Float;

  public function new(): Void {
    super();
    this.fl_hitWall = false;
    this.fl_hitBorder = false;
    this.fl_hitGround = false;
    this.fl_gravity = false;
    this.fl_friction = false;
    this.fl_borderBounce = false;
    this.fl_checkBounds = true;
    this.fl_bump = false;
    this.coolDown = 50;
  }

  public function initShoot(g: GameMode, x: Float, y: Float): Void {
    this.init(g);
    this.moveTo(x, y);
    this.endUpdate();
  }

  public function onSideBorderBounce(): Void {
  }

  public function onHorizontalBorderBounce(): Void {
  }

  override public function endUpdate(): Void {
    if (this.dy == 0) {
      if (this.dx < 0) {
        this._xscale = -Math.abs(this._xscale);
      } else {
        this._xscale = Math.abs(this._xscale);
      }
    }
    super.endUpdate();
  }

  override public function init(g: GameMode): Void {
    super.init(g);
    this.register(Data.SHOOT);
    this.setSub(cast this);
    this.playAnim(Data.ANIM_SHOOT);
  }

  override public function update(): Void {
    super.update();
    if (this.fl_checkBounds) {
      if (this.fl_borderBounce) {
        if (this.x < 0) {
          this.onSideBorderBounce();
          this.dx = Math.abs(this.dx);
        }
        if (this.x >= Data.GAME_WIDTH) {
          this.onSideBorderBounce();
          this.dx = -Math.abs(this.dx);
        }
        if (this.y < 0) {
          this.onHorizontalBorderBounce();
          this.dy = Math.abs(this.dy);
        }
        if (this.y >= Data.GAME_HEIGHT) {
          this.onHorizontalBorderBounce();
          this.dy = -Math.abs(this.dy);
        }
      } else {
        if (!this.world.inBound(this.cx, this.cy)) {
          this.destroy();
        }
      }
    }
  }
}
