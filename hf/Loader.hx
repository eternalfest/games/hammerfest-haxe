package hf;

import etwin.flash.LoadVars;
import etwin.flash.MovieClip;
import etwin.flash.DynamicMovieClip;
import etwin.flash.MovieClipLoader;
import etwin.flash.Sound;
import etwin.flash.XMLNode;
import etwin.flash.SharedObject;
import etwin.flash.XML;
using hf.compat.XMLNodeTools;

class Loader {
  public var root_mc: DynamicMovieClip /* TODO */;
  public var timeOutId: Int;
  public var musicTimeOut: Float;
  public var nearEndTimer: Float;
  public var fVersion: Int;
  public var fl_flash8: Bool;
  public var rawLang: String;
  public var xmlLang: XMLNode;
  public var _options: Dynamic /*TODO*/;
  public var _mode: Dynamic /*TODO*/;
  public var testPlayer6: Dynamic /*TODO*/;
  public var lv: LoadVars;
  public var _swf: String;
  public var _srv: String;
  public var _musics: Array<String>;
  public var musicId: Int;
  public var musics: Array<Sound>;
  public var game: MovieClip;
  public var music_mc: MovieClip;
  public var loading: Dynamic /*TODO*/;
  public var mcl: MovieClipLoader;
  public var fl_gameOver: Bool;
  public var fl_fade: Bool;
  public var fl_exit: Bool;
  public var save_score: LoadVars;
  public var uniqueItems: Int;
  public var fullurl: String;
  public var sendData: Void -> Void;
  public var retry_id: Float;
  public var exitUrl: String;
  public var exitParams: Dynamic /*TODO*/;
  public var gameInst: Dynamic /*TODO*/;
  public var _qid: Null<String>;
  public var _mid: Null<String>;
  public var _gid: Null<String>;
  public var _key: Null<String>;
  public var _families: Dynamic /*TODO*/;
  public var fl_saveAgain: Bool;

  public static var BASE_SCRIPT_URL: String = "__dollar__xNOpZUjm:_wKLIcGViGo9htkYisbkE:s0BH";
  public static var BASE_SCRIPT_URL_ALT: String = "__dollar__xNOpZUjm:_wKLIcL3oV1k:fk_8PHGcn";
  public static var BASE_SWF_URL: String = "__dollar__xNOpZUjm:AaPpI3N7iOdPgfltiLvCF9yeOdC";
  public static var BASE_MUSIC_URL: String = "sfx/";
  public static var TIMEOUT: Float = 8000;
  public static var MUSIC_TIMEOUT: Float = 32 * 10;
  public static var MUSIC_NEAR_END: Float = 32 * 2.5;
  public static var URL_KEY: String = "$RJrjk05eeJrzp5Pazre7z9an788baz61kBKJ1EZ4";

  public function new(mc: DynamicMovieClip /* TODO */): Void {
    this.root_mc = mc;
    Log.setColor(16776960);
    this.timeOutId = 0;
    this.musicTimeOut = 0;
    this.nearEndTimer = 0;
    var v3 = (HfStd.getRoot()).__dollar__version;
    this.fVersion = HfStd.parseInt(((v3.split(" "))[1].split(","))[0], 10);
    this.fl_flash8 = this.fVersion != null && !HfStd.isNaN(this.fVersion) && this.fVersion >= 8;
    var v4 = "$" + this.root_mc.__dollar__lang;
    switch (v4) {
      case "$fr":
        this.rawLang = HfStd.getVar(HfStd.getRoot(), "xml_lang_fr");
      case "$es":
        this.rawLang = HfStd.getVar(HfStd.getRoot(), "xml_lang_es");
      case "$en":
        this.rawLang = HfStd.getVar(HfStd.getRoot(), "xml_lang_en");
    }
    this.xmlLang = (new XML(this.rawLang)).firstChild.firstChild;
    while (true) {
      if (!(this.xmlLang != null && this.xmlLang.nodeName != "$statics".substring(1))) break;
      this.xmlLang = this.xmlLang.nextSibling;
    }
    this._options = this.root_mc.__dollar__options;
    this._mode = this.root_mc.__dollar__mode;
    if (this.root_mc.__dollar__alt == "1") {
      Loader.BASE_SCRIPT_URL = Loader.BASE_SCRIPT_URL_ALT;
    }
    ++this.testPlayer6;
    if (this.testPlayer6 == 1) {
      this.error(this.getLangStr(65));
      this.lv = new LoadVars();
      this.lv.send("http://www.macromedia.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash&Lang=French", "_blank", "");
      return;
    }
    this.initMachineId___();
    if (!this.initBaseUrl()) {
      this.error("base init error");
      return;
    }
    var v5 = HfStd.getRoot();
    var v6 = HfStd.getVar(v5, "$redirect".substring(1));
    if (v6 != null) {
      this.lv = new LoadVars();
      Reflect.setField(this.lv, "$mid".substring(1), this._mid);
      this.lv.send(v6, "_self", "POST");
      return;
    }
    this._swf = HfStd.getVar(HfStd.getRoot(), "$swf".substring(1));
    this._srv = HfStd.getVar(HfStd.getRoot(), "$srv".substring(1));
    this._musics = ["music.mp3", "boss.mp3", "hurryUp.mp3"];
    this.musicId = 0;
    this.musics = new Array();
    if (!this.fl_flash8) {
      this.error(this.getLangStr(15) + "\n\nhttp://www.macromedia.com/go/getflash");
    } else {
      var v7 = SharedObject.getLocal("$hd");
      this.fullurl = HfStd.getVar(v7.data, "data");
      if (this.fullurl != null) {
        this.saveAgain();
      } else {
        this.initLoader();
      }
    }
  }

  public function initLoader(): Void {
    this.game = HfStd.createEmptyMC(this.root_mc, 1);
    this.music_mc = HfStd.createEmptyMC(this.root_mc, 2);
    this.attachLoading(1);
    this.loading.sub.bar._xscale = 0;
    this.loading.sub.km.text = "";
    var v2 = this.loading._url;
    if (v2.substr(0, Loader.BASE_SWF_URL.length) != Loader.BASE_SWF_URL) {
      this.error(this.getLangStr(66));
      return;
    }
    this.mcl = new MovieClipLoader();
    this.mcl.onLoadError = function (al, msg, status) {
      this.error(this.getLangStr(67) + " : " + msg + "\nURL=" + Loader.BASE_SWF_URL);
    };
    this.mcl.onLoadInit = function (al) {
      this.gameLoadDone();
    };
    if (!this.mcl.loadClip(Loader.BASE_SWF_URL + this._swf, this.game)) {
      this.error(this.getLangStr(68) + "\n\nURL=" + Loader.BASE_SWF_URL);
    }
  }

  public function loadMusic(): Void {
    this.attachLoading(7);
    this.musics[this.musicId] = new Sound(this.music_mc);
    this.musics[this.musicId].loadSound(Loader.BASE_SWF_URL + Loader.BASE_MUSIC_URL + this._musics[this.musicId], false);
    this.musics[this.musicId].onLoad = HfStd.callback(this, "musicLoadDone");
    this.musicTimeOut = Loader.MUSIC_TIMEOUT;
    this.nearEndTimer = Loader.MUSIC_NEAR_END;
  }

  public function gameLoadDone(): Void {
    if (this.root_mc.__dollar__music != "0") {
      this.loadMusic();
    } else {
      this.gameReady();
    }
  }

  public function musicLoadDone(fl: Bool): Void {
    if (fl) {
      ++this.musicId;
      if (this.musicId == this._musics.length) {
        this.gameReady();
      } else {
        this.gameLoadDone();
      }
    } else {
      this.error(this.getLangStr(69));
    }
  }

  public function gameReady(): Void {
    if ((HfStd.getVar(this.game, "GameManager")).BASE_VOLUME == null) {
      this.error(this.getLangStr(70));
      return;
    }
    this.attachLoading(2);
    if (Loader.BASE_SCRIPT_URL == "" || this._srv == null) {
      this.loading.onRelease = HfStd.callback(this, "startGame");
    } else {
      this.loading.onRelease = HfStd.callback(this, "queryStart");
    }
  }

  public function menu(url: Null<String>): Void {
    this.lv = new LoadVars();
    if (url == null) {
      url = "";
    }
    this.lv.onData = function (d) {
      this.menu(url);
    };
    this.lv.send(url, "_self", "");
  }

  public function makeUrl(url: String, params: Dynamic /*TODO*/): String {
    var v3 = url.indexOf("?", 0) != -1;
    var delim;
    if (v3) {
      delim = "&";
    } else {
      delim = "?";
    }
    params.iter(function (k, v) {
      url += delim;
      url += k;
      url += "=";
      url += v;
      delim = "&";
    });
    return url;
  }

  public function getRunId(): Dynamic /*TODO*/ {
    var v2 = null;
    var v3 = this._options.indexOf("$set_".substring(1), 0);
    var v4 = null;
    if (v3 >= 0) {
      v3 = this._options.indexOf("$_".substring(1), v3 + 4);
      v2 = HfStd.parseInt(this._options.substr(v3 + 1, 1), 10);
    }
    return v2;
  }

  public function queryStart(): Void {
    this._qid = "";
    for (v2 in 0...16) {
      this._qid += Codec.BASE64.charAt(1 + HfStd.random(63));
    }
    this.attachLoading(3);
    HfStd.deleteField(this.loading, "onRelease");
    this.lv = new LoadVars();
    var v3 = new Hash();
    v3.set("$qid".substring(1), this._qid);
    v3.set("$mid".substring(1), this._mid);
    v3.set("$mode".substring(1), this._mode);
    v3.set("$options".substring(1), this._options);
    var v4 = this.getRunId();
    if (v4 == null) {
      v3.set("$runid", '' + (v4));
    }
    var v5 = this.makeUrl(Loader.BASE_SCRIPT_URL + this._srv, v3);
    this.lv.onData = HfStd.callback(this, "serverData");
    this.lv.load(v5);
  }

  public function serverData(s: Null<String>): Void {
    if (s == null) {
      this.error(this.getLangStr(64));
      return;
    }
    while (true) {
      var v3 = s.charAt(s.length - 1);
      if (v3 == " " || v3 == "\n" || v3 == "\r" || v3 == "\t") {
        s = s.substr(0, s.length - 1);
      } else {
        break;
      }
    }
    while (true) {
      var v4 = s.charAt(0);
      if (v4 == " " || v4 == "\n" || v4 == "\r" || v4 == "\t") {
        s = s.substring(1);
      } else {
        break;
      }
    }
    var v5 = s.split("&");
    var v7 = new Hash();
    for (v6 in 0...v5.length) {
      var v8 = v5[v6].split("=");
      if (v8.length == 2) {
        v7.set("$" + v8[0], v8[1]);
      }
    }
    this._gid = v7.get("$gid");
    this._key = v7.get("$key");
    this._families = v7.get("$families");
    if (this._gid == null || this._key == null) {
      this.menu(v7.get("$url"));
      return;
    }
    this.startGame();
  }

  public function startGame(): Void {
    HfStd.deleteField(this.loading, "onRelease");
    HfStd.setGlobal("gameOver", HfStd.callback(this, "gameOver"));
    HfStd.setGlobal("exitGame", HfStd.callback(this, "exitGame"));
    this.gameInst = HfStd.makeNew(HfStd.getVar(this.game, "GameManager"), this.game, {rawLang: this.rawLang, fl_local: this.isLocal(), families: this._families, options: this._options, musics: this.musics});
    this.main = HfStd.callback(this, "mainGame");
    this.attachLoading(6);
  }

  public function countUniques(a: Array<Dynamic /*TODO*/>): Int {
    var v3 = 0;
    for (v4 in 0...a.length) {
      if (a[v4] != null) {
        ++v3;
      }
    }
    return v3;
  }

  public function asc(c: String): Int {
    return c.charCodeAt(0);
  }

  public function gameOver(score: Dynamic /*TODO*/, runId: Dynamic /*TODO*/, stats: Dynamic /*TODO*/): Void {
    if (this.fl_gameOver) {
      return;
    }
    this.fl_gameOver = true;
    this.fl_fade = true;
    this.attachLoading(5);
    this.loading._alpha = 0;
    this.save_score = new LoadVars();
    var v5 = new Codec(this._key);
    this.uniqueItems = this.countUniques(stats.__dollar__item2);
    var v7 = new Hash();
    v7.set("$swfsize".substring(1), (this.mcl.getProgress(this.game)).bytesTotal);
    v7.set("$lsize".substring(1), (HfStd.getRoot()).getBytesTotal());
    v7.set("$score".substring(1), score);
    if (runId != null) {
      v7.set("$runid", runId);
    }
    v7.set("$p".substring(1), stats);
    v7.set("$gid".substring(1), this._gid);
    v7.set("$mid".substring(1), this._mid);
    var v6 = new Hash();
    v6.set("$gid".substring(1), this._gid);
    v6.set("$score".substring(1), v5.encode(v7));
    this.fullurl = this.makeUrl(Loader.BASE_SCRIPT_URL + this._srv, v6);
    if ("$" + Data.computeMd5((HfStd.getVar(this.game, "Data")).SCORE_ITEM_FAMILIES) != "$0e76f8e2c8b1b225b2e843b90d600d06") {
      return;
    }
    if ("$" + Data.computeMd5((HfStd.getVar(this.game, "Data")).SPECIAL_ITEM_FAMILIES) != "$ffc9db3b419c9ebf8a7e3ff852b7ce19") {
      return;
    }
    if (this.isLocal()) {
      this.error(score + " / " + (v6.get("$score".substring(1))).length + "b ");
    } else {
      this.sendData = function () {
        if (this.timeOutId > 0) {
          this.loading.subBottom.text = this.getLangStr(60) + (this.timeOutId + 1);
          if (this.timeOutId >= 3) {
            this.loading.bottom.text = "";
            this.loading.subBottom.text = this.getLangStr(72);
            var v2 = SharedObject.getLocal("$hd");
            v2.clear();
            HfStd.setVar(v2.data, "data", this.fullurl);
            var v3 = v2.flush();
            if (v3) {
              Log.trace("Suivez les instructions de la page Support technique (en bas du site) pour VIDER VOTRE CACHE.");
              Log.trace("Déconnectez-vous puis reconnectez-vous au site, avant de lancer le jeu à nouveau: une nouvelle tentative de sauvegarde sera alors effectuée.");
            } else {
              Log.trace("Sauvegarde impossible.");
              Log.trace("Pour éviter ce genre de problèthis à l'avenir, faites un clic droit sur le jeu, choisissez Paramètres, puis l'icone du Dossier dans la nouvelle fenetre. Décochez \"Jamais\" et assurez-vous d'autoriser au moins 100ko de données (en déplaçant la petite barre).");
            }
            this.root_mc.stop();
            (HfStd.getGlobal("clearInterval"))(this.retry_id);
          }
        }
        ++this.timeOutId;
        this.save_score.load(this.fullurl + "&retry=" + this.timeOutId);
      };
      this.sendData();
      this.retry_id = (HfStd.getGlobal("setInterval"))(this.sendData, Loader.TIMEOUT);
    }
  }

  public function saveAgain(): Void {
    this.attachLoading(5);
    this.loading.bottom.text = this.getLangStr(71);
    this.save_score = new LoadVars();
    this.fl_saveAgain = true;
    this.fl_fade = true;
    this.retry_id = 0;
    this.sendData = function () {
      if (this.timeOutId > 0) {
        this.loading.subBottom.text = this.getLangStr(60) + (this.timeOutId + 1);
      }
      if (this.timeOutId >= 3) {
        Log.trace("Impossible de terminer cette sauvegarde, cette partie ne pourra pas être enregistrée.");
        var v2 = SharedObject.getLocal("$hd");
        v2.clear();
        this.root_mc.stop();
        (HfStd.getGlobal("clearInterval"))(this.retry_id);
      }
      ++this.timeOutId;
      this.save_score.load(this.fullurl + "&retry=" + this.timeOutId);
    };
    this.sendData();
    this.retry_id = (HfStd.getGlobal("setInterval"))(this.sendData, Loader.TIMEOUT);
  }

  public function exitGame(url: String, params: Dynamic /*TODO*/): Void {
    if (this.fl_exit) {
      return;
    }
    this.fl_fade = true;
    this.fl_exit = true;
    this.exitUrl = url;
    this.exitParams = params;
    this.attachLoading(6);
    this.loading._alpha = 0;
  }

  public function replace(str: Null<String>, search: String, replace: String): Null<String> {
    if (str == null) {
      return null;
    }
    var v5 = search.length;
    if (v5 == 1) {
      return (str.split(search)).join(replace);
    }
    var v7 = 0;
    var v8 = "";
    while (true) {
      var v6 = str.indexOf(search, v7);
      if (v6 == -1) {
        break;
      }
      v8 += str.substr(v7, v6 - v7) + replace;
      v7 = v6 + v5;
    }
    return v8 + str.substring(v7);
  }

  public function checkBaseUrl___(): Bool {
    return true;
  }

  public function decodeUrl(url: String): String {
    var v3 = new Codec(Loader.URL_KEY);
    url = v3.decode(url.substring(1));
    url = this.replace(url, "$YY".substr(1, 3), ".");
    url = this.replace(url, "$XX".substr(1, 3), ":");
    url = this.replace(url, "$_".substring(1), "/");
    return url;
  }

  public function initBaseUrl(): Bool {
    if (Loader.BASE_SCRIPT_URL == null) {
      Loader.BASE_SCRIPT_URL = "";
      Loader.BASE_SWF_URL = "";
      return this.checkBaseUrl___();
    }
    Loader.BASE_SCRIPT_URL = this.decodeUrl(Loader.BASE_SCRIPT_URL);
    Loader.BASE_SWF_URL = this.decodeUrl(Loader.BASE_SWF_URL);
    return Loader.BASE_SCRIPT_URL != null && Loader.BASE_SWF_URL != null;
  }

  public function error(msg: String): Void {
    this.attachLoading(4);
    this.loading.error.text = "URL=" + Loader.BASE_SCRIPT_URL + "\n\n\n" + msg;
    Log.trace("build=" + "01-11 11:48:51");
    Log.trace("msg=" + msg);
    this.loading.onRelease = HfStd.callback(this, "menu", "");
  }

  public function initMachineId___(): Void {
    var v2 = SharedObject.getLocal("$mid");
    this._mid = v2.data.__dollar__v;
    if (this._mid.length != 8) {
      this._mid = "";
      for (v3 in 0...8) {
        this._mid += Codec.BASE64.charAt(1 + HfStd.random(63));
      }
      v2.data.__dollar__v = this._mid;
    }
  }

  public function isLocal(): Bool {
    return Loader.BASE_SCRIPT_URL == "";
  }

  public function isDev(): Bool {
    var v2 = Loader.BASE_SCRIPT_URL == "" || Loader.BASE_SCRIPT_URL == null || Loader.BASE_SCRIPT_URL.indexOf("$dev".substring(1), 0) >= 0 || Loader.BASE_SWF_URL.indexOf("$dev".substring(1), 0) >= 0;
    return v2;
  }

  public function isMode(modeName: String): Bool {
    return this._mode == modeName.substring(1);
  }

  public function getLangStr(id: Int): String {
    var v3 = this.xmlLang.firstChild;
    while (v3.get("$id".substring(1)) != "" + id) {
      v3 = v3.nextSibling;
    }
    return v3.get("$v".substring(1));
  }

  public function getStupidTrackName(): String {
    var v2 = ["Battle for ", "The great ", "Lost ", "An almighty ", "Spirits of ", "Desperate ", "Beyond ", "Everlasting ", "Prepare for ", "The legend of ", "Blades of ", "Hammerfest, quest for ", "Wings of ", "Song for ", "Unblessed ", "Searching for ", "No pain, no "];
    var v3 = ["Igor ", "Wanda ", "hope ", "sadness ", "death ", "souls ", "glory ", "redemption ", "destruction ", "flames ", "love ", "forgiveness ", "darkness ", "carrot !"];
    if (this.musicId < 10) {
      return "0" + (this.musicId + 1) + ". «" + v2[HfStd.random(v2.length)] + v3[HfStd.random(v3.length)] + "»";
    }
    return "" + (this.musicId + 1) + ". «" + v2[HfStd.random(v2.length)] + v3[HfStd.random(v3.length)] + "»";
  }

  public function attachLoading(frame: Int): Void {
    this.loading.removeMovieClip();
    this.loading = HfStd.attachMC(this.root_mc, "loading", 99);
    this.loading.gotoAndStop("" + frame);
    if (this.isDev()) {
      this.loading.bg.gotoAndStop("2");
    } else {
      this.loading.bg.gotoAndStop("1");
    }
    if (frame == 1) {
      this.loading.sub.currentWeapon = 1;
      this.loading.sub.title.text = this.getLangStr(50);
    }
    if (frame == 2 || frame == 3) {
      this.loading.currentWeapon = 1;
      this.loading.title.text = this.getLangStr(50);
      this.loading.km.text = this.getLangStr(52);
      if (frame == 2) {
        this.loading.bottom.text = this.getLangStr(53);
        if (this.isMode("$tutorial") || this.isMode("$soccer")) {
          this.loading.subBottom.text = this.getLangStr(58);
        } else {
          if (this.isDev()) {
            this.loading.subBottom.text = this.getLangStr(59);
          } else {
            this.loading.subBottom.text = this.getLangStr(57);
          }
        }
      }
      if (frame == 3) {
        this.loading.bottom.text = this.getLangStr(54);
        this.loading.subBottom.text = "";
      }
    }
    if (frame == 5) {
      this.loading.bottom.text = this.getLangStr(55);
      this.loading.subBottom.text = this.getLangStr(56);
    }
    if (frame == 6) {
    }
    if (frame == 7) {
      this.loading.currentWeapon = 1;
      this.loading.sub.title.text = this.getLangStr(61);
      this.loading.sub.km.text = this.getStupidTrackName();
    }
  }

  public function redirect(url: String): Void {
    this.lv = new LoadVars();
    this.lv.send(url, "_self", "GET");
  }

  public dynamic function main(): Void {
    if (this.loading != null) {
      if (this.loading._currentframe == 1) {
        var v2 = this.mcl.getProgress(this.game);
        if (v2.bytesTotal > 0) {
          var v3 = v2.bytesLoaded / v2.bytesTotal;
          this.loading.sub.km.text = Math.round(v3 * 100) + " " + this.getLangStr(51);
          this.loading.sub.bar._xscale = v3 * 100;
        }
      }
      if (this.loading._currentframe == 7) {
        var v4 = this.musics[this.musicId].getBytesTotal();
        var v5 = this.musics[this.musicId].getBytesLoaded();
        if (HfStd.isNaN(v5) || HfStd.isNaN(v4)) {
          this.loading.sub.bar._xscale = 0;
          --this.musicTimeOut;
          if (this.musicTimeOut <= 0) {
            Log.trace("Impossible de charger la musique. Merci de VIDER VOTRE CACHE INTERNET, comme expliqué dans la section Support technique. Vous pouvez également désactiver les musiques depuis les Paramètres du jeu.\n");
            Log.trace("Vous pouvez enfin laisser un message dans le forum en précisant les informations suivantes:");
            Log.trace("\n---------------\n");
            Log.trace("music #" + this.musicId + " timed out\nLOADED=" + v5 + "\nTOTAL=" + v4 + "\nURL=" + (Loader.BASE_SWF_URL + Loader.BASE_MUSIC_URL + this._musics[this.musicId]));
            this.musicTimeOut = 999999;
          }
        }
        if (!HfStd.isNaN(v4) && v4 > 0) {
          var v6 = v5 / v4;
          if (v6 >= 0.99) {
            --this.nearEndTimer;
            if (this.nearEndTimer <= 0) {
              this.musics[this.musicId].onLoad = null;
              this.musicLoadDone(true);
            }
          }
          this.loading.sub.bar._xscale = v6 * 100;
        }
      }
    }
    if (this.fl_saveAgain) {
      var v7 = Reflect.field(this.save_score, "$ok".substring(1));
      if (v7 != null) {
        var v8 = SharedObject.getLocal("$hd");
        v8.clear();
        this.lv = new LoadVars();
        this.lv.send(v7, "_self", "");
        this.save_score = null;
      }
    }
  }

  public dynamic function mainGame(): Void {
    if (this.fl_fade) {
      this.loading._alpha += 5;
      if (this.loading._alpha >= 100 && this.game._name != null) {
        this.game.removeMovieClip();
        this.gameInst = null;
      }
      if (this.loading._alpha >= 220) {
        this.loading._alpha = 100;
        this.fl_fade = false;
      }
    }
    if (this.fl_exit) {
      if (!this.fl_fade) {
        var v2 = new LoadVars();
        if (this.exitParams != null) {
          v2.d = this.exitParams;
        }
        v2.send(this.exitUrl, "_self", "POST");
        this.fl_exit = false;
        this.mainGame = null;
        this.main = null;
        return;
      }
    }
    if (this.fl_gameOver) {
      var v3 = Reflect.field(this.save_score, "$ok".substring(1));
      if (v3 != null && !this.fl_fade) {
        var v4 = SharedObject.getLocal("$hd");
        v4.clear();
        (HfStd.getGlobal("clearInterval"))(this.retry_id);
        this.loading.bottom.text = this.getLangStr(73);
        this.loading.subBottom.text = "";
        this.lv = new LoadVars();
        this.lv.send(v3, "_self", "GET");
        this.save_score = null;
      }
      return;
    }
    if (!this.fl_fade && !this.fl_exit && !this.fl_gameOver) {
      if (this.loading != null) {
        this.loading._alpha -= 2;
        if (this.loading._alpha <= 0) {
          this.loading.removeMovieClip();
          this.loading = null;
        }
      }
    }
    this.gameInst.main();
  }
}
