package hf.mode;

import hf.gui.Container;
import hf.levels.SetManager;
import hf.Mode;
import etwin.flash.Key;

class Browser extends Mode {
  public var current: Int;
  public var initialId: Dynamic /*TODO*/;
  public var dimensionId: Dynamic /*TODO*/;
  public var fl_modified: Array<Bool>;
  public var fl_buildCache: Bool;
  public var world: SetManager;
  public var navC: Container;
  public var menuC: Container;
  public var draw: Dynamic /*TODO*/;
  public var footer: Dynamic /*TODO*/;
  public var first: Int;
  public var fields: Dynamic /*TODO*/;
  public var buffer: Dynamic /*TODO*/;
  public var cacheId: Dynamic /*TODO*/;
  public var views: Dynamic /*TODO*/;

  public static var THUMBS: Int = 9;
  public static var LINE: Int = Math.ceil(Math.sqrt(hf.mode.Browser.THUMBS));
  public static var PAGE: Int = hf.mode.Browser.LINE * hf.mode.Browser.LINE;
  public static var SCALE: Float = 1 / hf.mode.Browser.LINE;

  public function new(m: Dynamic /*TODO*/, w: SetManager, did: Dynamic /*TODO*/, fl_mod: Array<Bool>): Void {
    super(m);
    this._name = "$levelBrowser";
    this.current = w.currentId;
    this.initialId = w.currentId;
    this.dimensionId = did;
    this.fl_modified = fl_mod;
    this.fl_buildCache = false;
    this.world = w;
    this.world.goto(this.current);
    this.updatePage();
    this.navC = new hf.gui.Container(this, 5, -100, Data.DOC_WIDTH - 10);
    hf.gui.SimpleButton.attach(this.navC, " GO ", Key.ENTER, HfStd.callback(this, "onValidate"));
    hf.gui.SimpleButton.attach(this.navC, " cancel ", Key.ESCAPE, HfStd.callback(this, "onCancel"));
    hf.gui.SimpleButton.attach(this.navC, " ^ ", Key.UP, HfStd.callback(this, "onPreviousLine"));
    hf.gui.SimpleButton.attach(this.navC, " v ", Key.DOWN, HfStd.callback(this, "onNextLine"));
    hf.gui.SimpleButton.attach(this.navC, " < ", Key.LEFT, HfStd.callback(this, "onPrevious"));
    hf.gui.SimpleButton.attach(this.navC, " > ", Key.RIGHT, HfStd.callback(this, "onNext"));
    hf.gui.SimpleButton.attach(this.navC, " << ", Key.PGUP, HfStd.callback(this, "onPreviousPage"));
    hf.gui.SimpleButton.attach(this.navC, " >> ", Key.PGDN, HfStd.callback(this, "onNextPage"));
    hf.gui.SimpleButton.attach(this.navC, " FIRST ", Key.HOME, HfStd.callback(this, "onFirst"));
    hf.gui.SimpleButton.attach(this.navC, " LAST ", Key.END, HfStd.callback(this, "onLast"));
    this.menuC = new hf.gui.Container(this, 5, 0, Data.DOC_WIDTH - 10);
    this.menuC.setScale(0.8);
    var v7 = hf.gui.SimpleButton.attach(this.menuC, " <SWAP ", Key.LEFT, HfStd.callback(this, "onMoveBefore"));
    v7.setToggleKey(Key.CONTROL);
    v7 = hf.gui.SimpleButton.attach(this.menuC, " SWAP> ", Key.RIGHT, HfStd.callback(this, "onMoveAfter"));
    v7.setToggleKey(Key.CONTROL);
    hf.gui.SimpleButton.attach(this.menuC, " DEL ", Key.DELETEKEY, HfStd.callback(this, "onDelete"));
    hf.gui.SimpleButton.attach(this.menuC, " INS ", Key.INSERT, HfStd.callback(this, "onInsert"));
    hf.gui.SimpleButton.attach(this.menuC, " INS>", 107, HfStd.callback(this, "onInsertAfter"));
    hf.gui.SimpleButton.attach(this.menuC, " CLEAR ", null, HfStd.callback(this, "onClear"));
    hf.gui.SimpleButton.attach(this.menuC, " CUT ", 88, HfStd.callback(this, "onCut"));
    hf.gui.SimpleButton.attach(this.menuC, " COPY ", 67, HfStd.callback(this, "onCopy"));
    hf.gui.SimpleButton.attach(this.menuC, " PASTE ", 86, HfStd.callback(this, "onPaste"));
    hf.gui.SimpleButton.attach(this.menuC, " SWAP TO BUFFER ", 87, HfStd.callback(this, "onSwapBuffer"));
    this.draw = this.depthMan.empty(Data.DP_INTERF);
    this.footer = this.depthMan.attach("hammer_editor_footer", Data.DP_INTERF);
    this.footer._x = Data.DOC_WIDTH * 0.5;
    this.footer._y = Data.DOC_HEIGHT - 9;
  }

  public function refresh(): Void {
    var v2 = HfStd.getTimer();
    var v3 = Math.floor(1 / hf.mode.Browser.SCALE);
    var v4 = Data.DOC_WIDTH * hf.mode.Browser.SCALE;
    var v5 = Data.DOC_HEIGHT * hf.mode.Browser.SCALE * 1.01;
    for (v6 in 0...this.views.length) {
      this.views[v6].destroy();
    }
    this.views = new Array();
    for (v7 in 0...this.fields.length) {
      this.fields[v7].removeTextField();
    }
    this.fields = new Array();
    var v8 = 0;
    var v9 = 0;
    var v10 = new Array();
    var v11 = null;
    var v12 = 0;
    for (v13 in 0...this.world.levels.length) {
      var v14 = Data.getTagFromLevel(this.dimensionId, v13);
      if (v14 != null) {
        v10[v13] = v14;
        v11 = v14;
        v12 = 0;
      } else {
        if (v11 != null) {
          v10[v13] = v11 + "+" + v12;
        }
      }
      ++v12;
    }
    for (v15 in 0...hf.mode.Browser.THUMBS) {
      if (v15 + this.first < this.world.levels.length) {
        var v16 = HfStd.createTextField(this.mc, 50000 + this.manager.uniq++);
        v16.border = true;
        v16.backgroundColor = 0;
        v16.background = true;
        v16._x = v8 * v4 + 10;
        v16._y = v9 * v5 + 145;
        v16._width = 115;
        v16._height = 20;
        if (v10[this.first + v15].indexOf("+") >= 0) {
          v16.textColor = 13408512;
        } else {
          v16.textColor = 16776960;
        }
        v16.text = this.first + v15 + ": " + v10[this.first + v15];
        v16.wordWrap = true;
        v16.selectable = false;
        this.fields.push(v16);
        this.world.goto(this.first + v15);
        var v17 = new hf.levels.ViewLight(this.world, this.depthMan, this.first + v15);
        v17.scale(hf.mode.Browser.SCALE);
        v17.attach(v8 * v4 + 10, v9 * v5);
        if (this.world.isEmptyLevel(this.first + v15, null)) {
          v17.strike();
          v16.removeTextField();
        }
        this.views.push(v17);
        v2 = HfStd.getTimer();
        ++v8;
        if (v8 >= v3) {
          ++v9;
          v8 = 0;
        }
      }
    }
  }

  public function updateBoxes(): Void {
    this.draw.clear();
    var v2 = this.views[this.current - this.first];
    this.highlight(v2, 4, 16776960);
    for (v3 in 0...this.views.length) {
      v2 = this.views[v3];
      if (this.fl_modified[this.first + v3]) {
        this.highlight(v2, 1, 16711680);
      }
    }
  }

  public function highlight(view: Dynamic /*TODO*/, thickness: Float, color: Dynamic /*TODO*/): Void {
    var v5 = view.mc._x;
    var v6 = view.mc._y;
    var v7 = Data.DOC_WIDTH * hf.mode.Browser.SCALE * 0.95;
    var v8 = Data.DOC_HEIGHT * hf.mode.Browser.SCALE;
    this.draw.lineStyle(thickness, color, 100);
    this.draw.moveTo(v5, v6);
    this.draw.lineTo(v5 + v7, v6);
    this.draw.lineTo(v5 + v7, v6 + v8);
    this.draw.lineTo(v5, v6 + v8);
    this.draw.lineTo(v5, v6);
  }

  public function updatePage(): Void {
    if (!(this.current >= this.first && this.current < this.first + hf.mode.Browser.THUMBS)) {
      this.first = hf.mode.Browser.THUMBS * Std.int(Math.max(0, Math.floor(this.current / hf.mode.Browser.THUMBS)));
      this.refresh();
    }
  }

  public function swapLevels(from: Dynamic /*TODO*/, to: Dynamic /*TODO*/): Bool {
    if (to < 0 || to >= this.world.levels.length) {
      return false;
    }
    if (!this.world.fl_read[from]) {
      this.world.levels[from] = this.world.unserialize(from);
    }
    if (!this.world.fl_read[to]) {
      this.world.levels[to] = this.world.unserialize(to);
    }
    var v4 = Data.duplicate(this.world.levels[from]);
    this.world.levels[from] = this.world.levels[to];
    this.world.levels[to] = v4;
    this.world.fl_read[from] = true;
    this.world.fl_read[to] = true;
    this.fl_modified[from] = true;
    this.fl_modified[to] = true;
    this.current = to;
    this.refresh();
    return true;
  }

  public function onPrevious(): Void {
    this.current = Std.int(Math.max(this.current - 1, 0));
    this.updatePage();
  }

  public function onNext(): Void {
    this.current = Std.int(Math.min(this.current + 1, this.world.levels.length - 1));
    this.updatePage();
  }

  public function onPreviousLine(): Void {
    this.current = Std.int(Math.max(this.current - hf.mode.Browser.LINE, 0));
    this.updatePage();
  }

  public function onNextLine(): Void {
    this.current = Std.int(Math.min(this.current + hf.mode.Browser.LINE, this.world.levels.length - 1));
    this.updatePage();
  }

  public function onPreviousPage(): Void {
    this.current = Std.int(Math.max(this.current - hf.mode.Browser.PAGE, 0));
    this.updatePage();
  }

  public function onNextPage(): Void {
    this.current = Std.int(Math.min(this.current + hf.mode.Browser.PAGE, this.world.levels.length - 1));
    this.updatePage();
  }

  public function onFirst(): Void {
    this.current = 0;
    this.updatePage();
    this.refresh();
  }

  public function onLast(): Void {
    this.current = this.world.levels.length - 1;
    this.updatePage();
    this.refresh();
  }

  public function onValidate(): Void {
    this.manager.stopChild(this.current);
  }

  public function onCancel(): Void {
    this.manager.stopChild(this.initialId);
  }

  public function onMoveAfter(): Void {
    if (this.swapLevels(this.current, this.current + 1)) {
      this.updatePage();
      this.refresh();
    }
  }

  public function onMoveBefore(): Void {
    if (this.swapLevels(this.current, this.current - 1)) {
      this.updatePage();
      this.refresh();
    }
  }

  public function onDelete(): Void {
    if (this.world.levels.length == 1) {
      return;
    }
    this.world.delete(this.current);
    this.fl_modified.splice(this.current, 1);
    for (v2 in this.current...this.world.levels.length) {
      this.fl_modified[v2] = true;
    }
    if (this.current >= this.world.levels.length) {
      --this.current;
    }
    this.world.goto(this.current);
    if (this.current < this.first) {
      this.updatePage();
      this.refresh();
    } else {
      this.refresh();
    }
  }

  public function onInsert(): Void {
    this.world.insert(this.current, new hf.levels.Data());
    for (v2 in this.current...this.world.levels.length) {
      this.fl_modified[v2] = true;
    }
    this.refresh();
  }

  public function onClear(): Void {
    this.world.levels[this.current] = new hf.levels.Data();
    this.fl_modified[this.current] = true;
    this.refresh();
  }

  public function onCut(): Void {
    this.buffer = Data.duplicate(this.world.levels[this.current]);
    this.onClear();
  }

  public function onCopy(): Void {
    this.buffer = Data.duplicate(this.world.levels[this.current]);
  }

  public function onPaste(): Void {
    if (this.buffer == null) {
      return;
    }
    this.world.levels[this.current] = Data.duplicate(this.buffer);
    this.fl_modified[this.current] = true;
    this.refresh();
  }

  public function onSwapBuffer(): Void {
    if (this.buffer == null) {
      this.onCopy();
      this.world.levels[this.current] = new hf.levels.Data();
      this.fl_modified[this.current] = true;
      this.refresh();
    } else {
      var v2 = Data.duplicate(this.world.levels[this.current]);
      this.world.levels[this.current] = Data.duplicate(this.buffer);
      this.fl_modified[this.current] = true;
      this.buffer = v2;
      this.refresh();
    }
  }

  public function onInsertAfter(): Void {
    if (this.current == this.world.levels.length - 1) {
      this.world.push(new hf.levels.Data());
    } else {
      this.world.insert(this.current + 1, new hf.levels.Data());
    }
    for (v2 in this.current + 1...this.world.levels.length) {
      this.fl_modified[v2] = true;
    }
    ++this.current;
    this.updatePage();
    this.refresh();
  }

  public function onBuildCache(): Void {
    this.cacheId = 0;
    this.fl_buildCache = true;
    this.navC.lock();
    this.menuC.lock();
  }

  override public function init(): Void {
    super.init();
  }

  override public function endMode(): Void {
  }

  override public function destroy(): Void {
    for (v3 in 0...this.fields.length) {
      this.fields[v3].removeTextField();
    }
    this.fields = new Array();
    super.destroy();
  }

  override public function main(): Void {
    if (this.fl_buildCache) {
      this.manager.progress(this.cacheId / this.world.levels.length);
      if (!this.world.fl_read[this.cacheId]) {
        this.world.levels[this.cacheId] = this.world.unserialize(this.cacheId);
      }
      ++this.cacheId;
      if (this.cacheId >= this.world.levels.length) {
        this.fl_buildCache = false;
        this.manager.progress(null);
        this.navC.unlock();
        this.menuC.unlock();
      } else {
        return;
      }
    }
    super.main();
    this.navC.update();
    this.menuC.update();
    this.updateBoxes();
    this.footer.field.text = this.current + " / " + (this.world.levels.length - 1);
    if (this.world.levels[this.current].__dollar__script != "" && this.world.levels[this.current].__dollar__script != null) {
      this.footer.scriptIcon._visible = true;
    } else {
      this.footer.scriptIcon._visible = false;
    }
  }
}
