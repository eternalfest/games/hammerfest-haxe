package hf.entity.bomb.bad;

import hf.entity.bomb.BadBomb;
import hf.entity.Player;
import hf.entity.Animator.Anim;
import hf.mode.GameMode;

class Mine extends BadBomb {
  public static var SUDDEN_DEATH: Float = Data.SECOND * 1.1;
  public static var HIDE_SPEED: Float = 3;
  public static var DETECT_RADIUS: Float = Data.CASE_WIDTH * 2.5;

  public var fl_trigger: Bool;
  public var fl_defuse: Bool;
  public var fl_plant: Bool;

  public function new(): Void {
    super();
    this.fl_blink = true;
    this.fl_alphaBlink = false;
    this.duration = Data.SECOND * 15;
    this.power = 50;
    this.radius = Data.CASE_WIDTH * 3;
    this.fl_trigger = false;
    this.fl_defuse = false;
    this.fl_plant = false;
  }

  public static function attach(g: GameMode, x: Float, y: Float): Mine {
    var v5 = "hammer_bomb_mine";
    var v6: Mine = cast g.depthMan.attach(v5, Data.DP_BOMBS);
    v6.initBomb(g, x, y);
    return v6;
  }

  public function triggerMine(): Void {
    if (this.fl_trigger) {
      return;
    }
    this.fl_trigger = true;
    this.playAnim(Data.ANIM_BOMB_DROP);
    this.dy = -7;
    this.show();
    this.alpha = 100;
    this.setLifeTimer(hf.entity.bomb.bad.Mine.SUDDEN_DEATH * 3);
    this.updateLifeTimer(hf.entity.bomb.bad.Mine.SUDDEN_DEATH);
    this.blinkLife();
  }

  override public function duplicate(): Mine {
    return hf.entity.bomb.bad.Mine.attach(this.game, this.x, this.y);
  }

  override public function initBomb(g: GameMode, x: Float, y: Float): Void {
    super.initBomb(g, x, y);
    if (this.game.fl_bombExpert) {
      this.radius *= 1.3;
    }
  }

  override public function onExplode(): Void {
    if (!this.fl_trigger || this.fl_defuse) {
      this.game.fxMan.attachFx(this.x, this.y - Data.CASE_HEIGHT, "hammer_fx_pop");
      this.destroy();
    } else {
      super.onExplode();
      this.game.fxMan.attachExplodeZone(this.x, this.y, this.radius);
      var v3 = this.game.getClose(Data.PLAYER, this.x, this.y, this.radius, false);
      for (v4 in 0...v3.length) {
        var v5 = v3[v4];
        v5.killHit(0);
        this.shockWave(v5, this.radius, this.power);
        if (!v5.fl_shield) {
          v5.dy = -10 - HfStd.random(20);
        }
      }
    }
  }

  override public function onHitGround(h: Float): Void {
    super.onHitGround(h);
    if (!this.fl_trigger) {
      this.playAnim(Data.ANIM_BOMB_LOOP);
    }
    if (!this.fl_defuse) {
      this.rotation = 0;
    }
  }

  override public function onKick(p: Player): Void {
    super.onKick(p);
    this.triggerMine();
    this.updateLifeTimer(Data.SECOND * 0.7);
    this.dx *= 0.8 + HfStd.random(10) / 10;
  }

  override public function playAnim(a: Anim): Void {
    super.playAnim(a);
    if (a.id == Data.ANIM_BOMB_DROP.id) {
      this.fl_loop = true;
    }
    if (a.id == Data.ANIM_BOMB_LOOP.id) {
      this.fl_loop = false;
    }
  }

  override public function update(): Void {
    super.update();
    if (this.fl_stable && !this.fl_plant) {
      this.fl_plant = true;
    }
    if (this.fl_plant && !this.fl_trigger && this.alpha > 0) {
      this.alpha -= Timer.tmod * hf.entity.bomb.bad.Mine.HIDE_SPEED;
      if (this.alpha <= 0) {
        this.hide();
      }
    }
    if (this.fl_plant && !this.fl_trigger) {
      var v3 = this.game.getClose(Data.PLAYER, this.x, this.y, hf.entity.bomb.bad.Mine.DETECT_RADIUS, false);
      for (v4 in 0...v3.length) {
        if (!v3[v4].fl_kill) {
          this.triggerMine();
        }
      }
    }
  }
}
