package hf.mode;

import hf.mode.Adventure;
import etwin.flash.SharedObject;

class Fjv extends Adventure {
  public var fCookie: Dynamic /*TODO*/;

  public function new(m: Dynamic /*TODO*/, id: Dynamic /*TODO*/): Void {
    super(m, id);
    this.fCookie = SharedObject.getLocal("$fjv_data");
    this._name = "$fjv_special";
  }

  public function darknessManager(): Void {
  }

  override public function initWorld(): Void {
    this.world = new hf.levels.GameMechanics(this.manager, "xml_fjv");
    this.world.setDepthMan(this.depthMan);
    this.world.setGame(this);
  }

  override public function initGame(): Void {
    super.initGame();
    var v3 = this.getOne(Data.PLAYER);
    v3.lives = 2;
    this.gi.setLives(v3.pid, v3.lives);
  }

  override public function endMode(): Void {
    HfStd.setVar(this.fCookie.data, "score", (this.getPlayerList())[0].score);
    this.stopMusic();
    this.manager.startMode(new hf.mode.FjvEnd(this.manager, false));
  }

  override public function usePortal(pid: Dynamic /*TODO*/, e: Dynamic /*TODO*/): Bool {
    this.stopMusic();
    HfStd.setVar(this.fCookie.data, "score", (this.getPlayerList())[0].score);
    this.manager.startMode(new hf.mode.FjvEnd(this.manager, true));
    return true;
  }

  override public function onEndOfSet(): Void {
    this.endMode();
  }

  override public function onGameOver(): Void {
    this.endMode();
  }

  override public function onLevelReady(): Void {
    super.onLevelReady();
    if (this.world.currentId == 0) {
      this.fxMan.attachLevelPop(Lang.get(17), true);
    } else {
      this.fxMan.levelName.removeMovieClip();
    }
  }

  override public function onPause(): Void {
    super.onPause();
    this.pauseMC.sector.text = Lang.get(14) + " «" + Lang.get(17) + "»";
  }
}
