package hf.entity.bad;

import hf.entity.bad.Walker;
import hf.mode.GameMode;

class Jumper extends Walker {
  public var fl_jUp: Bool;
  public var chanceJumpUp: Float;
  public var fl_jDown: Bool;
  public var chanceJumpDown: Float;
  public var fl_jH: Bool;
  public var chanceJumpH: Float;
  public var fl_climb: Bool;
  public var chanceClimb: Float;
  public var maxClimb: Int;
  public var fl_jumper: Bool;
  public var jumpTimer: Float;

  public function new(): Void {
    super();
    this.jumpTimer = 0;
    this.setJumpUp(null);
    this.setJumpDown(null);
    this.setJumpH(null);
    this.setClimb(null, null);
  }

  public function setJumpUp(chance: Null<Float>): Void {
    if (chance == null) {
      this.fl_jUp = false;
    } else {
      this.fl_jUp = true;
      this.chanceJumpUp = chance * 10;
    }
    this.setJumper();
  }

  public function setJumpDown(chance: Null<Float>): Void {
    if (chance == null) {
      this.fl_jDown = false;
    } else {
      this.fl_jDown = true;
      this.chanceJumpDown = chance * 10;
    }
    this.setJumper();
  }

  public function setJumpH(chance: Null<Float>): Void {
    if (chance == null) {
      this.fl_jH = false;
    } else {
      this.fl_jH = true;
      this.chanceJumpH = chance * 10;
    }
    this.setJumper();
  }

  public function setClimb(chance: Null<Float>, max: Null<Int>): Void {
    if (chance == null) {
      this.fl_climb = false;
      this.maxClimb = 0;
    } else {
      this.fl_climb = true;
      this.maxClimb = max;
      this.chanceClimb = chance * 10;
    }
    this.setJumper();
  }

  public function setJumper(): Void {
    this.fl_jumper = this.fl_jUp || this.fl_jDown || this.fl_jH;
  }

  public function decideJumpUp(): Bool {
    if (this.fl_playerClose) {
      return this.player.cy < this.cy && HfStd.random(1000) < this.chanceJumpUp * this.chaseFactor && this.isReady();
    }
    return HfStd.random(1000) < this.chanceJumpUp && this.isReady();
  }

  public function decideJumpDown(): Bool {
    if (this.fl_playerClose) {
      return this.player.cy > this.cy && HfStd.random(1000) < this.chanceJumpDown * this.chaseFactor && this.isReady();
    }
    return HfStd.random(1000) < this.chanceJumpDown && this.isReady();
  }

  public function decideClimb(): Bool {
    var v2 = Math.abs(this.player.cy - this.cy);
    var v3 = v2 >= 3 || v2 < 3 && (this.player.cx < this.cx && this.dx < 0 || this.player.cx > this.cx && this.dx > 0);
    var v4 = (this.world.checkFlag({x: this.cx, y: this.cy}, Data.IA_CLIMB_LEFT) || this.world.checkFlag({x: this.cx, y: this.cy}, Data.IA_CLIMB_RIGHT)) && this.world.checkFlag({x: this.cx, y: this.cy}, Data.IA_SMALL_SPOT);
    if (this.fl_playerClose) {
      return this.isReady() && (v4 || v3 && HfStd.random(1000) < this.chanceClimb * this.chaseFactor);
    }
    return this.isReady() && (v4 || HfStd.random(1000) < this.chanceClimb);
  }

  public function jump(dx: Float, dy: Float, delay: Float): Void {
    this.halt();
    this.setNext(dx, dy, delay, Data.ACTION_MOVE);
    if (delay > 0) {
      this.playAnim(Data.ANIM_BAD_THINK);
    }
  }

  public function checkClimb(): Void {
    var v4;
    var v2;
    if (this.decideClimb()) {
      if (this.dx < 0 && this.world.checkFlag({x: this.cx, y: this.cy}, Data.IA_CLIMB_LEFT)) {
        if (this.world.checkFlag({x: this.cx, y: this.cy}, Data.IA_TILE_TOP)) {
          v2 = this.world.getWallHeight(this.cx - 1, this.cy, Data.IA_CLIMB);
        } else {
          v2 = this.world.getStepHeight(this.cx, this.cy, Data.IA_CLIMB);
        }
        if (v2 <= this.maxClimb) {
          var v3 = v2 <= 1 ? 0 : Data.SECOND;
          if (this.world.checkFlag({x: this.cx, y: this.cy}, Data.IA_TILE_TOP)) {
            this.jump(-Data.BAD_VJUMP_X_CLIFF, -Data.BAD_VJUMP_Y_LIST[v2 - 1], v3);
          } else {
            this.jump(-Data.BAD_VJUMP_X, -Data.BAD_VJUMP_Y_LIST[v2 - 1], v3);
            this.x = this.oldX;
          }
          this.fl_stopStepping = true;
        }
      }
      if (this.dx > 0 && this.world.checkFlag({x: this.cx, y: this.cy}, Data.IA_CLIMB_RIGHT)) {
        if (this.world.checkFlag({x: this.cx, y: this.cy}, Data.IA_TILE_TOP)) {
          v4 = this.world.getWallHeight(this.cx + 1, this.cy, Data.IA_CLIMB);
        } else {
          v4 = this.world.getStepHeight(this.cx, this.cy, Data.IA_CLIMB);
        }
        if (v4 <= this.maxClimb) {
          var v5 = v4 <= 1 ? 0 : Data.SECOND;
          if (this.world.checkFlag({x: this.cx, y: this.cy}, Data.IA_TILE_TOP)) {
            this.jump(Data.BAD_VJUMP_X_CLIFF, -Data.BAD_VJUMP_Y_LIST[v4 - 1], v5);
          } else {
            this.jump(Data.BAD_VJUMP_X, -Data.BAD_VJUMP_Y_LIST[v4 - 1], v5);
            this.x = this.oldX;
          }
          this.fl_stopStepping = true;
        }
      }
    }
  }

  override public function infix(): Void {
    super.infix();
    if (this.fl_jumper) {
      this.updateCoords();
      if (this.fl_jUp) {
        if (this.decideJumpUp()) {
          if (this.world.checkFlag({x: this.cx, y: this.cy}, Data.IA_JUMP_UP)) {
            this.jump(0, -Data.BAD_VJUMP_Y, Data.SECOND);
            this.fl_stopStepping = true;
          }
        }
      }
      if (this.fl_jDown) {
        if (this.decideJumpDown()) {
          if (this.world.checkFlag({x: this.cx, y: this.cy}, Data.IA_JUMP_DOWN)) {
            this.jump(0, -Data.BAD_VDJUMP_Y, Data.SECOND);
            this.fl_skipNextGround = true;
            this.fl_stopStepping = true;
          }
        }
      }
    }
  }

  override public function init(g: GameMode): Void {
    super.init(g);
  }

  override public function onFall(): Void {
    if (this.fl_jumper) {
      if (this.isReady() && this.fl_jH) {
        if (this.fl_fall && this.decideFall()) {
          if (this.world.checkFlag({x: this.cx, y: this.cy}, Data.IA_ALLOW_FALL)) {
            this.fl_willFallDown = true;
          }
        }
        if (this.fl_fall && this.fl_climb) {
          if (this.world.checkFlag({x: this.cx, y: this.cy + 1}, Data.IA_CLIMB_LEFT) || this.world.checkFlag({x: this.cx, y: this.cy + 1}, Data.IA_CLIMB_RIGHT)) {
            this.fl_willFallDown = true;
          }
        }
        if (!this.fl_willFallDown) {
          if (this.dx < 0 && this.world.checkFlag({x: this.cx, y: this.cy}, Data.IA_JUMP_LEFT)) {
            this.jump(-Data.BAD_HJUMP_X, -Data.BAD_HJUMP_Y, 0);
            this.adjustToRight();
          }
          if (this.dx > 0 && this.world.checkFlag({x: this.cx, y: this.cy}, Data.IA_JUMP_RIGHT)) {
            this.jump(Data.BAD_HJUMP_X, -Data.BAD_HJUMP_Y, 0);
            this.adjustToLeft();
          }
          if (this.fl_climb) {
            this.checkClimb();
          }
        }
      }
    }
    super.onFall();
  }

  override public function onFreeze(): Void {
    super.onFreeze();
    this.fl_skipNextGround = false;
  }

  override public function onHitWall(): Void {
    if (this.fl_climb) {
      this.checkClimb();
    }
    super.onHitWall();
  }

  override public function onKnock(): Void {
    super.onKnock();
    this.fl_skipNextGround = false;
  }

  // Added to restore the "super chain" between Abricot and Bad.
  // If this weren't here, Bad.onDeathLine would end up being called twice.
  override public function onDeathLine(): Void {
    super.onDeathLine();
  }

  override public function onNext(): Void {
    if (this.next.action == Data.ACTION_MOVE && this.next.dy != 0) {
      this.playAnim(Data.ANIM_BAD_JUMP);
    }
    super.onNext();
  }
}
