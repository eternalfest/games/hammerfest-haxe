package hf;

import etwin.flash.Color;
import etwin.flash.MovieClip;
import hf.levels.GameMechanics;
import hf.mode.GameMode;
import hf.Pos;
import etwin.flash.Key;
import etwin.flash.System;
using hf.compat.StringTools;
using hf.compat.ColorTools;

class Entity extends MovieClip {
  public var game: GameMode;

  /**
   * Unique id.
   *
   * This integer uniquely identifies the entity instance within the current
   * game mode.
   */
  public var uniqId: Int;
  public var parent: Null<Entity>;
  public var world: Null<GameMechanics>;

  /**
   * Bitset of types.
   */
  // @:deprecated("TODO unsafe explanation")
  public var types: Int;

  public var x: Float;
  public var y: Float;
  public var oldX: Float;
  public var oldY: Float;
  public var cx: Int;
  public var cy: Int;
  public var fcx: Int;
  public var fcy: Int;
  public var dir: Null<Int>;
  public var dx: Null<Float>;
  public var dy: Null<Float>;
  public var alpha: Float;
  public var rotation: Float;
  public var minAlpha: Float;
  public var blendId: Int;
  public var _xOffset: Float;
  public var _yOffset: Float;
  public var scaleFactor: Float;
  public var color: Color;
  public var totalLife: Float;
  public var lifeTimer: Float;
  public var fl_kill: Bool;
  public var fl_destroy: Bool;
  public var fl_stick: Bool;
  public var sticker: MovieClip;
  public var stickerX: Float;
  public var stickerY: Float;
  public var stickTimer: Float;
  public var fl_elastick: Bool;
  public var elaStickFactor: Float;
  public var fl_stickRot: Bool;
  public var fl_stickBound: Bool;
  public var fl_softRecal: Bool;
  public var softRecalFactor: Float;
  public var defaultBlend: Dynamic /*TODO*/;
  public var scriptId: Null<Int>;

  public function new(): Void {
    this.types = 0;
    this.x = 0;
    this.y = 0;
    this.alpha = 100;
    this.rotation = 0;
    this.minAlpha = 35;
    this.defaultBlend = BlendMode.NORMAL;
    this.stickTimer = 0;
    this._xOffset = 0;
    this._yOffset = 0;
    this.updateCoords();
    this.fl_kill = false;
    this.fl_destroy = false;
    this.fl_stickRot = false;
    this.fl_stickBound = false;
    this.fl_softRecal = false;
    if (this.game.manager.fl_debug) {
      this.onRelease = this.release;
      this.onRollOver = this.rollOver;
      this.onRollOut = this.rollOut;
    }
  }

  public function init(g: GameMode): Void {
    this.game = g;
    this.uniqId = this.game.getUniqId();
    this.register(Data.ENTITY);
    this.world = this.game.world;
    this.scale(100);
  }

  // TODO: how to prevent register/unregister with invalid EntityType???
  // @:deprecated("TODO unsafe explanation")
  public function register<E: Entity>(type: EntityType<E>): Void {
    this.game.addToList(type, cast this);
    this.types |= type.asInt();
  }

  // @:deprecated("TODO unsafe explanation")
  public function unregister<E: Entity>(type: EntityType<E>): Void {
    this.game.removeFromList(type, this);
    this.types ^= type.asInt();
  }

  public function isType<E: Entity>(t: EntityType<E>): Bool {
    return t.check(this);
  }

  public function setParent(e: Entity): Void {
    this.parent = e;
  }

  public function setLifeTimer(t: Float): Void {
    this.lifeTimer = t;
    this.totalLife = t;
  }

  public function updateLifeTimer(t: Float): Void {
    if (this.totalLife == null) {
      this.setLifeTimer(t);
    } else {
      this.lifeTimer = t;
    }
  }

  public function onLifeTimer(): Void {
    this.destroy();
  }

  public function hitBound(e: Entity): Bool {
    var v3 = this.x + this._width / 2 > e.x - e._width / 2 && this.y > e.y - e._height && this.x - this._width / 2 < e.x + e._width / 2 && this.y - this._height < e.y;
    return v3;
  }

  public function hit(e: Entity): Void {
  }

  public function destroy(): Void {
    this.fl_kill = true;
    this.fl_destroy = true;
    this.unstick();
    for (v2 in 0...32) {
      if ((this.types & 1 << v2) > 0) {
        this.game.unregList.push({
          type: EntityType.fromUnchecked(Math.round(Math.pow(2, v2))),
          ent: this,
        });
      }
    }
    this.game.killList.push(this);
  }

  public function stick(mc: MovieClip, ox: Float, oy: Float): Void {
    if (this.sticker._name != null) {
      this.unstick();
    }
    this.sticker = mc;
    this.stickerX = ox;
    this.stickerY = oy;
    this.fl_stick = true;
    this.fl_stickRot = false;
    this.fl_stickBound = false;
    this.fl_elastick = false;
  }

  public function setElaStick(f: Float): Void {
    if (this.fl_elastick) {
      return;
    }
    this.elaStickFactor = f;
    this.fl_elastick = true;
    this.stickerX *= this.elaStickFactor;
    this.stickerY *= this.elaStickFactor;
  }

  public function unstick(): Void {
    this.fl_stick = false;
    this.sticker.removeMovieClip();
  }

  public function activateSoftRecal(): Void {
    this.fl_softRecal = true;
    this.softRecalFactor = 0.1;
  }

  public function release(): Void {
    if (Key.isDown(Key.SHIFT)) {
      if (Key.isDown(Key.CONTROL)) {
        Log.trace("Full serialization: " + this.short());
        System.setClipboard(Log.toString(this));
      } else {
        Log.clear();
        Log.trace(this.short());
        Log.trace("----------");
        Log.trace("dir=" + this.dir + " dx=" + this.dx + " dy=" + this.dy + " xscale=" + this._xscale);
      }
    }
  }

  public function rollOver(): Void {
    if (Key.isDown(Key.SHIFT)) {
      var v2 = new etwin.flash.filters.GlowFilter();
      v2.quality = 1;
      v2.color = 16777215;
      v2.strength = 200;
      this.filters = [v2];
    }
  }

  public function rollOut(): Void {
    if (this.filters != null) {
      this.filters = null;
    }
  }

  public function hide(): Void {
    this._visible = false;
    if (this.sticker._name != null) {
      this.sticker._visible = this._visible;
    }
  }

  public function show(): Void {
    this._visible = true;
    if (this.sticker._name != null) {
      this.sticker._visible = this._visible;
    }
  }

  public function scale(n: Float): Void {
    this.scaleFactor = n / 100;
    this._xscale = n;
    this._yscale = this._xscale;
  }

  public function setColorHex(a: Float, col: Int): Void {
    var v4 = {r: col >> 16, g: col >> 8 & 255, b: col & 255};
    var v5 = a / 100;
    var v6: etwin.flash.ColorTransform = {ra: Std.int(100 - a), ga: Std.int(100 - a), ba: Std.int(100 - a), aa: 100, rb: Std.int(v5 * v4.r), gb: Std.int(v5 * v4.g), bb: Std.int(v5 * v4.b), ab: 0};
    this.color = new Color(this);
    this.color.setTransform(v6);
  }

  public function resetColor(): Void {
    this.color.reset();
    this.color = null;
  }

  public function setBlend(m: Int): Void {
    this.defaultBlend = m;
    this.blendMode = m;
    this.blendId = m;
  }

  public function updateCoords(): Void {
    this.cx = Entity.x_rtc(this.x);
    this.cy = Entity.y_rtc(this.y);
    this.fcx = Entity.x_rtc(this.x);
    this.fcy = Entity.y_rtc(this.y + Math.floor(Data.CASE_HEIGHT / 2));
  }

  public static function rtc(x: Float, y: Float): Pos<Int> {
    return {x: Entity.x_rtc(x), y: Entity.y_rtc(y)};
  }

  public static function x_rtc(n: Float): Int {
    return Math.floor(n / Data.CASE_WIDTH);
  }

  public static function y_rtc(n: Float): Int {
    return Math.floor((n - Data.CASE_HEIGHT / 2) / Data.CASE_HEIGHT);
  }

  public static function x_ctr(n: Float): Float {
    return n * Data.CASE_WIDTH + Data.CASE_WIDTH * 0.5;
  }

  public static function y_ctr(n: Float): Float {
    return n * Data.CASE_HEIGHT + Data.CASE_HEIGHT;
  }

  public function adjustAngle(a: Float): Float {
    while (a < 0) {
      a += 360;
    }
    while (a >= 360) {
      a -= 360;
    }
    return a;
  }

  public function adjustToLeft(): Void {
    this.x = Entity.x_ctr(this.cx);
    this.y = Entity.y_ctr(this.cy);
    this.x -= Data.CASE_WIDTH * 0.5 + 1;
  }

  public function adjustToRight(): Void {
    this.x = Entity.x_ctr(this.cx);
    this.y = Entity.y_ctr(this.cy);
    this.x += Data.CASE_WIDTH * 0.5 - 1;
  }

  public function centerInCase(): Void {
    this.x = Entity.x_ctr(this.cx);
    this.y = Entity.y_ctr(this.cy);
  }

  public function distanceCase(cx: Float, cy: Float): Float {
    return Math.sqrt(Math.pow(cy - this.cy, 2) + Math.pow(cx - this.cx, 2));
  }

  public function distance(x: Float, y: Float): Float {
    return Math.sqrt(Math.pow(y - this.y, 2) + Math.pow(x - this.x, 2));
  }

  public function update(): Void {
    if (this.lifeTimer > 0) {
      this.lifeTimer -= Timer.tmod;
      if (this.lifeTimer <= 0) {
        this.onLifeTimer();
      }
    }
    if (this.stickTimer > 0) {
      this.stickTimer -= Timer.tmod;
      if (this.stickTimer <= 0) {
        this.unstick();
      }
    }
  }

  public function short(): String {
    var v2 = "" + this;
    v2 = v2.slice(v2.lastIndexOf(".", 9999) + 1, 9999);
    v2 = v2 + "(@" + this.cx + "," + this.cy + ")";
    return v2;
  }

  public function printTypes(): String {
    var v2 = new Array();
    var v3 = 0;
    for (v4 in 0...30) {
      var v5 = (this.types & 1 << v3++) > 0;
      if (v5) {
        v2.push(v4);
      }
    }
    return v2.join(",");
  }

  public function endUpdate(): Void {
    this.updateCoords();
    if (this.fl_softRecal) {
      var v2 = this.x + this._xOffset;
      var v3 = this.y + this._yOffset;
      this._x += (v2 - this._x) * this.softRecalFactor;
      this._y += (v3 - this._y) * this.softRecalFactor;
      this.softRecalFactor += 0.02 * Timer.tmod;
      if (this.softRecalFactor >= 1 || Math.abs(v2 - this._x) <= 1.5 && Math.abs(v3 - this._y) <= 1.5) {
        this.fl_softRecal = false;
      }
    }
    if (!this.fl_softRecal) {
      this._x = this.x + this._xOffset;
      this._y = this.y + this._yOffset;
    }
    this._rotation = this.rotation;
    this._alpha = Math.max(this.minAlpha, this.alpha);
    if (this.alpha != 100 && this.blendId <= 2) {
      this.blendMode = BlendMode.LAYER;
    } else {
      this.blendMode = this.defaultBlend;
    }
    this.oldX = this.x;
    this.oldY = this.y;
    if (this.fl_stick) {
      if (this.fl_elastick) {
        this.sticker._x = this.sticker._x + (this.x - this.sticker._x) * this.elaStickFactor + this.stickerX;
        this.sticker._y = this.sticker._y + (this.y - this.sticker._y) * this.elaStickFactor + this.stickerY;
      } else {
        this.sticker._x = this.x + this.stickerX;
        this.sticker._y = this.y + this.stickerY;
      }
      if (this.fl_stickRot) {
        this.sticker._rotation += 8 * Timer.tmod;
      }
      if (this.fl_stickBound) {
        this.sticker._x = Math.max(this.sticker._x, this.sticker._width * 0.5);
        this.sticker._x = Math.min(this.sticker._x, Data.GAME_WIDTH - this.sticker._width * 0.5);
      }
    }
  }
}
