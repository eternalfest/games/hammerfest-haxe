package hf.entity.bad;

import etwin.flash.MovieClip;
import hf.entity.Bad;
import hf.entity.Player;
import hf.entity.Animator.Anim;
import hf.Entity;
import hf.mode.GameMode;

class FireBall extends Bad {
  public var speed: Float;
  public var angSpeed: Float;
  public var ang: Float;
  public var summonTimer: Float;
  public var fl_summon: Bool;
  public var angerTimer: Float;
  public var body: MovieClip;
  public var eyes: MovieClip;
  public var tang: Float;

  public function new(): Void {
    super();
    this.disableAnimator();
    this.fl_hitGround = false;
    this.fl_hitWall = false;
    this.fl_gravity = false;
    this.fl_hitBorder = false;
    this.fl_alphaBlink = true;
    this.speed = 2.5;
    this.angSpeed = 1.5;
    this.ang = 270;
    this.summonTimer = 85;
    this.fl_summon = true;
    this.blink(Data.BLINK_DURATION);
    this.angerTimer = 0;
    this.angerFactor = 0.05;
    this.maxAnger = 9999;
  }

  public static function attach(g: GameMode, p: Player): FireBall {
    var v4 = Data.LINKAGES[Data.BAD_FIREBALL];
    var v5: FireBall = cast g.depthMan.attach(v4, Data.DP_BADS);
    var v6 = g.countList(Data.PLAYER);
    if (v6 == 1) {
      var v7 = 0;
      v5.initBad(g, Data.GAME_WIDTH / 2 + v7, Data.GAME_HEIGHT + 10);
      v5.hate(p);
      return v5;
    }
    var v7 = -30 + p.pid * 60;
    v5.initBad(g, Data.GAME_WIDTH / 2 + v7, Data.GAME_HEIGHT + 10);
    v5.hate(p);
    return v5;
  }

  override public function angerMore(): Void {
    super.angerMore();
    this.angSpeed *= 1.15;
  }

  override public function endUpdate(): Void {
    super.endUpdate();
    this.body._rotation = this.ang;
    this.eyes.gotoAndStop("" + (Math.round((this.ang / 360) * this.eyes._totalframes) + 1));
  }

  override public function freeze(d: Float): Void {
  }

  override public function hit(e: Entity): Void {
    if (this.fl_summon) {
      return;
    }
    var v3 = Data.PLAYER.downcast(e);
    if (v3 != null) {
      if (v3.animId != Data.ANIM_PLAYER_DIE.id) {
        v3.fl_shield = false;
        v3.killHit(this.dx);
      }
    }
    var v4 = Data.BOMB.downcast(e);
    if (v4 != null) {
      if (!v4.fl_kill && !v4.fl_explode) {
        v4.onExplode();
      }
    }
  }

  override public function init(g: GameMode): Void {
    super.init(g);
    this.register(Data.HU_BAD);
    this.unregister(Data.BAD_CLEAR);
  }

  override public function killHit(dx: Null<Float>): Void {
  }

  override public function knock(d: Float): Void {
  }

  override public function onDeathLine(): Void {
  }

  override public function onHurryUp(): Void {
  }

  override public function playAnim(o: Anim): Void {
  }

  override public function tAdd(cx: Int, cy: Int): Void {
    super.tAdd(cx, cy);
    this.tAddSingle(cx, cy + 1);
  }

  override public function tRem(cx: Int, cy: Int): Void {
    super.tRem(cx, cy);
    this.tRemSingle(cx, cy + 1);
  }

  override public function update(): Void {
    if (this.fl_summon) {
      this.summonTimer -= Timer.tmod;
      if (this.summonTimer <= 0) {
        this.fl_summon = false;
        this.stopBlink();
      }
    }
    if (this.player.fl_kill || this.player._name == null) {
      this.game.fxMan.attachShine(this.x, this.y);
      this.game.fxMan.attachExplodeZone(this.x, this.y, 4 * Data.CASE_WIDTH);
      this.game.shake(Data.SECOND * 1, 5);
      this.destroy();
      return;
    }
    this.angerTimer += Timer.tmod * this.game.diffFactor;
    if (this.angerTimer >= Data.AUTO_ANGER) {
      this.angerTimer = 0;
      this.angerMore();
    }
    this.tang = Math.atan2(this.player.y - this.y, this.player.x - this.x);
    this.tang = this.adjustAngle(this.tang * 180 / Math.PI);
    if (this.ang - this.tang > 180) {
      this.ang -= 360;
    }
    if (this.tang - this.ang > 180) {
      this.ang += 360;
    }
    if (this.ang < this.tang) {
      this.ang += this.angSpeed * this.speedFactor * Timer.tmod;
      if (this.ang > this.tang) {
        this.ang = this.tang;
      }
    }
    if (this.ang > this.tang) {
      this.ang -= this.angSpeed * this.speedFactor * Timer.tmod;
      if (this.ang < this.tang) {
        this.ang = this.tang;
      }
    }
    this.ang = this.adjustAngle(this.ang);
    this.moveToAng(this.ang, this.speed * this.speedFactor);
    super.update();
  }
}
