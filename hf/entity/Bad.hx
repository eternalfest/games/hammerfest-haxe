package hf.entity;

import etwin.flash.MovieClip;
import hf.entity.Mover;
import hf.entity.Player;
import hf.entity.Animator.Anim;
import hf.Entity;
import hf.mode.GameMode;

class Bad extends Mover {
  public var player: Null<Player>;
  public var fl_playerClose: Null<Bool>;
  public var comboId: Null<Int>;
  public var freezeTimer: Float;
  public var freezeTotal: Null<Float>;
  public var fl_freeze: Bool;
  public var knockTimer: Float;
  public var fl_knock: Bool;
  public var deathTimer: Null<Float>;
  public var yTrigger: Null<Float>;
  public var fl_ninFoe: Bool;
  public var fl_ninFriend: Bool;
  public var fl_showIA: Bool;
  public var fl_trap: Bool;
  public var closeDistance: Float;
  public var realRadius: Float;
  public var speedFactor: Float;
  public var anger: Int;
  public var angerFactor: Float;
  public var maxAnger: Int;
  public var chaseFactor: Float;
  public var iceMc: MovieClip;

  public function new(): Void {
    super();
    this.comboId = null;
    this.freezeTimer = 0;
    this.fl_freeze = false;
    this.knockTimer = 0;
    this.fl_knock = false;
    this.yTrigger = null;
    this.fl_ninFoe = false;
    this.fl_ninFriend = false;
    this.fl_showIA = false;
    this.fl_trap = true;
    this.fl_strictGravity = false;
    this.fl_largeTrigger = true;
    this.closeDistance = Data.IA_CLOSE_DISTANCE;
    this.realRadius = Data.CASE_WIDTH;
    this.maxAnger = Data.MAX_ANGER;
    this.speedFactor = 1.0;
    this.anger = 0;
    this.angerFactor = 0.7;
    this.chaseFactor = 5.0;
    this.calcSpeed();
  }

  public function initBad(g: GameMode, x: Float, y: Float): Void {
    this.init(g);
    this.moveTo(x + Data.CASE_WIDTH * 0.5, y + Data.CASE_HEIGHT);
    this.endUpdate();
  }

  public function forceKill(dx: Null<Float>): Void {
    this.killHit(dx);
  }

  public function burn(): Void {
    var v2 = this.game.fxMan.attachFx(this.x, this.y, "hammer_fx_burning");
    this.dropReward();
    this.onKill();
    this.destroy();
  }

  public function isHealthy(): Bool {
    return !this.fl_kill && !this.fl_freeze && !this.fl_knock;
  }

  public function setCombo(id: Int): Void {
    if (!this.fl_kill) {
      this.comboId = id;
    }
  }

  public function calcSpeed(): Void {
    this.speedFactor = 1.0 + this.angerFactor * this.anger;
    if (this.game.globalActives[69]) {
      this.speedFactor *= 0.6;
    }
    if (this.game.globalActives[80]) {
      this.speedFactor *= 0.3;
    }
  }

  public function updateSpeed(): Void {
    this.calcSpeed();
  }

  public dynamic function dropReward(): Void {
    var v2;
    if (this.world.inBound(this.cx, this.cy)) {
      v2 = this.y;
    } else {
      v2 = -30;
    }
    if (this.comboId != null) {
      var v3 = this.game.countCombo(this.comboId) - 1;
      if (v3 + 1 > this.game.statsMan.read(Data.STAT_MAX_COMBO)) {
        this.game.statsMan.write(Data.STAT_MAX_COMBO, v3 + 1);
      }
      hf.entity.item.ScoreItem.attach(this.game, this.x, v2, 0, v3);
    } else {
      hf.entity.item.ScoreItem.attach(this.game, this.x, v2, Data.DIAMANT, null);
    }
  }

  public function evaluateSpeed(): Float {
    return Math.sqrt(Math.pow(this.dx, 2) + Math.pow(this.dy, 2));
  }

  public function onMelt(): Void {
    this.iceMc.removeMovieClip();
  }

  public function onWakeUp(): Void {
  }

  public function onFreeze(): Void {
    this.playAnim(Data.ANIM_BAD_FREEZE);
    if (this.iceMc._name == null) {
      this.iceMc = this.game.depthMan.attach("GiftGod", Data.DP_BADS);
    }
  }

  public function onKnock(): Void {
    this.playAnim(Data.ANIM_BAD_KNOCK);
  }

  public function onHurryUp(): Void {
    this.angerMore();
  }

  public function freeze(timer: Float): Void {
    if (this.fl_kill) {
      return;
    }
    if (this.fl_knock) {
      this.wakeUp();
    }
    this.fallFactor = Data.FALL_FACTOR_FROZEN;
    this.fl_slide = true;
    this.freezeTimer = timer;
    this.freezeTotal = timer;
    this.fl_freeze = true;
    this.onFreeze();
  }

  public function knock(timer: Float): Void {
    if (this.fl_freeze) {
      this.melt();
    }
    this.fallFactor = Data.FALL_FACTOR_KNOCK;
    this.knockTimer = timer;
    this.fl_knock = true;
    this.game.statsMan.inc(Data.STAT_KNOCK, 1);
    this.onKnock();
  }

  public function melt(): Void {
    this.next = null;
    this.angerMore();
    this.freezeTimer = 0;
    this.fl_freeze = false;
    this.fl_slide = false;
    this.fallFactor = 1.0;
    this.onMelt();
  }

  public function wakeUp(): Void {
    this.next = null;
    this.knockTimer = 0;
    this.fl_knock = false;
    this.fallFactor = 1.0;
    this.onWakeUp();
  }

  public function calmDown(): Void {
    if (this.fl_kill) {
      return;
    }
    this.anger = 0;
    this.updateSpeed();
  }

  public function angerMore(): Void {
    this.anger = Math.round(Math.min(this.anger + 1, this.maxAnger));
    this.updateSpeed();
  }

  public function hate(p: Player): Void {
    this.player = p;
  }

  override public function destroy(): Void {
    super.destroy();
    this.iceMc.removeMovieClip();
  }

  override public function endUpdate(): Void {
    if (this.fl_ninFriend || this.fl_ninFoe) {
      if (this.isHealthy() && this.sticker._name == null) {
        var v3 = this.game.depthMan.attach("hammer_interf_ninjaIcon", Data.DP_BADS);
        if (this.fl_ninFoe) {
          v3.gotoAndStop("1");
        } else {
          v3.gotoAndStop("2");
        }
        this.stick(v3, 0, -Data.CASE_HEIGHT * 1.8);
      }
    }
    var v4 = this._x;
    var v5 = this._y;
    if (this.fl_softRecal) {
      this.softRecalFactor += 0.1 * Timer.tmod * this.speedFactor;
    }
    super.endUpdate();
    var v6 = 2;
    if (GameManager.CONFIG.fl_detail) {
      if (this.fl_freeze && this.fl_stable && Math.abs(this.dx) > v6) {
        var v7 = HfStd.random(5) + 2;
        for (v8 in 0...v7) {
          var v9 = this.game.fxMan.attachFx(v4 + HfStd.random(12) * (HfStd.random(2) * 2 - 1), v5 - HfStd.random(5) + 2, "hammer_fx_partIce");
          v9.mc._rotation = HfStd.random(360);
          v9.mc._xscale = HfStd.random(80) + 20;
          v9.mc._yscale = v9.mc._xscale;
          v9.mc._alpha = Math.min(100, ((Math.abs(this.dx) - v6) / 5) * 100);
        }
      }
    }
    if (this.iceMc._name != null) {
      this.iceMc._x = this._x;
      this.iceMc._y = this._y;
      this.iceMc._xscale = 0.85 * this.scaleFactor * 100;
      this.iceMc._yscale = (0.85 * this.scaleFactor * this.freezeTimer / this.freezeTotal) * 100;
    }
    if (!this.fl_stable && this.isHealthy() && (this.animId == null || this.animId == Data.ANIM_BAD_WALK.id || this.animId == Data.ANIM_BAD_ANGER.id)) {
      this.playAnim(Data.ANIM_BAD_JUMP);
    }
  }

  override public function hit(e: Entity): Void {
    var v3 = Data.PLAYER.downcast(e);
    if (v3 != null) {
      var v4 = true;
      if (this.realRadius != null) {
        if (this.distance(v3.x, v3.y) > this.realRadius) {
          v4 = false;
        }
      }
      if (v4) {
        if (v3.specialMan.actives[86]) {
          this.game.fxMan.attachFx(this.x, this.y - Data.CASE_HEIGHT, "hammer_fx_shine");
          v3.getScore(this, 666);
          this.destroy();
        } else {
          if (this.isHealthy()) {
            if (v3.specialMan.actives[114] && v3.oldY <= this.y - Data.CASE_HEIGHT * 0.5 && v3.dy > 0) {
              v3.dy = -Data.PLAYER_AIR_JUMP * 2.5;
              this.freeze(Data.FREEZE_DURATION);
              this.setCombo(null);
              this.game.fxMan.attachExplodeZone(this.x, this.y - 5, 30);
              this.game.fxMan.inGameParticles(Data.PARTICLE_CLASSIC_BOMB, this.x, this.y - 5, 3 + HfStd.random(3));
            } else {
              if (!v3.fl_shield) {
                if (v3.x < this.x) {
                  v3.killHit(-1);
                } else {
                  v3.killHit(1);
                }
              }
            }
          }
        }
      }
    }
    var v5 = Data.BAD.downcast(e);
    if (v5 != null) {
      if (this.fl_freeze && v5.fl_freeze == false) {
        var v6 = this.evaluateSpeed();
        if (v6 >= Data.ICE_HIT_MIN_SPEED) {
          v5.setCombo(this.comboId);
          v5.killHit(this.dx);
        } else {
          if (v6 >= Data.ICE_KNOCK_MIN_SPEED) {
            v5.setCombo(this.comboId);
            v5.knock(Data.KNOCK_DURATION);
          }
        }
      }
    }
  }

  override public function init(g: GameMode): Void {
    super.init(g);
    this.register(Data.BAD);
    this.register(Data.BAD_CLEAR);
  }

  override public function killHit(dx: Null<Float>): Void {
    if (this.fl_freeze) {
      this.game.fxMan.inGameParticles(Data.PARTICLE_ICE, this.x, this.y, HfStd.random(3) + 2);
      this.game.fxMan.attachExplosion(this.x, this.y - Data.CASE_HEIGHT * 0.5, Data.CASE_WIDTH * 2);
      this.melt();
    }
    if (this.fl_knock) {
      this.wakeUp();
    }
    if (dx == null) {
      dx = (HfStd.random(200) / 10) * (HfStd.random(2) * 2 - 1);
    }
    this.playAnim(Data.ANIM_BAD_DIE);
    super.killHit(dx);
  }

  override public function onDeathLine(): Void {
    var v3 = this.game.depthMan.attach("hammer_fx_death", Data.FX.asInt());
    v3._x = this.x;
    v3._y = Data.GAME_HEIGHT * 0.5;
    this.game.shake(10, 3);
    this.game.soundMan.playSound("sound_bad_death", Data.CHAN_BAD);
    this.deathTimer = 0;
    super.onDeathLine();
    if (!this.fl_kill) {
      this.onKill();
    }
    this.dropReward();
    this.destroy();
  }

  override public function onHitGround(h: Float): Void {
    super.onHitGround(h);
    if (this.fl_freeze) {
      this.game.fxMan.inGameParticles(Data.PARTICLE_ICE_BAD, this.x, this.y, HfStd.random(3) + 2);
      if (h >= Data.DUST_FALL_HEIGHT) {
        this.game.fxMan.dust(this.cx, this.cy + 1);
      }
    }
  }

  override public function onKill(): Void {
    super.onKill();
    if (this.fl_ninFriend) {
      var v3 = this.game.getPlayerList();
      for (v4 in 0...v3.length) {
        var v5 = v3[v4];
        if (!v5.fl_kill) {
          v5.unshield();
          v5.killHit(HfStd.random(20));
          this.game.fxMan.detachLastAlert();
          this.game.fxMan.attachAlert(Lang.get(44));
        }
      }
    }
    if (this.fl_ninFoe) {
      var v6 = this.game.getBadClearList();
      var v7 = 1;
      for (v8 in 0...v6.length) {
        var v9 = v6[v8];
        v9.fl_ninFriend = false;
        if (v9.uniqId != this.uniqId) {
          hf.entity.item.ScoreItem.attach(this.game, v9.x, v9.y, 0, v7);
          v9.destroy();
          ++v7;
        }
        v9.unstick();
      }
      hf.entity.item.ScoreItem.attach(this.game, Data.GAME_WIDTH * 0.5, -20, 236, null);
      this.game.fxMan.detachLastAlert();
      this.game.fxMan.attachAlert(Lang.get(45));
    }
    this.deathTimer = Data.SECOND * 5;
    this.game.onKillBad(this);
  }

  override public function playAnim(o: Anim): Void {
    if (o.id == Data.ANIM_BAD_WALK.id && this.anger > 0) {
      super.playAnim(Data.ANIM_BAD_ANGER);
    } else {
      super.playAnim(o);
    }
  }

  override public function needsPatch(): Bool {
    return this.fl_freeze || this.fl_knock;
  }

  override public function update(): Void {
    if (this.player._name == null) {
      this.hate(this.game.getOne(Data.PLAYER));
    }
    this.fl_playerClose = this.isHealthy() && this.distance(this.player.x, this.player.y) <= this.closeDistance * (this.anger + 1);
    if (this.fl_showIA) {
      if (this.fl_playerClose) {
        if (!this.fl_stick) {
          var v3 = this.game.depthMan.attach("curse", Data.DP_FX);
          v3.gotoAndStop("" + Data.CURSE_TAUNT);
          this.stick(v3, 0, -Data.CASE_HEIGHT * 2.5);
        }
      } else {
        this.unstick();
      }
    }
    if (this.freezeTimer > 0) {
      this.freezeTimer -= Timer.tmod;
      if (this.freezeTimer <= 0) {
        this.melt();
      }
    }
    if (this.deathTimer > 0) {
      this.deathTimer -= Timer.tmod;
      if (this.deathTimer <= 0) {
        this.game.fxMan.attachExplosion(this.x, this.y, 80);
        this.y = 1000;
      }
    }
    if (this.knockTimer > 0) {
      this.knockTimer -= Timer.tmod;
      if (this.knockTimer <= 0) {
        this.wakeUp();
      }
    }
    super.update();
    if (this.yTrigger != null && !this.fl_kill) {
      if (this.y >= this.yTrigger) {
        this.fl_hitGround = true;
        this.yTrigger = null;
      }
    }
    if (this.comboId != null) {
      if (this.isHealthy()) {
        this.setCombo(null);
      }
    }
  }
}
