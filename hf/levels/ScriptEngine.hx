package hf.levels;

import etwin.flash.XML;
import etwin.flash.XMLNode;
import hf.levels.Data in LevelData;
import hf.mode.GameMode;
import hf.Case;
import hf.Data in Data;
using hf.compat.XMLNodeTools;

typedef ScriptExp = {
  var x: Float;
  var y: Float;
  var r: Float;
}

typedef ScriptMc = {
  var sid: Int;
  var mc: etwin.flash.DynamicMovieClip /* TODO */;
}

class ScriptEngine {
  public var game: GameMode;
  public var data: LevelData;
  public var baseScript: String;
  public var extraScript: String;

  /**
   * Compiled script.
   *
   * This field is not set initially. It is set by the `compile` method.
   * The following property is verified: `(script != null) == fl_compile`.
   *
   * A Flash `XML` object.
   */
  public var script: Null<XML>;
  public var cycle: Float;
  public var bads: Int;
  public var fl_compile: Bool;
  public var fl_birth: Bool;
  public var fl_death: Bool;
  public var fl_onAttach: Bool;
  public var fl_safe: Bool;
  public var fl_elevatorOpen: Bool;
  public var fl_firstTorch: Bool;
  /**
   * Flag indicating that the view should be redrawn after the script execution.
   */
  public var fl_redraw: Null<Bool>;
  public var bossDoorTimer: Float;
  public var recentExp: Array<ScriptExp>;
  public var entries: Array<Case>;
  public var mcList: Array<ScriptMc>;
  public var history: Array<String>;

  public function new(g: GameMode, d: LevelData): Void {
    this.game = g;
    this.data = d;
    this.baseScript = this.data.__dollar__script;
    this.bossDoorTimer = Data.SECOND * 1.2;
    this.extraScript = "";
    this.cycle = 0;
    this.bads = 0;
    this.fl_birth = false;
    this.fl_death = false;
    this.fl_safe = false;
    this.fl_elevatorOpen = false;
    this.fl_onAttach = false;
    this.fl_firstTorch = false;
    this.recentExp = new Array();
    this.mcList = new Array();
    this.history = new Array();
    this.entries = new Array();
  }

  public function destroy(): Void {
    this.script = null;
    this.fl_compile = false;
  }

  public function traceHistory(str: String): Void {
    this.history.push("@" + Math.round(this.cycle * 10) / 10 + "\t: " + str);
  }

  public function onPlayerBirth(): Void {
    this.fl_birth = true;
  }

  public function onPlayerDeath(): Void {
    this.fl_death = true;
  }

  public function onExplode(x: Float, y: Float, radius: Float): Void {
    this.recentExp.push({x: x, y: y, r: radius});
  }

  public function onEnterCase(cx: Int, cy: Int): Void {
    this.entries.push({cx: cx, cy: cy});
  }

  public function onLevelAttach(): Void {
    this.fl_onAttach = true;
  }

  public function safeMode(): Void {
    this.fl_safe = true;
  }

  public function normalMode(): Void {
    this.fl_safe = false;
  }

  public function isVerbose(t: String): Bool {
    var v3 = false;
    for (v4 in 0...hf.levels.ScriptEngine.VERBOSE_TRIGGERS.length) {
      if (t == hf.levels.ScriptEngine.VERBOSE_TRIGGERS[v4]) {
        v3 = true;
      }
    }
    return v3;
  }

  public function getInt(node: XMLNode, name: String): Null<Int> {
    if (node.get(name) == null) {
      return null;
    }
    return Math.floor(HfStd.parseInt(node.get(name), 10));
  }

  public function getFloat(node: XMLNode, name: String): Null<Float> {
    if (node.get(name) == null) {
      return null;
    }
    return HfStd.parseInt(node.get(name), 10);
  }

  public function getString(node: XMLNode, name: String): Null<String> {
    if (node.get(name) == null) {
      return null;
    }
    return node.get(name);
  }

  public function addScript(str: String): Void {
    if (this.fl_compile) {
      var v3 = new XML(str);
      if (v3 == null) {
        GameManager.fatal("invalid XML !");
      } else {
        this.script.appendChild(v3.firstChild);
      }
    } else {
      this.extraScript += " " + str;
    }
    var v4 = (new XML(str)).firstChild;
    this.traceHistory("+" + v4.nodeName);
    v4 = v4.firstChild;
    while (v4 != null) {
      this.traceHistory("  +" + v4.nodeName);
      v4 = v4.nextSibling;
    }
  }

  public function addNode(name: String, att: String, inner: String): Void {
    this.addScript("<" + name + " " + att + ">" + inner + "</" + name + ">");
  }

  public function addShortNode(name: String, att: String): Void {
    this.addScript("<" + name + " " + att + "/>");
  }

  public function executeEvent(event: XMLNode): Void {
    var v40;
    var v39;
    var v27;
    var v25;
    if (event.nodeName == null) {
      return;
    }
    this.traceHistory(" |--" + event.nodeName);
    switch (event.nodeName) {
      case hf.levels.ScriptEngine.E_SCORE:
        var v3 = Entity.x_ctr(this.getInt(event, "$x"));
        var v4 = Entity.y_ctr(this.getInt(event, "$y"));
        v3 = this.game.flipCoordReal(v3);
        var v5 = this.getInt(event, "$i");
        var v6 = this.getInt(event, "$si");
        var v7 = hf.entity.item.ScoreItem.attach(this.game, v3, v4, v5, v6);
        var v8 = this.getInt(event, "$inf");
        if (v8 == 1) {
          v7.setLifeTimer(-1);
        }
        var v9 = this.getInt(event, "$sid");
        this.killById(v9);
        v7.scriptId = v9;
      case hf.levels.ScriptEngine.E_SPECIAL:
        if (this.game.canAddItem() && !this.fl_safe) {
          var v10 = Entity.x_ctr(this.getInt(event, "$x"));
          v10 = this.game.flipCoordReal(v10);
          var v11 = Entity.y_ctr(this.getInt(event, "$y"));
          var v12 = this.getInt(event, "$i");
          var v13 = this.getInt(event, "$si");
          var v14 = hf.entity.item.SpecialItem.attach(this.game, v10, v11, v12, v13);
          var v15 = this.getInt(event, "$inf");
          if (v15 == 1) {
            v14.setLifeTimer(-1);
          }
          var v16 = this.getInt(event, "$sid");
          this.killById(v16);
          v14.scriptId = v16;
        }
      case hf.levels.ScriptEngine.E_EXTEND:
        if (this.game.canAddItem() && !this.fl_safe) {
          this.game.statsMan.attachExtend();
        }
      case hf.levels.ScriptEngine.E_BAD:
        if (!this.fl_safe) {
          var v17 = Entity.x_ctr(this.game.flipCoordCase(this.getInt(event, "$x"))) - Data.CASE_WIDTH * 0.5;
          var v18 = Entity.y_ctr(this.getInt(event, "$y") - 1);
          var v19 = this.getInt(event, "$i");
          var v20 = this.getInt(event, "$sys") != 0 && this.getInt(event, "$sys") != null;
          var v21 = this.game.attachBad(v19, v17, v18);
          if (Data.BAD_CLEAR.check(v21)) {
            if (v20 && this.game.world.isVisited()) {
              v21.destroy();
              --this.game.badCount;
            } else {
              ++this.bads;
              this.game.fl_clear = false;
              var v22 = this.getInt(event, "$sid");
              this.killById(v22);
              v21.scriptId = v22;
            }
          }
        }
      case hf.levels.ScriptEngine.E_KILL:
        var v23 = this.getInt(event, "$sid");
        this.killById(v23);
      case hf.levels.ScriptEngine.E_TUTORIAL:
        var v24 = this.getInt(event, "$id");
        if (v24 == null) {
          v25 = event.firstChild.nodeValue;
          GameManager.warning("@ level " + this.game.world.currentId + ", script still using inline text value");
        } else {
          v25 = Lang.get(v24);
        }
        if (v25 != null) {
          this.game.attachPop("\n" + v25, true);
        }
      case hf.levels.ScriptEngine.E_MESSAGE:
        var v26 = this.getInt(event, "$id");
        if (v26 == null) {
          v27 = event.firstChild.nodeValue;
          GameManager.warning("@ level " + this.game.world.currentId + ", script still using inline text value");
        } else {
          v27 = Lang.get(v26);
        }
        if (v27 != null) {
          this.game.attachPop("\n" + v27, false);
        }
      case hf.levels.ScriptEngine.E_KILLMSG:
        this.game.killPop();
      case hf.levels.ScriptEngine.E_POINTER:
        var v28 = this.game.getOne(Data.PLAYER);
        var v29 = this.getInt(event, "$x");
        var v30 = this.getInt(event, "$y");
        v29 = this.game.flipCoordCase(v29);
        this.game.attachPointer(v29, v30, v28.cx, v28.cy);
      case hf.levels.ScriptEngine.E_KILLPTR:
        this.game.killPointer();
      case hf.levels.ScriptEngine.E_MC:
        var v31 = this.getInt(event, "$x");
        var v32 = this.getInt(event, "$y");
        var v33 = this.getInt(event, "$xr");
        var v34 = this.getInt(event, "$yr");
        var v35 = this.getInt(event, "$sid");
        var v36 = this.getInt(event, "$back");
        var v37 = this.getString(event, "$n");
        var v38 = this.getInt(event, "$p");
        this.killById(v35);
        if (v33 == null) {
          v39 = Entity.x_ctr(v31);
          v40 = Entity.y_ctr(v32);
        } else {
          v39 = v33;
          v40 = v34;
        }
        v39 = this.game.flipCoordReal(v39);
        if (this.game.fl_mirror) {
          v39 += Data.CASE_WIDTH;
        }
        var v41: etwin.flash.DynamicMovieClip = cast this.game.world.view.attachSprite(v37, v39, v40, (v36 != 1) ? false : true);
        if (this.game.fl_mirror) {
          v41._xscale *= -1;
        }
        if (v38 > 0) {
          v41.play();
        } else {
          v41.stop();
        }
        v41.sub.stop();
        if (v37 == "$torch") {
          if (!this.fl_firstTorch) {
            this.game.clearExtraHoles();
          }
          this.game.addHole(v39 + Data.CASE_WIDTH * 0.5, v40 - Data.CASE_HEIGHT * 0.5, 180);
          this.game.updateDarkness();
          this.fl_firstTorch = true;
        }
        this.mcList.push({sid: v35, mc: v41});
      case hf.levels.ScriptEngine.E_PLAYMC:
        var v42 = this.getInt(event, "$sid");
        this.playById(v42);
      case hf.levels.ScriptEngine.E_MUSIC:
        var v43 = this.getInt(event, "$id");
        this.game.playMusic(v43);
      case hf.levels.ScriptEngine.E_ADDTILE:
        var v44 = this.getInt(event, "$x1");
        var v45 = this.getInt(event, "$y1");
        var v46 = this.getInt(event, "$x2");
        var v47 = this.getInt(event, "$y2");
        v44 = this.game.flipCoordCase(v44);
        v46 = this.game.flipCoordCase(v46);
        var v48 = this.getInt(event, "$type");
        if (v48 > 0) {
          v48 = -v48;
        } else {
          v48 = Data.GROUND;
        }
        while (true) {
          if (!(v44 != v46 || v45 != v47)) break;
          this.game.world.forceCase(v44, v45, v48);
          if (v44 < v46) {
            ++v44;
          }
          if (v44 > v46) {
            --v44;
          }
          if (v45 < v47) {
            ++v45;
          }
          if (v45 > v47) {
            --v45;
          }
        }
        this.game.world.forceCase(v44, v45, v48);
        this.fl_redraw = true;
      case hf.levels.ScriptEngine.E_REMTILE:
        var v49 = this.getInt(event, "$x1");
        var v50 = this.getInt(event, "$y1");
        var v51 = this.getInt(event, "$x2");
        var v52 = this.getInt(event, "$y2");
        v49 = this.game.flipCoordCase(v49);
        v51 = this.game.flipCoordCase(v51);
        while (true) {
          if (!(v49 != v51 || v50 != v52)) break;
          this.game.world.forceCase(v49, v50, 0);
          if (v49 < v51) {
            ++v49;
          }
          if (v49 > v51) {
            --v49;
          }
          if (v50 < v52) {
            ++v50;
          }
          if (v50 > v52) {
            --v50;
          }
        }
        this.game.world.forceCase(v49, v50, 0);
        this.fl_redraw = true;
      case hf.levels.ScriptEngine.E_ITEMLINE:
        var v53 = this.getInt(event, "$x1");
        var v54 = this.getInt(event, "$x2");
        var v55 = this.getInt(event, "$y");
        var v56 = this.getInt(event, "$i");
        var v57 = this.getInt(event, "$si");
        var v58 = this.getInt(event, "$t");
        var v59 = 0;
        var v60 = false;
        while (!v60) {
          this.addScript("<" + hf.levels.ScriptEngine.T_TIMER + " $t=\"" + (this.cycle + v59 * v58) + "\">" + "<" + hf.levels.ScriptEngine.E_SCORE + " $i=\"" + v56 + "\" $si=\"" + v57 + "\" $x=\"" + v53 + "\" $y=\"" + v55 + "\" $inf=\"1\" />" + "</$" + hf.levels.ScriptEngine.T_TIMER + ">");
          if (v53 == v54) {
            v60 = true;
          }
          if (v53 < v54) {
            ++v53;
          }
          if (v53 > v54) {
            --v53;
          }
          ++v59;
        }
      case hf.levels.ScriptEngine.E_GOTO:
        var v61 = this.getInt(event, "$id");
        this.game.forcedGoto(v61);
      case hf.levels.ScriptEngine.E_HIDE:
        var v62 = (this.getInt(event, "$tiles") != 1) ? false : true;
        var v63 = (this.getInt(event, "$borders") != 1) ? false : true;
        this.game.world.view.fl_hideTiles = v62;
        this.game.world.view.fl_hideBorders = v63;
        this.game.world.view.detach();
        this.game.world.view.attach();
        this.game.world.view.moveToPreviousPos();
      case hf.levels.ScriptEngine.E_HIDEBORDERS:
        this.game.world.view.fl_hideTiles = true;
        this.game.world.view.detach();
        this.game.world.view.attach();
        this.game.world.view.moveToPreviousPos();
      case hf.levels.ScriptEngine.E_CODETRIGGER:
        var v64 = this.getInt(event, "$id");
        this.codeTrigger(v64);
      case hf.levels.ScriptEngine.E_PORTAL:
        if (this.game.fl_clear && this.cycle > 10) {
          var v65 = this.getInt(event, "$pid");
          if (!this.game.usePortal(v65, null)) {
          }
        }
      case hf.levels.ScriptEngine.E_SETVAR:
        var v66 = this.getString(event, "$var");
        var v67 = this.getString(event, "$value");
        this.game.setDynamicVar(v66, v67);
      case hf.levels.ScriptEngine.E_OPENPORTAL:
        var v68 = this.getInt(event, "$x");
        var v69 = this.getInt(event, "$y");
        var v70 = this.getInt(event, "$pid");
        this.game.openPortal(v68, v69, v70);
      case hf.levels.ScriptEngine.E_DARKNESS:
        var v71 = this.getInt(event, "$v");
        this.game.forcedDarkness = v71;
        this.game.updateDarkness();
      case hf.levels.ScriptEngine.E_FAKELID:
        var v72 = this.getInt(event, "$lid");
        if (HfStd.isNaN(v72)) {
          this.game.fakeLevelId = null;
          this.game.gi.hideLevel();
        } else {
          this.game.fakeLevelId = v72;
          this.game.gi.setLevel(v72);
        }
      default:
        if (this.isTrigger(event.nodeName)) {
          this.script.appendChild(event);
        } else {
          GameManager.warning("unknown event: " + event.nodeName + " (not a trigger)");
        }
    }
  }

  public function isTrigger(n: String): Bool {
    return n == hf.levels.ScriptEngine.T_TIMER || n == hf.levels.ScriptEngine.T_POS || n == hf.levels.ScriptEngine.T_ATTACH || n == hf.levels.ScriptEngine.T_DO || n == hf.levels.ScriptEngine.T_END || n == hf.levels.ScriptEngine.T_BIRTH || n == hf.levels.ScriptEngine.T_DEATH || n == hf.levels.ScriptEngine.T_EXPLODE || n == hf.levels.ScriptEngine.T_ENTER || n == hf.levels.ScriptEngine.T_NIGHTMARE || n == hf.levels.ScriptEngine.T_MIRROR || n == hf.levels.ScriptEngine.T_MULTI || n == hf.levels.ScriptEngine.T_NINJA;
  }

  public function checkTrigger(trigger: XMLNode): Bool {
    if (trigger.nodeName == null || trigger.nodeName == "") {
      return false;
    }
    switch (trigger.nodeName) {
      case hf.levels.ScriptEngine.T_TIMER:
        if (this.cycle >= this.getInt(trigger, "$t")) {
          return true;
        }
        return false;
      case hf.levels.ScriptEngine.T_POS:
        var v3 = this.game.getPlayerList();
        var v4 = this.getInt(trigger, "$x");
        var v5 = this.getInt(trigger, "$y");
        v4 = this.game.flipCoordCase(v4);
        var v6 = this.getInt(trigger, "$d");
        for (v7 in 0...v3.length) {
          if (!v3[v7].fl_kill && !v3[v7].fl_destroy) {
            var v8 = v3[v7].distanceCase(v4, v5);
            if (v8 <= v6 && !HfStd.isNaN(v8)) {
              return true;
            }
          }
        }
        return false;
      case hf.levels.ScriptEngine.T_ATTACH:
        if (this.fl_onAttach) {
          return true;
        }
        return false;
      case hf.levels.ScriptEngine.T_DO:
        return true;
      case hf.levels.ScriptEngine.T_END:
        if (this.game.fl_clear && this.cycle > 10) {
          return true;
        }
        return false;
      case hf.levels.ScriptEngine.T_BIRTH:
        if (this.fl_birth) {
          return true;
        }
        return false;
      case hf.levels.ScriptEngine.T_DEATH:
        if (this.fl_death) {
          return true;
        }
        return false;
      case hf.levels.ScriptEngine.T_EXPLODE:
        var v9 = Entity.x_ctr(this.getInt(trigger, "$x"));
        var v10 = Entity.y_ctr(this.getInt(trigger, "$y"));
        v9 = this.game.flipCoordReal(v9);
        for (v11 in 0...this.recentExp.length) {
          var v12 = this.recentExp[v11];
          var v13 = Math.pow(v9 - v12.x, 2) + Math.pow(v10 - v12.y, 2);
          if (v13 <= Math.pow(v12.r, 2)) {
            if (Math.sqrt(v13) <= v12.r) {
              return true;
            }
          }
        }
        return false;
      case hf.levels.ScriptEngine.T_ENTER:
        var v14 = this.getInt(trigger, "$x");
        var v15 = this.getInt(trigger, "$y");
        v14 = this.game.flipCoordCase(v14);
        for (v16 in 0...this.entries.length) {
          if (this.entries[v16].cx == v14 && this.entries[v16].cy == v15) {
            return true;
          }
        }
        return false;
      case hf.levels.ScriptEngine.T_NIGHTMARE:
        return this.game.fl_nightmare;
      case hf.levels.ScriptEngine.T_MIRROR:
        return this.game.fl_mirror;
      case hf.levels.ScriptEngine.T_MULTI:
        return (this.game.getPlayerList()).length > 1;
      case hf.levels.ScriptEngine.T_NINJA:
        return this.game.fl_ninja;
    }
    GameManager.warning("unknown trigger " + trigger.nodeName);
    return false;
  }

  public function executeTrigger(trigger: XMLNode): Void {
    this.traceHistory(trigger.nodeName);
    var v3 = trigger.firstChild;
    while (v3 != null) {
      this.executeEvent(v3);
      v3 = v3.nextSibling;
    }
    var v4 = this.getInt(trigger, "$repeat");
    if (!HfStd.isNaN(v4)) {
      --v4;
      this.traceHistory("R " + trigger.nodeName + ": " + v4);
      trigger.set("$repeat", "" + v4);
      if (v4 == 0) {
        trigger.removeNode();
      } else {
        if (trigger.nodeName == hf.levels.ScriptEngine.T_TIMER) {
          var v5 = trigger.get("$base");
          if (v5 == null) {
            v5 = trigger.get("$t");
            trigger.set("$base", v5);
          }
          var v6: Float = HfStd.parseInt(v5, 10);
          v6 += this.cycle;
          trigger.set("$t", "" + v6);
        }
      }
    } else {
      this.traceHistory("X " + trigger.nodeName);
      trigger.removeNode();
    }
  }

  public function insertBads(): Int {
    var v2 = "<" + hf.levels.ScriptEngine.T_DO + ">";
    for (v3 in 0...this.data.__dollar__badList.length) {
      var v4 = this.data.__dollar__badList[v3];
      v2 += "<" + hf.levels.ScriptEngine.E_BAD + " $i=\"" + v4.__dollar__id + "\" $x=\"" + v4.__dollar__x + "\" $y=\"" + v4.__dollar__y + "\" $sys=\"1\"/>";
    }
    v2 += "</" + hf.levels.ScriptEngine.T_DO + ">";
    this.addScript(v2);
    return this.data.__dollar__badList.length;
  }

  public function insertItem(event: String, id: Int, subId: Int, x: Float, y: Float, t: Float, repeat: Int, fl_inf: Bool, fl_clearAtEnd: Bool): Void {
    var v11;
    if (subId == null) {
      v11 = "";
    } else {
      v11 = '' + (subId);
    }
    var v12 = "";
    if (repeat != null) {
      v12 = " $repeat=\"" + repeat + "\"";
    }
    this.addScript("<" + hf.levels.ScriptEngine.T_TIMER + " $t=\"" + (this.cycle + t) + "\" " + v12 + " $endClear=\"" + (!fl_clearAtEnd ? "0" : "1") + "\">" + "<" + event + " $x=\"" + x + "\" $y=\"" + y + "\" $i=\"" + id + "\" $si=\"" + v11 + "\" $inf=\"" + (!fl_inf ? "" : "1") + "\" $sys=\"1\"/>" + "</" + hf.levels.ScriptEngine.T_TIMER + ">");
  }

  public function insertSpecialItem(id: Int, sid: Int, x: Float, y: Float, t: Float, repeat: Int, fl_inf: Bool, fl_clearAtEnd: Bool): Void {
    this.insertItem(hf.levels.ScriptEngine.E_SPECIAL, id, sid, x, y, t, repeat, fl_inf, fl_clearAtEnd);
  }

  public function insertScoreItem(id: Int, sid: Int, x: Float, y: Float, t: Float, repeat: Int, fl_inf: Bool, fl_clearAtEnd: Bool): Void {
    this.insertItem(hf.levels.ScriptEngine.E_SCORE, id, sid, x, y, t, repeat, fl_inf, fl_clearAtEnd);
  }

  public function insertExtend(): Void {
    var v2 = "<" + hf.levels.ScriptEngine.T_TIMER + " $t=\"" + Data.EXTEND_TIMER + "\" $repeat=\"-1\" $endClear=\"1\"><" + hf.levels.ScriptEngine.E_EXTEND + "/></" + hf.levels.ScriptEngine.T_TIMER + ">";
    this.addScript(v2);
  }

  public function insertPortal(cx: Float, cy: Float, pid: Int): Void {
    this.addScript("<" + hf.levels.ScriptEngine.T_POS + " $x=\"" + cx + "\" $y=\"" + cy + "\" $d=\"1\" $repeat=\"-1\">" + "<" + hf.levels.ScriptEngine.E_PORTAL + " $pid=\"" + pid + "\"/>" + "</" + hf.levels.ScriptEngine.T_POS + ">");
  }

  public function runScript(): Void {
    if (this.script == null) {
      return;
    }
    var v2 = this.script.firstChild;
    while (v2 != null) {
      if (this.checkTrigger(v2)) {
        var v3 = this.getInt(v2, "$key");
        if (!this.game.hasKey(v3)) {
          if (this.isVerbose(v2.nodeName)) {
            this.game.fxMan.keyRequired(v3);
          }
        } else {
          if (v3 != null) {
            this.game.fxMan.keyUsed(v3);
          }
          this.executeTrigger(v2);
        }
      }
      v2 = v2.nextSibling;
    }
    this.fl_birth = false;
    this.fl_death = false;
    this.fl_onAttach = false;
    this.recentExp = new Array();
    this.entries = new Array();
    this.fl_onAttach = false;
  }

  public function compile(): Void {
    this.history = new Array();
    this.traceHistory(this.baseScript);
    var v2 = (new XML(this.baseScript)).firstChild;
    while (v2 != null) {
      if (v2.nodeName != null) {
        this.traceHistory("b " + v2.nodeName);
      }
      v2 = v2.nextSibling;
    }
    v2 = (new XML(this.extraScript)).firstChild;
    while (v2 != null) {
      if (v2.nodeName != null) {
        this.traceHistory("b2 " + v2.nodeName);
      }
      v2 = v2.nextSibling;
    }
    var v3 = new XML(this.baseScript + " " + this.extraScript);
    v3.ignoreWhite = true;
    if (v3 == null) {
      GameManager.fatal("compile: invalid XML");
    } else {
      this.script = v3;
    }
    this.normalMode();
    this.fl_compile = true;
    this.traceHistory("first=" + this.cycle);
    this.runScript();
  }

  public function clearScript(): Void {
    this.script = null;
    this.baseScript = "";
    this.extraScript = "";
    this.cycle = 0;
    this.fl_compile = false;
  }

  public function clearEndTriggers(): Void {
    var v2 = this.script.firstChild;
    while (v2 != null) {
      var v3 = v2.nextSibling;
      if (v2.get("$endClear") == "1") {
        this.traceHistory("eX " + v2.nodeName);
        v2.removeNode();
      }
      v2 = v3;
    }
  }

  public function reset(): Void {
    this.cycle = 0;
    this.traceHistory("(r)");
  }

  public function killById(id: Int): Void {
    if (id == null) {
      return;
    }
    var v3 = this.game.getList(Data.ENTITY);
    for (v4 in 0...v3.length) {
      if (v3[v4].scriptId == id) {
        v3[v4].destroy();
      }
    }
    var v5 = 0;
    while (v5 < this.mcList.length) {
      if (this.mcList[v5].sid == id) {
        this.mcList[v5].mc.removeMovieClip();
        this.mcList.splice(v5, 1);
        --v5;
      }
      ++v5;
    }
  }

  public function playById(id: Int): Void {
    if (id == null) {
      return;
    }
    for (v3 in 0...this.mcList.length) {
      if (this.mcList[v3].sid == id) {
        this.mcList[v3].mc.play();
        this.mcList[v3].mc.sub.play();
      }
    }
  }

  public function codeTrigger(id: Int): Void {
    switch (id) {
      case 0:
        this.game.fl_warpStart = true;
      case 1:
        this.game.huTimer -= Timer.tmod * 0.5;
      case 2:
        this.game.fxMan.detachExit();
      case 3:
        this.playById(101);
        this.fl_elevatorOpen = true;
        var v3 = this.game.getPlayerList();
        for (v4 in 0...v3.length) {
          v3[v4].lockControls(Data.SECOND * 12.5);
          v3[v4].dx = 0;
        }
        this.game.huTimer = 0;
      case 4:
        if (this.fl_elevatorOpen) {
          var v5 = this.game.getPlayerList();
          for (v6 in 0...v5.length) {
            v5[v6].hide();
            v5[v6].lockControls(99999);
            this.game.huTimer = 0;
          }
          for (v7 in 0...this.mcList.length) {
            if (this.mcList[v7].sid == 101) {
              this.mcList[v7].mc.head = (this.game.getPlayerList())[0].head;
            }
          }
          this.playById(101);
          this.game.endModeTimer = Data.SECOND * 14;
          this.fl_elevatorOpen = false;
        }
      case 5:
        if (((cast this.game.getOne(Data.BOSS)): hf.entity.boss.Tuberculoz).fl_defeated) {
          this.bossDoorTimer -= Timer.tmod;
          if (this.bossDoorTimer <= 0) {
            this.game.destroyList(Data.BOSS);
            this.game.world.view.destroy();
            this.game.forcedGoto(102);
          }
        }
      case 6:
        var v8 = this.game.world.current.__dollar__specialSlots[HfStd.random(this.game.world.current.__dollar__specialSlots.length)];
        var v9 = hf.entity.bomb.player.SoccerBall.attach(this.game, Entity.x_ctr(v8.__dollar__x), Entity.y_ctr(v8.__dollar__y));
        v9.dx = (10 + HfStd.random(10)) * (HfStd.random(2) * 2 - 1);
        v9.dy = -HfStd.random(5) - 5;
      case 7:
        var v10 = this.game.getPlayerList();
        for (v11 in 0...v10.length) {
          v10[v11].setBaseAnims(Data.ANIM_PLAYER_WALK, Data.ANIM_PLAYER_STOP_L);
        }
      case 8:
        var v12 = this.game.getPlayerList();
        for (v13 in 0...v12.length) {
          v12[v13].setBaseAnims(Data.ANIM_PLAYER_WALK_V, Data.ANIM_PLAYER_STOP_V);
        }
      case 9:
        this.game.soundMan.playSound("sound_boss_laugh", Data.CHAN_BAD);
      case 10:
        var v14 = this.game.getBadList();
        for (v15 in 0...v14.length) {
          ((cast v14[v15]): hf.entity.bad.Jumper).setJumpDown(null);
        }
      case 11:
        var v16 = this.game.getBadClearList();
        for (v17 in 0...v16.length) {
          var v18 = v16[v17];
          this.game.fxMan.attachFx(v18.x, v18.y - Data.CASE_HEIGHT, "hammer_fx_pop");
          v18.destroy();
        }
      case 12:
        while (this.game.huState < 2) {
          var v19 = this.game.onHurryUp();
          if (this.game.huState < 2) {
            v19.removeMovieClip();
          }
        }
      case 13:
        var v20 = this.game.getList(Data.ITEM);
        for (v21 in 0...v20.length) {
          var v22 = v20[v21];
          this.game.fxMan.attachFx(v22.x, v22.y - Data.CASE_HEIGHT, "hammer_fx_pop");
          v22.destroy();
        }
      case 14:
        this.game.clearExtraHoles();
      case 15:
        this.game.resetHurry();
      default:
        GameManager.fatal("code trigger #" + id + " not found!");
    }
  }

  public function update(): Void {
    if (this.fl_compile) {
      this.cycle += Timer.tmod;
      this.runScript();
    }
    if (this.fl_redraw) {
      this.fl_redraw = false;
      this.game.world.view.detachLevel();
      this.game.world.view.displayCurrent();
      this.game.world.view.moveToPreviousPos();
    }
    this.fl_firstTorch = false;
  }

  public static var T_TIMER: String = "$t_timer";
  public static var T_POS: String = "$t_pos";
  public static var T_ATTACH: String = "$attach";
  public static var T_DO: String = "$do";
  public static var T_END: String = "$end";
  public static var T_BIRTH: String = "$birth";
  public static var T_DEATH: String = "$death";
  public static var T_EXPLODE: String = "$exp";
  public static var T_ENTER: String = "$enter";
  public static var T_NIGHTMARE: String = "$night";
  public static var T_MIRROR: String = "$mirror";
  public static var T_MULTI: String = "$multi";
  public static var T_NINJA: String = "$ninja";
  public static var E_SCORE: String = "$e_score";
  public static var E_SPECIAL: String = "$e_spec";
  public static var E_EXTEND: String = "$e_ext";
  public static var E_BAD: String = "$e_bad";
  public static var E_KILL: String = "$e_kill";
  public static var E_TUTORIAL: String = "$e_tuto";
  public static var E_MESSAGE: String = "$e_msg";
  public static var E_KILLMSG: String = "$e_killMsg";
  public static var E_POINTER: String = "$e_pointer";
  public static var E_KILLPTR: String = "$e_killPt";
  public static var E_MC: String = "$e_mc";
  public static var E_PLAYMC: String = "$e_pmc";
  public static var E_MUSIC: String = "$e_music";
  public static var E_ADDTILE: String = "$e_add";
  public static var E_REMTILE: String = "$e_rem";
  public static var E_ITEMLINE: String = "$e_itemLine";
  public static var E_GOTO: String = "$e_goto";
  public static var E_HIDE: String = "$e_hide";
  public static var E_HIDEBORDERS: String = "$e_hideBorders";
  public static var E_CODETRIGGER: String = "$e_ctrigger";
  public static var E_PORTAL: String = "$e_portal";
  public static var E_SETVAR: String = "$e_setVar";
  public static var E_OPENPORTAL: String = "$e_openPortal";
  public static var E_DARKNESS: String = "$e_darkness";
  public static var E_FAKELID: String = "$e_fakelid";
  public static var VERBOSE_TRIGGERS: Array<String> = [hf.levels.ScriptEngine.T_POS, hf.levels.ScriptEngine.T_EXPLODE, hf.levels.ScriptEngine.T_ENTER];
}
