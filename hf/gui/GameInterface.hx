package hf.gui;

import etwin.flash.MovieClip;
import etwin.flash.DynamicMovieClip;
import hf.mode.GameMode;

class GameInterface {
  public static var GLOW_COLOR: Int = 7366029;
  public static var BASE_X: Float = 92;
  public static var BASE_X_RIGHT: Float = 300;
  public static var BASE_WIDTH: Float = 20;
  public static var MAX_LIVES: Int = 8;

  public var mc: DynamicMovieClip /* TODO */;
  public var game: GameMode;
  public var more: Array<MovieClip>;
  public var fl_light: Bool;
  public var fl_print: Bool;
  public var level: DynamicMovieClip /* TODO */;
  public var scores: Array<DynamicMovieClip /* TODO */>;
  public var baseColor: Int;
  public var fl_multi: Null<Bool>;
  public var letters: Array<Array<MovieClip>>;
  public var fakeScores: Array<Int>;
  public var realScores: Array<Int>;
  public var currentLives: Array<Int>;
  public var lives: Array<Array<MovieClip>>;

  public function new(game: GameMode): Void {
    this.game = game;
    this.more = new Array();
    if (game._name == "$time" || game._name == "$timeMulti") {
      this.initTime();
    } else {
      if (game.countList(Data.PLAYER) == 1) {
        this.initSingle();
      } else {
        this.initMulti();
      }
    }
    FxManager.addGlow(this.level, hf.gui.GameInterface.GLOW_COLOR, 2);
    this.setLevel(game.world.currentId);
    this.fl_light = false;
    this.fl_print = false;
    this.baseColor = Data.BASE_COLORS[0];
    this.update();
  }

  public function initSingle(): Void {
    this.fl_multi = false;
    this.mc = ((cast this.game.depthMan.attach("hammer_interf_game", Data.DP_TOP)): DynamicMovieClip);
    this.mc._x = -this.game.xOffset;
    this.mc._y = Data.DOC_HEIGHT;
    this.mc.gotoAndStop("1");
    this.mc.cacheAsBitmap = true;
    this.scores = [this.mc.score0];
    this.level = this.mc.level;
    this.letters = new Array();
    this.letters[0] = new Array();
    this.letters[0].push(this.mc.letter0_0);
    this.letters[0].push(this.mc.letter0_1);
    this.letters[0].push(this.mc.letter0_2);
    this.letters[0].push(this.mc.letter0_3);
    this.letters[0].push(this.mc.letter0_4);
    this.letters[0].push(this.mc.letter0_5);
    this.letters[0].push(this.mc.letter0_6);
    this.fakeScores = [0];
    this.realScores = [0];
    this.currentLives = [0];
    this.lives = [[]];
    var v2 = (this.game.getPlayerList())[0];
    this.setScore(0, v2.score);
    this.setLives(0, v2.lives);
    this.clearExtends(0);
    this.scores[0].textColor = Data.BASE_COLORS[0];
    FxManager.addGlow(this.scores[0], hf.gui.GameInterface.GLOW_COLOR, 2);
  }

  public function initMulti(): Void {
    this.fl_multi = true;
    this.mc = ((cast this.game.depthMan.attach("hammer_interf_game", Data.DP_TOP)): DynamicMovieClip);
    this.mc._x = -this.game.xOffset;
    this.mc._y = Data.DOC_HEIGHT;
    this.mc.gotoAndStop("2");
    this.mc.cacheAsBitmap = true;
    this.scores = [this.mc.score0, this.mc.score1];
    this.level = this.mc.level;
    this.letters = new Array();
    this.letters[0] = new Array();
    this.letters[0].push(this.mc.letter0_0);
    this.letters[0].push(this.mc.letter0_1);
    this.letters[0].push(this.mc.letter0_2);
    this.letters[0].push(this.mc.letter0_3);
    this.letters[0].push(this.mc.letter0_4);
    this.letters[0].push(this.mc.letter0_5);
    this.letters[0].push(this.mc.letter0_6);
    this.letters[1] = new Array();
    this.letters[1].push(this.mc.letter1_0);
    this.letters[1].push(this.mc.letter1_1);
    this.letters[1].push(this.mc.letter1_2);
    this.letters[1].push(this.mc.letter1_3);
    this.letters[1].push(this.mc.letter1_4);
    this.letters[1].push(this.mc.letter1_5);
    this.letters[1].push(this.mc.letter1_6);
    this.fakeScores = new Array();
    this.realScores = new Array();
    this.currentLives = new Array();
    this.lives = new Array();
    var v2 = this.game.getPlayerList();
    for (v3 in 0...v2.length) {
      var v4 = v2[v3];
      var v5 = v4.pid;
      this.fakeScores[v5] = 0;
      this.realScores[v5] = 0;
      this.currentLives[v5] = 0;
      this.lives[v5] = new Array();
      this.setScore(v5, v4.score);
      this.setLives(v5, v4.lives);
      this.clearExtends(v5);
      this.scores[v5].textColor = Data.BASE_COLORS[0];
      FxManager.addGlow(this.scores[v5], hf.gui.GameInterface.GLOW_COLOR, 2);
    }
  }

  public function initTime(): Void {
    hf.gui.GameInterface.BASE_X = 8;
    hf.gui.GameInterface.BASE_X_RIGHT = 386;
    hf.gui.GameInterface.BASE_WIDTH *= 0.75;
    this.mc = ((cast this.game.depthMan.attach("hammer_interf_game", Data.DP_TOP)): DynamicMovieClip);
    this.mc._x = -this.game.xOffset;
    this.mc._y = Data.DOC_HEIGHT;
    this.mc.gotoAndStop("3");
    this.mc.cacheAsBitmap = true;
    this.scores = [this.mc.time];
    this.level = this.mc.level;
    this.letters = new Array();
    this.fakeScores = [0, 0];
    this.realScores = [0, 0];
    this.currentLives = [0, 0];
    this.lives = [[], []];
    var v2 = this.game.getPlayerList();
    for (v3 in 0...v2.length) {
      var v4 = v2[v3];
      var v5 = v4.pid;
      this.fakeScores[v5] = 0;
      this.realScores[v5] = 0;
      this.currentLives[v5] = 0;
      this.lives[v5] = new Array();
      this.setScore(v5, v4.score);
      this.setLives(v5, v4.lives);
    }
    this.clearExtends(0);
    this.scores[0].textColor = Data.BASE_COLORS[0];
    FxManager.addGlow(this.scores[0], hf.gui.GameInterface.GLOW_COLOR, 2);
  }

  public function lightMode(): Void {
    this.scores[0]._visible = false;
    this.setLives(0, 0);
    this.more[0].removeMovieClip();
    this.more[1].removeMovieClip();
    this.fl_light = true;
  }

  public function setScore(pid: Int, v: Int): Void {
    this.realScores[pid] = v;
  }

  public function getScoreTxt(v: Int): String {
    var v3 = (v + "").split("");
    var v4 = v3.length - 3;
    while (v4 >= 0) {
      v3.insert(v4, " ");
      v4 -= 3;
    }
    return v3.join("");
  }

  public function setLevel(id: Int): Void {
    this.level.text = "" + id;
    this.level.textColor = this.baseColor;
  }

  public function hideLevel(): Void {
    this.level.text = "?";
    this.level.textColor = this.baseColor;
  }

  public function setLives(pid: Int, v: Int): Void {
    var v4 = hf.gui.GameInterface.BASE_X;
    var v5 = hf.gui.GameInterface.BASE_WIDTH;
    if (this.fl_multi) {
      v5 = 0.6 * hf.gui.GameInterface.BASE_WIDTH;
    }
    if (pid == 1) {
      v5 *= -1;
      v4 = hf.gui.GameInterface.BASE_X_RIGHT;
    }
    var v6 = this.lives[pid];
    if (this.fl_light) {
      return;
    }
    if (this.currentLives[pid] > v) {
      this.game.manager.logAction("$LL");
      while (this.currentLives[pid] > v) {
        v6[this.currentLives[pid] - 1].removeMovieClip();
        --this.currentLives[pid];
      }
    } else {
      while (true) {
        if (!(this.currentLives[pid] < v && this.currentLives[pid] < hf.gui.GameInterface.MAX_LIVES)) break;
        var v7 = HfStd.attachMC(this.mc, "hammer_interf_life", this.game.manager.uniq++);
        v7._x = v4 + this.currentLives[pid] * v5;
        v7._y = -19;
        v6[this.currentLives[pid]] = v7;
        ++this.currentLives[pid];
      }
      if (v > hf.gui.GameInterface.MAX_LIVES && this.more[pid]._name == null) {
        this.more[pid] = HfStd.attachMC(this.mc, "hammer_interf_more", this.game.manager.uniq++);
        this.more[pid]._x = v4 + v5 * hf.gui.GameInterface.MAX_LIVES - 4;
        if (pid > 0) {
          this.more[pid]._x -= v5;
        }
        this.more[pid]._y = -25;
      }
      if (v <= hf.gui.GameInterface.MAX_LIVES && this.more[pid]._name != null) {
        this.more[pid].removeMovieClip();
      }
    }
  }

  public function print(pid: Int, s: String): Void {
    this.scores[pid].text = s;
    this.fl_print = true;
  }

  public function cls(): Void {
    this.fl_print = false;
  }

  public function getExtend(pid: Int, id: Int): Void {
    var v4 = this.letters[pid][id];
    if (!v4._visible) {
      var v5 = HfStd.attachMC(this.mc, "hammer_fx_letter_pop", this.game.manager.uniq++);
      v5._x = v4._x + v4._width * 0.5;
      v5._y = v4._y;
      v4._visible = true;
    }
  }

  public function clearExtends(pid: Int): Void {
    for (v3 in 0...this.letters[pid].length) {
      this.letters[pid][v3]._visible = false;
    }
  }

  public function destroy(): Void {
    this.mc.removeMovieClip();
  }

  public function update(): Void {
    if (!this.fl_print) {
      for (v2 in 0...this.scores.length) {
        if (this.scores[v2] != null) {
          if (this.fakeScores[v2] < this.realScores[v2]) {
            this.fakeScores[v2] += Math.round(Math.max(90, (this.realScores[v2] - this.fakeScores[v2]) / 5));
          }
          if (this.fakeScores[v2] > this.realScores[v2]) {
            this.fakeScores[v2] = this.realScores[v2];
          }
          this.scores[v2].text = this.getScoreTxt(this.fakeScores[v2]);
        }
      }
    }
  }
}
