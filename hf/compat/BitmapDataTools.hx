package hf.compat;

import etwin.flash.display.BitmapData;
import etwin.flash.MovieClip;

class BitmapDataTools {

  public static inline function drawMC(bitmap: BitmapData, mc: MovieClip, x: Float, y: Float): Void {
    untyped bitmap.drawMC(mc, x, y);
  }

}