package hf.entity.shoot;

import hf.entity.Shoot;
import hf.Entity;
import hf.mode.GameMode;

class FireRain extends Shoot {
  public function new(): Void {
    super();
    this.shootSpeed = 10 + HfStd.random(5);
    this.fl_checkBounds = false;
  }

  public static function attach(g: GameMode, x: Float, y: Float): FireRain {
    var v5 = "hammer_shoot_firerain";
    var v6: FireRain = cast g.depthMan.attach(v5, Data.DP_SHOTS);
    v6.initShoot(g, x, y - 10);
    return v6;
  }

  public function hitLevel(): Void {
    this.game.fxMan.attachExplodeZone(this.x, this.y, 30);
    this.game.fxMan.inGameParticles(Data.PARTICLE_STONE, this.x, this.y, HfStd.random(4));
    this.destroy();
  }

  override public function destroy(): Void {
    super.destroy();
  }

  override public function hit(e: Entity): Void {
    var v3 = Data.BAD_CLEAR.downcast(e);
    if (v3 != null) {
      this.game.fxMan.inGameParticles(Data.PARTICLE_SPARK, this.x, this.y, HfStd.random(5) + 1);
      v3.burn();
      this.destroy();
    }
  }

  override public function init(g: GameMode): Void {
    super.init(g);
    this.playAnim(Data.ANIM_SHOOT_LOOP);
  }

  override public function onDeathLine(): Void {
    this.destroy();
  }

  override public function onHitGround(h: Float): Void {
    this.hitLevel();
  }

  override public function onHitWall(): Void {
    this.hitLevel();
  }

  override public function update(): Void {
    super.update();
    if (this.x < 0) {
      this.hitLevel();
    }
  }
}
