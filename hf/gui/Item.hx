package hf.gui;

import etwin.flash.MovieClip;
import hf.gui.Container;

class Item extends MovieClip {
  public var field: Dynamic /*TODO*/;
  public var width: Dynamic /*TODO*/;
  public var container: Container;

  public function new(): Void {
  }

  public function setLabel(l: String): Void {
    this.field.text = l;
    this.width = this.field.textWidth + 5;
  }

  public function scale(ratio: Float): Void {
    this._xscale = ratio * 100;
    this._yscale = this._xscale;
  }

  public function init(c: Container, l: String): Void {
    this.container = c;
    this.setLabel(l);
    var v4 = this.container.insert(this);
    this._x = v4.x;
    this._y = v4.y;
  }

  public function update(): Void {
  }
}
