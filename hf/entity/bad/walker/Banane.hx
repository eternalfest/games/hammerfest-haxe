package hf.entity.bad.walker;

import hf.entity.bad.Jumper;
import hf.mode.GameMode;

class Banane extends Jumper {
  public function new(): Void {
    super();
    this.setJumpUp(5);
    this.setJumpDown(5);
    this.setJumpH(100);
    this.setClimb(100, Data.IA_CLIMB);
    this.setFall(5);
  }

  public static function attach(g: GameMode, x: Float, y: Float): Banane {
    var v5 = Data.LINKAGES[Data.BAD_BANANE];
    var v6: Banane = cast g.depthMan.attach(v5, Data.DP_BADS);
    v6.initBad(g, x, y);
    return v6;
  }

  override public function init(g: GameMode): Void {
    super.init(g);
  }
}
