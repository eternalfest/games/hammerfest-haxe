package hf.entity.shoot;

import hf.entity.boss.Bat;
import hf.entity.Shoot;
import hf.Entity;
import hf.mode.GameMode;

class BossFireBall extends Shoot {
  public var turnSpeed: Float;
  public var distSpeed: Float;
  public var dist: Float;
  public var maxDist: Float;
  public var bat: Bat;
  public var ang: Float;

  public function new(): Void {
    super();
    this.fl_checkBounds = false;
    this.fl_largeTrigger = true;
    this.turnSpeed = 0.025;
    this.distSpeed = 2;
    this.dist = Data.CASE_WIDTH * 0.2;
    this.maxDist = Data.CASE_WIDTH * 5;
    this.disablePhysics();
  }

  public function initBossShoot(b: Bat, a: Float): Void {
    this.bat = b;
    this.ang = a * Math.PI / 180;
    this.center();
  }

  public function center(): Void {
    this.moveTo(this.bat.x + Math.cos(this.ang) * this.dist, this.bat.y + Math.sin(this.ang) * this.dist);
  }

  public static function attach(g: GameMode, x: Float, y: Float): BossFireBall {
    var v5 = "hammer_shoot_boss_fireball";
    var v6: BossFireBall = cast g.depthMan.attach(v5, Data.DP_SHOTS);
    v6.initShoot(g, x, y - 10);
    return v6;
  }

  override public function destroy(): Void {
    this.game.fxMan.attachExplodeZone(this.x, this.y, Data.CASE_WIDTH * 2);
    super.destroy();
  }

  override public function hit(e: Entity): Void {
    var v3 = Data.BOMB.downcast(e);
    if (v3 != null) {
      if (!v3.fl_explode) {
        this.game.fxMan.attachFx(v3.x, v3.y - Data.CASE_HEIGHT, "hammer_fx_pop");
        v3.destroy();
        this.game.fxMan.inGameParticles(Data.PARTICLE_CLASSIC_BOMB, this.x, this.y, 6);
        this.destroy();
        return;
      }
    }
    var v4 = Data.PLAYER.downcast(e);
    if (v4 != null) {
      v4.killHit(this.dx);
    }
  }

  override public function init(g: GameMode): Void {
    super.init(g);
    this.playAnim(Data.ANIM_SHOOT_LOOP);
  }

  override public function update(): Void {
    super.update();
    var v3 = this.cx;
    var v4 = this.cy;
    this.ang += Timer.tmod * this.turnSpeed;
    this.dist += Timer.tmod * this.distSpeed;
    this.dist = Math.min(this.dist, this.maxDist);
    this.center();
    if (v3 != this.cx || v4 != this.cy) {
      this.checkHits();
    }
  }
}
