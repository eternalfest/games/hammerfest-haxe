package hf;

class BitCodec {
  public var error_flag: Bool;
  public var data: String;
  public var in_pos: Int;
  public var nbits: Int;
  public var bits: Int;
  public var crc: Int;

  public function new(): Void {
    this.setData("");
  }

  public function crcStr(): String {
    return BitCodec.dif(this.crc & 63) + BitCodec.dif(this.crc >> 6 & 63) + BitCodec.dif(this.crc >> 12 & 63) + BitCodec.dif(this.crc >> 18 & 63);
  }

  public function setData(d: String): Void {
    this.error_flag = false;
    this.data = d;
    this.in_pos = 0;
    this.nbits = 0;
    this.bits = 0;
    this.crc = 0;
  }

  public function read(n: Int): Int {
    while (this.nbits < n) {
      var v3 = BitCodec.eif(this.data.charAt(this.in_pos++));
      if (this.in_pos > this.data.length || v3 == null) {
        this.error_flag = true;
        return -1;
      }
      this.crc ^= v3;
      this.crc &= 16777215;
      this.crc *= v3;
      this.nbits += 6;
      this.bits <<= 6;
      this.bits |= v3;
    }
    this.nbits -= n;
    return this.bits >> this.nbits & (1 << n) - 1;
  }

  public function nextPart(): Void {
    this.nbits = 0;
  }

  public function hasError(): Bool {
    return this.error_flag;
  }

  public function toString(): String {
    if (this.nbits > 0) {
      this.write(6 - this.nbits, 0);
    }
    return this.data;
  }

  public function write(n: Int, b: Int): Void {
    this.nbits += n;
    this.bits <<= n;
    this.bits |= b;
    while (this.nbits >= 6) {
      this.nbits -= 6;
      var v4 = this.bits >> this.nbits & 63;
      this.crc ^= v4;
      this.crc &= 16777215;
      this.crc *= v4;
      this.data += BitCodec.dif(v4);
    }
  }

  public static function ord(code: String): Int {
    return code.charCodeAt(0);
  }

  public static function chr(code: Int): String {
    return String.fromCharCode(code);
  }

  public static function eif(code: String): Null<Int> {
    var v3 = "$azAZ_";
    if (code >= v3.charAt(1) && code <= v3.charAt(2)) {
      return BitCodec.ord(code) - BitCodec.ord(v3.charAt(1));
    }
    if (code >= v3.charAt(3) && code <= v3.charAt(4)) {
      return BitCodec.ord(code) - BitCodec.ord(v3.charAt(3)) + 26;
    }
    if (code >= "0" && code <= "9") {
      return BitCodec.ord(code) - BitCodec.ord("0") + 52;
    }
    if (code == "-") {
      return 62;
    }
    if (code == v3.charAt(5)) {
      return 63;
    }
    return null;
  }

  public static function dif(code: Int): String {
    var v3 = "$aA_";
    if (code < 0) {
      return "?";
    }
    if (code < 26) {
      return BitCodec.chr(code + BitCodec.ord(v3.charAt(1)));
    }
    if (code < 52) {
      return BitCodec.chr(code - 26 + BitCodec.ord(v3.charAt(2)));
    }
    if (code < 62) {
      return BitCodec.chr(code - 52 + BitCodec.ord("0"));
    }
    if (code == 62) {
      return "-";
    }
    if (code == 63) {
      return v3.charAt(3);
    }
    return "?";
  }
}
