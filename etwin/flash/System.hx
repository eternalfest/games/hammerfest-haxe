package etwin.flash;

@:native("System")
extern class System {
  /**
    A Boolean value that specifies whether to use superdomain (`false`) or exact domain (`true`) matching rules when accessing local settings (such as camera or microphone access permissions) or locally persistent data (shared objects).
  **/
  public static var exactSettings: Bool;

  /**
    A Boolean value that tells Flash Player whether to use Unicode or the traditional code page of the operating system running the player to interpret external text files.
  **/
  public static var useCodepage: Bool;

  /**
    Replaces the contents of the Clipboard with a specified text string.
  **/
  public static function setClipboard(text: String): Void;

  /**
    Shows the specified Flash Player Settings panel.
  **/
  public static function showSettings(?tabID: Int): Void;

  /**
    Event handler: provides a super event handler for certain objects.
  **/
  public static dynamic function onStatus(infoObject:Dynamic):Void;
}
