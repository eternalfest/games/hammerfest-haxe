package hf.entity.bomb.bad;

import hf.entity.bomb.BadBomb;
import hf.entity.Player;
import hf.mode.GameMode;

class BossBomb extends BadBomb {
  public function new(): Void {
    super();
    this.duration = Data.SECOND * 2 + (HfStd.random(50) / 10) * (HfStd.random(2) * 2 - 1);
    this.fl_blink = true;
    this.fl_alphaBlink = false;
    this.blinkColorAlpha = 50;
    this.explodeSound = null;
  }

  public static function attach(g: GameMode, x: Float, y: Float): BossBomb {
    var v5 = "hammer_bomb_boss";
    var v6: BossBomb = cast g.depthMan.attach(v5, Data.DP_BOMBS);
    v6.initBomb(g, x, y);
    return v6;
  }

  override public function duplicate(): BossBomb {
    return hf.entity.bomb.bad.BossBomb.attach(this.game, this.x, this.y);
  }

  override public function initBomb(g: GameMode, x: Float, y: Float): Void {
    super.initBomb(g, x, y);
    this.setLifeTimer(this.duration * 1.5);
    this.updateLifeTimer(this.duration);
  }

  override public function onExplode(): Void {
    super.onExplode();
    var v3 = hf.entity.bad.walker.Orange.attach(this.game, this.x - Data.CASE_WIDTH * 0.5, this.y - Data.CASE_HEIGHT * 0.5);
    var v4: hf.entity.boss.Tuberculoz = cast this.game.getOne(Data.BOSS);
    if (v4.lives <= 70) {
      v3.angerMore();
    }
    if (v4.lives <= 50) {
      v3.angerMore();
    }
    v3.moveUp(10);
    v3.knock(Data.SECOND);
    v3.dropReward = null;
    this.playAnim(Data.ANIM_BOMB_EXPLODE);
  }

  override public function onKick(p: Player): Void {
    super.onKick(p);
    this.setLifeTimer(this.lifeTimer + Data.SECOND * 0.5);
  }
}
