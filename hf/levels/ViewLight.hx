package hf.levels;

import etwin.flash.MovieClip;
import hf.DepthManager;
import hf.Data in Data;

class ViewLight {
  public var world: Dynamic /*TODO*/;
  public var depthMan: DepthManager;
  public var levelId: Int;
  public var data: Dynamic /*TODO*/;
  public var scalef: Float;
  public var size: Float;
  public var mc: Null<MovieClip>;

  public static var BASE_SIZE: Float = Data.CASE_WIDTH;

  public function new(w: Dynamic /*TODO*/, dm: DepthManager, lid: Int): Void {
    this.world = w;
    this.depthMan = dm;
    this.levelId = lid;
    this.data = this.world.levels[lid];
    if (this.data == null) {
      GameManager.fatal("ERROR: null view");
    }
    this.scale(1);
  }

  public function scale(f: Float): Void {
    this.scalef = f;
    this.size = hf.levels.ViewLight.BASE_SIZE * f;
  }

  public function attach(vx: Float, vy: Float): Void {
    this.mc.removeMovieClip();
    this.mc = this.depthMan.empty(Data.DP_BACK_LAYER);
    this.mc._x = vx;
    this.mc._y = vy;
    var v4 = this.data.__dollar__map;
    this.mc.lineStyle(1, 10066329, 100);
    this.mc.moveTo(-5, 0);
    this.mc.lineTo(hf.levels.ViewLight.BASE_SIZE * Data.LEVEL_WIDTH * this.scalef - 5, 0);
    this.mc.lineTo(hf.levels.ViewLight.BASE_SIZE * Data.LEVEL_WIDTH * this.scalef - 5, hf.levels.ViewLight.BASE_SIZE * Data.LEVEL_HEIGHT * this.scalef);
    this.mc.lineTo(-5, hf.levels.ViewLight.BASE_SIZE * Data.LEVEL_HEIGHT * this.scalef);
    this.mc.lineTo(-5, 0);
    for (v5 in 0...Data.LEVEL_HEIGHT) {
      for (v6 in 0...Data.LEVEL_WIDTH) {
        if (v4[v6][v5] > 0) {
          var v7 = v6 * this.size + this.size * 0.1;
          var v8 = v5 * this.size + this.size * 0.1;
          this.mc.lineStyle(1, 16777215, 100);
          this.mc.moveTo(v7, v8);
          this.mc.lineTo(v7 + this.size * 0.9, v8);
          this.mc.lineTo(v7 + this.size * 0.9, v8 + this.size * 0.9);
          this.mc.lineTo(v7, v8 + this.size * 0.9);
          this.mc.lineTo(v7, v8);
          this.mc.lineTo(v7 + this.size * 0.9, v8 + this.size * 0.9);
          this.mc.moveTo(v7 + this.size * 0.9, v8);
          this.mc.lineTo(v7, v8 + this.size * 0.9);
        }
      }
    }
  }

  public function strike(): Void {
    this.mc.lineStyle(2, 10027008, 100);
    this.mc.moveTo(-5, 0);
    this.mc.lineTo(hf.levels.ViewLight.BASE_SIZE * Data.LEVEL_WIDTH * this.scalef - 5, hf.levels.ViewLight.BASE_SIZE * Data.LEVEL_HEIGHT * this.scalef);
    this.mc.moveTo(hf.levels.ViewLight.BASE_SIZE * Data.LEVEL_WIDTH * this.scalef - 5, 0);
    this.mc.lineTo(-5, hf.levels.ViewLight.BASE_SIZE * Data.LEVEL_HEIGHT * this.scalef);
  }

  public function destroy(): Void {
    this.mc.removeMovieClip();
  }
}
