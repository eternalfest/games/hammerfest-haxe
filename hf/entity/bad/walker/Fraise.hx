package hf.entity.bad.walker;

import hf.entity.bad.Shooter;
import hf.entity.shoot.Ball;
import hf.mode.GameMode;

class Fraise extends Shooter {
  public var fl_ball: Bool;
  public var catchCD: Float;
  public var ballTarget: Null<Entity>;

  public function new(): Void {
    super();
    this.catchCD = 0;
    this.animFactor = 1.0;
    this.setShoot(4);
    this.initShooter(0, 10);
  }

  public static function attach(g: GameMode, x: Float, y: Float): Fraise {
    var v5 = Data.LINKAGES[Data.BAD_FRAISE];
    var v6: Fraise = cast g.depthMan.attach(v5, Data.DP_BADS);
    v6.initBad(g, x, y);
    return v6;
  }

  public function catchBall(b: Ball): Void {
    if (!this.isHealthy()) {
      return;
    }
    this.next = null;
    this.walk();
    this.assignBall();
    b.destroy();
    this.ballTarget = null;
  }

  public function assignBall(): Void {
    this.fl_ball = true;
    this.catchCD = Data.SECOND * 1.5;
  }

  override public function destroy(): Void {
    if (this.fl_ball) {
      var v3 = this.game.getAnotherOne(Data.CATCHER, this);
      if (v3 != null) {
        v3.assignBall();
      }
    }
    super.destroy();
  }

  override public function endUpdate(): Void {
    super.endUpdate();
    if (this.fl_ball) {
      this.sub.balle._visible = true;
    } else {
      this.sub.balle._visible = false;
    }
    if (this.dx == 0 && this.ballTarget.fl_destroy == false) {
      if (this.ballTarget.x > this.x) {
        this._xscale = Math.abs(this._xscale);
      }
      if (this.ballTarget.x < this.x) {
        this._xscale = -Math.abs(this._xscale);
      }
    }
  }

  override public function init(g: GameMode): Void {
    super.init(g);
    this.fl_ball = false;
    this.assignBall();
    var v4 = this.game.getList(Data.CATCHER);
    for (v5 in 0...v4.length) {
      var v6 = v4[v5];
      if (v6.fl_ball) {
        this.fl_ball = false;
      }
    }
    if ((this.game.getList(Data.BALL)).length > 0) {
      this.fl_ball = false;
    }
    this.register(Data.CATCHER);
  }

  override public function onEndAnim(id: Int): Void {
    super.onEndAnim(id);
    this.ballTarget = null;
  }

  override public function onShoot(): Void {
    super.onShoot();
    this.fl_ball = false;
    if (Data.BAD.check(this.ballTarget)) {
      var v3: Fraise = cast this.ballTarget;
      v3.setNext(null, null, Data.BALL_TIMEOUT + 5, Data.ACTION_WALK);
      v3.halt();
      v3.playAnim(Data.ANIM_BAD_THINK);
    }
    var v4 = hf.entity.shoot.Ball.attach(this.game, this.x, this.y);
    v4.moveToTarget(this.ballTarget, v4.shootSpeed);
    Reflect.setField(v4, "targetCatcher", this.ballTarget);
  }

  override public function startShoot(): Void {
    var v4;
    if (!this.fl_ball || this.catchCD > 0) {
      return;
    }
    var v3 = this.game.getListCopy(Data.CATCHER);
    do {
      var v5 = HfStd.random(v3.length);
      v4 = v3[v5];
      v3.splice(v5, 1);
    } while(v4 != null && (v4 == this || !v4.isReady()));
    if (v4 != null) {
      this.ballTarget = v4;
    } else {
      if ((this.game.getList(Data.CATCHER)).length == 1) {
        this.ballTarget = this.game.getOne(Data.PLAYER);
        if (this.ballTarget == null) {
          return;
        }
      } else {
        return;
      }
    }
    super.startShoot();
  }

  override public function update(): Void {
    super.update();
    if (this.catchCD > 0) {
      this.catchCD -= Timer.tmod;
      if (this.catchCD <= 0) {
        this.catchCD = 0;
      }
    }
    if (this.game.countList(Data.BALL) == 0) {
      var v3 = true;
      var v4: Array<Fraise> = cast this.game.getList(Data.BAD_CLEAR);
      for (v5 in 0...v4.length) {
        if (v4[v5].fl_ball) {
          v3 = false;
        }
      }
      if (v3) {
        this.assignBall();
      }
    }
  }
}
