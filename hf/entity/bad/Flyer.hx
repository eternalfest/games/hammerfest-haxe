package hf.entity.bad;

import hf.entity.Bad;
import hf.entity.Animator.Anim;
import hf.mode.GameMode;

class Flyer extends Bad {
  public var speed: Float;
  public var fl_fly: Bool;
  public var xSpeed: Float;
  public var ySpeed: Float;

  public function new(): Void {
    super();
    this.speed = 4;
    this.angerFactor = 0.5;
  }

  public function fly(): Void {
    if (!this.isHealthy()) {
      return;
    }
    if (this.anger > 0) {
      this.playAnim(Data.ANIM_BAD_ANGER);
    } else {
      this.playAnim(Data.ANIM_BAD_WALK);
    }
    this.dx = this.dir * this.xSpeed * this.speedFactor;
    this.dy = -this.ySpeed * this.speedFactor;
    this.fl_fly = true;
    this.fl_intercept = false;
    this.fl_gravity = false;
    this.fl_friction = false;
    this.fl_hitCeil = true;
  }

  public function land(): Void {
    this.fl_fly = false;
    this.fl_gravity = true;
    this.fl_friction = true;
    this.fl_hitCeil = false;
  }

  override public function endUpdate(): Void {
    super.endUpdate();
    this._xscale = this.dir * Math.abs(this._xscale);
  }

  override public function infix(): Void {
    super.infix();
    if (this.fl_fly && this.y >= Data.GAME_HEIGHT) {
      this.y = Data.GAME_HEIGHT;
      this.dy = -this.ySpeed * this.speedFactor;
    }
  }

  override public function init(g: GameMode): Void {
    super.init(g);
    this.xSpeed = Math.cos(Math.PI * 0.25) * this.speed;
    this.ySpeed = Math.sin(Math.PI * 0.25) * this.speed;
    if (this.game.fl_static) {
      this.dir = -1;
    } else {
      this.dir = HfStd.random(2) * 2 - 1;
    }
    this.fly();
  }

  override public function killHit(dx: Null<Float>): Void {
    super.killHit(dx);
    this.land();
  }

  override public function onFreeze(): Void {
    super.onFreeze();
    if (this.fl_fly) {
      this.fl_intercept = true;
    }
    this.land();
  }

  override public function onHitCeil(): Void {
    if (!this.fl_fly) {
      super.onHitCeil();
      return;
    }
    this.fl_stopStepping = true;
    this.dy = this.ySpeed * this.speedFactor;
  }

  override public function onHitGround(h: Float): Void {
    if (!this.fl_fly) {
      this.fl_intercept = false;
      super.onHitGround(h);
      return;
    }
    this.fl_stopStepping = true;
    this.dy = -this.ySpeed * this.speedFactor;
  }

  override public function onHitWall(): Void {
    if (!this.fl_fly) {
      if (this.world.getCase({x: this.cx, y: this.cy}) != Data.WALL) {
        this.dx = -this.dx;
      }
      return;
    }
    this.fl_stopStepping = true;
    this.dir = -this.dir;
    this.dx = this.dir * this.xSpeed * this.speedFactor;
  }

  override public function onKnock(): Void {
    super.onKnock();
    this.land();
  }

  override public function onMelt(): Void {
    super.onMelt();
    this.fly();
  }

  override public function onNext(): Void {
    if (!this.fl_fly) {
      super.onNext();
    }
  }

  override public function onWakeUp(): Void {
    super.onWakeUp();
    this.fly();
  }

  override public function playAnim(a: Anim): Void {
    if (a.id != Data.ANIM_BAD_JUMP.id) {
      super.playAnim(a);
    }
  }

  override public function postfix(): Void {
    super.postfix();
    if (this.fl_fly) {
      this.fl_friction = false;
    }
  }

  override public function update(): Void {
    super.update();
    if (this.fl_fly && this.dy < 0 && this.y <= Data.CASE_HEIGHT) {
      this.dy = Math.abs(this.dy);
    }
  }

  override public function updateSpeed(): Void {
    super.updateSpeed();
    if (this.fl_fly) {
      this.dx = this.dir * this.xSpeed * this.speedFactor;
      if (this.dy < 0) {
        this.dy = -this.ySpeed * this.speedFactor;
      } else {
        this.dy = this.ySpeed * this.speedFactor;
      }
    }
  }
}
