package hf.entity.shoot;

import hf.entity.Shoot;
import hf.Entity;
import hf.mode.GameMode;

class PlayerArrow extends Shoot {
  public var fl_livedOneTurn: Bool;

  public function new(): Void {
    super();
    this.shootSpeed = 4;
    this.coolDown = Data.SECOND * 2;
    this._yOffset = -15;
    this.fl_hitWall = true;
    this.fl_teleport = true;
    this.fl_livedOneTurn = false;
  }

  public static function attach(g: GameMode, x: Float, y: Float): PlayerArrow {
    var v5 = "hammer_shoot_arrow";
    var v6: PlayerArrow = cast g.depthMan.attach(v5, Data.DP_SHOTS);
    v6.initShoot(g, x, y);
    return v6;
  }

  override public function hit(e: Entity): Void {
    var v3 = Data.BAD.downcast(e);
    if (v3 != null) {
      v3.setCombo(this.uniqId);
      v3.killHit(this.dx);
      this.game.fxMan.attachFx(this.x, this.y - Data.CASE_HEIGHT * 0.5, "hammer_fx_pop");
      this.destroy();
    }
  }

  override public function init(g: GameMode): Void {
    super.init(g);
    this.register(Data.PLAYER_SHOOT);
    this.playAnim(Data.ANIM_SHOOT_LOOP);
  }

  override public function onHitWall(): Void {
    if (this.fl_livedOneTurn) {
      var v2 = this.game.fxMan.attachFx(this.x, this.y + this._yOffset, "hammer_fx_arrowPouf");
      if (this.dx < 0) {
        v2.mc._xscale = -v2.mc._xscale;
      }
    } else {
      this.game.fxMan.attachFx(this.x, this.y - Data.CASE_HEIGHT * 0.5, "hammer_fx_pop");
    }
    this.destroy();
  }

  override public function update(): Void {
    super.update();
    this.fl_livedOneTurn = true;
  }
}
