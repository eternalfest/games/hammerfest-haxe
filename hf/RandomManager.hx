package hf;

class RandomManager {
  public var bulks: Array<Array<Int>>;
  public var expanded: Array<Array<Int>>;
  public var sums: Array<Int>;

  public function new(): Void {
    this.bulks = new Array();
    this.expanded = new Array();
    this.sums = new Array();
  }

  public function register(id: Int, bulk: Array<Int>): Void {
    this.bulks[id] = bulk;
    this.computeSum(id);
  }

  public function computeSum(id: Int): Void {
    this.sums[id] = 0;
    for (v3 in 0...this.bulks[id].length) {
      if (this.bulks[id][v3] == null) {
        this.bulks[id][v3] = 0;
      }
      this.sums[id] += this.bulks[id][v3];
    }
  }

  public function expand(id: Int): Void {
    this.expanded[id] = new Array();
    for (v3 in 0...this.bulks[id].length) {
      for (v4 in 0...this.bulks[id][v3]) {
        this.expanded[id].push(v3);
      }
    }
  }

  public function draw(id: Int): Null<Int> {
    var v3 = this.bulks[id];
    var v4 = 0;
    var v5 = HfStd.random(this.sums[id]);
    var v6 = 0;
    var v7 = null;
    while (true) {
      if (!(v4 < v3.length && v7 == null)) break;
      v6 += v3[v4];
      if (v5 < v6) {
        v7 = v4;
      }
      ++v4;
    }
    if (v7 == null) {
      GameManager.warning("null draw in array " + id);
    }
    return v7;
  }

  public function drawSpecial(): Void {
  }

  public function evaluateChances(id: Int, value: Int): Float {
    Log.trace("item=" + value);
    Log.trace(this.bulks[id][value]);
    Log.trace(this.sums[id]);
    return this.bulks[id][value] / this.sums[id];
  }

  public function remove(rid: Int, id: Int): Void {
    this.bulks[rid][id] = 0;
    this.computeSum(rid);
  }
}
