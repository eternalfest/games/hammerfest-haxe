package etwin.ds;

import etwin.Symbol;

/**
 * The WeakMap class allows to map object keys to arbitrary values.
 *
 * A WeakMap can be used to safely attach a value to an object. It is the
 * preferred way to store custom data attached to objects from the `hf`
 * package.
 *
 * This class should be preferred over the standard `haxe.ds.WeakMap` class
 * because the standard class does not support the `flash` target.
 */
class WeakMap<K/*: {}*/, V> {
  private var symbol(default, null): Symbol;

  public function new(): Void {
    this.symbol = new Symbol();
  }

  public function get(k: K): Null<V> {
    return Reflect.field(k, this.symbol.toString());
  }

  public function set(k: K, v: V): Void {
    Reflect.setField(k, this.symbol.toString(), v);
    #if flash8
    // Make the property non-enumerable
    untyped ASSetPropFlags(k, this.symbol.toString(), 1, 0);
    #end
  }

  public function exists(k: K): Bool {
    return Reflect.hasField(k, this.symbol.toString());
  }

  public function remove(k: K): Void {
    Reflect.deleteField(k, this.symbol.toString());
  }
}
