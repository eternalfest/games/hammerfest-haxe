package hf.mode;

import hf.mode.Adventure;

class AdventureTest extends Adventure {
  public var testData: Dynamic /*TODO*/;

  public function new(m: Dynamic /*TODO*/, l: Dynamic /*TODO*/): Void {
    super(m, 1);
    this.testData = l;
    this._name = "$advTest";
  }

  override public function startLevel(): Void {
    super.startLevel();
    var v3 = this.getPlayerList();
    for (v4 in 0...v3.length) {
      v3[v4].moveToCase(this.world.current.__dollar__playerX, this.world.current.__dollar__playerY);
    }
  }

  override public function initWorld(): Void {
    super.initWorld();
    this.world.raw[1] = this.world.serializeExternal(this.testData);
  }

  override public function nextLevel(): Void {
    this.endMode();
  }

  override public function endMode(): Void {
    this.stopMusic();
    this.world.destroy();
    this.manager.stopChild(null);
  }

  override public function onEndOfSet(): Void {
    this.endMode();
  }

  override public function onGameOver(): Void {
    this.endMode();
  }

  override public function main(): Void {
    Log.print(Math.round(Timer.fps()) + "fps");
    super.main();
  }
}
