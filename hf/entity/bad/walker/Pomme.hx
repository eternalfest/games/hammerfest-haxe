package hf.entity.bad.walker;

import hf.entity.bad.Shooter;
import hf.mode.GameMode;

class Pomme extends Shooter {
  public function new(): Void {
    super();
    this.setJumpUp(3);
    this.setJumpH(100);
    this.setClimb(100, 1);
    this.setFall(20);
    this.setShoot(2);
    this.initShooter(20, 12);
  }

  public static function attach(g: GameMode, x: Float, y: Float): Pomme {
    var v5 = Data.LINKAGES[Data.BAD_POMME];
    var v6: Pomme = cast g.depthMan.attach(v5, Data.DP_BADS);
    v6.initBad(g, x, y);
    return v6;
  }

  override public function init(g: GameMode): Void {
    super.init(g);
  }

  override public function onShoot(): Void {
    var v2 = hf.entity.shoot.Pepin.attach(this.game, this.x, this.y);
    if (this.dir < 0) {
      v2.moveLeft(v2.shootSpeed);
    } else {
      v2.moveRight(v2.shootSpeed);
    }
  }
}
