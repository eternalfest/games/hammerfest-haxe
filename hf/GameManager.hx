package hf;

import etwin.flash.DynamicMovieClip;
import etwin.flash.Sound;
import hf.Hf;
import hf.Cookie;
import hf.DepthManager;
import hf.GameParameters;
import hf.Hash;
import hf.mode.GameMode;
import hf.Mode;
import hf.SoundManager;

class GameManager {
  public static var BASE_VOLUME: Float = 50;
  public static var CONFIG: GameParameters = null;
  public static var KEY: Int = HfStd.random(999999) * HfStd.random(999999);
  public static var HH: Hash = new Hash();
  public static var SELF: GameManager = null;

  public var root: Hf;
  public var uniq: Int;
  public var fl_local: Bool;
  public var fl_cookie: Bool;
  public var musics: Array<Sound>;
  public var history: Array<String>;
  public var depthMan: DepthManager;
  public var soundMan: SoundManager;
  public var fl_debug: Bool;
  public var cookie: Cookie;
  public var fVersion: Int;
  public var fl_flash8: Bool;
  public var progressBar: Null<DynamicMovieClip /* TODO */>;
  public var current: Mode;
  public var child: Null<Mode>;
  public var fps: Float;

  public function new(mc: Hf, initObj: Dynamic /*TODO*/): Void {
    this.root = mc;
    this.uniq = 666;
    GameManager.SELF = this;
    this.fl_local = initObj.fl_local;
    this.musics = initObj.musics;
    this.history = new Array();
    this.logAction("$B" + "01-11 11:48:51");
    Lang.init(initObj.rawLang);
    Data.init(this);
    if (this.isDev()) {
      this.fl_debug = true;
    }
    if (this.isTutorial() || Loader.BASE_SCRIPT_URL == null) {
      this.logAction("$using sysfam");
      initObj.families = "0,7,1000,18";
      if (this.isDev()) {
        initObj.families = "0,7,1000,1001,1002,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19";
      }
    }
    GameManager.CONFIG = new GameParameters(HfStd.getRoot(), this, initObj.families, initObj.options);
    if (this.fl_local) {
      this.cookie = new Cookie(this);
    }
    Log.setColor(13421772);
    Log.tprint._x = Data.DOC_WIDTH * 0.7;
    Log.tprint._y = 4;
    Log.tprint.textColor = 16776960;
    this.depthMan = new DepthManager(this.root);
    this.fl_cookie = this.fl_debug && this.fl_local;
    var v4 = (HfStd.getRoot()).__dollar__version;
    this.fVersion = HfStd.parseInt(((v4.split(" "))[1].split(","))[0], 10);
    this.fl_flash8 = this.fVersion != null && !HfStd.isNaN(this.fVersion) && this.fVersion >= 8;
    if (!this.fl_flash8) {
      GameManager.fatal(Lang.get(15) + "\nhttp://www.macromedia.com/go/getflash");
      return;
    }
    hf.flash.Init.init();
    if (this.isMode(null) || this.isMode("")) {
      GameManager.fatal("Veuillez vider votre cache internet (voir page Support Technique en bas du site).");
      return;
    }
    var v5 = GameManager.CONFIG.soundVolume * 100;
    this.soundMan = new SoundManager(this.depthMan.empty(Data.DP_SOUNDS), 0);
    this.soundMan.setVolume(Data.CHAN_MUSIC, GameManager.CONFIG.musicVolume * 100);
    this.soundMan.setVolume(Data.CHAN_PLAYER, v5);
    this.soundMan.setVolume(Data.CHAN_BOMB, v5);
    this.soundMan.setVolume(Data.CHAN_ITEM, v5);
    this.soundMan.setVolume(Data.CHAN_FIELD, v5);
    this.soundMan.setVolume(Data.CHAN_BAD, Math.max(0, v5 - 10));
    this.soundMan.setVolume(Data.CHAN_INTERF, Math.max(0, v5 - 10));
    for (v6 in 0...this.musics.length) {
      this.musics[v6].setVolume(GameManager.CONFIG.musicVolume * 100);
    }
    if (!GameManager.CONFIG.hasMusic()) {
      this.soundMan.loop("sound_kick", Data.CHAN_MUSIC);
      this.soundMan.setVolume(Data.CHAN_MUSIC, 0);
    }
    this.registerClasses();
    var v7 = GameManager.HH;
    v7.set("$8d6fff6186db2e4f436852f16dcfbba8", "$4768dc07c5f8a02389dd5bc1ab2e8cf4");
    v7.set("$3bb529b9fb9f62833d42c0c1a7b36a43", "$53d55e84334a88faa7699ef49647028d");
    v7.set("$9fa2a5eb602c6e8df97aeff54eecce7b", "$0d3258c27fa25f609757dbec3c4d5b40");
    v7.set("$041ccec10a38ecef7d4f5d7acb7b7c46", "$9004f8d219ddeaf70d806031845b81a8");
    v7.set("$51cc93b000b284de6097c9319221e891", "$b04ce150619f8443c69122877c18abb9");
    v7.set("$38fe1fbe22565c2f6691f1f666a600d9", "$05a8015ab342ed46b92ef29b88d30069");
    v7.set("$0255841255b29dd91b88a57b4a27f422", "$14460ca82946da41bc53c89e6670b8c0");
    v7.set("$1def3777b79eb80048ebf70a0ae83b77", "$8783468478de453e70eeb3b3cb1327cf");
    v7.set("$a1a9405afb4576a3bceb75995ad17d09", "$d4a546d78441aba69dc043b9b23dc068");
    v7.set("$8c49e07bc65be554538effb12eced2c2", "$3c5fee37f81ebe52a1dc76d7bbdd2c07");
    v7.set("$bfe3df5761159d38a6419760d8613c26", "$e701d0c9283358ab44c899db4f13a3fb");
    this.startDefaultGame();
  }

  public function registerClasses(): Void {
    HfStd.registerClass("hammer_item_score", hf.entity.item.ScoreItem);
    HfStd.registerClass("hammer_item_special", hf.entity.item.SpecialItem);
    HfStd.registerClass(Data.LINKAGES[Data.BAD_POMME], hf.entity.bad.walker.Pomme);
    HfStd.registerClass(Data.LINKAGES[Data.BAD_CERISE], hf.entity.bad.walker.Cerise);
    HfStd.registerClass(Data.LINKAGES[Data.BAD_BANANE], hf.entity.bad.walker.Banane);
    HfStd.registerClass(Data.LINKAGES[Data.BAD_ANANAS], hf.entity.bad.walker.Ananas);
    HfStd.registerClass(Data.LINKAGES[Data.BAD_BOMBE], hf.entity.bad.walker.Bombe);
    HfStd.registerClass(Data.LINKAGES[Data.BAD_ORANGE], hf.entity.bad.walker.Orange);
    HfStd.registerClass(Data.LINKAGES[Data.BAD_FRAISE], hf.entity.bad.walker.Fraise);
    HfStd.registerClass(Data.LINKAGES[Data.BAD_ABRICOT], hf.entity.bad.walker.Abricot);
    HfStd.registerClass(Data.LINKAGES[Data.BAD_POIRE], hf.entity.bad.walker.Poire);
    HfStd.registerClass(Data.LINKAGES[Data.BAD_CITRON], hf.entity.bad.walker.Citron);
    HfStd.registerClass(Data.LINKAGES[Data.BAD_FIREBALL], hf.entity.bad.FireBall);
    HfStd.registerClass(Data.LINKAGES[Data.BAD_BALEINE], hf.entity.bad.flyer.Baleine);
    HfStd.registerClass(Data.LINKAGES[Data.BAD_SPEAR], hf.entity.bad.Spear);
    HfStd.registerClass(Data.LINKAGES[Data.BAD_CRAWLER], hf.entity.bad.ww.Crawler);
    HfStd.registerClass(Data.LINKAGES[Data.BAD_TZONGRE], hf.entity.bad.flyer.Tzongre);
    HfStd.registerClass(Data.LINKAGES[Data.BAD_SAW], hf.entity.bad.ww.Saw);
    HfStd.registerClass(Data.LINKAGES[Data.BAD_KIWI], hf.entity.bad.walker.Kiwi);
    HfStd.registerClass(Data.LINKAGES[Data.BAD_LITCHI], hf.entity.bad.walker.Litchi);
    HfStd.registerClass(Data.LINKAGES[Data.BAD_LITCHI_WEAK], hf.entity.bad.walker.LitchiWeak);
    HfStd.registerClass(Data.LINKAGES[Data.BAD_FRAMBOISE], hf.entity.bad.walker.Framboise);
    HfStd.registerClass("hammer_boss_bat", hf.entity.boss.Bat);
    HfStd.registerClass("hammer_boss_human", hf.entity.boss.Tuberculoz);
    HfStd.registerClass("hammer_shoot_pepin", hf.entity.shoot.Pepin);
    HfStd.registerClass("hammer_shoot_fireball", hf.entity.shoot.FireBall);
    HfStd.registerClass("hammer_shoot_arrow", hf.entity.shoot.PlayerArrow);
    HfStd.registerClass("hammer_shoot_ball", hf.entity.shoot.Ball);
    HfStd.registerClass("hammer_shoot_zest", hf.entity.shoot.Zeste);
    HfStd.registerClass("hammer_shoot_player_fireball", hf.entity.shoot.PlayerFireBall);
    HfStd.registerClass("hammer_shoot_player_pearl", hf.entity.shoot.PlayerPearl);
    HfStd.registerClass("hammer_shoot_boss_fireball", hf.entity.shoot.BossFireBall);
    HfStd.registerClass("hammer_shoot_firerain", hf.entity.shoot.FireRain);
    HfStd.registerClass("hammer_shoot_hammer", hf.entity.shoot.Hammer);
    HfStd.registerClass("hammer_shoot_framBall2", hf.entity.shoot.FramBall);
    HfStd.registerClass("hammer_bomb_classic", hf.entity.bomb.player.Classic);
    HfStd.registerClass("hammer_bomb_black", hf.entity.bomb.player.Black);
    HfStd.registerClass("hammer_bomb_blue", hf.entity.bomb.player.Blue);
    HfStd.registerClass("hammer_bomb_green", hf.entity.bomb.player.Green);
    HfStd.registerClass("hammer_bomb_red", hf.entity.bomb.player.Red);
    HfStd.registerClass("hammer_bomb_poire_frozen", hf.entity.bomb.player.PoireBombFrozen);
    HfStd.registerClass("hammer_bomb_mine_frozen", hf.entity.bomb.player.MineFrozen);
    HfStd.registerClass("hammer_bomb_soccer", hf.entity.bomb.player.SoccerBall);
    HfStd.registerClass("hammer_bomb_repel", hf.entity.bomb.player.RepelBomb);
    HfStd.registerClass("hammer_bomb_poire", hf.entity.bomb.bad.PoireBomb);
    HfStd.registerClass("hammer_bomb_mine", hf.entity.bomb.bad.Mine);
    HfStd.registerClass("hammer_bomb_boss", hf.entity.bomb.bad.BossBomb);
    HfStd.registerClass("hammer_supa_icemeteor", hf.entity.supa.IceMeteor);
    HfStd.registerClass("hammer_supa_smoke", hf.entity.supa.Smoke);
    HfStd.registerClass("hammer_supa_ball", hf.entity.supa.Ball);
    HfStd.registerClass("hammer_supa_bubble", hf.entity.supa.Bubble);
    HfStd.registerClass("hammer_supa_tons", hf.entity.supa.Tons);
    HfStd.registerClass("hammer_supa_item", hf.entity.supa.SupaItem);
    HfStd.registerClass("hammer_supa_arrow", hf.entity.supa.Arrow);
    HfStd.registerClass("hammer_player", hf.entity.Player);
    HfStd.registerClass("hammer_player_wbomb", hf.entity.WalkingBomb);
    HfStd.registerClass("hammer_fx_particle", hf.entity.fx.Particle);
    HfStd.registerClass("hammer_editor_button", hf.gui.SimpleButton);
    HfStd.registerClass("hammer_editor_label", hf.gui.Label);
    HfStd.registerClass("hammer_editor_field", hf.gui.Field);
  }

  public function setExists(n: String): Bool {
    var v3 = HfStd.getVar(this.root, n);
    return v3 != null;
  }

  public function progress(ratio: Float): Void {
    if (ratio == null || ratio >= 1) {
      this.progressBar.removeMovieClip();
      this.progressBar = null;
      return;
    }
    if (this.progressBar == null) {
      this.progressBar = ((cast this.depthMan.attach("lifeBar", Data.DP_INTERF)): DynamicMovieClip);
      this.progressBar._x = Data.GAME_WIDTH / 2;
      this.progressBar._y = Data.GAME_HEIGHT - 40;
    }
    this.progressBar.bar._xscale = ratio * 100;
  }

  public static function fatal(msg: String): Void {
    Log.setColor(16711680);
    Log.trace("*** CRITICAL ERROR *** " + msg);
    GameManager.SELF.current.destroy();
    GameManager.SELF.root.stop();
    GameManager.SELF.root.removeMovieClip();
  }

  public static function warning(msg: String): Void {
    Log.trace("* WARNING * " + msg);
  }

  public function redirect(url: String, params: Dynamic /*TODO*/): Void {
    this.current.lock();
    (HfStd.getGlobal("exitGame"))(url, params);
  }

  public function logIllegal(str: String): Void {
    this.logAction("$!" + str);
  }

  public function logAction(str: String): Void {
    str = Tools.replace(str, "$", "");
    str = Tools.replace(str, ":", ".");
    this.history.push(str);
  }

  public function transition(prev: Mode, next: Mode): Void {
    next.init();
    if (prev == null) {
      this.current = next;
    } else {
      prev.destroy();
      this.current = next;
    }
  }

  public function startChild(c: Mode): Mode {
    if (this.child != null) {
      GameManager.fatal("another child process is running!");
    }
    if (this.current.fl_lock) {
      GameManager.fatal("process is locked, can't create a child");
    }
    this.current.lock();
    this.current.onSleep();
    this.current.hide();
    this.child = c;
    this.child.fl_runAsChild = true;
    this.child.init();
    return this.child;
  }

  public function stopChild(data: Dynamic /*TODO*/): Void {
    var v3 = this.child._name;
    this.child.destroy();
    this.child = null;
    this.current.unlock();
    this.current.show();
    this.current.onWakeUp(v3, data);
  }

  public function startMode(m: Mode): Void {
    this.transition(this.current, m);
  }

  public function startGameMode(m: GameMode): Void {
    this.transition(this.current, m);
  }

  public function isAdventure(): Bool {
    return this.isMode("$solo");
  }

  public function isTutorial(): Bool {
    return this.isMode("$tutorial");
  }

  public function isSoccer(): Bool {
    return this.isMode("$soccer");
  }

  public function isMultiCoop(): Bool {
    return this.isMode("$multicoop");
  }

  public function isTimeAttack(): Bool {
    return this.isMode("$timeattack");
  }

  public function isMultiTime(): Bool {
    return this.isMode("$multitime");
  }

  public function isBossRush(): Bool {
    return this.isMode("$bossrush");
  }

  public function isFjv(): Bool {
    return false;
  }

  public function isDev(): Bool {
    return this.setExists("xml_dev") && !this.isFjv();
  }

  public function isMode(modeName: String): Bool {
    return HfStd.getVar(HfStd.getRoot(), "$mode") == modeName.substring(1);
  }

  public function startDefaultGame(): Void {
    if (this.isTutorial()) {
      this.startGameMode(new hf.mode.Tutorial(this));
      return;
    }
    if (this.isSoccer()) {
      this.startMode(new hf.mode.Soccer(this, 0));
      return;
    }
    if (this.isMultiCoop()) {
      this.startGameMode(new hf.mode.MultiCoop(this, 0));
      return;
    }
    if (this.isTimeAttack()) {
      this.startGameMode(new hf.mode.TimeAttack(this, 0));
      return;
    }
    if (this.isMultiTime()) {
      this.startGameMode(new hf.mode.TimeAttackMulti(this, 0));
      return;
    }
    if (this.isBossRush()) {
      this.startGameMode(new hf.mode.BossRush(this, 0));
      return;
    }
    if (this.isFjv()) {
      this.startMode(new hf.mode.FjvEnd(this, false));
      return;
    }
    if (this.isAdventure()) {
      this.startGameMode(new hf.mode.Adventure(this, 0));
      return;
    }
    GameManager.fatal("Invalid mode '" + HfStd.getVar(HfStd.getRoot(), "$mode") + "' found.");
  }

  public function main(): Void {
    Timer.update();
    this.fps = Timer.fps();
    Timer.tmod = Math.min(2.8, Timer.tmod);
    HfStd.setGlobal("tmod", Timer.tmod);
    HfStd.setGlobal("Debug", Log);
    this.soundMan.main();
    this.current.main();
    if (this.child != null) {
      this.child.main();
    }
  }
}
