package hf.entity.bad.walker;

import hf.entity.bad.Jumper;
import hf.entity.Animator.Anim;
import hf.mode.GameMode;

class Bombe extends Jumper {
  public static var RADIUS: Float = Data.CASE_WIDTH * 5;
  public static var EXPERT_RADIUS: Float = Data.CASE_WIDTH * 8;
  public static var POWER: Float = 30;

  public var fl_overheat: Bool;

  public function new(): Void {
    super();
    this.setJumpUp(10);
    this.setJumpDown(6);
    this.setJumpH(100);
    this.setClimb(100, 3);
    this.fl_alphaBlink = false;
    this.fl_overheat = false;
    this.blinkColor = 16752222;
    this.blinkColorAlpha = 50;
  }

  public static function attach(g: GameMode, x: Float, y: Float): Bombe {
    var v5 = Data.LINKAGES[Data.BAD_BOMBE];
    var v6: Bombe = cast g.depthMan.attach(v5, Data.DP_BADS);
    v6.initBad(g, x, y);
    return v6;
  }

  public function trigger(): Void {
    if (this.fl_overheat) {
      return;
    }
    this.halt();
    this.playAnim(Data.ANIM_BAD_DIE);
    this.fl_loop = false;
    this.fl_freeze = false;
    var v2 = Data.SECOND * (0.25 + (HfStd.random(10) / 100) * (HfStd.random(2) * 2 - 1));
    this.setLifeTimer(Data.SECOND * 3);
    this.updateLifeTimer(Data.SECOND);
    this.setNext(null, null, Data.SECOND * 3, Data.ACTION_WALK);
    this.fl_overheat = true;
  }

  public function selfDestruct(): Void {
    this.game.fxMan.attachExplodeZone(this.x, this.y, hf.entity.bad.walker.Bombe.RADIUS);
    var v2 = this.game.getClose(Data.PLAYER, this.x, this.y, hf.entity.bad.walker.Bombe.RADIUS, false);
    for (v3 in 0...v2.length) {
      var v4 = v2[v3];
      v4.killHit(0);
      this.shockWave(v4, hf.entity.bad.walker.Bombe.RADIUS, hf.entity.bad.walker.Bombe.POWER);
      if (!v4.fl_shield) {
        v4.dy = -10 - HfStd.random(20);
      }
    }
    this.game.soundMan.playSound("sound_bomb_black", Data.CHAN_BOMB);
    this.dropReward();
    this.game.fxMan.inGameParticles(Data.PARTICLE_METAL, this.x, this.y, HfStd.random(4) + 5);
    this.game.fxMan.inGameParticles(Data.PARTICLE_SPARK, this.x, this.y, HfStd.random(4));
    this.onKill();
    this.destroy();
  }

  override public function init(g: GameMode): Void {
    super.init(g);
    if (this.game.fl_bombExpert) {
      hf.entity.bad.walker.Bombe.RADIUS = hf.entity.bad.walker.Bombe.EXPERT_RADIUS;
    }
  }

  override public function isHealthy(): Bool {
    return !this.fl_overheat && super.isHealthy();
  }

  override public function killHit(dx: Null<Float>): Void {
    this.trigger();
  }

  override public function onFreeze(): Void {
    this.trigger();
  }

  override public function onLifeTimer(): Void {
    this.selfDestruct();
  }

  override public function playAnim(obj: Anim): Void {
    if (this.fl_overheat) {
      return;
    }
    super.playAnim(obj);
    if (obj.id == Data.ANIM_BAD_THINK.id) {
      this.fl_loop = true;
    }
    if (obj.id == Data.ANIM_BAD_JUMP.id) {
      this.fl_loop = false;
    }
  }

  override public function prefix(): Void {
    if (!this.fl_stable && this.fl_overheat) {
      this.dx = 0;
    }
    super.prefix();
  }
}
