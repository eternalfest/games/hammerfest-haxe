package hf.mode;

import hf.Mode;
import etwin.flash.Key;

class ItemBrowser extends Mode {
  public var page: Int;
  public var mcList: Array<Dynamic /*TODO*/>;
  public var fl_score: Bool;
  public var fl_names: Bool;
  public var header: Dynamic /*TODO*/;
  public var footer: Dynamic /*TODO*/;
  public var ffilter: Dynamic /*TODO*/;
  public var fullRandList: Array<Dynamic /*TODO*/>;
  public var max_pages: Int;
  public var klock: Dynamic /*TODO*/;

  public static var ITEM_WIDTH: Float = 65;
  public static var ITEM_HEIGHT: Float = 60;
  public static var PAGE_LENGTH: Int = Math.floor(Data.DOC_WIDTH / hf.mode.ItemBrowser.ITEM_WIDTH) * Math.floor(Data.DOC_HEIGHT / hf.mode.ItemBrowser.ITEM_HEIGHT);

  public function new(m: Dynamic /*TODO*/): Void {
    super(m);
    this._name = "$itemBrowser";
    this.page = 0;
    this.mcList = new Array();
    this.fl_score = true;
    this.fl_names = false;
    this.header = this.depthMan.attach("hammer_editor_footer", Data.DP_TOP);
    this.header._x = Data.DOC_WIDTH * 0.5;
    this.header._y = 7;
    this.header.field.text = "--";
    this.footer = this.depthMan.attach("hammer_editor_footer", Data.DP_TOP);
    this.footer._x = Data.DOC_WIDTH * 0.5;
    this.footer._y = Data.DOC_HEIGHT - 7;
    this.ffilter = -1;
    this.fullRandList = new Array();
    for (v4 in 0...Data.SCORE_ITEM_FAMILIES.length) {
      var v5 = Data.SCORE_ITEM_FAMILIES[v4];
      for (v6 in 0...v5.length) {
        this.fullRandList[v5[v6].id] = v5[v6].r;
      }
    }
    for (v7 in 0...Data.SPECIAL_ITEM_FAMILIES.length) {
      var v8 = Data.SPECIAL_ITEM_FAMILIES[v7];
      for (v9 in 0...v8.length) {
        this.fullRandList[v8[v9].id] = v8[v9].r;
      }
    }
    this.refresh();
  }

  public function getRandColor(proba: Int): Int {
    var v3 = 5592405;
    switch (proba) {
      case Data.__NA:
        v3 = 11184810;
        return v3;
      case Data.COMM:
        v3 = 65280;
        return v3;
      case Data.UNCO:
        v3 = 16776960;
        return v3;
      case Data.RARE:
        v3 = 16750848;
        return v3;
      case Data.UNIQ:
        v3 = 16711680;
        return v3;
      case Data.MYTH:
        v3 = 4474111;
        return v3;
      case Data.CANE:
        v3 = 3534050;
        return v3;
      case Data.LEGEND:
        v3 = 16729343;
    }
    return v3;
  }

  public function refresh(): Void {
    var v2;
    var v4;
    for (v3 in 0...this.mcList.length) {
      this.mcList[v3].removeMovieClip();
    }
    this.mcList = new Array();
    if (this.fl_score) {
      v4 = "hammer_item_score";
      v2 = 1000;
    } else {
      v2 = 0;
      v4 = "hammer_item_special";
    }
    this.max_pages = Math.ceil(Data.MAX_ITEMS / hf.mode.ItemBrowser.PAGE_LENGTH);
    var v5 = 0.0;
    var v6 = 0.0;
    var v7 = Math.floor(this.page * hf.mode.ItemBrowser.PAGE_LENGTH);
    while (true) {
      if (!(v7 < Data.MAX_ITEMS && v7 < (this.page + 1) * hf.mode.ItemBrowser.PAGE_LENGTH)) break;
      var v8: hf.entity.Item = cast this.depthMan.attach(v4, Data.DP_ITEMS);
      v8.blendMode = BlendMode.LAYER;
      v8._x = v5 + hf.mode.ItemBrowser.ITEM_WIDTH * 0.5;
      v8._y = v6 + hf.mode.ItemBrowser.ITEM_HEIGHT - 7;
      v8.gotoAndStop("" + (v7 + 1));
      v8.onRollOver = HfStd.callback(this, "onOver", v7 + v2);
      v8.sub.stop();
      if (v7 + 1 > v8._totalframes) {
        v8._visible = false;
      }
      this.mcList.push(v8);
      if (this.fl_names) {
        if (!(this.fl_score && Data.ITEM_VALUES[v7 + v2] == null) && !(!this.fl_score && Data.FAMILY_CACHE[v7 + v2] == null)) {
          var v9 = Lang.getItemName(v7 + v2);
          if (v9 != null || v9 == "") {
            var v10 = HfStd.createTextField(v8, this.manager.uniq++);
            v10.border = true;
            v10.background = true;
            if (v9 == null) {
              v10.borderColor = 16711680;
              v10.backgroundColor = 7798784;
            } else {
              v10.borderColor = 11184810;
              v10.backgroundColor = 0;
            }
            v10._x = -20;
            v10._y = -35;
            v10._width = 60;
            v10._height = 40;
            v10.textColor = 16777215;
            v10.text = v9;
            v10.wordWrap = true;
            v10.selectable = false;
            v10._xscale = 90;
            v10._yscale = v10._xscale;
          }
        }
      } else {
        if (this.ffilter < 0) {
          if (this.fullRandList[v7 + v2] == null) {
            v8._alpha = 75;
          }
        } else {
          if (this.ffilter + v2 != Data.FAMILY_CACHE[v7 + v2]) {
            var v11 = new etwin.flash.filters.BlurFilter();
            v11.blurX = 8;
            v11.blurY = v11.blurX;
            v8.filters = [v11];
            v8._alpha = 40;
          }
        }
      }
      var v12: etwin.flash.DynamicMovieClip = cast this.depthMan.attach("hammer_editor_simple_label", Data.DP_INTERF);
      v12._x = v5 + hf.mode.ItemBrowser.ITEM_WIDTH * 0.5;
      v12._y = v6 + hf.mode.ItemBrowser.ITEM_HEIGHT;
      var v13 = v12.field;
      if (this.fl_score) {
        if (Data.ITEM_VALUES[v7 + v2] == null) {
          v13.text = "(" + (v7 + v2) + ")[--]\n--";
        } else {
          v13.text = "(" + (v7 + v2) + ")[" + Data.FAMILY_CACHE[v7 + v2] + "]\n" + Data.ITEM_VALUES[v7 + v2];
        }
      } else {
        if (Data.FAMILY_CACHE[v7 + v2] == null) {
          v13.text = "(" + (v7 + v2) + ")[--]\n--";
        } else {
          v13.text = "(" + (v7 + v2) + ")[" + Data.FAMILY_CACHE[v7 + v2] + "]";
        }
      }
      if (this.ffilter < 0) {
        v13.textColor = this.getRandColor(this.fullRandList[v7 + v2]);
      } else {
        if (this.ffilter + v2 == Data.FAMILY_CACHE[v7 + v2]) {
          v13.textColor = this.getRandColor(this.fullRandList[v7 + v2]);
        } else {
          v13.textColor = 5592405;
        }
      }
      if (Data.FAMILY_CACHE[v7 + v2] == null) {
        v13.textColor = 16777215;
      }
      this.mcList.push(v12);
      v5 += hf.mode.ItemBrowser.ITEM_WIDTH;
      if (v5 >= Data.DOC_WIDTH - hf.mode.ItemBrowser.ITEM_WIDTH) {
        v5 = 0;
        v6 += hf.mode.ItemBrowser.ITEM_HEIGHT;
      }
      ++v7;
    }
    this.footer.field.text = "";
    if (this.fl_score) {
      this.footer.field.text += "Score items, ";
    } else {
      this.footer.field.text += "Special items, ";
    }
    if (this.ffilter >= 0) {
      this.footer.field.text += " | Filter: " + Lang.getFamilyName(this.ffilter + v2) + " (" + (this.ffilter + v2) + ") | ";
    }
    this.footer.field.text += "PAGE " + (this.page + 1) + "/" + this.max_pages;
  }

  public function onOver(id: Int): Void {
    this.header.field.text = Lang.getItemName(id);
  }

  override public function endMode(): Void {
    if (this.fl_runAsChild) {
      this.manager.stopChild(null);
    }
  }

  override public function main(): Void {
    super.main();
    if (this.klock != null && !Key.isDown(this.klock)) {
      this.klock = null;
    }
    if (Key.isDown(Key.LEFT) && this.page > 0 && this.klock != Key.LEFT) {
      --this.page;
      this.refresh();
      this.klock = Key.LEFT;
    }
    if (Key.isDown(Key.RIGHT) && this.page < this.max_pages - 1 && this.klock != Key.RIGHT) {
      ++this.page;
      this.refresh();
      this.klock = Key.RIGHT;
    }
    if (Key.isDown(Key.SPACE) && this.klock != Key.SPACE) {
      this.fl_score = !this.fl_score;
      this.ffilter = -1;
      this.page = 0;
      this.klock = Key.SPACE;
      this.refresh();
    }
    if (this.fl_names != Key.isDown(78)) {
      this.fl_names = Key.isDown(78);
      this.refresh();
    }
    if (Key.isDown(Key.PGUP) && this.ffilter > -1 && this.klock != Key.PGUP) {
      --this.ffilter;
      this.refresh();
      this.klock = Key.PGUP;
    }
    if (Key.isDown(Key.PGDN) && this.klock != Key.PGDN) {
      ++this.ffilter;
      this.refresh();
      this.klock = Key.PGDN;
    }
    if (Key.isDown(Key.ESCAPE)) {
      this.endMode();
    }
    Log.print(this.fl_names);
  }
}
