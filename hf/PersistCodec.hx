package hf;

import hf.BitCodec;
import hf.Hash;

class PersistCodec {
  public var obfu_mode: Bool;
  public var crc: Bool;
  public var fast: Null<Bool>;
  // An older version of this class may be found on the internet, it calls this field `bc`.
  public var pou: Null<BitCodec>;
  public var fields: Null<Hash>;
  public var nfields: Null<Int>;
  public var next_field_bits: Null<Int>;
  public var nfields_bits: Null<Int>;
  public var cache: Null<Array<Dynamic>>;
  public var fieldtbl: Null<Array<Dynamic>>;
  public var result: Null<Dynamic>;

  public function new(): Void {
    this.obfu_mode = false;
    this.crc = false;
  }

  public function encode_array(a: Array<Dynamic>): Void {
    var v4 = 0;
    for (v3 in 0...a.length) {
      if (a[v3] == null) {
        ++v4;
      } else {
        if (v4 > 0) {
          this.pou.write(2, 2);
          this.encode_int(v4);
          v4 = 0;
        }
        this.pou.write(1, 0);
        this.do_encode(a[v3]);
      }
    }
    this.pou.write(2, 3);
  }

  public function decode_array(): Array<Dynamic> {
    var v2 = new Array();
    Reflect.setField(v2, "pos", 0);
    this.cache.unshift(v2);
    return v2;
  }

  public function decode_array_item(a: Dynamic): Bool {
    var v3 = this.pou.read(1) == 0;
    if (v3) {
      var pos: Int = Reflect.field(a, "pos");
      a[pos++] = this.do_decode();
      Reflect.setField(a, "pos", pos);
      return true;
    }
    var v4 = this.pou.read(1) == 1;
    if (v4) {
      return false;
    }
    a.pos += this.decode_int();
    return true;
  }

  public function decode_array_fast(): Array<Dynamic> {
    var v2 = new Array();
    var v3 = 0;
    while (true) {
      var v4 = this.pou.read(1) == 0;
      if (v4) {
        v2[v3++] = this.do_decode();
      } else {
        var v5 = this.pou.read(1) == 1;
        if (v5) {
          break;
        }
        v3 += this.decode_int();
      }
      if (this.pou.error_flag) {
        break;
      }
    }
    return v2;
  }

  public function encode_string(s: String): Void {
    var v3 = true;
    var v4 = true;
    for (v5 in 0...s.length) {
      if (s.charCodeAt(v5) > 127) {
        v3 = false;
        v4 = false;
        break;
      }
      if (v3) {
        var v6 = s.charAt(v5);
        if (BitCodec.eif(v6) == null) {
          v3 = false;
        }
      }
    }
    this.encode_int(s.length);
    if (v3) {
      this.pou.write(1, 0);
      for (v5 in 0...s.length) {
        this.pou.write(6, BitCodec.eif(s.charAt(v5)));
      }
    } else {
      this.pou.write(2, !v4 ? 3 : 2);
      for (v5 in 0...s.length) {
        this.pou.write(!v4 ? 8 : 7, s.charCodeAt(v5));
      }
    }
  }

  public function decode_string(): String {
    var v2 = this.decode_int();
    var v3 = this.pou.read(1) == 0;
    var v4 = "";
    if (v3) {
      for (v5 in 0...v2) {
        v4 += BitCodec.dif(this.pou.read(6));
      }
      return v4;
    }
    var v6 = this.pou.read(1) == 0;
    for (v5 in 0...v2) {
      v4 += BitCodec.chr(this.pou.read(!v6 ? 8 : 7));
    }
    return v4;
  }

  public function encode_object(o: Dynamic): Void {
    HfStd.forin(o, function (k) {
      this.encode_object_field(k, Reflect.field(o, k));
    });
    this.pou.write(2, 3);
  }

  public function encode_object_field(k: String, d: Dynamic): Void {
    if (HfStd._typeof(d) != "function" && d != null) {
      if (this.obfu_mode && k.charAt(0) == "$") {
        k = k.substring(1);
      }
      if (this.fields.get(k) != null) {
        this.pou.write(1, 0);
        this.pou.write(this.nfields_bits, this.fields.get(k));
      } else {
        this.fields.set(k, this.nfields++);
        if (this.nfields >= this.next_field_bits) {
          ++this.nfields_bits;
          this.next_field_bits *= 2;
        }
        this.pou.write(2, 2);
        this.encode_string(k);
      }
      this.do_encode(d);
    }
  }

  public function decode_object_fast(): Dynamic {
    var v3;
    var v2 = {};
    while (true) {
      var v4 = this.pou.read(1) == 0;
      if (v4) {
        v3 = this.fieldtbl[this.pou.read(this.nfields_bits)];
      } else {
        var v5 = this.pou.read(1) == 1;
        if (v5) {
          break;
        }
        v3 = this.decode_string();
        if (this.obfu_mode && v3.charAt(0) != "$") {
          v3 = "$" + v3;
        }
        this.fieldtbl[this.nfields++] = v3;
        if (this.nfields >= this.next_field_bits) {
          ++this.nfields_bits;
          this.next_field_bits *= 2;
        }
      }
      Reflect.setField(v2, v3, this.do_decode());
      if (this.pou.error_flag) {
        break;
      }
    }
    return v2;
  }

  public function decode_object(): Dynamic {
    var v2 = {};
    this.cache.unshift(v2);
    return v2;
  }

  public function decode_object_field(o: Dynamic): Bool {
    var v3: Dynamic;
    var v4 = this.pou.read(1) == 0;
    if (v4) {
      v3 = this.fieldtbl[this.pou.read(this.nfields_bits)];
      o[v3] = this.do_decode();
      return true;
    }
    var v5 = this.pou.read(1) == 1;
    if (v5) {
      return false;
    }
    v3 = this.decode_string();
    if (this.obfu_mode && v3.charAt(0) != "$") {
      v3 = "$" + v3;
    }
    this.fieldtbl[this.nfields++] = v3;
    if (this.nfields >= this.next_field_bits) {
      ++this.nfields_bits;
      this.next_field_bits *= 2;
    }
    o[v3] = this.do_decode();
    return true;
  }

  public function encode_int(o: Int): Void {
    if (o < 0) {
      this.pou.write(3, 7);
      this.encode_int(-o);
    } else {
      if (o < 4) {
        this.pou.write(2, 0);
        this.pou.write(2, o);
      } else {
        if (o < 16) {
          this.pou.write(2, 1);
          this.pou.write(4, o);
        } else {
          if (o < 64) {
            this.pou.write(2, 2);
            this.pou.write(6, o);
          } else {
            if (o < 65536) {
              this.pou.write(4, 12);
              this.pou.write(16, o);
            } else {
              this.pou.write(4, 13);
              this.pou.write(16, o & 65535);
              this.pou.write(16, o >> 16 & 65535);
            }
          }
        }
      }
    }
  }

  public function decode_int(): Int {
    var v2 = this.pou.read(2);
    if (v2 == 3) {
      var v3 = this.pou.read(1) == 1;
      if (v3) {
        return -this.decode_int();
      }
      var v4 = this.pou.read(1) == 1;
      if (v4) {
        var v5 = this.pou.read(16);
        var v6 = this.pou.read(16);
        return v5 | v6 << 16;
      }
      return this.pou.read(16);
    }
    var v7 = this.pou.read((v2 + 1) * 2);
    return v7;
  }

  public function encode_float(o: Float): Void {
    var v3 = '' + (o);
    var v4 = v3.length;
    this.pou.write(5, v4);
    for (v5 in 0...v4) {
      var v6 = v3.charCodeAt(v5);
      if (v6 >= 48 && v6 <= 58) {
        this.pou.write(4, v6 - 48);
      } else {
        if (v6 == 46) {
          this.pou.write(4, 10);
        } else {
          if (v6 == 43) {
            this.pou.write(4, 11);
          } else {
            if (v6 == 45) {
              this.pou.write(4, 12);
            } else {
              this.pou.write(4, 13);
            }
          }
        }
      }
    }
  }

  public function decode_float(): Float {
    var v2 = this.pou.read(5);
    var v4 = "";
    for (v3 in 0...v2) {
      var v5 = this.pou.read(4);
      if (v5 < 10) {
        v5 += 48;
      } else {
        switch (v5) {
          case 10:
            v5 = 46;
          case 11:
            v5 = 43;
          case 12:
            v5 = 45;
          default:
            v5 = 101;
        }
      }
      v4 += String.fromCharCode(v5);
    }
    return (HfStd.getGlobal("parseFloat"))(v4);
  }

  public function do_encode(o: Dynamic): Bool {
    if (o == null) {
      this.pou.write(4, 15);
      return true;
    }
    if (HfStd._instanceof(o, Array)) {
      this.pou.write(3, 4);
      this.encode_array(o);
      return true;
    }
    switch (HfStd._typeof(o)) {
      case "string":
        this.pou.write(4, 14);
        this.encode_string(o);
        return true;
      case "number":
        var v3 = o;
        if (HfStd.isNaN(v3)) {
          this.pou.write(4, 6);
        } else {
          if (v3 == HfStd.infinity) {
            this.pou.write(5, 14);
          } else {
            if (v3 == -HfStd.infinity) {
              this.pou.write(5, 15);
            } else {
              if (Std.int(v3) == v3) {
                this.pou.write(2, 0);
                this.encode_int(cast v3);
              } else {
                this.pou.write(3, 2);
                this.encode_float(v3);
              }
            }
          }
        }
        return true;
      case "boolean":
        if (o == true) {
          this.pou.write(4, 13);
        } else {
          this.pou.write(4, 12);
        }
        return true;
    }
    this.pou.write(3, 5);
    this.encode_object(o);
    return true;
  }

  public function do_decode(): Dynamic {
    var v2 = this.pou.read(1) == 0;
    if (v2) {
      var v3 = this.pou.read(1) == 1;
      if (v3) {
        var v4 = this.pou.read(1) == 1;
        if (v4) {
          var v5 = this.pou.read(1) == 1;
          if (v5) {
            var v6 = this.pou.read(1) == 1;
            if (v6) {
              return -HfStd.infinity;
            }
            return HfStd.infinity;
          }
          return 0 * null;
        }
        return this.decode_float();
      }
      return this.decode_int();
    }
    var v7 = this.pou.read(1) == 0;
    if (v7) {
      var v8 = this.pou.read(1) == 1;
      if (v8) {
        return !this.fast ? this.decode_object() : this.decode_object_fast();
      }
      return !this.fast ? this.decode_array() : this.decode_array_fast();
    }
    var v9 = this.pou.read(2);
    if (v9 == 0) {
      return false;
    }
    if (v9 == 1) {
      return true;
    }
    if (v9 == 2) {
      return this.decode_string();
    }
    return null;
  }

  public function encodeInit(o: Dynamic): Void {
    this.fast = false;
    this.pou = new BitCodec();
    this.fields = new Hash();
    this.nfields = 0;
    this.next_field_bits = 1;
    this.nfields_bits = 0;
    this.cache = new Array();
    this.cache.push(o);
  }

  public function encodeLoop(): Bool {
    if (this.cache.length == 0) {
      return true;
    }
    this.do_encode(this.cache.shift());
    return false;
  }

  public function encodeEnd(): String {
    var v2 = this.pou.toString();
    if (this.crc) {
      v2 += this.pou.crcStr();
    }
    return v2;
  }

  public function encode(o: Dynamic): String {
    this.encodeInit(o);
    this.fast = true;
    while (this.encodeLoop()) {
    }
    return this.encodeEnd();
  }

  public function progress(): Float {
    return this.pou.in_pos * 100 / this.pou.data.length;
  }

  public function decodeInit(data: String): Void {
    this.fast = false;
    this.pou = new BitCodec();
    this.pou.setData(data);
    this.fieldtbl = new Array();
    this.nfields = 0;
    this.next_field_bits = 1;
    this.nfields_bits = 0;
    this.cache = new Array();
    this.result = null;
  }

  public function decodeLoop(): Bool {
    if (this.cache.length == 0) {
      this.result = this.do_decode();
    } else {
      var v2 = this.cache[0];
      if (HfStd._instanceof(v2, Array)) {
        if (!this.decode_array_item(v2)) {
          this.cache.shift();
        }
      } else {
        if (!this.decode_object_field(v2)) {
          HfStd.deleteField(v2, "pos");
          this.cache.shift();
        }
      }
    }
    if (this.pou.error_flag) {
      this.result = null;
      return false;
    }
    return this.cache.length != 0;
  }

  public function decodeEnd(): Dynamic {
    if (this.crc) {
      var v2 = this.pou.crcStr();
      var v3 = this.pou.data.substr(this.pou.in_pos, 4);
      if (v2 != v3) {
        return null;
      }
    }
    return this.result;
  }

  public function decode(data: String): Dynamic {
    this.decodeInit(data);
    while (this.decodeLoop()) {
    }
    return this.decodeEnd();
  }
}
