package hf.entity;

import hf.entity.Player;
import hf.mode.GameMode;
import etwin.flash.Key;

class PlayerController {
  public var lastKeys: Array<Int>;
  public var keyLocks: Array<Bool>;
  public var alts: Array<Int>;
  public var player: Player;
  public var game: GameMode;
  public var fl_upKick: Bool;
  public var fl_powerControl: Bool;
  public var walkTimer: Float;
  public var waterJump: Int;
  public var attack: Int;
  public var jump: Int;
  public var down: Int;
  public var left: Int;
  public var right: Int;

  public function new(p: Player): Void {
    this.lastKeys = new Array();
    this.keyLocks = new Array();
    this.alts = new Array();
    this.player = p;
    this.game = this.player.game;
    this.fl_upKick = GameManager.CONFIG.hasFamily(101);
    this.fl_powerControl = false;
    this.setKeys(Key.UP, Key.DOWN, Key.LEFT, Key.RIGHT, Key.SPACE);
    this.setAlt(this.attack, Key.CONTROL);
    this.walkTimer = 0;
    this.waterJump = 0;
  }

  public function setKeys(j: Int, d: Int, l: Int, r: Int, a: Int): Void {
    this.jump = j;
    this.down = d;
    this.left = l;
    this.right = r;
    this.attack = a;
  }

  public function setAlt(id: Int, idAlt: Int): Void {
    this.alts[id] = idAlt;
  }

  public function keyIsDown(id: Int): Bool {
    var v3 = Key.isDown(id) && !this.keyLocks[id] || Key.isDown(this.alts[id]) && !this.keyLocks[this.alts[id]];
    if (v3) {
      this.lockKey(id);
    }
    return v3;
  }

  public function lockKey(id: Int): Void {
    this.keyLocks[id] = true;
    this.keyLocks[this.alts[id]] = true;
    this.lastKeys.push(id);
    this.lastKeys.push(this.alts[id]);
  }

  public function getControls(): Void {
    var v4;
    var v3;
    var v2 = 0;
    while (v2 < this.lastKeys.length) {
      if (!Key.isDown(this.lastKeys[v2])) {
        this.keyLocks[this.lastKeys[v2]] = false;
        this.lastKeys.splice(v2, 1);
        --v2;
      }
      ++v2;
    }
    if (this.player.fl_stable) {
      this.waterJump = 3;
    }
    if (Key.isDown(this.left)) {
      if (this.game.fl_ice || this.game.fl_aqua) {
        if (!this.player.fl_stable && this.game.fl_ice) {
          v3 = 0.35;
        } else {
          v3 = 0.1;
        }
        this.player.dx -= v3 * Data.PLAYER_SPEED * this.player.speedFactor;
        this.player.dx = Math.max(this.player.dx, -Data.PLAYER_SPEED * this.player.speedFactor);
      } else {
        this.player.dx = -Data.PLAYER_SPEED * this.player.speedFactor;
      }
      this.player.dir = -1;
      if (this.player.fl_stable) {
        this.player.playAnim(this.player.baseWalkAnim);
      }
    }
    if (Key.isDown(this.right)) {
      if (this.game.fl_ice || this.game.fl_aqua) {
        if (!this.player.fl_stable && this.game.fl_ice) {
          v4 = 0.35;
        } else {
          v4 = 0.1;
        }
        this.player.dx += v4 * Data.PLAYER_SPEED * this.player.speedFactor;
        this.player.dx = Math.min(this.player.dx, Data.PLAYER_SPEED * this.player.speedFactor);
      } else {
        this.player.dx = Data.PLAYER_SPEED * this.player.speedFactor;
      }
      this.player.dir = 1;
      if (this.player.fl_stable) {
        this.player.playAnim(this.player.baseWalkAnim);
      }
    }
    if (this.player.specialMan.actives[73]) {
      if (this.player.fl_stable && this.player.dx != 0) {
        this.walkTimer -= Timer.tmod;
        if (this.walkTimer <= 0) {
          this.walkTimer = Data.SECOND;
          this.player.getScore(this.player, 10);
        }
      }
    }
    if (!Key.isDown(this.left) && !Key.isDown(this.right)) {
      if (!this.game.fl_ice) {
        this.player.dx *= this.game.gFriction * 0.8;
      }
      if (this.player.animId == this.player.baseWalkAnim.id || this.player.animId == Data.ANIM_PLAYER_RUN.id) {
        this.player.playAnim(this.player.baseStopAnim);
      }
    }
    if (this.game.fl_aqua && this.waterJump > 0) {
      if (!this.player.fl_stable && this.keyIsDown(this.jump)) {
        this.player.airJump();
        --this.waterJump;
      }
    }
    if (this.player.fl_stable && Key.isDown(this.jump)) {
      if (this.player.specialMan.actives[88]) {
        this.player.dy = -Data.PLAYER_JUMP * 0.5;
      } else {
        this.player.dy = -Data.PLAYER_JUMP;
      }
      this.game.soundMan.playSound("sound_jump", Data.CHAN_PLAYER);
      this.player.playAnim(Data.ANIM_PLAYER_JUMP_UP);
      var v5 = this.game.fxMan.attachFx(this.player.x, this.player.y, "hammer_fx_jump");
      v5.mc._alpha = 50;
      if (this.player.specialMan.actives[66]) {
        this.player.getScore(this.player, 10);
      }
      this.game.statsMan.inc(Data.STAT_JUMP, 1);
      this.lockKey(this.jump);
    }
    if (this.keyIsDown(this.attack) && this.player.coolDown == 0) {
      var v6 = Data.KICK_DISTANCE;
      if (!this.player.fl_stable) {
        v6 = Data.AIR_KICK_DISTANCE;
      }
      if (this.player.specialMan.actives[115]) {
        v6 *= 1.2;
      }
      var v7 = this.game.getClose(Data.BOMB, this.player.x, this.player.y, v6, false);
      if (v7.length == 0) {
        if (this.player.currentWeapon > 0 && this.player.countBombs() < this.player.maxBombs) {
          var v8 = this.player.attack();
          if (this.game.fl_bombControl && v8.isType(Data.BOMB)) {
            var v9: hf.entity.bomb.PlayerBomb = cast v8;
            var v10 = hf.entity.WalkingBomb.attach(this.game, v9);
          }
          if (v8 != null) {
            v8.setParent(this.player);
          }
          if (this.player.fl_stable) {
            this.player.playAnim(Data.ANIM_PLAYER_ATTACK);
          }
        }
      } else {
        if (this.fl_powerControl && this.player.fl_stable) {
          var v11 = Math.min(1.2, 0.5 + Math.abs(this.player.dx) / 4);
          this.player.kickBomb(v7, v11);
        } else {
          this.player.kickBomb(v7, 1.0);
        }
      }
    } else {
      if (this.keyIsDown(this.down) && this.fl_upKick) {
        var v12 = Data.KICK_DISTANCE;
        if (!this.player.fl_stable) {
          v12 = Data.AIR_KICK_DISTANCE;
        }
        if (this.player.specialMan.actives[115]) {
          v12 *= 1.2;
        }
        var v13 = this.game.getClose(Data.BOMB, this.player.x, this.player.y, v12, false);
        if (v13.length > 0) {
          this.player.upKickBomb(v13);
        }
      }
    }
  }

  public function update(): Void {
    this.getControls();
  }
}
