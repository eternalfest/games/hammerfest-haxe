package hf.entity.bad;

import hf.entity.bad.Jumper;
import hf.mode.GameMode;

class Shooter extends Jumper {
  public var shootCD: Float;
  public var shootPreparation: Float;
  public var shootDuration: Float;
  public var fl_shooter: Bool;
  public var chanceShoot: Float;

  public function new(): Void {
    super();
    this.shootCD = Data.PEACE_COOLDOWN;
    this.disableShooter();
    this.setShoot(null);
  }

  public function initShooter(prepa: Float, duration: Float): Void {
    this.shootPreparation = prepa;
    this.shootDuration = duration;
  }

  public function enableShooter(): Void {
    this.fl_shooter = true;
  }

  public function disableShooter(): Void {
    this.fl_shooter = false;
  }

  public function setShoot(chance: Null<Float>): Void {
    if (chance == null) {
      this.fl_shooter = false;
    } else {
      this.fl_shooter = true;
      this.chanceShoot = chance * 10;
    }
  }

  public function startShoot(): Void {
    this.setNext(this.dx, this.dy, this.shootPreparation, Data.ACTION_SHOOT);
    this.halt();
    this.playAnim(Data.ANIM_BAD_SHOOT_START);
  }

  public function onShoot(): Void {
  }

  override public function init(g: GameMode): Void {
    super.init(g);
  }

  override public function onEndAnim(id: Int): Void {
    super.onEndAnim(id);
    if (id == Data.ANIM_BAD_SHOOT_START.id) {
      this.playAnim(Data.ANIM_BAD_SHOOT_LOOP);
    }
  }

  override public function onNext(): Void {
    super.onNext();
    if (this.next.action == Data.ACTION_SHOOT) {
      this.setNext(null, null, this.shootDuration, Data.ACTION_WALK);
      this.halt();
      this.playAnim(Data.ANIM_BAD_SHOOT_END);
      this.onShoot();
    }
  }

  override public function update(): Void {
    if (this.shootCD > 0) {
      this.shootCD -= Timer.tmod;
    }
    if (this.fl_shooter && this.shootCD <= 0) {
      if (this.isReady() && HfStd.random(1000) < this.chanceShoot) {
        this.startShoot();
      }
    }
    super.update();
  }
}
