package hf.mode;

import hf.GameManager;
import hf.mode.Adventure;

class Shareware extends Adventure {
  public function new(m: GameManager, id: Int): Void {
    super(m, id);
    this._name = "$shareware";
  }

  public function darknessManager(): Void {
  }

  override public function endMode(): Void {
    this.lock();
    var v2 = HfStd.getVar(HfStd.getRoot(), "$out");
    this.manager.redirect(v2, null);
  }

  override public function initGame(): Void {
    super.initGame();
    var v3 = this.getOne(Data.PLAYER);
    v3.lives = 2;
    this.gi.setLives(v3.pid, v3.lives);
  }

  override public function initWorld(): Void {
    this.world = new hf.levels.GameMechanics(this.manager, "xml_shareware");
    this.world.setDepthMan(this.depthMan);
    this.world.setGame(this);
  }

  override public function onEndOfSet(): Void {
    this.endMode();
  }

  override public function onGameOver(): Void {
    this.endMode();
  }

  override public function onLevelReady(): Void {
    super.onLevelReady();
    if (this.world.currentId == 0) {
      this.fxMan.attachLevelPop(Lang.get(17), true);
    }
  }

  override public function onPause(): Void {
    super.onPause();
    this.pauseMC.sector.text = Lang.get(14) + " «" + Lang.get(17) + "»";
  }
}
