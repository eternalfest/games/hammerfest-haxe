package hf.entity.shoot;

import hf.entity.Shoot;
import hf.Entity;
import hf.mode.GameMode;

class PlayerPearl extends Shoot {
  public var shotList: Array<Int>;
  public var fl_bounceBorders: Bool;

  public function new(): Void {
    super();
    this.shootSpeed = 8.5;
    this.coolDown = Data.SECOND * 2;
    this.shotList = new Array();
    this._yOffset = -16;
    this.fl_bounceBorders = false;
  }

  public static function attach(g: GameMode, x: Float, y: Float): PlayerPearl {
    var v5 = "hammer_shoot_player_pearl";
    var v6: PlayerPearl = cast g.depthMan.attach(v5, Data.DP_SHOTS);
    v6.initShoot(g, x, y);
    return v6;
  }

  public function hasBeenShot(id: Int): Bool {
    for (v3 in 0...this.shotList.length) {
      if (this.shotList[v3] == id) {
        return true;
      }
    }
    return false;
  }

  public function hitWallAnim(): Void {
    this.game.fxMan.inGameParticles(Data.PARTICLE_ICE, this.x, this.y, 2);
    this.game.fxMan.inGameParticles(Data.PARTICLE_STONE, this.x, this.y, 3);
    if (this.dx < 0) {
      this.game.fxMan.attachFx(this.x + Data.CASE_WIDTH * 0.5, this.y + this._yOffset, "hammer_fx_icePouf");
    } else {
      this.game.fxMan.attachFx(this.x - Data.CASE_WIDTH * 0.5, this.y + this._yOffset, "hammer_fx_icePouf");
    }
  }

  override public function destroy(): Void {
    if (!this.fl_bounceBorders) {
      this.hitWallAnim();
    }
    super.destroy();
  }

  override public function endUpdate(): Void {
    if (this.dy != 0) {
      this.rotation = Math.atan2(this.dy, this.dx) * 180 / Math.PI;
    }
    super.endUpdate();
  }

  override public function hit(e: Entity): Void {
    var v3 = Data.BAD.downcast(e);
    if (v3 != null) {
      if (!this.hasBeenShot(v3.uniqId)) {
        v3.setCombo(this.uniqId);
        v3.freeze(Data.FREEZE_DURATION);
        v3.dx = this.dx * 2;
        v3.dy -= 3;
        this.shotList.push(v3.uniqId);
      }
    }
  }

  override public function init(g: GameMode): Void {
    super.init(g);
    this.register(Data.PLAYER_SHOOT);
  }

  override public function onLifeTimer(): Void {
    this.game.fxMan.attachFx(this.x, this.y, "hammer_fx_pop");
    super.onLifeTimer();
  }

  override public function onSideBorderBounce(): Void {
    super.onSideBorderBounce();
    this.hitWallAnim();
  }
}
