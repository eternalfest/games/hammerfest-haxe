package hf.gui;

import hf.gui.Item;

class Field extends Item {
  public var bg: Dynamic /*TODO*/;

  public function new(): Void {
    super();
    this.field.text = "";
  }

  public function initField(c: Container): Void {
    this.setWidth(50);
    this.scale(1);
    this.init(c, this.getField());
  }

  public function setWidth(w: Float): Void {
    this.field._width = w;
    this.bg._width = w + 5;
    this.width = w + 10;
  }

  public function setField(s: String): Void {
    this.field.text = s;
  }

  public function getField(): String {
    return this.field.text;
  }

  public static function attach(c: Container): Field {
    var v3: Field = cast c.depthMan.attach("hammer_editor_field", Data.DP_INTERF);
    v3.initField(c);
    return v3;
  }

  override public function setLabel(s: String): Void {
    this.setField(s);
  }
}
