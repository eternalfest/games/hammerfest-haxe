package hf.mode;

import etwin.flash.TextField;
import etwin.flash.XMLNode;
import hf.Mode;
import etwin.flash.Key;
import etwin.flash.XML;
using hf.compat.XMLNodeTools;

class QuestBrowser extends Mode {
  public var mcList: Array<Dynamic>;
  public var footer: Dynamic;
  public var qfilter: Int;
  public var fullRandList: Array<Dynamic>;
  public var quests: XMLNode;
  public var desc: TextField;
  public var klock: Dynamic /*TODO*/;

  public static var ITEM_WIDTH: Float = 40;
  public static var ITEM_HEIGHT: Float = 25;

  public function new(m: Dynamic /*TODO*/): Void {
    super(m);
    this._name = "$questBrowser";
    this.mcList = new Array();
    this.footer = this.depthMan.attach("hammer_editor_footer", Data.DP_TOP);
    this.footer._x = Data.DOC_WIDTH * 0.5;
    this.footer._y = Data.DOC_HEIGHT - 7;
    this.qfilter = 0;
    this.fullRandList = new Array();
    for (v4 in 0...Data.SCORE_ITEM_FAMILIES.length) {
      var v5 = Data.SCORE_ITEM_FAMILIES[v4];
      for (v6 in 0...v5.length) {
        this.fullRandList[v5[v6].id] = v5[v6].r;
      }
    }
    for (v7 in 0...Data.SPECIAL_ITEM_FAMILIES.length) {
      var v8 = Data.SPECIAL_ITEM_FAMILIES[v7];
      for (v9 in 0...v8.length) {
        this.fullRandList[v8[v9].id] = v8[v9].r;
      }
    }
    var v10 = HfStd.getVar(this.manager.root, "xml_quests");
    this.quests = (new XML(v10)).firstChild;
    this.desc = HfStd.createTextField(this.mc, this.manager.uniq++);
    this.desc._x = 0;
    this.desc._y = 415;
    this.desc._width = Data.DOC_WIDTH;
    this.desc._height = 100;
    this.desc.textColor = 7829452;
    this.desc.wordWrap = true;
    this.desc.html = true;
    this.desc.selectable = false;
    this.refresh();
  }

  public function getRandColor(proba: Int): Int {
    var v3 = 5592405;
    switch (proba) {
      case Data.__NA:
        v3 = 11184810;
        return v3;
      case Data.COMM:
        v3 = 65280;
        return v3;
      case Data.UNCO:
        v3 = 16776960;
        return v3;
      case Data.RARE:
        v3 = 16750848;
        return v3;
      case Data.UNIQ:
        v3 = 16711680;
        return v3;
      case Data.MYTH:
        v3 = 4474111;
        return v3;
      case Data.CANE:
        v3 = 3534050;
        return v3;
      case Data.LEGEND:
        v3 = 16729343;
    }
    return v3;
  }

  public function refresh(): Void {
    for (v2 in 0...this.mcList.length) {
      this.mcList[v2].removeMovieClip();
    }
    this.mcList = new Array();
    var v3 = this.getQuest(this.qfilter);
    var v4 = v3.firstChild;
    var v5 = 0;
    while (v4 != null) {
      var v6 = null;
      switch ("$" + v4.nodeName) {
        case "$require":
          var v7 = HfStd.parseInt(v4.get("$item".substring(1)), 10);
          var v8 = HfStd.parseInt(v4.get("$qty".substring(1)), 10);
          var v9 = v7 < 1000 ? "hammer_item_special" : "hammer_item_score";
          v6 = this.depthMan.attach(v9, Data.DP_ITEMS);
          v6.blendMode = BlendMode.LAYER;
          v6._x = 20;
          v6._y = 30 + v5 * hf.mode.QuestBrowser.ITEM_HEIGHT;
          v6._xscale = 75;
          v6._yscale = v6._xscale;
          if (v7 >= 1000) {
            v6.gotoAndStop("" + (v7 - 1000 + 1));
          } else {
            v6.gotoAndStop("" + (v7 + 1));
          }
          var v10 = HfStd.createTextField(v6, this.manager.uniq++);
          v10._x = hf.mode.QuestBrowser.ITEM_WIDTH;
          v10._y = -30;
          v10._width = 350;
          v10._height = hf.mode.QuestBrowser.ITEM_HEIGHT;
          v10.textColor = this.getRandColor(this.fullRandList[v7]);
          v10.text = "\tx " + v8 + " \t\t\t" + v7 + " (" + Lang.getItemName(v7) + ")";
          v10.wordWrap = false;
          v10.selectable = false;
          v10._xscale = 250 - v6._xscale;
          v10._yscale = v10._xscale;
        case "$give":
        case "$remove":
          v6 = this.depthMan.empty(Data.DP_INTERF);
          v6._x = 10;
          v6._y = 30 + v5 * hf.mode.QuestBrowser.ITEM_HEIGHT;
          var v11 = HfStd.createTextField(v6, this.manager.uniq++);
          v11._x = 0;
          v11._y = -20;
          v11._width = 380;
          if (v4.nodeName == "$remove".substring(1)) {
            v11.textColor = 16737894;
          } else {
            v11.textColor = 16776960;
          }
          var v12 = HfStd.parseInt(v4.get("$family".substring(1)), 10);
          if (!HfStd.isNaN(v12)) {
            var v13 = Lang.getFamilyName(v12);
            if (v13 == null) {
              v13 = "SYSTEM FAMILY";
            }
            v11.text = v4.nodeName.toUpperCase() + " " + v12 + " ('" + v13 + "')";
          } else {
            var v14 = v4.get("$option".substring(1));
            if (v14 != null) {
              v11.text = "option [" + v14 + "]";
              v11.textColor = 16777215;
            }
            var v15 = HfStd.parseInt(v4.get("$bank".substring(1)), 10);
            if (!HfStd.isNaN(v15)) {
              v11.text = "bank upgrade (+" + v15 + ")";
              v11.textColor = 16777215;
            }
            var v16 = HfStd.parseInt(v4.get("$tokens".substring(1)), 10);
            if (!HfStd.isNaN(v16)) {
              v11.text = v4.nodeName.toUpperCase() + " Tokens +" + v16;
              v11.textColor = 65535;
            }
            var v17 = v4.get("$mode".substring(1));
            if (v17 == null) break;
            v11.text = "mode [" + v17 + "]";
            v11.textColor = 16711935;
          }
      }
      v4 = v4.nextSibling;
      ++v5;
      this.mcList.push(v6);
    }
    var v18 = HfStd.parseInt(v3.get("$id".substring(1)), 10);
    this.desc.htmlText = "<p align=\"right\">" + Lang.getQuestDesc(v18) + "</p>";
    this.footer.field.text = "Quest #" + v18 + ", " + Lang.getQuestName(v18);
  }

  public function getQuest(id: Int): Null<XMLNode> {
    var v3 = null;
    var v4 = this.quests.firstChild;
    while (true) {
      if (!(v4 != null && v3 == null)) break;
      if (v4.get("$id".substring(1)) == id + "") {
        v3 = v4;
      }
      v4 = v4.nextSibling;
    }
    return v3;
  }

  override public function endMode(): Void {
    if (this.fl_runAsChild) {
      this.manager.stopChild(null);
    }
  }

  override public function destroy(): Void {
    this.desc.removeTextField();
    super.destroy();
  }

  override public function main(): Void {
    super.main();
    if (this.klock != null && !Key.isDown(this.klock)) {
      this.klock = null;
    }
    if (Key.isDown(Key.LEFT) && this.qfilter > 0 && this.klock != Key.LEFT) {
      --this.qfilter;
      this.refresh();
      this.klock = Key.LEFT;
    }
    if (Key.isDown(Key.RIGHT) && this.qfilter < 999 && this.klock != Key.RIGHT) {
      ++this.qfilter;
      this.refresh();
      this.klock = Key.RIGHT;
    }
    if (Key.isDown(Key.PGUP) && this.qfilter > 0 && this.klock != Key.PGUP) {
      this.qfilter -= 10;
      if (this.qfilter < 0) {
        this.qfilter = 0;
      }
      this.refresh();
      this.klock = Key.PGUP;
    }
    if (Key.isDown(Key.PGDN) && this.qfilter < 999 && this.klock != Key.PGDN) {
      this.qfilter += 10;
      this.refresh();
      this.klock = Key.PGDN;
    }
    if (Key.isDown(Key.ESCAPE)) {
      this.endMode();
    }
  }
}
