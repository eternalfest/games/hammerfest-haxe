package hf.entity.supa;

import hf.entity.Supa;
import hf.mode.GameMode;

class Ball extends Supa {
  public function new(): Void {
    super();
    this.fl_gravity = true;
  }

  public static function attach(g: GameMode): Ball {
    var v3 = "hammer_supa_ball";
    var v4: Ball = cast g.depthMan.attach(v3, Data.DP_SUPA);
    v4.initSupa(g, Data.GAME_WIDTH, 0);
    return v4;
  }

  override public function initSupa(g: GameMode, x: Float, y: Float): Void {
    super.initSupa(g, x, y);
    this.speed = 2;
    this.radius = 40;
    this.fallFactor = 0.8;
    this.gravityFactor = 0.8;
    this.dx = this.speed;
    this.moveTo(0, -50);
    this.scale(220);
  }

  override public function postfix(): Void {
    super.postfix();
    if (this.dy > 0 && this.y >= Data.GAME_HEIGHT) {
      this.dy = -Math.abs(this.dy);
      this.game.shake(Data.SECOND * 0.5, 3);
    }
    if (this.x >= Data.GAME_WIDTH + 50) {
      this.destroy();
    }
  }

  override public function prefix(): Void {
    super.prefix();
    var v3 = this.game.getClose(Data.BAD, this.x, this.y, this.radius, false);
    for (v4 in 0...v3.length) {
      var v5 = v3[v4];
      if (!v5.fl_knock) {
        v5.knock(Data.KNOCK_DURATION * 3);
        v5.dx = this.dx * 2;
        v5.dy = -5;
      }
    }
  }
}
