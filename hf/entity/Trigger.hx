package hf.entity;

import hf.Entity;

class Trigger extends Entity {
  public var fl_largeTrigger: Bool;

  public function new(): Void {
    super();
    this.fl_largeTrigger = false;
  }

  public function tAddSingle(cx: Int, cy: Int): Void {
    if (cx < 0 || cx >= Data.LEVEL_WIDTH || cy < 0 || cy >= Data.LEVEL_HEIGHT) {
      return;
    }
    this.world.triggers[cx][cy].push(this);
  }

  public function tRemSingle(cx: Int, cy: Int): Void {
    if (cx < 0 || cx >= Data.LEVEL_WIDTH || cy < 0 || cy >= Data.LEVEL_HEIGHT) {
      return;
    }
    var v4 = this.world.triggers[cx][cy];
    var v5 = 0;
    while (v5 < v4.length) {
      if (v4[v5] == this) {
        v4.splice(v5, 1);
        --v5;
      }
      ++v5;
    }
  }

  public function tAdd(cx: Int, cy: Int): Void {
    if (cx == null || cy == null) {
      return;
    }
    this.tAddSingle(cx, cy);
    this.tAddSingle(cx - 1, cy);
    this.tAddSingle(cx + 1, cy);
    this.tAddSingle(cx, cy - 1);
    if (this.fl_largeTrigger) {
      this.tAddSingle(cx, cy + 1);
    }
  }

  public function tRem(cx: Int, cy: Int): Void {
    if (cx == null || cy == null) {
      return;
    }
    this.tRemSingle(cx, cy);
    this.tRemSingle(cx - 1, cy);
    this.tRemSingle(cx + 1, cy);
    this.tRemSingle(cx, cy - 1);
    if (this.fl_largeTrigger) {
      this.tRemSingle(cx, cy + 1);
    }
  }

  public function moveToCase(cx: Float, cy: Float): Void {
    this.moveTo(Entity.x_ctr(cx), Entity.y_ctr(cy));
  }

  public function getByType<E: Entity>(type: EntityType<E>): Array<E> {
    var v3 = this.world.triggers[this.cx][this.cy];
    var v4 = new Array();
    for (v5 in 0...v3.length) {
      var e = type.downcast(v3[v5]);
      if (e != null) {
        v4.push(e);
      }
    }
    return v4;
  }

  override public function moveTo(x: Float, y: Float): Void {
    this.tRem(this.cx, this.cy);
    this.x = x;
    this.y = y - 1;
    this.updateCoords();
    this.tAdd(this.cx, this.cy);
  }
}
