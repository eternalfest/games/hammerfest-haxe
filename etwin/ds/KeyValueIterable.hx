package etwin.ds;

/**
 * A `KeyValueIterable` is a data structure which has a `keyValueIterator()`
 * method to iterate over key-value-pairs.
 */
typedef KeyValueIterable<K, V> = {
  function keyValueIterator(): KeyValueIterator<K, V>;
}
