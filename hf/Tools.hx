package hf;

import etwin.flash.MovieClip;
import hf.Delta;
import hf.Pos;
using hf.compat.StringTools;
using hf.compat.ArrayTools;

class Tools {
  /**
   * The raw string `"x"`.
   *
   * Initial value: `Obfu.raw("x")`
   */
  public static var __x: String = "$x".substring(1);

  /**
   * The raw string `"y"`.
   *
   * Initial value: `Obfu.raw("y")`
   */
  public static var __y: String = "$y".substring(1);

  /**
   * List of raw strings used internally by `Tools`.
   *
   * It is declared with a `null` value and must be initialized by calling
   * `Tools.initStrings`.
   */
  public static var strings: Null<Array<String>> = null;

  /**
   * Schedules a timeout to call `f` after `time` milliseconds.
   */
  public static function delay(f: Void -> Void, time: Float): Void {
    var id = null;
    id = (HfStd.getGlobal("setInterval"))(function () {
      (HfStd.getGlobal("clearInterval"))(id);
      f();
    }, time);
  }

  /**
   * Finds all the occurences of `search` in `str` and replaces them with `replace`.
   *
   * This function first finds all the occurences to replace, and only then applies the
   * replacements.
   * If `str` is `null`, returns `null`.
   * `search` MUST be non-empty.
   */
  public static function replace(str: Null<String>, search: String, replace: String): Null<String> {
    if (str == null) {
      return null;
    }
    var v5 = search.length;
    if (v5 == 1) {
      return (str.split(search)).join(replace);
    }
    var v7 = 0;
    var v8 = "";
    while (true) {
      var v6 = str.indexOf(search, v7);
      if (v6 == -1) {
        break;
      }
      v8 += str.slice(v7, v6) + replace;
      v7 = v6 + v5;
    }
    return v8 + str.substring(v7);
  }

  /**
   * Splits `str` into an array of lines.
   *
   * The lines are split at the end of lines: `\r?\n`.
   * The strings in the result array do not include the end of line.
   * The result array always has at least one line.
   *
   * Example:
   * ```
   * Tools.lines(""); // [""]
   * Tools.lines("a\nb"); // ["a", "b"]
   * Tools.lines("a\r\nb"); // ["a", "b"]
   * Tools.lines("a\n\nb"); // ["a", "", "b"]
   * Tools.lines("a\nb\n"); // ["a", "b", ""]
   * ```
   */
  public static function lines(str: String): Array<String> {
    var v3;
    var v4 = new Array();
    var v5 = 0;
    var v6 = str.length;
    v3 = 0;
    while (v3 < v6) {
      var v7 = str.charCodeAt(v3);
      if (v7 == 10 || v7 == 13) {
        v4.push(str.slice(v5, v3));
        if (v7 == 13 && str.charCodeAt(v3 + 1) == 10) {
          ++v3;
        }
        v5 = v3 + 1;
      }
      ++v3;
    }
    v4.push(str.slice(v5, v3));
    return v4;
  }

  /**
   * Shallow-clones `_a` and shuffles its items.
   */
  public static function shuffle<T>(_a: Array<T>): Array<T> {
    var v4 = _a.duplicate();
    var v5 = v4.length;
    for (v3 in 0...v5) {
      var v6 = HfStd.random(v5);
      var v7 = HfStd.random(v5);
      var v8 = v4[v6];
      v4[v6] = v4[v7];
      v4[v7] = v8;
    }
    return v4;
  }

  public static function rootDelta(mc: MovieClip): Delta {
    var v3 = 0.0;
    var v4 = 0.0;
    while (mc._parent != null) {
      v3 += mc._x;
      v4 += mc._y;
      mc = mc._parent;
    }
    return {dx: v3, dy: v4};
  }

  /**
   * Stringifies `x` with at least `ndigits` digits, padding with zeros if needed.
   *
   * ```
   * Tools.padZeros(3, 0); // "3"
   * Tools.padZeros(3, 1); // "3"
   * Tools.padZeros(3, 2); // "03"
   * Tools.padZeros(15, 1); // "15"
   * Tools.padZeros(15, 3); // "015"
   * Tools.padZeros(15, 6); // "000015"
   * ```
   */
  public static function padZeros(x: Int, ndigits: Int): String {
    var v4 = '' + (x);
    while (v4.length < ndigits) {
      v4 = "0" + v4;
    }
    return v4;
  }

  /**
   * Picks an index of `a` weighted with the corresponding value.
   *
   * Example:
   * ```
   * Tools.randomProbas([20, 0, 10, 10]);
   * // Possible return values:
   * // - `0` (50% chance)
   * // - `1` has a 0% chance so it won't be returned.
   * // - `2` (25% chance)
   * // - `3` (25% chance)
   * ```
   */
  public static function randomProbas(a: Array<Int>): Int {
    var v4;
    var v3 = 0;
    v4 = a.length - 1;
    while (v4 >= 0) {
      v3 += a[v4];
      --v4;
    }
    v3 = HfStd.random(v3);
    v4 = 0;
    while (v3 >= a[v4]) {
      v3 -= a[v4];
      ++v4;
    }
    return v4;
  }

  /**
   * Returns the distance between the centers of the bounds of the provided movie clips.
   *
   * Both movie clips must use the same coordinate space, otherwise the result is unspecified.
   */
  public static function distMC(mc1: MovieClip, mc2: MovieClip): Float {
    var v4 = mc1.getBounds(mc1);
    var v5 = mc2.getBounds(mc1);
    var v6 = (v4.xMin + v4.xMax) / 2 - (v5.xMin + v5.xMax) / 2;
    var v7 = (v4.yMin + v4.yMax) / 2 - (v5.yMin + v5.yMax) / 2;
    return Math.sqrt(v6 * v6 + v7 * v7);
  }

  /**
   * Returns the child movie clips of `mc`.
   */
  public static function subs(mc: MovieClip): Array<MovieClip> {
    var a = new Array();
    HfStd.forin(mc, function (k) {
      var v3: MovieClip = Reflect.field(mc, k);
      if (HfStd._typeof(v3) == "movieclip" && v3._parent == mc) {
        a.push(v3);
      }
    });
    return a;
  }

  public static function localToGlobal(mc: MovieClip, x: Float, y: Float): Pos<Float> {
    var v5 = new Hash();
    v5.set(Tools.__x, x);
    v5.set(Tools.__y, y);
    mc.localToGlobal(cast v5);
    return {x: v5.get(Tools.__x), y: v5.get(Tools.__y)};
  }

  public static function globalToLocal(mc: MovieClip, x: Float, y: Float): Pos<Float> {
    var v5 = new Hash();
    v5.set(Tools.__x, x);
    v5.set(Tools.__y, y);
    mc.localToGlobal(cast v5);
    return {x: v5.get(Tools.__x), y: v5.get(Tools.__y)};
  }

  /**
   * Initializes `Tools.strings` with a list of raw strings.
   *
   * After the initialization, `Tools.strings` has the following value:
   * ```
   * ["linear", "radial", "matrixType", "box", "x", "y", "w", "h", "r"]
   * ```
   */
  public static function initStrings(): Void {
    var v2 = ["$linear", "$radial", "$matrixType", "$box", "$x", "$y", "$w", "$h", "$r"];
    Tools.strings = new Array();
    for (v3 in 0...v2.length) {
      var v4 = v2[v3];
      Tools.strings[v3] = v4.substring(1);
    }
  }

  public static function beginGradientFill(mc: MovieClip, linear: Bool, colors: Dynamic /*TODO*/, alphas: Dynamic /*TODO*/, coefs: Dynamic /*TODO*/, matrix: Dynamic /*TODO*/): Void {
    if (Tools.strings == null) {
      Tools.initStrings();
    }
    var v8 = new Hash();
    v8.set(Tools.strings[2], Tools.strings[3]);
    v8.set(Tools.strings[4], matrix.x);
    v8.set(Tools.strings[5], matrix.y);
    v8.set(Tools.strings[6], matrix.w);
    v8.set(Tools.strings[7], matrix.h);
    v8.set(Tools.strings[8], matrix.r);
    mc.beginGradientFill(!linear ? Tools.strings[1] : Tools.strings[0], colors, alphas, coefs, v8);
  }
}
