package hf.entity;

import hf.Animation;
import hf.Entity;
import hf.Pos;
import hf.entity.Bomb;
import hf.entity.bomb.PlayerBomb;
import hf.entity.Physics;
import hf.entity.PlayerController;
import hf.entity.Animator.Anim;
import hf.mode.GameMode;
import hf.SpecialManager;
import etwin.flash.Key;

typedef KickedBomb = {
  var t: Float;
  var bid: Int;
}

class Player extends Physics {
  public var name: String;
  public var baseWalkAnim: Anim;
  public var baseStopAnim: Anim;
  public var score: Int;
  public var scoreCS: Int;
  public var speedFactor: Float;
  public var extraLifeCurrent: Int;
  public var fl_chourou: Bool;
  public var fl_carot: Bool;
  public var fl_candle: Bool;
  public var defaultHead: Int;
  public var head: Int;
  public var fl_knock: Bool;
  public var knockTimer: Float;
  public var skin: Int;
  public var oxygen: Float;
  public var recentKicks: Array<KickedBomb>;
  public var currentWeapon: Int;
  public var lastBomb: Int;
  public var maxBombs: Int;
  public var initialMaxBombs: Int;
  public var coolDown: Float;
  public var lives: Int;
  public var fl_torch: Bool;
  public var baseColor: Int;
  public var darkColor: Int;
  public var fl_lockControls: Bool;
  public var lockTimer: Float;
  public var fl_entering: Bool;
  public var extendList: Array<Bool>;
  public var extendOrder: Array<Int>;
  public var pid: Null<Int>;
  public var edgeTimer: Float;
  public var waitTimer: Float;
  public var dbg_grid: Int;
  public var debugInput: String;
  public var ctrl: Null<PlayerController>;
  public var specialMan: Null<SpecialManager>;
  public var dbg_lastKey: Int;
  public var fl_shield: Bool;
  public var shieldMC: Animation;
  public var shieldTimer: Float;
  public var startX: Float;
  public var bounceLimit: Int;

  public function new(): Void {
    super();
    this.name = "$Igor".substring(1);
    this.baseWalkAnim = Data.ANIM_PLAYER_WALK;
    this.baseStopAnim = Data.ANIM_PLAYER_STOP;
    this.score = 0;
    this.dir = 1;
    this.speedFactor = 1.0;
    this.fallFactor = 1.1;
    this.extraLifeCurrent = 0;
    this.fl_teleport = true;
    this.fl_portal = true;
    this.fl_wind = true;
    this.fl_strictGravity = false;
    this.fl_bump = true;
    this.fl_chourou = false;
    this.fl_carot = false;
    this.fl_candle = false;
    this.defaultHead = Data.HEAD_NORMAL;
    this.head = this.defaultHead;
    this.fl_knock = false;
    this.knockTimer = 0;
    this.bounceLimit = 2;
    this.skin = 1;
    this.oxygen = 100;
    this.recentKicks = new Array();
    this.currentWeapon = Data.WEAPON_B_CLASSIC;
    this.lastBomb = 1;
    this.initialMaxBombs = 1;
    if (GameManager.CONFIG.hasFamily(100)) {
      ++this.initialMaxBombs;
    }
    this.maxBombs = this.initialMaxBombs;
    this.coolDown = 0;
    this.lives = 1;
    if (GameManager.CONFIG.hasFamily(102)) {
      ++this.lives;
    }
    if (GameManager.CONFIG.hasFamily(103)) {
      ++this.lives;
    }
    if (GameManager.CONFIG.hasFamily(104)) {
      ++this.lives;
    }
    if (GameManager.CONFIG.hasFamily(105)) {
      ++this.lives;
    }
    if (GameManager.CONFIG.hasFamily(108)) {
      ++this.lives;
    }
    if (GameManager.CONFIG.hasFamily(106)) {
      this.fl_candle = true;
    }
    if (GameManager.CONFIG.hasFamily(107)) {
      this.fl_torch = true;
    }
    if (GameManager.CONFIG.hasFamily(108)) {
      this.fl_carot = true;
    }
    this.baseColor = Data.BASE_COLORS[0];
    this.darkColor = Data.DARK_COLORS[0];
    this.fl_lockControls = false;
    this.fl_entering = false;
    this.extendList = new Array();
    this.extendOrder = new Array();
    this.pid = 0;
    this.edgeTimer = 0;
    this.waitTimer = 0;
    this.dbg_grid = 11;
    this.debugInput = "";
  }

  public function initPlayer(g: GameMode, x: Float, y: Float): Void {
    this.init(g);
    this.moveTo(x, y);
    if (this.game.fl_nightmare) {
      this.speedFactor = 1.3;
    }
    if (this.game._name != "$time" && GameManager.CONFIG.hasOption(Data.OPT_BOOST)) {
      this.speedFactor = 1.3;
      if (this.game.fl_nightmare) {
        this.speedFactor = 1.6;
      }
    }
    this.endUpdate();
  }

  public function getDebugControls(): Void {
    if (this.dbg_lastKey > 0 && !Key.isDown(this.dbg_lastKey)) {
      this.dbg_lastKey = 0;
    }
    for (v2 in 0...10) {
      if (Key.isDown(96 + v2) && this.dbg_lastKey != 96 + v2) {
        this.debugInput += '' + (v2);
        this.dbg_lastKey = 96 + v2;
      }
    }
    if (this.debugInput.length >= 3) {
      var v3 = HfStd.parseInt(this.debugInput, 10);
      if (Key.isDown(Key.CONTROL)) {
        this.game.forcedGoto(v3);
      } else {
        if (Key.isDown(Key.ALT)) {
          hf.entity.item.ScoreItem.attach(this.game, this.x + this.dir * 30, this.y, v3, null);
        } else {
          hf.entity.item.SpecialItem.attach(this.game, this.x + this.dir * 30, this.y, v3, null);
        }
      }
      this.debugInput = "";
    }
    if (Key.isDown(Key.BACKSPACE)) {
      this.debugInput = "";
    }
    if (this.debugInput.length > 0) {
      var v4 = this.debugInput;
      while (v4.length < 3) {
        v4 += "$_".substring(1);
      }
      Log.print("INPUT: " + v4);
      Log.print("(backspace to clear)");
    }
    if (Key.isDown(78) && this.dbg_lastKey != 78) {
      this.game.nextLevel();
      this.dbg_lastKey = 78;
    }
    if (Key.isDown(75) && this.dbg_lastKey != 75) {
      for (v5 in 0...50) {
        this.game.giveKey(v5);
      }
      this.dbg_lastKey = 75;
    }
    if (Key.isDown(73) && this.dbg_lastKey != 73) {
      hf.entity.item.SpecialItem.attach(this.game, HfStd.random(200) + 200, 20, this.game.randMan.draw(Data.RAND_ITEMS_ID), 0);
      this.dbg_lastKey = 73;
    }
    if (Key.isDown(111) && this.dbg_lastKey != 111) {
      this.game.huTimer = 9999999;
      ++this.game.huState;
      this.dbg_lastKey = 111;
    }
    if (Key.isDown(107) && this.dbg_lastKey != 107) {
      var v6 = this.game.getBadList();
      for (v7 in 0...v6.length) {
        v6[v7].angerMore();
      }
      this.dbg_lastKey = 107;
    }
    if (Key.isDown(109) && this.dbg_lastKey != 109) {
      var v8 = this.game.getBadList();
      for (v9 in 0...v8.length) {
        v8[v9].calmDown();
      }
      this.dbg_lastKey = 109;
    }
    if (Key.isDown(71) && this.dbg_lastKey != 71) {
      this.world.view.detachGrid();
      this.world.view.attachGrid(Math.round(Math.pow(2, this.dbg_grid)), true);
      GameManager.warning("grid: " + Data.GRID_NAMES[this.dbg_grid]);
      ++this.dbg_grid;
      if (Data.GRID_NAMES[this.dbg_grid] == null) {
        this.dbg_grid = 0;
      }
      this.dbg_lastKey = 71;
    }
    if (Key.isDown(87) && this.dbg_lastKey != 87) {
      ++this.currentWeapon;
      if (this.currentWeapon > 9) {
        this.currentWeapon = 1;
      }
      this.changeWeapon(this.currentWeapon);
      this.dbg_lastKey = 87;
    }
    if (Key.isDown(106) && this.dbg_lastKey != 106) {
      var v10 = this.game.getBadList();
      for (v11 in 0...v10.length) {
        v10[v11].destroy();
      }
      this.dbg_lastKey = 106;
    }
  }

  public static function attach(g: GameMode, x: Float, y: Float): Player {
    var v5: Player = cast g.depthMan.attach("hammer_player", Data.DP_PLAYER);
    v5.initPlayer(g, x, y);
    return v5;
  }

  public function forceKill(dx: Null<Float>): Void {
    this.fl_shield = false;
    this.killHit(dx);
  }

  public dynamic function getScore(origin: Pos<Float>, value: Int): Void {
    if (origin != null) {
      if (this.specialMan.actives[95]) {
        this.game.fxMan.attachScorePop(this.baseColor, this.darkColor, origin.x, origin.y, "" + value * 2);
      } else {
        this.game.fxMan.attachScorePop(this.baseColor, this.darkColor, origin.x, origin.y, "" + value);
      }
    }
    this.getScoreHidden(value);
  }

  public dynamic function getScoreHidden(value: Int): Void {
    if (this.specialMan.actives[95]) {
      value *= 2;
    }
    var v3 = Data.EXTRA_LIFE_STEPS[this.extraLifeCurrent];
    var gi: hf.gui.GameInterface = this.game.gi;
    if (v3 != null && this.score < v3 && this.score + value >= v3) {
      ++this.lives;
      gi.setLives(this.pid, this.lives);
      this.game.manager.logAction("$EL" + this.extraLifeCurrent);
      ++this.extraLifeCurrent;
    }
    if (this.score != 0 && (this.scoreCS ^ GameManager.KEY) != this.score) {
      this.game.manager.logIllegal("$SCS");
    }
    this.score += value;
    this.scoreCS = this.score ^ GameManager.KEY;
    gi.setScore(this.pid, this.score);
  }

  public function getExtend(id: Int): Void {
    var gi: hf.gui.GameInterface = this.game.gi;
    if (this.extendList[id] != true) {
      gi.getExtend(this.pid, id);
    }
    if (!this.extendList[id]) {
      this.extendOrder.push(id);
    }
    this.extendList[id] = true;
    var v3 = true;
    for (v4 in 0...Data.EXTENDS.length) {
      if (this.extendList[v4] != true) {
        v3 = false;
      }
    }
    if (v3) {
      var v5 = true;
      for (v6 in 0...this.extendOrder.length) {
        if (this.extendOrder[v6] != v6) {
          v5 = false;
        }
      }
      gi.clearExtends(this.pid);
      this.extendList = new Array();
      this.extendOrder = new Array();
      this.specialMan.executeExtend(v5);
    }
  }

  public function killPlayer(): Void {
    var gi: hf.gui.GameInterface = this.game.gi;
    this.game.fxMan.attachFx(this.x, Data.GAME_HEIGHT, "hammer_fx_death_player");
    this.game.statsMan.inc(Data.STAT_DEATH, 1);
    --this.lives;
    gi.setLives(this.pid, this.lives);
    if (this.lives >= 0) {
      this.resurrect();
    } else {
      var v2 = true;
      var v3 = this.game.getPlayerList();
      for (v4 in 0...v3.length) {
        if (v3[v4].lives >= 0) {
          v2 = false;
        }
      }
      if (!v2) {
        if (GameManager.CONFIG.hasOption(Data.OPT_LIFE_SHARING)) {
          var v5 = this.game.getPlayerList();
          for (v6 in 0...v5.length) {
            var v7 = v5[v6];
            if (v7.uniqId != this.uniqId && v7.lives > 0) {
              --v7.lives;
              gi.setLives(v7.pid, v7.lives);
              this.resurrect();
              return;
            }
          }
        }
      }
      var v8 = this.game.getPlayerList();
      for (v9 in 0...v8.length) {
        this.game.registerScore(v8[v9].pid, v8[v9].score);
      }
      this.game.onGameOver();
      this.destroy();
    }
  }

  public function shield(duration: Null<Float>): Void {
    if (duration == null) {
      duration = Data.SHIELD_DURATION;
    }
    this.shieldMC.destroy();
    this.shieldMC = this.game.fxMan.attachFx(this.x, this.y, "hammer_player_shield");
    this.shieldMC.fl_loop = true;
    this.shieldMC.stopBlink();
    this.shieldTimer = duration;
    this.shieldMC.lifeTimer = this.shieldTimer;
    this.fl_shield = true;
  }

  public function unshield(): Void {
    this.fl_shield = false;
    this.shieldTimer = 0;
    this.onShieldOut();
  }

  public function knock(d: Float): Void {
    if (this.fl_knock) {
      return;
    }
    this.fl_lockControls = true;
    this.fl_knock = true;
    this.knockTimer = d;
  }

  public function curse(id: Int): Void {
    var v3 = this.game.depthMan.attach("curse", Data.DP_FX);
    v3._alpha = 70;
    v3.gotoAndStop("" + id);
    this.stick(v3, 0, -Data.CASE_HEIGHT * 2.5);
    this.setElaStick(0.25);
  }

  public function setBaseAnims(a_walk: Anim, a_stop: Anim): Void {
    var v4 = false;
    var v5 = false;
    if (this.animId == this.baseWalkAnim.id) {
      v4 = true;
    }
    if (this.animId == this.baseStopAnim.id) {
      v5 = true;
    }
    if (this.animId == Data.ANIM_PLAYER_WAIT1.id || this.animId == Data.ANIM_PLAYER_WAIT2.id) {
      v5 = true;
    }
    if (a_walk != null) {
      this.baseWalkAnim = a_walk;
    }
    if (a_stop != null) {
      this.baseStopAnim = a_stop;
    }
    if (v4) {
      this.playAnim(this.baseWalkAnim);
    }
    if (v5) {
      this.playAnim(this.baseStopAnim);
    }
  }

  public function showTeleporters(): Void {
    var v2 = this.world.teleporterList;
    for (v3 in 0...v2.length) {
      this.world.hideField(v2[v3]);
    }
    for (v4 in 0...v2.length) {
      var v5 = v2[v4];
      var v6 = false;
      if (v5.dir == Data.VERTICAL) {
        if (Math.abs(v5.centerX - this.x) <= Data.TELEPORTER_DISTANCE && this.y >= v5.startY - Data.CASE_HEIGHT * 0.5 && this.y <= v5.endY + Data.CASE_HEIGHT * 0.5) {
          v6 = true;
        }
      } else {
        if (Math.abs(v5.centerY - this.y) <= Data.TELEPORTER_DISTANCE && this.x >= v5.startX - Data.CASE_WIDTH && this.x <= v5.endX + Data.CASE_WIDTH) {
          v6 = true;
        }
      }
      if (v6) {
        this.world.showField(v5);
        var v7 = this.world.getNextTeleporter(v5);
        if (v7 != null && !v7.fl_rand) {
          this.world.showField(v7.td);
        }
      }
    }
  }

  public function lockControls(d: Float): Void {
    this.lockTimer = d;
    this.playAnim(this.baseStopAnim);
    this.fl_lockControls = true;
  }

  public function isRecentKick(b: Bomb): Bool {
    var v3 = false;
    for (v4 in 0...this.recentKicks.length) {
      if (b.uniqId == this.recentKicks[v4].bid) {
        v3 = true;
      }
    }
    return v3;
  }

  public function attack(): Null<Entity> {
    if (this.specialMan.actives[91] || this.specialMan.actives[85]) {
      return null;
    }
    switch (this.currentWeapon) {
      case Data.WEAPON_B_CLASSIC:
        return this.drop(hf.entity.bomb.player.Classic.attach(this.game, this.x, this.y));
      case Data.WEAPON_B_BLACK:
        return this.drop(hf.entity.bomb.player.Black.attach(this.game, this.x, this.y));
      case Data.WEAPON_B_BLUE:
        return this.drop(hf.entity.bomb.player.Blue.attach(this.game, this.x, this.y));
      case Data.WEAPON_B_GREEN:
        return this.drop(hf.entity.bomb.player.Green.attach(this.game, this.x, this.y));
      case Data.WEAPON_B_RED:
        return this.drop(hf.entity.bomb.player.Red.attach(this.game, this.x, this.y));
      case Data.WEAPON_B_REPEL:
        return this.drop(hf.entity.bomb.player.RepelBomb.attach(this.game, this.x, this.y));
      case Data.WEAPON_S_ARROW:
        return this.shoot(hf.entity.shoot.PlayerArrow.attach(this.game, this.x, this.y));
      case Data.WEAPON_S_FIRE:
        return this.shoot(hf.entity.shoot.PlayerFireBall.attach(this.game, this.x, this.y));
      case Data.WEAPON_S_ICE:
        return this.shoot(hf.entity.shoot.PlayerPearl.attach(this.game, this.x, this.y));
    }
    GameManager.fatal("invalid weapon id : " + this.currentWeapon);
    return null;
  }

  public function isBombWeapon(id: Int): Bool {
    return id == Data.WEAPON_B_CLASSIC || id == Data.WEAPON_B_BLACK || id == Data.WEAPON_B_BLUE || id == Data.WEAPON_B_GREEN || id == Data.WEAPON_B_RED || id == Data.WEAPON_B_REPEL;
  }

  public function isShootWeapon(id: Int): Bool {
    return id == Data.WEAPON_S_ARROW || id == Data.WEAPON_S_FIRE || id == Data.WEAPON_S_ICE;
  }

  public function drop(b: PlayerBomb): PlayerBomb {
    if (!this.fl_stable) {
      this.airJump();
    }
    this.game.statsMan.inc(Data.STAT_BOMB, 1);
    b.setOwner(this);
    this.game.soundMan.playSound("sound_bomb_drop", Data.CHAN_PLAYER);
    if (!this.fl_stable) {
      this.kickBomb([b], 1.0);
    }
    return b;
  }

  public function airJump(): Void {
    this.playAnim(Data.ANIM_PLAYER_JUMP_UP);
    if (this.dy > 0) {
      this.dy = -Data.PLAYER_AIR_JUMP;
    } else {
      if (Math.abs(this.dy) < Data.PLAYER_AIR_JUMP) {
        this.dy = -Data.PLAYER_AIR_JUMP;
      }
    }
  }

  public function shoot(s: Shoot): Shoot {
    this.game.statsMan.inc(Data.STAT_SHOT, 1);
    this.coolDown = s.coolDown;
    if (this.dir < 0) {
      s.moveLeft(s.shootSpeed);
      return s;
    }
    s.moveRight(s.shootSpeed);
    return s;
  }

  public function changeWeapon(id: Int): Void {
    if (id == null) {
      id = this.lastBomb;
    }
    if (id > 0 && !this.isBombWeapon(id) && !this.isShootWeapon(id)) {
      return;
    }
    if (this.isBombWeapon(this.currentWeapon) && !this.isBombWeapon(id)) {
      this.lastBomb = this.currentWeapon;
    }
    if (this.isBombWeapon(id)) {
      this.lastBomb = id;
    }
    this.currentWeapon = id;
    this.replayAnim();
  }

  public function kickBomb(l: Array<Bomb>, powerFactor: Float): Void {
    var v4 = 0;
    while (v4 < l.length) {
      var v5 = l[v4];
      if (!this.isRecentKick(v5)) {
        if ((v5.fl_airKick || !v5.fl_airKick && v5.fl_stable) && !v5.fl_explode) {
          if (!v5.isType(Data.SOCCERBALL)) {
            v5.dx = this.dir * Data.PLAYER_HKICK_X;
          } else {
            v5.dx = this.dir * Data.PLAYER_HKICK_X * powerFactor;
          }
          if (this.dir < 0 && this.world.checkFlag({x: this.cx, y: this.cy}, Data.IA_CLIMB_LEFT)) {
            var v6 = this.world.getWallHeight(this.cx - 1, this.cy, Data.IA_CLIMB);
            if (v6 <= 1) {
              v5.moveTo(v5.x, v5.y - Data.CASE_HEIGHT * 0.5);
            }
          }
          if (this.dir > 0 && this.world.checkFlag({x: this.cx, y: this.cy}, Data.IA_CLIMB_RIGHT)) {
            var v7 = this.world.getWallHeight(this.cx + 1, this.cy, Data.IA_CLIMB);
            if (v7 <= 1) {
              v5.moveTo(v5.x, v5.y - Data.CASE_HEIGHT * 0.5);
            }
          }
          if (this.specialMan.actives[13]) {
            v5.dx *= 2;
          }
          if (this.game.fl_bombExpert && this.dx / v5.dx > 0) {
            v5.dx *= 2.5;
          }
          if (this.specialMan.actives[115]) {
            v5.dx *= 1.5;
          }
          v5.dy = -Data.PLAYER_HKICK_Y;
          v5.onKick(this);
          v5.next = null;
          v5.fl_bounce = true;
          this.recentKicks.push({t: this.game.cycle, bid: v5.uniqId});
          this.playAnim(Data.ANIM_PLAYER_KICK);
          this.game.soundMan.playSound("sound_kick", Data.CHAN_PLAYER);
          this.game.statsMan.inc(Data.STAT_KICK, 1);
          if (this.specialMan.actives[92]) {
            if (v5.lifeTimer > 0) {
              var v8 = v5.duplicate();
              var b = Data.PLAYER_BOMB.downcast(v8);
              if (b != null) {
                b.owner = this;
              }
              v8.lifeTimer = v5.lifeTimer;
              v8.dx = -v5.dx;
              v8.dy = v5.dy;
              v8.fl_bounce = true;
            }
          }
          if (this.specialMan.actives[70]) {
            this.getScore(this, 10);
          }
        }
      }
      ++v4;
    }
  }

  public function upKickBomb(l: Array<Bomb>): Void {
    var v3 = 0;
    while (v3 < l.length) {
      var v4 = l[v3];
      if (!this.isRecentKick(v4)) {
        if ((v4.fl_airKick || !v4.fl_airKick && v4.fl_stable) && !v4.fl_explode) {
          v4.dx *= 2;
          if (Math.abs(v4.dx) <= 1.5) {
            v4.dx = 0.5 * this.dir;
          }
          v4.dy = -Data.PLAYER_VKICK;
          if (this.specialMan.actives[13]) {
            v4.dy *= 2;
          }
          if (this.specialMan.actives[115]) {
            v4.dy *= 2;
            v4.dx *= 1.3;
          }
          v4.next = null;
          v4.onKick(this);
          v4.fl_stable = false;
          v4.fl_bounce = true;
          this.recentKicks.push({t: this.game.cycle, bid: v4.uniqId});
          this.playAnim(Data.ANIM_PLAYER_KICK);
          this.game.soundMan.playSound("sound_kick", Data.CHAN_PLAYER);
          this.game.statsMan.inc(Data.STAT_KICK, 1);
          if (this.specialMan.actives[70]) {
            this.getScore(this, 10);
          }
        }
      }
      ++v3;
    }
  }

  public function countBombs(): Int {
    var v2 = 0;
    var v3 = this.game.getList(Data.PLAYER_BOMB);
    for (v4 in 0...v3.length) {
      if (!v3[v4].fl_explode && v3[v4].parent == this) {
        ++v2;
      }
    }
    return v2;
  }

  public function onShieldOut(): Void {
    this.game.fxMan.attachFx(this.x, this.y, "popShield");
    this.shieldMC.destroy();
    this.checkHits();
  }

  public function onWakeUp(): Void {
    this.fl_knock = false;
    if (this.fl_stable) {
      this.dx = 0;
      this.playAnim(Data.ANIM_PLAYER_KNOCK_OUT);
    } else {
      this.fl_lockControls = false;
      this.playAnim(Data.ANIM_PLAYER_JUMP_DOWN);
    }
  }

  public function onNextLevel(): Void {
    this.changeWeapon(1);
    if (this.fl_shield) {
      this.shieldTimer = 1;
    }
    this.specialMan.clearTemp();
    this.specialMan.clearRec();
  }

  public function onStartLevel(): Void {
    var gi: hf.gui.GameInterface = this.game.gi;
    this.show();
    this.game.manager.logAction(this.world.currentId + "," + Math.floor(this.score / 1000));
    if (this.game.world.fl_mainWorld) {
      gi.setLevel(this.game.world.currentId);
    } else {
      if (this.game.fakeLevelId == null) {
        gi.hideLevel();
      } else {
        gi.setLevel(this.game.fakeLevelId);
      }
    }
    if (this.fl_shield && this.shieldMC == null) {
      this.shield(this.shieldTimer);
    }
    this.startX = this.x;
    this.fl_entering = true;
  }

  override public function destroy(): Void {
    this.game.registerScore(this.pid, this.score);
    this.specialMan.clearPerm();
    this.specialMan.clearTemp();
    this.specialMan.clearRec();
    super.destroy();
  }

  override public function endUpdate(): Void {
    super.endUpdate();
    if (this.shieldMC != null) {
      if (this.shieldTimer <= Data.SECOND * 3) {
        this.shieldMC.blink();
      }
      this.shieldMC.mc._x += (this.x - this.shieldMC.mc._x) * 0.75;
      this.shieldMC.mc._y += (this.y - 20 - this.shieldMC.mc._y) * 0.75;
    }
    this._xscale = this.dir * Math.abs(this._xscale);
  }

  override public function hide(): Void {
    super.hide();
    this.shieldMC.mc._visible = false;
  }

  override public function hit(e: Entity): Void {
    var v3 = Data.ITEM.downcast(e);
    if (v3 != null) {
      v3.execute(this);
      if (v3.id == Data.CONVERT_DIAMANT) {
        this.specialMan.onPickPerfectItem();
      }
    }
  }

  override public function infix(): Void {
    super.infix();
    var v3 = this.world.getCase({x: this.cx, y: this.cy});
    if (v3 > Data.FIELD_TELEPORT && v3 < 0) {
      if (this.currentWeapon != Std.int(Math.abs(v3))) {
        var v4 = this.game.fxMan.attachShine(this.x, this.y - Data.CASE_HEIGHT * 0.5);
        v4.mc._xscale = 65;
        v4.mc._yscale = v4.mc._xscale;
        this.game.soundMan.playSound("sound_field", Data.CHAN_FIELD);
      }
      this.changeWeapon(Std.int(Math.abs(v3)));
    }
    if (v3 == Data.FIELD_PEACE) {
      if (this.currentWeapon != Std.int(Math.abs(v3))) {
        var v5 = this.game.fxMan.attachShine(this.x, this.y - Data.CASE_HEIGHT * 0.5);
        v5.mc._xscale = 65;
        v5.mc._yscale = v5.mc._xscale;
        this.game.soundMan.playSound("sound_field", Data.CHAN_FIELD);
      }
      this.changeWeapon(Data.WEAPON_NONE);
    }
    if (!this.fl_kill && !this.fl_destroy) {
      this.game.world.scriptEngine.onEnterCase(this.cx, this.cy);
    }
    this.showTeleporters();
  }

  override public function init(g: GameMode): Void {
    super.init(g);
    this.ctrl = new hf.entity.PlayerController(this);
    this.register(Data.PLAYER);
    this.specialMan = new SpecialManager(this.game, this);
    this.game.manager.logAction("__dollar__P:" + this.lives);
  }

  override public function killHit(dx: Null<Float>): Void {
    if (this.fl_kill || this.fl_shield) {
      return;
    }
    this.fl_knock = false;
    this.game.soundMan.playSound("sound_player_death", Data.CHAN_PLAYER);
    var v4 = dx / Math.abs(dx);
    if (HfStd.isNaN(v4)) {
      v4 = HfStd.random(2) * 2 - 1;
    }
    if (this.x >= 0.85 * Data.GAME_WIDTH) {
      v4 = -1;
    }
    if (this.x <= 0.15 * Data.GAME_WIDTH) {
      v4 = 1;
    }
    this.playAnim(Data.ANIM_PLAYER_DIE);
    var v5 = 20;
    if (Timer.tmod <= 0.6) {
      v5 = 40;
    }
    super.killHit(v4 * v5);
  }

  override public function onBump(): Void {
    super.onBump();
    this.fl_knock = false;
    this.knock(Data.SECOND * 0.7);
  }

  override public function onDeathLine(): Void {
    super.onDeathLine();
    if (this.fl_kill) {
      this.killPlayer();
    } else {
      if (this.game.checkLevelClear()) {
        this.dy = 0;
        this.game.nextLevel();
      } else {
        this.y = Data.LEVEL_HEIGHT * Data.CASE_HEIGHT - Data.CASE_HEIGHT - 1;
        this.dx = 0;
        this.forceKill(0);
      }
    }
  }

  override public function onEndAnim(id: Int): Void {
    super.onEndAnim(id);
    if (id == Data.ANIM_PLAYER_KNOCK_OUT.id) {
      this.fl_lockControls = false;
      this.playAnim(this.baseStopAnim);
    }
    if (id == Data.ANIM_PLAYER_AIRKICK.id) {
      this.animId = null;
      this.playAnim(Data.ANIM_PLAYER_JUMP_DOWN);
    }
    if (id == Data.ANIM_PLAYER_RESURRECT.id) {
      this.fl_lockControls = false;
      this.playAnim(this.baseStopAnim);
    }
    if (id == Data.ANIM_PLAYER_KICK.id && !this.fl_stable) {
      this.animId = null;
      this.playAnim(Data.ANIM_PLAYER_JUMP_DOWN);
      return;
    }
    if (id == Data.ANIM_PLAYER_CARROT.id) {
      this.playAnim(this.baseStopAnim);
    }
    if (id == Data.ANIM_PLAYER_KICK.id || id == Data.ANIM_PLAYER_ATTACK.id || id == Data.ANIM_PLAYER_WAIT1.id || id == Data.ANIM_PLAYER_WAIT2.id || id == Data.ANIM_PLAYER_JUMP_LAND.id) {
      this.playAnim(this.baseStopAnim);
    }
  }

  override public function onHitGround(h: Float): Void {
    if (this.specialMan.actives[90]) {
      if (!this.fl_knock && h >= Data.CASE_HEIGHT * 2) {
        this.knock(Data.SECOND);
      }
    }
    super.onHitGround(h);
    if (h >= Data.DUST_FALL_HEIGHT) {
      this.game.fxMan.dust(this.cx, this.cy + 1);
    }
    this.game.fxMan.attachFx(this.x, this.y, "hammer_fx_fall");
    this.game.soundMan.playSound("sound_land", Data.CHAN_PLAYER);
    if (this.specialMan.actives[39]) {
      this.game.shake(10, 2);
      var v4 = this.game.getBadClearList();
      for (v5 in 0...v4.length) {
        v4[v5].knock(Data.SECOND);
      }
    }
    this.showTeleporters();
  }

  override public function onHitWall(): Void {
    if (this.fl_knock) {
      this.dx = -this.dx * 0.5;
      if (Math.abs(this.dx) >= 10 && this.world.getCase({x: this.cx, y: this.cy}) <= 0) {
        this.game.shake(Data.SECOND * 0.7, 5);
        this.game.fxMan.inGameParticlesDir(Data.PARTICLE_STONE, this.x, this.y, 1 + HfStd.random(3), this.dx);
        this.game.fxMan.inGameParticlesDir(Data.PARTICLE_CLASSIC_BOMB, this.x, this.y, 3 + HfStd.random(5), this.dx);
      }
    } else {
      super.onHitWall();
    }
  }

  override public function onPortal(pid: Int): Void {
    super.onPortal(pid);
    if (!this.game.usePortal(pid, this)) {
      this.onPortalRefusal();
    }
  }

  override public function onPortalRefusal(): Void {
    super.onPortalRefusal();
    this.x = this.oldX;
    this.y = this.oldY;
    this.dx = -this.dx;
    this.knock(Data.SECOND * 0.5);
    this.game.fxMan.inGameParticles(Data.PARTICLE_PORTAL, this.x, this.y, HfStd.random(5) + 5);
    this.game.shake(Data.SECOND, 3);
    this.fl_stopStepping = true;
  }

  override public function onTeleport(): Void {
    super.onTeleport();
    this.dx = 0;
    this.dy = 0;
    if (this.shieldMC != null) {
      this.shieldMC.mc._x = this.x;
      this.shieldMC.mc._y = this.y;
    }
  }

  override public function playAnim(a: Anim): Void {
    if (a.id == this.baseWalkAnim.id && this.speedFactor > 1) {
      a = Data.ANIM_PLAYER_RUN;
    }
    if (a.id == Data.ANIM_PLAYER_JUMP_DOWN.id && this.animId == Data.ANIM_PLAYER_AIRKICK.id) {
      return;
    }
    if (this.fl_knock) {
      if (a.id != Data.ANIM_PLAYER_DIE.id && a.id != Data.ANIM_PLAYER_KNOCK_IN.id) {
        return;
      }
    }
    if (this.animId == Data.ANIM_PLAYER_KICK.id && a.id == Data.ANIM_PLAYER_JUMP_DOWN.id) {
      return;
    }
    if (this.animId == Data.ANIM_PLAYER_CARROT.id) {
      return;
    }
    super.playAnim(a);
  }

  override public function resurrect(): Void {
    super.resurrect();
    this.game.manager.logAction("$R" + this.lives);
    this.moveTo(Entity.x_ctr(this.world.current.__dollar__playerX), Entity.y_ctr(this.world.current.__dollar__playerY));
    this.dx = 0;
    this.dy = 0;
    this.shield(null);
    this.changeWeapon(1);
    this.oxygen = 100;
    this.fl_knock = false;
    this.specialMan.clearTemp();
    this.specialMan.clearPerm();
    this.specialMan.clearRec();
    this.playAnim(Data.ANIM_PLAYER_RESURRECT);
    this.stickAnim();
    this.fl_lockControls = true;
    if (this.game.fl_nightmare) {
      this.speedFactor = 1.3;
    }
    if (this.game._name != "$time" && GameManager.CONFIG.hasOption(Data.OPT_BOOST)) {
      this.speedFactor = 1.3;
      if (this.game.fl_nightmare) {
        this.speedFactor = 1.6;
      }
    }
    this.game.onResurrect();
  }

  override public function scale(n: Float): Void {
    super.scale(n);
    this._xscale *= this.dir;
  }

  override public function show(): Void {
    super.show();
    this.shieldMC.mc._visible = true;
  }

  override public function needsPatch(): Bool {
    return true;
  }

  override public function update(): Void {
    var v3 = 0;
    while (v3 < this.recentKicks.length) {
      if (this.game.cycle - this.recentKicks[v3].t > 2) {
        this.recentKicks.splice(v3, 1);
        --v3;
      }
      ++v3;
    }
    if (this.specialMan.actives[100]) {
      this.game.fxMan.inGameParticles(Data.PARTICLE_RAIN, this.x + HfStd.random(20) * (HfStd.random(2) * 2 - 1), this.y - 60 - HfStd.random(5), HfStd.random(2) + 1);
      this.scale(Math.min(100, (this.scaleFactor + 0.002 * Timer.tmod) * 100));
      if (this.scaleFactor <= 0.6) {
        this.killHit(null);
        this.specialMan.interrupt(100);
      }
    }
    if (!this.fl_kill && this.game.fl_aqua) {
      this.oxygen -= Timer.tmod * 0.1;
      this.game.manager.progress(this.oxygen / 100);
      if (this.oxygen <= 0) {
        this.killHit(0);
      }
    }
    this.specialMan.main();
    if (!this.fl_kill && !this.fl_lockControls) {
      if (!this.fl_entering && this._visible) {
        this.ctrl.update();
      }
      if (this.pid == 0 && this.game.manager.fl_debug) {
        this.getDebugControls();
      }
    }
    if (this.fl_entering) {
      if (this.cy >= 0) {
        this.fl_entering = false;
      }
    }
    if (this.coolDown > 0) {
      this.coolDown -= Timer.tmod;
      if (this.coolDown <= 0) {
        this.coolDown = 0;
      }
    }
    if (this.fl_knock) {
      this.knockTimer -= Timer.tmod;
      if (this.knockTimer <= 0) {
        this.onWakeUp();
      } else {
        if (this.fl_stable && this.animId != Data.ANIM_PLAYER_KNOCK_IN.id) {
          this.playAnim(Data.ANIM_PLAYER_KNOCK_IN);
        }
        if (!this.fl_stable && this.animId != Data.ANIM_PLAYER_DIE.id) {
          this.playAnim(Data.ANIM_PLAYER_DIE);
        }
      }
    }
    if (this.fl_lockControls) {
      if (this.fl_stable && this.animId == this.baseStopAnim.id) {
        this.fl_lockControls = false;
      }
    }
    if (this.lockTimer > 0) {
      this.lockTimer -= Timer.tmod;
      if (this.lockTimer <= 0) {
        this.fl_lockControls = false;
      } else {
        this.fl_lockControls = true;
      }
    }
    if (this.fl_shield) {
      this.shieldTimer -= Timer.tmod;
      if (this.shieldTimer <= 0) {
        this.unshield();
      }
    }
    super.update();
    this.updateCoords();
    if (this.dx != 0 || this.dy != 0) {
      this.edgeTimer = 0;
      this.waitTimer = 0;
    }
    if (!this.fl_kill) {
      if (!this.fl_stable && this.dy >= 0 && this.animId != Data.ANIM_PLAYER_JUMP_DOWN.id && !this.fl_lockControls) {
        this.playAnim(Data.ANIM_PLAYER_JUMP_DOWN);
      }
      if (this.animId == Data.ANIM_PLAYER_JUMP_DOWN.id && this.fl_stable) {
        this.playAnim(Data.ANIM_PLAYER_JUMP_LAND);
      }
      if (this.animId == Data.ANIM_PLAYER_STOP.id) {
        if (this.waitTimer <= 0) {
          this.waitTimer = Data.WAIT_TIMER;
        }
        this.waitTimer -= Timer.tmod;
        if (this.waitTimer <= 0) {
          if (HfStd.random(20) == 0) {
            this.playAnim(Data.ANIM_PLAYER_WAIT1);
          } else {
            this.playAnim(Data.ANIM_PLAYER_WAIT2);
          }
        }
      }
      if (this.fl_stable && this.dx == 0) {
        if (this.animId == this.baseStopAnim.id) {
          var v4 = Entity.rtc(this.x + this.dir * Data.CASE_WIDTH * 0.3, this.y + Data.CASE_HEIGHT);
          if (this.world.getCase(v4) <= 0 && this.world.getCase({x: this.cx + this.dir, y: this.cy}) == 0) {
            if (this.edgeTimer <= 0) {
              this.edgeTimer = Data.EDGE_TIMER;
            }
            this.edgeTimer -= Timer.tmod;
            if (this.edgeTimer <= 0) {
              this.playAnim(Data.ANIM_PLAYER_EDGE);
            }
          }
        }
      }
    }
  }
}
