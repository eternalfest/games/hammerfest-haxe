package hf.entity.boss;

import etwin.flash.DynamicMovieClip;
import hf.entity.Item;
import hf.entity.Mover;
import hf.Entity;
import hf.mode.GameMode;
import etwin.flash.Key;

class Tuberculoz extends Mover {
  public static var auto_inc: Int = 1;
  public static var WALK: Int = hf.entity.boss.Tuberculoz.auto_inc++;
  public static var JUMP: Int = hf.entity.boss.Tuberculoz.auto_inc++;
  public static var DASH: Int = hf.entity.boss.Tuberculoz.auto_inc++;
  public static var BOMB: Int = hf.entity.boss.Tuberculoz.auto_inc++;
  public static var HIT: Int = hf.entity.boss.Tuberculoz.auto_inc++;
  public static var BURN: Int = hf.entity.boss.Tuberculoz.auto_inc++;
  public static var TORNADO: Int = hf.entity.boss.Tuberculoz.auto_inc++;
  public static var TORNADO_END: Int = hf.entity.boss.Tuberculoz.auto_inc++;
  public static var DIE: Int = hf.entity.boss.Tuberculoz.auto_inc++;
  public static var seq_inc: Int = 0;
  public static var SEQ_BURN: Int = hf.entity.boss.Tuberculoz.seq_inc++;
  public static var SEQ_TORNADO: Int = hf.entity.boss.Tuberculoz.seq_inc++;
  public static var SEQ_DASH: Int = hf.entity.boss.Tuberculoz.seq_inc++;
  public static var LAST_SEQ: Int = hf.entity.boss.Tuberculoz.seq_inc - 1;
  public static var SEQ_DURATION: Float = Data.SECOND * 9;
  public static var LIVES: Int = 100;
  public static var GROUND_Y: Float = 446;
  public static var CENTER_OFFSET: Float = -28;
  public static var HEAD_OFFSET_X: Float = 10;
  public static var HEAD_OFFSET_Y: Float = -54;
  public static var RADIUS: Float = Data.CASE_WIDTH * 1.7;
  public static var HEAD_RADIUS: Float = Data.CASE_WIDTH * 0.9;
  public static var MAX_BADS: Int = 3;
  public static var INVERT_KICK_X: Float = 150;
  public static var WALK_SPEED: Float = 3;
  public static var DASH_SPEED: Float = 16;
  public static var FIREBALL_SPEED: Float = 3;
  public static var TORNADO_INTRO: Float = Data.SECOND * 2;
  public static var TORNADO_DURATION: Float = Data.SECOND * 7;
  public static var JUMP_Y: Float = 15;
  public static var JUMP_EST_X: Float = 80;
  public static var JUMP_EST_Y: Float = -145;
  public static var A_STEP: Int = 5;
  public static var B_STEP: Int = 10;
  public static var EXTRA_LIFE_STEP: Int = 30;
  public static var CHANCE_PLAYER_JUMP: Int = 22;
  public static var CHANCE_BOMB_JUMP: Int = 15;
  public static var CHANCE_DASH: Int = 1;
  public static var CHANCE_SPAWN: Int = 25;
  public static var CHANCE_BURN: Int = 5;
  public static var CHANCE_FINAL_ANGER: Int = 35;

  public var _firstUniq: Int;
  public var action: Int;
  public var fl_trap: Bool;
  public var fl_shield: Bool;
  public var fl_immune: Bool;
  public var immuneTimer: Float;
  public var recents: Array<Bool>;
  public var fl_death: Bool;
  public var fl_defeated: Bool;
  public var fbCoolDown: Float;
  public var seqTimer: Float;
  public var defeatTimeOut: Float;
  public var lives: Int;
  public var badKills: Int;
  public var totalKills: Int;
  public var seq: Int;
  public var lifeBar: DynamicMovieClip /* TODO */;
  public var fl_twister: Bool;
  public var dashCount: Null<Int>;
  public var itemA: Null<Item>;
  public var itemB: Null<Item>;

  public function new(): Void {
    super();
    this.fl_hitGround = false;
    this.fl_hitCeil = false;
    this.fl_hitWall = false;
    this.fl_hitBorder = true;
    this.fl_physics = true;
    this.fl_gravity = false;
    this.fl_friction = false;
    this.fl_moveable = false;
    this.fl_blink = true;
    this.fl_alphaBlink = false;
    this.fl_trap = false;
    this.blinkColorAlpha = 60;
    this.blinkColor = 16737792;
    this.fl_shield = false;
    this.fl_immune = false;
    this.immuneTimer = 0;
    this.recents = new Array();
    this.fl_death = false;
    this.fl_defeated = false;
    this.fbCoolDown = 0;
    this.defeatTimeOut = 0;
    this.x = Data.GAME_WIDTH * 0.5;
    this.y = hf.entity.boss.Tuberculoz.GROUND_Y;
    this.dir = HfStd.random(2) * 2 - 1;
    this.lives = hf.entity.boss.Tuberculoz.LIVES;
    this.badKills = 0;
    this.totalKills = 0;
    this.seq = 0;
    this.seqTimer = hf.entity.boss.Tuberculoz.SEQ_DURATION;
  }

  public static function attach(g: GameMode): Tuberculoz {
    var v3 = "hammer_boss_human";
    var v4: Tuberculoz = cast g.depthMan.attach(v3, Data.DP_BADS);
    v4.initBoss(g);
    return v4;
  }

  public function initBoss(g: GameMode): Void {
    this.init(g);
    this.playAnim(Data.ANIM_BOSS_BAT_FORM);
    this.endUpdate();
    this.lifeBar = cast this.game.depthMan.attach("hammer_interf_boss_bar", Data.DP_INTERF);
    this.lifeBar._rotation = -90;
    this.lifeBar._x = 0;
    this.lifeBar._y = Data.GAME_HEIGHT * 0.5;
  }

  public function getHitList<E: Entity>(t: EntityType<E>): Array<E> {
    var v3 = this.game.getClose(t, this.x, this.y + hf.entity.boss.Tuberculoz.CENTER_OFFSET, hf.entity.boss.Tuberculoz.RADIUS, false);
    if (this.action == hf.entity.boss.Tuberculoz.WALK) {
      var v4 = this.game.getClose(t, this.x + hf.entity.boss.Tuberculoz.HEAD_OFFSET_X * this.dir, this.y + hf.entity.boss.Tuberculoz.HEAD_OFFSET_Y, hf.entity.boss.Tuberculoz.HEAD_RADIUS, false);
      for (v5 in 0...v4.length) {
        v3.push(v4[v5]);
      }
    }
    return v3;
  }

  public function flag(uid: Int): Void {
    this.recents[uid - this._firstUniq] = true;
  }

  public function checkFlag(uid: Int): Bool {
    return this.recents[uid - this._firstUniq] == true;
  }

  public function updateBar(): Void {
    this.lifeBar.barFade._xscale = this.lifeBar.bar._xscale;
    this.lifeBar.barFade.gotoAndPlay("1");
    this.lifeBar.bar._xscale = (this.lives / hf.entity.boss.Tuberculoz.LIVES) * 100;
  }

  public function loseLife(n: Int): Void {
    if (this.fl_immune) {
      return;
    }
    this.lives -= n;
    this.updateBar();
    if (this.lives <= 0) {
      this.die();
    } else {
      this.game.killPointer();
      this.immune();
      if (n > 1) {
        this.fl_gravity = true;
        this.fl_hitBorder = true;
        this.dy = -9;
        this.next = null;
        this.action = hf.entity.boss.Tuberculoz.HIT;
        this.playAnim(Data.ANIM_BOSS_HIT);
        this.game.shake(Data.SECOND, 4);
        this.game.destroyList(Data.SHOOT);
        var v3 = this.game.getPlayerList();
        for (v4 in 0...v3.length) {
          v3[v4].knock(Data.SECOND);
        }
        var v5 = this.game.getBadList();
        for (v6 in 0...v5.length) {
          var v7 = v5[v6];
          if (this.fl_stable) {
            v7.dx = 0;
          }
          v7.knock(Data.KNOCK_DURATION);
        }
      }
    }
  }

  public function immune(): Void {
    this.fl_immune = true;
    this.immuneTimer = Data.SECOND * 3;
    this.blink(Data.BLINK_DURATION_FAST);
  }

  public function spawnBombs(n: Int): Void {
    var v3: Array<Bomb> = new Array();
    var v4 = false;
    for (v5 in 0...n) {
      var v6 = hf.entity.bomb.bad.BossBomb.attach(this.game, 0, 0);
      do {
        v6.moveTo(HfStd.random(Math.round(Data.GAME_WIDTH * 0.8)) + Data.GAME_WIDTH * 0.1, HfStd.random(150) + 100);
        v4 = false;
        for (v7 in 0...v3.length) {
          if (v3[v7].distance(v6.x, v6.y) <= Data.CASE_WIDTH * 6) {
            v4 = true;
          }
        }
      } while (v4);
      this.game.fxMan.attachFx(v6.x, v6.y - Data.CASE_HEIGHT * 0.5, "hammer_fx_pop");
      v3.push(v6);
    }
  }

  public function kickBomb(b: Bomb): Void {
    if (this.dx == 0) {
      b.dx = this.dir * 15;
    } else {
      b.dx = this.dx * 5;
    }
    b.dy = -7;
    b.setLifeTimer(Data.SECOND * 0.7);
    if (b.x < this.x && this.dir > 0 || b.x > this.x && this.dir < 0) {
      b.dx *= -1;
    }
  }

  public function isWindCompatible(e: Entity): Bool {
    if (!this.fl_twister && !Data.FX.check(e)) {
      return false;
    }
    if (e.fl_kill) {
      return false;
    }
    if (e.uniqId == this.uniqId) {
      return false;
    }
    if (Data.SHOOT.check(e) || Data.ITEM.check(e)) {
      return false;
    }
    var bad = Data.BAD.downcast(e);
    if (bad != null && bad.fl_freeze) {
      return false;
    }
    if (e.y < 30) {
      return false;
    }
    return true;
  }

  public function updateDash(): Void {
    if (this.oldX < 0 && this.x >= 0 || this.oldX > 0 && this.x <= 0) {
      this.game.fxMan.inGameParticles(Data.PARTICLE_STONE, 10, this.y - 30, 6);
    }
    if (this.oldX > Data.GAME_WIDTH && this.x <= Data.GAME_WIDTH || this.oldX < Data.GAME_WIDTH && this.x >= Data.GAME_WIDTH) {
      this.game.fxMan.inGameParticles(Data.PARTICLE_STONE, Data.GAME_WIDTH - 10, this.y - 30, 6);
    }
    if (this.dx < 0 && this.x <= -Data.GAME_WIDTH || this.dx > 0 && this.x >= Data.GAME_WIDTH * 2) {
      var v2 = this.game.getOne(Data.PLAYER);
      this.dx = -this.dx;
      this.y = v2.y;
      this.dir = -this.dir;
      this._xscale = -this._xscale;
      if (this.x < 0) {
        this.game.attachPointer(0, v2.cy - 2, v2.cx, v2.cy - 2);
      } else {
        this.game.attachPointer(Data.LEVEL_WIDTH, v2.cy - 2, v2.cx, v2.cy - 2);
      }
      ++this.dashCount;
      if (this.dashCount > 2) {
        if (this.dir < 0) {
          this.x = Data.GAME_WIDTH + 80;
        } else {
          this.x = -80;
        }
        this.y = hf.entity.boss.Tuberculoz.GROUND_Y;
        this.game.killPointer();
        this.fl_hitBorder = true;
        this.jump(hf.entity.boss.Tuberculoz.JUMP_Y * 0.6);
      }
    }
  }

  public function updateTornado(): Void {
    if (HfStd.random(3) > 0) {
      if (this.fl_twister || HfStd.random(5) == 0) {
        this.game.fxMan.inGameParticles(Data.PARTICLE_DUST, HfStd.random(Data.GAME_WIDTH), HfStd.random(Data.GAME_HEIGHT), HfStd.random(3));
      }
      var v2 = this.game.getList(Data.PHYSICS);
      for (v3 in 0...v2.length) {
        var v4 = v2[v3];
        if (this.isWindCompatible(v4)) {
          var v5 = HfStd.random(22) / 10 + 0.5;
          if (v4.fl_stable) {
            if (v5 > Data.GRAVITY) {
              v4.dy -= v5 + 3;
              v4.dx += HfStd.random(2) * (HfStd.random(2) * 2 - 1);
            }
          } else {
            v4.dy -= v5;
            v4.dx += HfStd.random(2) * (HfStd.random(2) * 2 - 1);
          }
        }
      }
    }
  }

  public function updateDeath(): Void {
    if ((this.dx != 0 || this.dy != 0 || this.next.action == hf.entity.boss.Tuberculoz.DIE) && HfStd.random(3) == 0) {
      if (HfStd.random(2) == 0) {
        this.game.fxMan.inGameParticles(Data.PARTICLE_STONE, HfStd.random(Data.GAME_WIDTH), 0, HfStd.random(2));
      } else {
        this.game.fxMan.inGameParticles(Data.PARTICLE_STONE, HfStd.random(Data.GAME_WIDTH), HfStd.random(Math.round(Data.GAME_HEIGHT * 0.6)), HfStd.random(2));
      }
      this.game.shake(Data.SECOND, 1);
    }
    if (this.dy > 0 && this.y >= hf.entity.boss.Tuberculoz.GROUND_Y - 8) {
      this.game.shake(Data.SECOND, 5);
      this.land();
      this.y = hf.entity.boss.Tuberculoz.GROUND_Y - 8;
    }
    if (this.next.action == hf.entity.boss.Tuberculoz.DIE) {
      this.dx *= this.game.xFriction;
      if (HfStd.random(3) == 0) {
        var v2 = this.game.depthMan.attach("hammer_fx_strike", Data.FX.asInt());
        this.playAnim(Data.ANIM_BOSS_HIT);
        this.replayAnim();
        v2._y = this.y - HfStd.random(60);
        v2._x = Data.GAME_WIDTH * 0.5;
        var v3 = HfStd.random(2) * 2 - 1;
        v2._xscale = 100 * v3;
        if (this.dx < 0) {
          this.dx = HfStd.random(4) + 2;
        } else {
          this.dx = -(HfStd.random(4) + 2);
        }
      }
    }
    if (this.animId != Data.ANIM_BOSS_DEATH.id) {
      if (HfStd.random(3) == 0) {
        this.game.fxMan.attachExplodeZone(HfStd.random(20) * (HfStd.random(2) * 2 - 1) + this.x, this.y - HfStd.random(70), HfStd.random(20) + 8);
      }
    }
  }

  public function ia(): Void {
    if (this.action == hf.entity.boss.Tuberculoz.WALK) {
      this.seqTimer -= Timer.tmod;
      if (this.x >= Data.GAME_WIDTH - hf.entity.boss.Tuberculoz.RADIUS && this.dir > 0 || this.x <= hf.entity.boss.Tuberculoz.RADIUS && this.dir < 0) {
        this.dir = -this.dir;
        this.walk();
        return;
      }
      if (HfStd.random(1000) < hf.entity.boss.Tuberculoz.CHANCE_PLAYER_JUMP) {
        if ((this.game.getClose(Data.PLAYER, this.x + hf.entity.boss.Tuberculoz.JUMP_EST_X * this.dir, this.y + hf.entity.boss.Tuberculoz.JUMP_EST_Y, hf.entity.boss.Tuberculoz.RADIUS, false)).length > 0) {
          this.jump(hf.entity.boss.Tuberculoz.JUMP_Y);
          return;
        }
      }
      if (HfStd.random(1000) < hf.entity.boss.Tuberculoz.CHANCE_BOMB_JUMP) {
        if ((this.game.getClose(Data.PLAYER_BOMB, this.x + hf.entity.boss.Tuberculoz.JUMP_EST_X * this.dir, this.y + hf.entity.boss.Tuberculoz.JUMP_EST_Y, hf.entity.boss.Tuberculoz.RADIUS, false)).length > 0) {
          this.jump(hf.entity.boss.Tuberculoz.JUMP_Y);
          return;
        }
      }
      if (this.seqTimer <= 0 && !this.fl_death) {
        this.seqTimer = hf.entity.boss.Tuberculoz.SEQ_DURATION;
        switch (this.seq) {
          case hf.entity.boss.Tuberculoz.SEQ_BURN:
            this.burn();
          case hf.entity.boss.Tuberculoz.SEQ_DASH:
            this.dash();
          case hf.entity.boss.Tuberculoz.SEQ_TORNADO:
            this.tornado();
        }
        ++this.seq;
        if (this.seq > hf.entity.boss.Tuberculoz.LAST_SEQ) {
          this.seq = 0;
        }
      }
      if (HfStd.random(1000) < hf.entity.boss.Tuberculoz.CHANCE_SPAWN) {
        if ((this.game.getBadList()).length + (this.game.getList(Data.BAD_BOMB)).length < hf.entity.boss.Tuberculoz.MAX_BADS) {
          this.dropBombs();
          return;
        }
      }
    }
  }

  public function halt(): Void {
    this.dx = 0;
  }

  public function walk(): Void {
    if (this.fl_death) {
      this.setNext(null, null, Data.SECOND * 3, hf.entity.boss.Tuberculoz.DIE);
      return;
    }
    if (this._xscale * this.dir < 0) {
      this.playAnim(Data.ANIM_BOSS_SWITCH);
      this.halt();
    } else {
      this.playAnim(Data.ANIM_BOSS_WAIT);
      this._xscale = this.dir * Math.abs(this._xscale);
      this.dx = hf.entity.boss.Tuberculoz.WALK_SPEED * this.dir;
      this.action = hf.entity.boss.Tuberculoz.WALK;
    }
  }

  public function jump(jumpY: Float): Void {
    this.action = hf.entity.boss.Tuberculoz.JUMP;
    this.fl_gravity = true;
    this.dx = this.dir * hf.entity.boss.Tuberculoz.WALK_SPEED * 1.6;
    this.dy = -jumpY;
    this.playAnim(Data.ANIM_BOSS_JUMP_UP);
  }

  public function land(): Void {
    this.action = null;
    this.fl_gravity = false;
    this.y = hf.entity.boss.Tuberculoz.GROUND_Y;
    this.dy = 0;
    this.playAnim(Data.ANIM_BOSS_JUMP_LAND);
  }

  public function dash(): Void {
    this.action = hf.entity.boss.Tuberculoz.DASH;
    this.halt();
    this.dashCount = 0;
    this.playAnim(Data.ANIM_BOSS_DASH_START);
  }

  public function dropBombs(): Void {
    this.action = hf.entity.boss.Tuberculoz.BOMB;
    this.halt();
    this.playAnim(Data.ANIM_BOSS_BOMB);
  }

  public function burn(): Void {
    this.halt();
    this.playAnim(Data.ANIM_BOSS_BURN_START);
    this.setNext(null, null, Data.SECOND * 3.5, hf.entity.boss.Tuberculoz.BURN);
    this.action = hf.entity.boss.Tuberculoz.BURN;
  }

  public function tornado(): Void {
    this.halt();
    this.playAnim(Data.ANIM_BOSS_TORNADO_START);
    this.setNext(null, null, hf.entity.boss.Tuberculoz.TORNADO_INTRO, hf.entity.boss.Tuberculoz.TORNADO);
    this.fl_twister = false;
    this.action = hf.entity.boss.Tuberculoz.TORNADO;
  }

  public function die(): Void {
    this.halt();
    this.action = null;
    this.defeatTimeOut = Data.SECOND * 15;
    this.fl_death = true;
    this.fl_gravity = true;
    var v2 = Data.GAME_WIDTH * 0.5 - this.x;
    this.dx = v2 * 0.025;
    this.dy = -13;
    this.dir = this.dx <= 0 ? 1 : -1;
    this._xscale = this.scaleFactor * 100 * this.dir;
    this.lifeBar.removeMovieClip();
    this.game.bulletTime(Data.SECOND * 2);
    this.game.shake(Data.SECOND, 5);
    this.playAnim(Data.ANIM_BOSS_HIT);
    var v3 = this.game.getBadList();
    for (v4 in 0...v3.length) {
      var v5 = v3[v4];
      v5.dropReward = null;
      v5.killHit(HfStd.random(50) * (HfStd.random(2) * 2 - 1));
      v5.dy -= HfStd.random(10);
      this.game.fxMan.inGameParticles(Data.PARTICLE_SPARK, v5.x, v5.y, HfStd.random(3) + 1);
    }
    var v6 = this.game.getList(Data.BOMB);
    for (v7 in 0...v6.length) {
      v6[v7].destroy();
      this.game.fxMan.inGameParticles(Data.PARTICLE_CLASSIC_BOMB, v6[v7].x, v6[v7].y, HfStd.random(3) + 1);
    }
    this.game.destroyList(Data.SHOOT);
    this.game.destroyList(Data.BOMB);
  }

  /**
   * Normalized to `final` during obfuscation.
   */
  public function _final(): Void {
    if (this.fl_defeated) {
      return;
    }
    this.halt();
    this.openExit();
    this.game.fxMan.attachExplodeZone(this.x, this.y + hf.entity.boss.Tuberculoz.CENTER_OFFSET, 150);
    this.game.fxMan.inGameParticles(Data.PARTICLE_TUBERCULOZ, this.x, this.y + hf.entity.boss.Tuberculoz.CENTER_OFFSET, Data.MAX_FX);
    this.game.shake(Data.SECOND, 5);
    this.playAnim(Data.ANIM_BOSS_DEATH);
    this.game.destroyList(Data.BAD);
    this.game.fl_clear = true;
    this.fl_defeated = true;
  }

  public function openExit(): Void {
    if (this.fl_defeated) {
      return;
    }
    this.game.depthMan.swap(this, Data.DP_SPRITE_BACK_LAYER);
    var v2 = hf.entity.item.SpecialItem.attach(this.game, Data.GAME_WIDTH * 0.5, Data.GAME_HEIGHT - 40, 113, null);
    v2.dy = -20;
    var v3 = hf.entity.item.ScoreItem.attach(this.game, Data.GAME_WIDTH * 0.5 - 40, Data.GAME_HEIGHT - 40, 199, null);
    v3.dy = -25;
    var v4 = this.game.world.view.attachSprite("$door", this.game.flipCoordReal(Entity.x_ctr(5)), Entity.y_ctr(6), true);
    this.game.fxMan.inGameParticles(Data.PARTICLE_STONE, v4._x + Data.CASE_WIDTH, v4._y, 3 + HfStd.random(10));
    this.game.fxMan.attachExplodeZone(v4._x + Data.CASE_WIDTH, v4._y - Data.CASE_HEIGHT, 40);
    this.game.playMusic(0);
    var v5 = this.game.getPlayerList();
    for (v6 in 0...v5.length) {
      var v7 = v5[v6];
      var v8 = v7.lives * 20000;
      v7.getScore(v7, v8);
      this.game.fxMan.attachAlert(Lang.get(35) + v7.lives + " x " + Data.formatNumber(20000));
    }
  }

  public function onPlayerDeath(): Void {
  }

  public function onKillBad(): Void {
    ++this.badKills;
    ++this.totalKills;
    if (this.badKills >= hf.entity.boss.Tuberculoz.B_STEP) {
      this.itemB.destroy();
      this.itemB = hf.entity.item.SpecialItem.attach(this.game, Data.GAME_WIDTH - 31, 0, 5, null);
      this.itemB.setLifeTimer(0);
      this.badKills = 0;
    } else {
      if (this.badKills == hf.entity.boss.Tuberculoz.A_STEP) {
        this.itemA.destroy();
        this.itemA = hf.entity.item.SpecialItem.attach(this.game, 31, 0, 4, null);
        this.itemA.setLifeTimer(0);
      }
    }
    if (this.totalKills == hf.entity.boss.Tuberculoz.EXTRA_LIFE_STEP) {
      var v2 = hf.entity.item.SpecialItem.attach(this.game, 214, 340, 36, null);
      v2.setLifeTimer(0);
    }
  }

  public function onExplode(x: Float, y: Float, radius: Float): Void {
    if (this.fl_death) {
      return;
    }
    var v5 = Math.sqrt(Math.pow(this.x - x, 2) + Math.pow(this.y + hf.entity.boss.Tuberculoz.CENTER_OFFSET - y, 2));
    if (v5 <= radius && this.action != hf.entity.boss.Tuberculoz.DASH) {
      this.loseLife(1);
    }
  }

  override public function checkHits(): Void {
    if (this.fl_death) {
      return;
    }
    var v2 = this.getHitList(Data.PLAYER_BOMB);
    for (v3 in 0...v2.length) {
      var v4 = v2[v3];
      if (!v4.fl_explode && this.action == hf.entity.boss.Tuberculoz.DASH && this.dx != 0) {
        v4.onExplode();
      }
      if (!v4.fl_explode && !this.checkFlag(v4.uniqId)) {
        this.kickBomb(v4);
        this.flag(v4.uniqId);
      }
    }
    if (!this.fl_immune) {
      var v2 = this.getHitList(Data.PLAYER);
      for (v5 in 0...v2.length) {
        var v6 = v2[v5];
        if (!v6.fl_kill) {
          v6.killHit(this.dx);
          if (this.action == hf.entity.boss.Tuberculoz.DASH && this.dx != 0) {
            this.game.fxMan.attachExplodeZone(v6.x, v6.y - Data.CASE_HEIGHT * 0.5, Data.CASE_WIDTH * 2);
          }
          this.game.shake(Data.SECOND, 3);
        }
      }
    }
    var v2 = this.getHitList(Data.BAD);
    for (v7 in 0...v2.length) {
      var v8 = v2[v7];
      if (v8.uniqId != this.uniqId && !v8.fl_kill && v8.fl_trap) {
        if (!this.fl_immune && v8.fl_freeze && v8.evaluateSpeed() >= Data.ICE_HIT_MIN_SPEED) {
          this.game.fxMan.inGameParticles(Data.PARTICLE_CLASSIC_BOMB, this.x, this.y, 4);
          this.game.fxMan.attachExplodeZone(v8.x, v8.y - Data.CASE_HEIGHT * 0.5, Data.CASE_WIDTH * 2);
          this.loseLife(10);
          this.onKillBad();
          v8.destroy();
        } else {
          v8.killHit(this.dx);
          if (this.action == hf.entity.boss.Tuberculoz.DASH && this.dx != 0) {
            this.game.fxMan.attachExplodeZone(v8.x, v8.y - Data.CASE_HEIGHT * 0.5, Data.CASE_WIDTH * 2);
          }
        }
      }
    }
  }

  override public function destroy(): Void {
    this.lifeBar.removeMovieClip();
    super.destroy();
  }

  override public function endUpdate(): Void {
    super.endUpdate();
    if (this.dir < 0) {
      this._xscale = -Math.abs(this._xscale);
    } else {
      this._xscale = Math.abs(this._xscale);
    }
    if (this.action == hf.entity.boss.Tuberculoz.TORNADO) {
      this.updateTornado();
    }
    this.lifeBar._x = this.game.mc._x - this.game.xOffset;
    this.lifeBar._y = this.game.mc._y + Data.GAME_HEIGHT * 0.5;
  }

  override public function init(g: GameMode): Void {
    super.init(g);
    this.register(Data.BOSS);
    this._firstUniq = this.game.getUniqId();
  }

  override public function onEndAnim(id: Int): Void {
    super.onEndAnim(id);
    if (id == Data.ANIM_BOSS_SWITCH.id) {
      this.walk();
    }
    if (id == Data.ANIM_BOSS_BAT_FORM.id) {
      this.walk();
    }
    if (id == Data.ANIM_BOSS_JUMP_LAND.id) {
      this.walk();
    }
    if (id == Data.ANIM_BOSS_DASH_START.id) {
      this.playAnim(Data.ANIM_BOSS_DASH_BUILD);
      this.setNext(null, null, Data.SECOND * 2, hf.entity.boss.Tuberculoz.DASH);
    }
    if (id == Data.ANIM_BOSS_DASH.id) {
      this.playAnim(Data.ANIM_BOSS_DASH_LOOP);
    }
    if (id == Data.ANIM_BOSS_BOMB.id) {
      this.spawnBombs(2);
      this.walk();
    }
    if (id == Data.ANIM_BOSS_HIT.id) {
      this.playAnim(Data.ANIM_BOSS_WAIT);
    }
    if (id == Data.ANIM_BOSS_BURN_START.id) {
      this.playAnim(Data.ANIM_BOSS_BURN_LOOP);
    }
    if (id == Data.ANIM_BOSS_TORNADO_START.id) {
      this.playAnim(Data.ANIM_BOSS_TORNADO_LOOP);
    }
    if (id == Data.ANIM_BOSS_TORNADO_END.id) {
      this.walk();
    }
  }

  override public function onNext(): Void {
    if (this.next.action == hf.entity.boss.Tuberculoz.DASH) {
      this.playAnim(Data.ANIM_BOSS_DASH);
      this.dx = this.dir * hf.entity.boss.Tuberculoz.DASH_SPEED;
      this.fl_hitBorder = false;
    }
    if (this.next.action == hf.entity.boss.Tuberculoz.BURN) {
      this.playAnim(Data.ANIM_BOSS_TORNADO_END);
      var v2 = 10;
      for (v3 in 0...v2) {
        var v4 = v3 * Data.GAME_WIDTH / v2 + Data.CASE_WIDTH;
        if (Math.abs(v4 - this.x) > 60) {
          var v5 = hf.entity.shoot.FireBall.attach(this.game, v4, Data.GAME_HEIGHT);
          v5.moveUp(hf.entity.boss.Tuberculoz.FIREBALL_SPEED);
        }
      }
    }
    if (this.next.action == hf.entity.boss.Tuberculoz.TORNADO) {
      if (!this.fl_twister) {
        this.fl_twister = true;
        this.setNext(null, null, hf.entity.boss.Tuberculoz.TORNADO_DURATION, hf.entity.boss.Tuberculoz.TORNADO);
        return;
      }
      this.playAnim(Data.ANIM_BOSS_TORNADO_END);
    }
    if (this.next.action == hf.entity.boss.Tuberculoz.DIE) {
      this._final();
    }
    this.next = null;
  }

  override public function update(): Void {
    if (!this.fl_death) {
      this.game.huTimer = 0;
    } else {
      this.game.huTimer += Timer.tmod * 3;
    }
    this.ia();
    if (this.fl_immune) {
      this.immuneTimer -= Timer.tmod;
      if (this.immuneTimer <= 0) {
        this.fl_immune = false;
        this.stopBlink();
      }
    }
    if (this.action == hf.entity.boss.Tuberculoz.JUMP) {
      if (this.dy >= 0 && this.animId == Data.ANIM_BOSS_JUMP_UP.id) {
        this.playAnim(Data.ANIM_BOSS_JUMP_DOWN);
      }
      if (this.dy > 0 && this.y >= hf.entity.boss.Tuberculoz.GROUND_Y) {
        this.land();
      }
    }
    if (!this.fl_death && this.lives <= 50) {
      this.fbCoolDown -= Timer.tmod;
      if (this.action == hf.entity.boss.Tuberculoz.WALK && this.fbCoolDown <= 0 && this.game.countList(Data.SHOOT) < 2 && HfStd.random(1000) <= hf.entity.boss.Tuberculoz.CHANCE_FINAL_ANGER) {
        this.fbCoolDown = Data.SECOND * 0.5;
        var v4 = hf.entity.shoot.FireBall.attach(this.game, this.x, this.y);
        v4.moveToTarget(this.game.getOne(Data.PLAYER), hf.entity.boss.Tuberculoz.FIREBALL_SPEED * 2);
      }
    }
    if (this.action == hf.entity.boss.Tuberculoz.HIT) {
      if (this.dy > 0 && this.y >= hf.entity.boss.Tuberculoz.GROUND_Y) {
        this.land();
      }
    }
    if (this.defeatTimeOut > 0 && !this.fl_defeated) {
      this.defeatTimeOut -= Timer.tmod;
      if (this.defeatTimeOut <= 0) {
        this._final();
      }
    }
    if (this.fl_death) {
      this.updateDeath();
    }
    super.update();
    if (this.action == hf.entity.boss.Tuberculoz.DASH) {
      this.updateDash();
    }
    if (this.game.manager.isDev() && Key.isDown(Key.ENTER)) {
      this.die();
    }
    if (this.game.manager.isDev() && Key.isDown(Key.SHIFT)) {
      this.lives -= 1;
      this.updateBar();
    }
    this.checkHits();
  }
}
