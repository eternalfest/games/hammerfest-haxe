package hf.entity.bad;

import hf.entity.Bad;
import hf.mode.GameMode;

class Walker extends Bad {
  public var speed: Float;
  public var fl_willFallDown: Bool;
  public var recentParticles: Float;
  public var fl_fall: Bool;
  public var chanceFall: Float;

  public function new(): Void {
    super();
    this.setFall(null);
    this.speed = 2;
    this.fl_willFallDown = false;
    this.recentParticles = 0;
  }

  public function halt(): Void {
    if (!this.fl_freeze && !this.fl_knock) {
      this.dx = 0;
    }
    this.dy = 0;
  }

  public function walk(): Void {
    var v2 = this.speedFactor * this.speed * this.game.speedFactor;
    if (!this.isReady()) {
      return;
    }
    if (this.dir == -1) {
      v2 = -v2;
    }
    if (this.fl_stable) {
      this.playAnim(Data.ANIM_BAD_WALK);
    }
    this.setNext(v2, 0, 0, Data.ACTION_MOVE);
  }

  public function fallBack(): Void {
    this.dx = -this.dx;
    if (!this.fl_freeze && !this.fl_knock) {
      this.dir = -this.dir;
    }
  }

  public function setFall(chance: Float): Void {
    if (chance == null) {
      this.fl_fall = false;
    } else {
      this.fl_fall = true;
      this.chanceFall = chance * 10;
    }
  }

  public function decideFall(): Bool {
    var v2 = this.player.cy - this.cy;
    var v3 = this.world.fallMap[this.cx][this.cy];
    var v4 = v3 > 0 && v2 > 0 && v3 <= v2 + 3;
    if (this.fl_playerClose) {
      return v4;
    }
    return HfStd.random(1000) < this.chanceFall;
  }

  public function onFall(): Void {
    if (!this.isReady()) {
      return;
    }
    if (this.world.checkFlag(Entity.rtc(this.oldX, this.oldY), Data.IA_SMALL_SPOT) && this.world.checkFlag({x: this.cx, y: this.cy}, Data.IA_ALLOW_FALL)) {
      this.halt();
      return;
    }
    if (this.fl_fall) {
      if (this.decideFall()) {
        if (this.world.checkFlag({x: this.cx, y: this.cy}, Data.IA_ALLOW_FALL)) {
          this.fl_willFallDown = true;
        }
      }
      if (this.fl_willFallDown) {
        this.fl_willFallDown = false;
        this.halt();
        return;
      }
    }
    this.x = this.oldX;
    this.y = this.oldY;
    this.fallBack();
  }

  override public function angerMore(): Void {
    super.angerMore();
    if (this.isReady()) {
      this.halt();
      this.walk();
    }
  }

  override public function calmDown(): Void {
    super.calmDown();
    if (this.animId == Data.ANIM_BAD_ANGER.id) {
      this.playAnim(Data.ANIM_BAD_WALK);
    }
    if (this.isReady()) {
      this.halt();
      this.walk();
    }
  }

  override public function endUpdate(): Void {
    super.endUpdate();
    if (!this.fl_freeze && !this.fl_knock) {
      if (this.dx < 0) {
        this.dir = -1;
      }
      if (this.dx > 0) {
        this.dir = 1;
      }
      this._xscale = this.dir * Math.abs(this._xscale);
    }
  }

  override public function infix(): Void {
    if (this.fl_stable && this.world.checkFlag({x: this.cx, y: this.cy}, Data.IA_FALL_SPOT)) {
      this.onFall();
      this.fl_stopStepping = true;
      return;
    }
    super.infix();
  }

  override public function init(g: GameMode): Void {
    super.init(g);
    if (this.game.fl_static) {
      if (this.x <= Data.GAME_WIDTH * 0.5) {
        this.dir = 1;
      } else {
        this.dir = -1;
      }
    } else {
      this.dir = HfStd.random(2) * 2 - 1;
    }
  }

  override public function isReady(): Bool {
    return super.isReady() && this.isHealthy();
  }

  override public function onFreeze(): Void {
    super.onFreeze();
    this.next = null;
  }

  override public function onHitGround(h: Float): Void {
    super.onHitGround(h);
    this.halt();
    this.walk();
  }

  override public function onHitWall(): Void {
    if (!this.fl_stopStepping) {
      if (this.world.getCase({x: this.cx, y: this.cy}) != Data.WALL) {
        if (!this.isHealthy() || this.fl_stable) {
          this.fallBack();
        }
      }
    }
    if (GameManager.CONFIG.fl_detail && this.fl_freeze && Math.abs(this.dx) >= 4) {
      if (this.recentParticles <= 0) {
        if (GameManager.CONFIG.fl_shaky) {
          this.game.shake(Data.SECOND * 0.2, 2);
        }
        this.game.fxMan.inGameParticles(Data.PARTICLE_ICE_BAD, this.x, this.y, HfStd.random(4) + 1);
        this.recentParticles = 10;
      }
      if (this.fl_stable) {
        this.game.fxMan.dust(this.cx, this.cy + 1);
      }
    }
  }

  override public function onKnock(): Void {
    super.onKnock();
    this.next = null;
  }

  override public function onMelt(): Void {
    super.onMelt();
    this.walk();
    if (!this.fl_stable) {
      this.playAnim(Data.ANIM_BAD_JUMP);
    }
  }

  override public function onNext(): Void {
    if (this.next.action == Data.ACTION_WALK) {
      this.next = null;
      this.walk();
    }
    if (this.next.action == Data.ACTION_FALLBACK) {
      this.next = null;
      this.walk();
      this.next.dx = -this.next.dx;
    }
    if (this.next.action == Data.ACTION_MOVE && this.next.dy == 0) {
      this.playAnim(Data.ANIM_BAD_WALK);
    }
    super.onNext();
  }

  override public function onWakeUp(): Void {
    super.onWakeUp();
    this.walk();
    if (!this.fl_stable) {
      this.playAnim(Data.ANIM_BAD_JUMP);
    }
  }

  override public function postfix(): Void {
    super.postfix();
    if (this.isReady()) {
      this.fl_friction = false;
    } else {
      this.fl_friction = true;
    }
  }

  override public function update(): Void {
    this.fl_willFallDown = false;
    if (this.recentParticles > 0) {
      this.recentParticles -= Timer.tmod;
    }
    super.update();
  }

  override public function updateSpeed(): Void {
    super.updateSpeed();
    this.walk();
  }
}
