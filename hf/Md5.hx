package hf;

class Md5 {
  public static var hex_chr: String = "0123456789abcdef";

  public function new(): Void {
  }

  public static function bitOR(a: Int, b: Int): Int {
    var v4 = (a & 1) | (b & 1);
    var v5 = a >>> 1 | b >>> 1;
    return v5 << 1 | v4;
  }

  public static function bitXOR(a: Int, b: Int): Int {
    var v4 = (a & 1) ^ (b & 1);
    var v5 = a >>> 1 ^ b >>> 1;
    return v5 << 1 | v4;
  }

  public static function bitAND(a: Int, b: Int): Int {
    var v4 = a & 1 & (b & 1);
    var v5 = a >>> 1 & b >>> 1;
    return v5 << 1 | v4;
  }

  public static function addme(x: Int, y: Int): Int {
    var v4 = (x & 65535) + (y & 65535);
    var v5 = (x >> 16) + (y >> 16) + (v4 >> 16);
    return (v5 << 16) | (v4 & 65535);
  }

  public static function rhex(num: Int): String {
    var v3 = "";
    var v4 = 0;
    while (v4 <= 3) {
      v3 += Md5.hex_chr.charAt(num >> v4 * 8 + 4 & 15) + Md5.hex_chr.charAt(num >> v4 * 8 & 15);
      ++v4;
    }
    return v3;
  }

  public static function str2blks(str: String): Array<Int> {
    var v5;
    var v3 = (str.length + 8 >> 6) + 1;
    var v4 = new Array();
    v5 = 0;
    while (v5 < v3 * 16) {
      v4[v5] = 0;
      ++v5;
    }
    v5 = 0;
    while (v5 < str.length) {
      v4[v5 >> 2] |= str.charCodeAt(v5) << ((str.length * 8 + v5) % 4) * 8;
      ++v5;
    }
    v4[v5 >> 2] |= 128 << ((str.length * 8 + v5) % 4) * 8;
    var v6 = str.length * 8;
    v4[v3 * 16 - 2] = v6 & 255;
    v4[v3 * 16 - 2] |= (v6 >>> 8 & 255) << 8;
    v4[v3 * 16 - 2] |= (v6 >>> 16 & 255) << 16;
    v4[v3 * 16 - 2] |= (v6 >>> 24 & 255) << 24;
    return v4;
  }

  public static function rol(num: Int, cnt: Int): Int {
    return num << cnt | num >>> 32 - cnt;
  }

  public static function cmn(q: Int, a: Int, b: Int, x: Int, s: Int, t: Int): Int {
    return Md5.addme(Md5.rol(Md5.addme(Md5.addme(a, q), Md5.addme(x, t)), s), b);
  }

  public static function ff(a: Int, b: Int, c: Int, d: Int, x: Int, s: Int, t: Int): Int {
    return Md5.cmn(Md5.bitOR(Md5.bitAND(b, c), Md5.bitAND(b ^ -1, d)), a, b, x, s, t);
  }

  public static function gg(a: Int, b: Int, c: Int, d: Int, x: Int, s: Int, t: Int): Int {
    return Md5.cmn(Md5.bitOR(Md5.bitAND(b, d), Md5.bitAND(c, d ^ -1)), a, b, x, s, t);
  }

  public static function hh(a: Int, b: Int, c: Int, d: Int, x: Int, s: Int, t: Int): Int {
    return Md5.cmn(Md5.bitXOR(Md5.bitXOR(b, c), d), a, b, x, s, t);
  }

  public static function ii(a: Int, b: Int, c: Int, d: Int, x: Int, s: Int, t: Int): Int {
    return Md5.cmn(Md5.bitXOR(c, Md5.bitOR(b, d ^ -1)), a, b, x, s, t);
  }

  public static function encode(str: String): String {
    var v3 = Md5.str2blks(str);
    var v4 = 1732584193;
    var v5 = -271733879;
    var v6 = -1732584194;
    var v7 = 271733878;
    var v9 = 0;
    while (v9 < v3.length) {
      var v10 = v4;
      var v11 = v5;
      var v12 = v6;
      var v13 = v7;
      var v8 = 0;
      v4 = Md5.ff(v4, v5, v6, v7, v3[v9 + 0], 7, -680876936);
      v7 = Md5.ff(v7, v4, v5, v6, v3[v9 + 1], 12, -389564586);
      v6 = Md5.ff(v6, v7, v4, v5, v3[v9 + 2], 17, 606105819);
      v5 = Md5.ff(v5, v6, v7, v4, v3[v9 + 3], 22, -1044525330);
      v4 = Md5.ff(v4, v5, v6, v7, v3[v9 + 4], 7, -176418897);
      v7 = Md5.ff(v7, v4, v5, v6, v3[v9 + 5], 12, 1200080426);
      v6 = Md5.ff(v6, v7, v4, v5, v3[v9 + 6], 17, -1473231341);
      v5 = Md5.ff(v5, v6, v7, v4, v3[v9 + 7], 22, -45705983);
      v4 = Md5.ff(v4, v5, v6, v7, v3[v9 + 8], 7, 1770035416);
      v7 = Md5.ff(v7, v4, v5, v6, v3[v9 + 9], 12, -1958414417);
      v6 = Md5.ff(v6, v7, v4, v5, v3[v9 + 10], 17, -42063);
      v5 = Md5.ff(v5, v6, v7, v4, v3[v9 + 11], 22, -1990404162);
      v4 = Md5.ff(v4, v5, v6, v7, v3[v9 + 12], 7, 1804603682);
      v7 = Md5.ff(v7, v4, v5, v6, v3[v9 + 13], 12, -40341101);
      v6 = Md5.ff(v6, v7, v4, v5, v3[v9 + 14], 17, -1502002290);
      v5 = Md5.ff(v5, v6, v7, v4, v3[v9 + 15], 22, 1236535329);
      v4 = Md5.gg(v4, v5, v6, v7, v3[v9 + 1], 5, -165796510);
      v7 = Md5.gg(v7, v4, v5, v6, v3[v9 + 6], 9, -1069501632);
      v6 = Md5.gg(v6, v7, v4, v5, v3[v9 + 11], 14, 643717713);
      v5 = Md5.gg(v5, v6, v7, v4, v3[v9 + 0], 20, -373897302);
      v4 = Md5.gg(v4, v5, v6, v7, v3[v9 + 5], 5, -701558691);
      v7 = Md5.gg(v7, v4, v5, v6, v3[v9 + 10], 9, 38016083);
      v6 = Md5.gg(v6, v7, v4, v5, v3[v9 + 15], 14, -660478335);
      v5 = Md5.gg(v5, v6, v7, v4, v3[v9 + 4], 20, -405537848);
      v4 = Md5.gg(v4, v5, v6, v7, v3[v9 + 9], 5, 568446438);
      v7 = Md5.gg(v7, v4, v5, v6, v3[v9 + 14], 9, -1019803690);
      v6 = Md5.gg(v6, v7, v4, v5, v3[v9 + 3], 14, -187363961);
      v5 = Md5.gg(v5, v6, v7, v4, v3[v9 + 8], 20, 1163531501);
      v4 = Md5.gg(v4, v5, v6, v7, v3[v9 + 13], 5, -1444681467);
      v7 = Md5.gg(v7, v4, v5, v6, v3[v9 + 2], 9, -51403784);
      v6 = Md5.gg(v6, v7, v4, v5, v3[v9 + 7], 14, 1735328473);
      v5 = Md5.gg(v5, v6, v7, v4, v3[v9 + 12], 20, -1926607734);
      v4 = Md5.hh(v4, v5, v6, v7, v3[v9 + 5], 4, -378558);
      v7 = Md5.hh(v7, v4, v5, v6, v3[v9 + 8], 11, -2022574463);
      v6 = Md5.hh(v6, v7, v4, v5, v3[v9 + 11], 16, 1839030562);
      v5 = Md5.hh(v5, v6, v7, v4, v3[v9 + 14], 23, -35309556);
      v4 = Md5.hh(v4, v5, v6, v7, v3[v9 + 1], 4, -1530992060);
      v7 = Md5.hh(v7, v4, v5, v6, v3[v9 + 4], 11, 1272893353);
      v6 = Md5.hh(v6, v7, v4, v5, v3[v9 + 7], 16, -155497632);
      v5 = Md5.hh(v5, v6, v7, v4, v3[v9 + 10], 23, -1094730640);
      v4 = Md5.hh(v4, v5, v6, v7, v3[v9 + 13], 4, 681279174);
      v7 = Md5.hh(v7, v4, v5, v6, v3[v9 + 0], 11, -358537222);
      v6 = Md5.hh(v6, v7, v4, v5, v3[v9 + 3], 16, -722521979);
      v5 = Md5.hh(v5, v6, v7, v4, v3[v9 + 6], 23, 76029189);
      v4 = Md5.hh(v4, v5, v6, v7, v3[v9 + 9], 4, -640364487);
      v7 = Md5.hh(v7, v4, v5, v6, v3[v9 + 12], 11, -421815835);
      v6 = Md5.hh(v6, v7, v4, v5, v3[v9 + 15], 16, 530742520);
      v5 = Md5.hh(v5, v6, v7, v4, v3[v9 + 2], 23, -995338651);
      v4 = Md5.ii(v4, v5, v6, v7, v3[v9 + 0], 6, -198630844);
      v7 = Md5.ii(v7, v4, v5, v6, v3[v9 + 7], 10, 1126891415);
      v6 = Md5.ii(v6, v7, v4, v5, v3[v9 + 14], 15, -1416354905);
      v5 = Md5.ii(v5, v6, v7, v4, v3[v9 + 5], 21, -57434055);
      v4 = Md5.ii(v4, v5, v6, v7, v3[v9 + 12], 6, 1700485571);
      v7 = Md5.ii(v7, v4, v5, v6, v3[v9 + 3], 10, -1894986606);
      v6 = Md5.ii(v6, v7, v4, v5, v3[v9 + 10], 15, -1051523);
      v5 = Md5.ii(v5, v6, v7, v4, v3[v9 + 1], 21, -2054922799);
      v4 = Md5.ii(v4, v5, v6, v7, v3[v9 + 8], 6, 1873313359);
      v7 = Md5.ii(v7, v4, v5, v6, v3[v9 + 15], 10, -30611744);
      v6 = Md5.ii(v6, v7, v4, v5, v3[v9 + 6], 15, -1560198380);
      v5 = Md5.ii(v5, v6, v7, v4, v3[v9 + 13], 21, 1309151649);
      v4 = Md5.ii(v4, v5, v6, v7, v3[v9 + 4], 6, -145523070);
      v7 = Md5.ii(v7, v4, v5, v6, v3[v9 + 11], 10, -1120210379);
      v6 = Md5.ii(v6, v7, v4, v5, v3[v9 + 2], 15, 718787259);
      v5 = Md5.ii(v5, v6, v7, v4, v3[v9 + 9], 21, -343485551);
      v4 = Md5.addme(v4, v10);
      v5 = Md5.addme(v5, v11);
      v6 = Md5.addme(v6, v12);
      v7 = Md5.addme(v7, v13);
      v9 += 16;
    }
    return Md5.rhex(v4) + Md5.rhex(v5) + Md5.rhex(v6) + Md5.rhex(v7);
  }
}
