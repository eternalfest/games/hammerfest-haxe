package hf.gui;

import hf.gui.Item;

class Label extends Item {
  public function new(): Void {
    super();
  }

  public static function attach(c: Container, l: String): Label {
    var v4: Label = cast c.depthMan.attach("hammer_editor_label", Data.DP_INTERF);
    v4.init(c, l);
    return v4;
  }
}
