package hf.compat;

class StringTools {

  public static inline function slice(str: String, start: Int, end: Int): String {
    return untyped str.slice(start, end);
  }

}