package hf.entity.shoot;

import hf.entity.Shoot;
import hf.Entity;
import hf.mode.GameMode;

class PlayerFireBall extends Shoot {
  public function new(): Void {
    super();
    this.shootSpeed = 8;
    this.coolDown = Data.SECOND * 2;
  }

  public static function attach(g: GameMode, x: Float, y: Float): PlayerFireBall {
    var v5 = "hammer_shoot_player_fireball";
    var v6: PlayerFireBall = cast g.depthMan.attach(v5, Data.DP_SHOTS);
    v6.initShoot(g, x, y);
    return v6;
  }

  override public function destroy(): Void {
    this.game.fxMan.inGameParticles(Data.PARTICLE_SPARK, this.x, this.y, 3);
    this.game.fxMan.inGameParticles(Data.PARTICLE_STONE, this.x, this.y, 4);
    super.destroy();
  }

  override public function hit(e: Entity): Void {
    var v3 = Data.BAD_CLEAR.downcast(e);
    if (v3 != null) {
      v3.setCombo(this.uniqId);
      v3.burn();
    }
  }

  override public function init(g: GameMode): Void {
    super.init(g);
    this.register(Data.PLAYER_SHOOT);
  }

  override public function update(): Void {
    super.update();
    if (HfStd.random(3) == 0) {
      this.game.fxMan.inGameParticles(Data.PARTICLE_SPARK, this.x, this.y, HfStd.random(3));
    }
  }
}
