package hf.entity.bomb.player;

import hf.entity.bomb.PlayerBomb;
import hf.mode.GameMode;

class RepelBomb extends PlayerBomb {
  public function new(): Void {
    super();
    this.duration = 38;
    this.power = 20;
    this.radius = Data.CASE_WIDTH * 3;
  }

  public static function attach(g: GameMode, x: Float, y: Float): RepelBomb {
    var v5 = "hammer_bomb_repel";
    var v6: RepelBomb = cast g.depthMan.attach(v5, Data.DP_BOMBS);
    v6.initBomb(g, x, y);
    return v6;
  }

  override public function duplicate(): RepelBomb {
    return hf.entity.bomb.player.RepelBomb.attach(this.game, this.x, this.y);
  }

  override public function onExplode(): Void {
    super.onExplode();
    this.game.fxMan.inGameParticles(Data.PARTICLE_ICE, this.x, this.y, HfStd.random(2) + 2);
    this.game.fxMan.attachExplodeZone(this.x, this.y, this.radius);
    var v3 = this.game.getPlayerList();
    for (v4 in 0...v3.length) {
      var v5 = v3[v4];
      var v6 = this.distance(v5.x, v5.y);
      if (v6 <= this.radius) {
        var v7 = (this.radius - v6) / this.radius;
        v5.knock(Data.SECOND + Data.SECOND * v7);
        var v8 = Math.atan2(v5.y - this.y, v5.x - this.x);
        v5.dx = Math.cos(v8) * this.power * v7;
        v5.dy = Math.sin(v8) * this.power * v7;
      }
    }
    var v9 = this.game.getList(Data.SOCCERBALL);
    for (v10 in 0...v9.length) {
      var v11 = v9[v10];
      var v12 = this.distance(v11.x, v11.y);
      if (v12 <= this.radius) {
        v11.lastPlayer = this.owner;
        var v13 = (this.radius - v12) / this.radius;
        var v14 = Math.atan2(v11.y - this.y, v11.x - this.x);
        v11.dx = Math.cos(v14) * this.power * v13 * 1.5;
        v11.dy = Math.sin(v14) * this.power * v13 * 1.5;
        if (v11.dy < 4 && v11.dy > -8) {
          v11.dy = -8;
        }
        v11.burn();
        if (v11.fl_stable) {
          v11.dx *= 2;
        } else {
          v11.dx *= 1.5;
        }
      }
    }
  }

  override public function onKick(p: Player): Void {
    super.onKick(p);
    this.dx *= 2.5;
  }
}
