package hf.entity.bad.walker;

import hf.entity.bad.Jumper;
import hf.mode.GameMode;

class Orange extends Jumper {
  public function new(): Void {
    super();
    this.setJumpH(100);
  }

  public static function attach(g: GameMode, x: Float, y: Float): Orange {
    var v5 = Data.LINKAGES[Data.BAD_ORANGE];
    var v6: Orange = cast g.depthMan.attach(v5, Data.DP_BADS);
    v6.initBad(g, x, y);
    return v6;
  }
}
