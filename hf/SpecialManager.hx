package hf;

import etwin.flash.MovieClip;
import etwin.flash.DynamicMovieClip;
import hf.entity.item.SpecialItem;
import hf.entity.Player;
import hf.mode.GameMode;
import etwin.flash.Date;

typedef RecEffect = {
  var timer: Float;
  var baseTimer: Float;
  var func: Void -> Void;
  var fl_repeat: Bool;
}

typedef TempEffect = {
  var id: Int;
  var end: Float;
}

class SpecialManager {
  public var game: GameMode;
  public var player: Player;
  public var permList: Array<Int>;
  public var tempList: Array<TempEffect>;
  public var actives: Array<Bool>;
  public var recurring: Array<RecEffect>;
  public var clouds: Array<MovieClip>;
  public var phoneMC: DynamicMovieClip /* TODO */;

  public function new(g: GameMode, p: Player): Void {
    this.game = g;
    this.player = p;
    this.permList = new Array();
    this.tempList = new Array();
    this.actives = new Array();
    this.recurring = new Array();
    this.clouds = new Array();
  }

  public function permanent(id: Int): Void {
    if (this.actives[id] != true) {
      this.actives[id] = true;
      this.permList.push(id);
    }
  }

  public function temporary(id: Int, duration: Null<Float>): Void {
    if (duration == null) {
      duration = 99999;
    }
    if (this.actives[id] != true) {
      this.actives[id] = true;
      this.tempList.push({id: id, end: this.game.cycle + duration});
    }
  }

  public function global(id: Int): Void {
    this.game.globalActives[id] = true;
  }

  public function clearTemp(): Void {
    while (this.tempList.length > 0) {
      this.interrupt(this.tempList[0].id);
      this.tempList.splice(0, 1);
    }
  }

  public function clearPerm(): Void {
    while (this.permList.length > 0) {
      this.interrupt(this.permList[0]);
      this.permList.splice(0, 1);
    }
  }

  public function clearRec(): Void {
    this.recurring = new Array();
  }

  public function registerRecurring(func: Void -> Void, t: Float, fl_repeat: Bool): Void {
    this.recurring.push({timer: t * 1.0, baseTimer: t, func: func, fl_repeat: fl_repeat});
  }

  public function levelConversion(id: Int, sid: Int): Void {
    var v4 = this.game.world.scriptEngine.script.toString();
    this.game.world.scriptEngine.safeMode();
    this.game.killPop();
    var v5 = 0;
    for (v6 in 0...Data.LEVEL_HEIGHT) {
      for (v7 in 0...Data.LEVEL_WIDTH) {
        if (this.game.world.checkFlag({x: v7, y: v6}, Data.IA_TILE_TOP)) {
          var v8 = v5 * 2;
          if (v5 < 4) {
            v8 = 1;
          }
          this.game.world.scriptEngine.insertScoreItem(id, sid, this.game.flipCoordCase(v7), v6, v8, null, true, false);
          ++v5;
        }
      }
    }
    this.game.perfectItemCpt = v5;
  }

  public function getZodiac(id: Int): Void {
    this.game.fxMan.attachBg(Data.BG_CONSTEL, id, Data.SECOND * 4);
    var v3 = this.game.getBadClearList();
    for (v4 in 0...v3.length) {
      hf.entity.item.ScoreItem.attach(this.game, v3[v4].x, v3[v4].y - Data.CASE_HEIGHT * 2, 169, 0);
    }
  }

  public function getZodiacPotion(id: Int): Void {
    var v3 = this.game.getBadClearList();
    for (v4 in 0...v3.length) {
      hf.entity.item.ScoreItem.attach(this.game, v3[v4].x, v3[v4].y - Data.CASE_HEIGHT * 2, 169, 0);
    }
  }

  public function onPerfect(): Void {
    this.interrupt(81);
    this.interrupt(96);
    this.interrupt(97);
    this.interrupt(98);
    var v2 = 50000;
    var v3 = ((cast this.game.depthMan.attach("hammer_fx_perfect", Data.DP_INTERF)): DynamicMovieClip);
    v3._x = Data.GAME_WIDTH * 0.5;
    v3._y = Data.GAME_HEIGHT * 0.5;
    v3.label = Lang.get(11);
    if (this.actives[95]) {
      v3.bonus = "" + v2 * 2;
    } else {
      v3.bonus = "" + v2;
    }
    var v4 = this.game.getPlayerList();
    for (v5 in 0...v4.length) {
      v4[v5].getScoreHidden(Math.floor(v2 / v4.length));
    }
  }

  public function onPickPerfectItem(): Void {
    --this.game.perfectItemCpt;
    if (this.game.perfectItemCpt <= 0) {
      this.onPerfect();
    }
  }

  public function executeExtend(fl_perfect: Bool): Void {
    this.game.registerMapEvent(Data.EVENT_EXTEND, null);
    this.game.manager.logAction("$ext");
    var v3 = this.game.fxMan.attachFx(Data.GAME_WIDTH / 2, Data.GAME_HEIGHT / 2, "extendSequence");
    v3.lifeTimer = 9999;
    this.game.destroyList(Data.BAD);
    this.game.destroyList(Data.BAD_BOMB);
    this.game.destroyList(Data.SHOOT);
    ++this.player.lives;
    this.game.gi.setLives(this.player.pid, this.player.lives);
    if (fl_perfect) {
      var v4: etwin.flash.DynamicMovieClip = cast this.game.depthMan.attach("hammer_fx_perfect", Data.DP_INTERF);
      v4._x = Data.GAME_WIDTH * 0.5;
      v4._y = Data.GAME_HEIGHT * 0.2;
      v4.label = Lang.get(11);
      if (this.actives[95]) {
        v4.bonus = "300000";
      } else {
        v4.bonus = "150000";
      }
      this.player.getScoreHidden(150000);
    }
  }

  public function warpZone(w: Int): Void {
    var v3 = this.game.world.currentId + w;
    this.game.manager.logAction("$WZ>" + v3);
    var v4 = this.game.world.currentId + 1;
    while (v4 <= v3) {
      if (this.game.isBossLevel(v4)) {
        v3 = v4;
      }
      if (this.game.world.isEmptyLevel(v4, this.game)) {
        v3 = v4 - 1;
      }
      ++v4;
    }
    if (v3 == this.game.world.currentId) {
      this.game.fxMan.attachAlert(Lang.get(34));
      return;
    }
    this.game.world.view.detach();
    this.game.forcedGoto(v3);
  }

  public function execute(item: SpecialItem): Void {
    var v17;
    var v3 = item.id;
    var v4 = item.subId;
    var v5 = 0;
    switch (v3) {
      case 0:
        if (this.actives[27]) {
          this.player.getScore(item, 25);
        }
        this.player.getScoreHidden(5);
        this.player.getExtend(v4);
      case 1:
        this.player.shield(Data.SECOND * 10);
      case 2:
        this.player.shield(Data.SECOND * 60);
      case 3:
        hf.entity.supa.Ball.attach(this.game);
      case 4:
        if (!this.actives[5]) {
          this.player.maxBombs = this.player.initialMaxBombs + 1;
          this.permanent(v3);
        }
      case 5:
        this.player.maxBombs = this.player.initialMaxBombs + 4;
        this.permanent(v3);
      case 6:
        var v6 = this.game.getBadClearList();
        for (v7 in 0...v6.length) {
          v6[v7].freeze(Data.FREEZE_DURATION * 2);
        }
      case 7:
        this.player.speedFactor = 1.5;
        this.permanent(v3);
      case 8:
        hf.entity.supa.Bubble.attach(this.game);
        hf.entity.supa.Bubble.attach(this.game);
      case 9:
        hf.entity.supa.Tons.attach(this.game);
      case 10:
        null;
      case 11:
        this.warpZone(1);
      case 12:
        this.warpZone(2);
      case 13:
        this.permanent(v3);
      case 14:
        this.permanent(v3);
        this.interrupt(15);
        this.interrupt(16);
        this.interrupt(17);
      case 15:
        this.permanent(v3);
        this.interrupt(14);
        this.interrupt(16);
        this.interrupt(17);
      case 16:
        this.permanent(v3);
        this.interrupt(14);
        this.interrupt(15);
        this.interrupt(17);
      case 17:
        this.permanent(v3);
        this.interrupt(14);
        this.interrupt(15);
        this.interrupt(16);
      case 18:
        this.permanent(v3);
        this.player.fallFactor = 0.55;
      case 19:
        hf.entity.supa.Smoke.attach(this.game);
        var v8 = this.game.getBadClearList();
        this.game.fxMan.attachBg(Data.BG_ORANGE, null, Data.SECOND * 3);
        for (v9 in 0...v8.length) {
          v8[v9].forceKill(null);
        }
      case 20:
        for (v10 in 0...5) {
          var v11 = hf.entity.item.ScoreItem.attach(this.game, item.x, item.y, 0, HfStd.random(4));
          v11.moveFrom(item, 8);
        }
      case 21:
        var v12 = this.game.getOne(Data.BAD_CLEAR);
        if (v12 != null) {
          this.game.fxMan.attachFx(v12.x, v12.y - Data.CASE_HEIGHT, "hammer_fx_pop");
          v12.forceKill(null);
          this.game.fxMan.attachBg(Data.BG_SINGER, null, Data.SECOND * 3);
        }
      case 22:
        this.interrupt(7);
        this.player.curse(Data.CURSE_SLOW);
        this.player.speedFactor = 0.6;
        this.temporary(v3, Data.SECOND * 40);
      case 23:
        var v13 = this.game.getBadClearList();
        for (v14 in 0...v13.length) {
          var v15 = v13[v14];
          var v16 = hf.entity.shoot.PlayerPearl.attach(this.game, this.player.x, this.player.y - Data.CASE_WIDTH);
          v16.moveToTarget(v13[v14], v16.shootSpeed);
          v16.fl_borderBounce = true;
          v16.setLifeTimer(Data.SECOND * 3 + HfStd.random(400) / 10);
          v16._yOffset = 0;
          v16.endUpdate();
        }
      case 24:
        hf.entity.supa.IceMeteor.attach(this.game);
      case 25:
        for (v18 in 0...4) {
          v17 = hf.entity.shoot.PlayerFireBall.attach(this.game, Data.GAME_WIDTH * 0.125 + Data.GAME_WIDTH * 0.25 * v18, 10);
          v17.moveDown(v17.shootSpeed);
        }
        for (v19 in 0...4) {
          v17 = hf.entity.shoot.PlayerFireBall.attach(this.game, Data.GAME_WIDTH * 0.25 + Data.GAME_WIDTH * 0.25 * v19, Data.GAME_HEIGHT - 10);
          v17.moveUp(v17.shootSpeed);
        }
      case 26:
        this.permanent(v3);
        this.game.updateDarkness();
      case 27:
        this.permanent(v3);
      case 28:
        this.game.fxMan.attachBg(Data.BG_STAR, null, Data.SECOND * 9);
        var v20 = this.game.getBadClearList();
        for (v21 in 0...v20.length) {
          v20[v21].knock(Data.SECOND * 10);
        }
      case 29:
        this.player.changeWeapon(Data.WEAPON_S_FIRE);
        this.temporary(v3, Data.WEAPON_DURATION);
      case 30:
        this.game.flipX(true);
        this.temporary(v3, Data.SECOND * 30);
      case 31:
        this.game.flipY(true);
        this.temporary(v3, Data.SECOND * 30);
      case 32:
        var v22 = this.game.getBadClearList();
        for (v23 in 0...v22.length) {
          v22[v23].destroy();
          hf.entity.item.ScoreItem.attach(this.game, v22[v23].x, v22[v23].y - Data.CASE_HEIGHT, 0, 0);
        }
      case 33:
        var v24 = this.game.getBadClearList();
        for (v25 in 0...v24.length) {
          v24[v25].destroy();
          hf.entity.item.ScoreItem.attach(this.game, v24[v25].x, v24[v25].y - Data.CASE_HEIGHT, 0, 2);
        }
      case 34:
        var v26 = this.game.getBadClearList();
        for (v27 in 0...v26.length) {
          v26[v27].destroy();
          hf.entity.item.ScoreItem.attach(this.game, v26[v27].x, v26[v27].y - Data.CASE_HEIGHT, 0, 5);
        }
      case 35:
        var v28 = this.game.getBadClearList();
        for (v29 in 0...v28.length) {
          v28[v29].destroy();
          hf.entity.item.ScoreItem.attach(this.game, v28[v29].x - Data.CASE_WIDTH, v28[v29].y - Data.CASE_HEIGHT, 0, 6);
          hf.entity.item.ScoreItem.attach(this.game, v28[v29].x + Data.CASE_WIDTH, v28[v29].y - Data.CASE_HEIGHT, 0, 6);
        }
      case 36:
        ++this.player.lives;
        this.game.gi.setLives(this.player.pid, this.player.lives);
        this.game.fxMan.attachShine(item.x, item.y - Data.CASE_HEIGHT * 0.5);
      case 37:
        this.player.changeWeapon(Data.WEAPON_S_ICE);
        this.temporary(v3, Data.WEAPON_DURATION);
      case 38:
        var v30 = hf.entity.supa.Arrow.attach(this.game);
        v30.setLifeTimer(Data.SUPA_DURATION);
      case 39:
        this.player.fallFactor = 1.6;
        this.temporary(v3, null);
      case 40:
        this.getZodiac(v3 - 40);
      case 41:
        this.getZodiac(v3 - 40);
      case 42:
        this.getZodiac(v3 - 40);
      case 43:
        this.getZodiac(v3 - 40);
      case 44:
        this.getZodiac(v3 - 40);
      case 45:
        this.getZodiac(v3 - 40);
      case 46:
        this.getZodiac(v3 - 40);
      case 47:
        this.getZodiac(v3 - 40);
      case 48:
        this.getZodiac(v3 - 40);
      case 49:
        this.getZodiac(v3 - 40);
      case 50:
        this.getZodiac(v3 - 40);
      case 51:
        this.getZodiac(v3 - 40);
      case 52:
        this.getZodiacPotion(v3 - 40);
      case 53:
        this.getZodiacPotion(v3 - 40);
      case 54:
        this.getZodiacPotion(v3 - 40);
      case 55:
        this.getZodiacPotion(v3 - 40);
      case 56:
        this.getZodiacPotion(v3 - 40);
      case 57:
        this.getZodiacPotion(v3 - 40);
      case 58:
        this.getZodiacPotion(v3 - 40);
      case 59:
        this.getZodiacPotion(v3 - 40);
      case 60:
        this.getZodiacPotion(v3 - 40);
      case 61:
        this.getZodiacPotion(v3 - 40);
      case 62:
        this.getZodiacPotion(v3 - 40);
      case 63:
        this.getZodiacPotion(v3 - 40);
      case 64:
        for (v31 in 0...5) {
          var v32 = {x: HfStd.random(Data.LEVEL_WIDTH), y: HfStd.random(Data.LEVEL_HEIGHT)};
          v32 = this.game.world.getGround(v32.x, v32.y);
          var v33 = hf.entity.item.SpecialItem.attach(this.game, v32.x * Data.CASE_WIDTH, v32.y * Data.CASE_HEIGHT, 0, HfStd.random(7));
        }
      case 65:
        var v34 = this.game.getBadList();
        for (v35 in 0...v34.length) {
          if (Data.BAD_CLEAR.check(v34[v35])) {
            this.player.getScore(v34[v35], 2500);
          } else {
            this.player.getScore(v34[v35], 600);
          }
        }
      case 66:
        this.permanent(v3);
      case 67:
        this.player.changeWeapon(Data.WEAPON_S_ARROW);
        this.temporary(v3, Data.WEAPON_DURATION);
      case 68:
        this.permanent(v3);
        this.game.updateDarkness();
      case 69:
        this.global(v3);
        this.temporary(v3, Data.SECOND * 30);
        var v36 = this.game.getBadClearList();
        for (v37 in 0...v36.length) {
          v36[v37].updateSpeed();
          v36[v37].animFactor *= 0.6;
        }
      case 70:
        this.permanent(v3);
      case 71:
        var v38 = hf.entity.shoot.PlayerFireBall.attach(this.game, item.x, item.y);
        v38.moveLeft(v38.shootSpeed);
        v38 = hf.entity.shoot.PlayerFireBall.attach(this.game, item.x, item.y);
        v38.moveRight(v38.shootSpeed);
      case 72:
        this.game.destroyList(Data.ITEM);
        this.game.destroyList(Data.BAD_BOMB);
        var v39 = this.game.getList(Data.BAD_CLEAR);
        var v40 = 1;
        for (v41 in 0...v39.length) {
          var v42 = v39[v41];
          hf.entity.item.ScoreItem.attach(this.game, v42.x, v42.y, 0, v40);
          v42.destroy();
          ++v40;
        }
      case 73:
        this.permanent(v3);
      case 74:
        for (v43 in 0...7) {
          var v44 = hf.entity.item.ScoreItem.attach(this.game, HfStd.random(Data.GAME_WIDTH), Data.GAME_HEIGHT, 3, null);
          v44.moveToAng(-20 - HfStd.random(160), HfStd.random(15) + 10);
        }
      case 75:
        for (v45 in 0...7) {
          var v46 = hf.entity.item.ScoreItem.attach(this.game, HfStd.random(Data.GAME_WIDTH), Data.GAME_HEIGHT, 4, null);
          v46.moveToAng(-20 - HfStd.random(160), HfStd.random(15) + 10);
        }
      case 76:
        for (v47 in 0...7) {
          var v48 = hf.entity.item.ScoreItem.attach(this.game, HfStd.random(Data.GAME_WIDTH), Data.GAME_HEIGHT, 5, null);
          v48.moveToAng(-20 - HfStd.random(160), HfStd.random(15) + 10);
        }
      case 77:
        this.game.endLevelStack.push(function () {
          for (v2 in 0...5) {
            hf.entity.item.ScoreItem.attach(this.game, HfStd.random(Data.GAME_WIDTH), -30 - HfStd.random(50), 0, 0);
          }
        });
      case 78:
        this.game.endLevelStack.push(function () {
          for (v2 in 0...5) {
            hf.entity.item.ScoreItem.attach(this.game, HfStd.random(Data.GAME_WIDTH), -30 - HfStd.random(50), 0, 2);
          }
        });
      case 79:
        this.game.endLevelStack.push(function () {
          for (v2 in 0...5) {
            hf.entity.item.ScoreItem.attach(this.game, HfStd.random(Data.GAME_WIDTH), -30 - HfStd.random(50), 0, 3);
          }
        });
      case 80:
        this.global(v3);
        this.temporary(v3, Data.SECOND * 30);
        var v49 = this.game.getBadClearList();
        for (v50 in 0...v49.length) {
          v49[v50].updateSpeed();
          v49[v50].animFactor *= 0.3;
        }
      case 81:
        this.game.destroyList(Data.BAD);
        this.game.destroyList(Data.ITEM);
        this.game.destroyList(Data.BAD_BOMB);
        this.levelConversion(Data.CONVERT_DIAMANT, 0);
        this.temporary(v3, Data.SECOND * 19);
      case 82:
        this.onStrike();
      case 83:
        this.temporary(v3, null);
        this.registerRecurring(HfStd.callback(this, "onStrike"), Data.SECOND, true);
        this.onStrike();
        this.game.fxMan.attachBg(Data.BG_PYRAMID, null, 9999);
      case 84:
        var v51: etwin.flash.DynamicMovieClip = cast this.game.depthMan.attach("hammer_fx_clouds", Data.DP_SPRITE_BACK_LAYER);
        v51.speed = 0.5;
        v51._y += 9;
        this.clouds.push(v51);
        var v52 = new etwin.flash.filters.BlurFilter();
        v52.blurX = 4;
        v52.blurY = v52.blurX;
        v51.filters = [v52];
        v51 = cast this.game.depthMan.attach("hammer_fx_clouds", Data.DP_SPRITE_TOP_LAYER);
        v51.speed = 1;
        this.clouds.push(v51);
        this.temporary(v3, null);
        this.registerRecurring(HfStd.callback(this, "onFireRain"), Data.SECOND * 0.8, true);
        this.onFireRain();
        this.game.fxMan.attachBg(Data.BG_STORM, null, 9999);
      case 85:
        var v53 = hf.entity.shoot.Hammer.attach(this.game, this.player.x, this.player.y);
        v53.setOwner(this.player);
        this.temporary(v3, null);
      case 86:
        var v54 = new etwin.flash.filters.GlowFilter();
        v54.color = 9224447;
        v54.alpha = 0.5;
        v54.strength = 100;
        v54.blurX = 2;
        v54.blurY = 2;
        this.player.filters = [v54];
        this.temporary(v3, Data.SECOND * 60);
        this.game.fxMan.attachBg(Data.BG_GHOSTS, null, Data.SECOND * 57);
        var v55 = this.game.getBadList();
        for (v56 in 0...v55.length) {
          v54.alpha = 1.0;
          v54.color = 16733440;
          v55[v56].filters = [v54];
        }
      case 87:
        var v57 = this.game.getOne(Data.BAD_CLEAR);
        if (v57 != null) {
          if ((this.game.getBadClearList()).length == 1) {
            hf.entity.item.ScoreItem.attach(this.game, v57.x, v57.y, Data.DIAMANT, null);
          } else {
            hf.entity.item.SpecialItem.attach(this.game, v57.x, v57.y, v3, v4);
          }
          this.game.fxMan.attachShine(v57.x, v57.y - Data.CASE_HEIGHT * 0.5);
          v57.destroy();
        }
      case 88:
        this.player.curse(Data.CURSE_SHRINK);
        this.game.fxMan.attachShine(this.player.x, this.player.y);
        this.player.scale(50);
        this.temporary(v3, Data.SECOND * 30);
      case 89:
        var v58 = this.game.getOne(Data.BAD_CLEAR);
        if (v58 != null) {
          hf.entity.bad.flyer.Tzongre.attach(this.game, v58.x, v58.y - Data.CASE_HEIGHT);
          v58.destroy();
        }
      case 90:
        this.temporary(v3, Data.SECOND * 40);
        this.player.curse(6);
      case 91:
        this.player.curse(Data.CURSE_PEACE);
        this.game.fxMan.attachShine(this.player.x, this.player.y);
        this.temporary(v3, Data.SECOND * 15);
      case 92:
        this.permanent(v3);
      case 93:
        this.game.destroyList(Data.BAD);
        this.game.destroyList(Data.ITEM);
        this.game.destroyList(Data.BAD_BOMB);
        var v59 = 6;
        do {
          var v60 = {x: HfStd.random(Data.GAME_WIDTH), y: HfStd.random(Data.GAME_HEIGHT)};
          if (this.player.distance(v60.x, v60.y) >= 100) {
            var v61 = hf.entity.item.SpecialItem.attach(this.game, v60.x, v60.y, 101, null);
            v61.setLifeTimer(null);
            --v59;
          }
        } while (v59 > 0);
      case 94:
        this.global(v3);
        this.permanent(v3);
      case 95:
        this.player.curse(Data.CURSE_MULTIPLY);
        this.permanent(v3);
      case 96:
        this.game.destroyList(Data.BAD);
        this.game.destroyList(Data.ITEM);
        this.game.destroyList(Data.BAD_BOMB);
        this.levelConversion(Data.CONVERT_DIAMANT, 1);
        this.temporary(v3, Data.SECOND * 19);
      case 97:
        this.game.destroyList(Data.BAD);
        this.game.destroyList(Data.ITEM);
        this.game.destroyList(Data.BAD_BOMB);
        this.levelConversion(Data.CONVERT_DIAMANT, 2);
        this.temporary(v3, Data.SECOND * 19);
      case 98:
        this.game.destroyList(Data.BAD);
        this.game.destroyList(Data.ITEM);
        this.game.destroyList(Data.BAD_BOMB);
        this.levelConversion(Data.CONVERT_DIAMANT, 3);
        this.temporary(v3, Data.SECOND * 19);
      case 99:
        this.registerRecurring(HfStd.callback(this, "onPoT"), Data.SECOND * 2, true);
        this.player.fl_chourou = true;
        this.temporary(v3, null);
      case 100:
        this.temporary(v3, Data.SECOND * 30);
        this.game.fxMan.attachBg(Data.BG_GUU, null, Data.SECOND * 30);
        var v62 = this.game.depthMan.attach("hammer_fx_cloud", Data.DP_PLAYER);
        this.player.stick(v62, 0, -80);
        this.player.setElaStick(0.4);
      case 101:
        if (HfStd.random(2) == 0) {
          this.player.getScore(item, 5000);
        } else {
          var v63 = hf.entity.bomb.bad.PoireBomb.attach(this.game, item.x, item.y);
          v63.moveUp(10);
        }
      case 102:
        this.game.world.scriptEngine.playById(100);
        this.game.huTimer = 0;
        this.player.getScore(item, 4 * 25000);
        this.player.fl_carot = true;
      case 103:
        ++this.player.lives;
        this.game.gi.setLives(this.player.pid, this.player.lives);
        this.game.fxMan.attachShine(item.x, item.y - Data.CASE_HEIGHT * 0.5);
        this.game.randMan.remove(Data.RAND_ITEMS_ID, v3);
      case 104:
        ++this.player.lives;
        this.game.gi.setLives(this.player.pid, this.player.lives);
        this.game.fxMan.attachShine(item.x, item.y - Data.CASE_HEIGHT * 0.5);
        this.game.randMan.remove(Data.RAND_ITEMS_ID, v3);
      case 105:
        ++this.player.lives;
        this.game.gi.setLives(this.player.pid, this.player.lives);
        this.game.fxMan.attachShine(item.x, item.y - Data.CASE_HEIGHT * 0.5);
        this.game.randMan.remove(Data.RAND_ITEMS_ID, v3);
      case 106:
        for (v64 in 0...5) {
          var v65 = hf.entity.item.ScoreItem.attach(this.game, item.x, item.y, 1047 + HfStd.random(4), null);
          v65.moveFrom(item, 8);
        }
      case 107:
        for (v66 in 0...5) {
          var v67 = hf.entity.item.ScoreItem.attach(this.game, item.x, item.y, 0, 0);
          v67.moveFrom(item, 8);
        }
      case 108:
        this.warpZone(3);
      case 109:
        this.game.fxMan.attachShine(item.x, item.y - Data.CASE_HEIGHT * 0.5);
        this.game.randMan.remove(Data.RAND_ITEMS_ID, v3);
      case 110:
        this.game.fxMan.attachShine(item.x, item.y - Data.CASE_HEIGHT * 0.5);
      case 111:
        this.game.fxMan.attachShine(item.x, item.y - Data.CASE_HEIGHT * 0.5);
      case 112:
        this.player.head = Data.HEAD_PIOU;
        this.player.replayAnim();
      case 113:
        this.player.getScore(item, 75000);
        this.player.head = Data.HEAD_TUB;
        this.player.replayAnim();
      case 114:
        this.temporary(v3, null);
        this.player.curse(Data.CURSE_MARIO);
      case 115:
        this.permanent(v3);
      case 116:
        ++this.player.lives;
        this.game.gi.setLives(this.player.pid, this.player.lives);
        this.player.getScore(item, 70000);
      case 117:
        this.game.giveKey(12);
        this.player.getScore(item, 10000);
      default:
        GameManager.warning("illegal item id=" + v3);
    }
  }

  public function interrupt(id: Int): Void {
    if (!this.actives[id]) {
      return;
    }
    this.actives[id] = false;
    this.game.fxMan.clearBg();
    switch (id) {
      case 4:
      case 5:
        this.player.maxBombs = this.player.initialMaxBombs;
      case 7:
        this.player.speedFactor = 1.0;
      case 10:
        this.phoneMC.removeMovieClip();
      case 18:
        this.player.fallFactor = 1.1;
      case 22:
        this.player.speedFactor = 1.0;
        this.player.unstick();
      case 29:
        if (this.player.currentWeapon == Data.WEAPON_S_FIRE) {
          this.player.changeWeapon(null);
        }
      case 30:
        this.game.flipX(false);
      case 31:
        this.game.flipY(false);
      case 37:
        if (this.player.currentWeapon == Data.WEAPON_S_ICE) {
          this.player.changeWeapon(null);
        }
      case 39:
        this.player.fallFactor = 1.1;
      case 67:
        if (this.player.currentWeapon == Data.WEAPON_S_ARROW) {
          this.player.changeWeapon(null);
        }
      case 69:
        this.game.globalActives[id] = false;
        var v3 = this.game.getBadClearList();
        for (v4 in 0...v3.length) {
          v3[v4].updateSpeed();
          v3[v4].animFactor *= 1 / 0.6;
        }
      case 80:
        this.game.globalActives[id] = false;
        var v5 = this.game.getBadClearList();
        for (v6 in 0...v5.length) {
          v5[v6].updateSpeed();
          v5[v6].animFactor *= 1 / 0.3;
        }
      case 81:
        this.game.destroyList(Data.PERFECT_ITEM);
      case 84:
        this.clearRec();
        for (v7 in 0...this.clouds.length) {
          this.clouds[v7].removeMovieClip();
        }
        this.clouds = new Array();
      case 86:
        var v8 = this.game.getBadList();
        for (v9 in 0...v8.length) {
          v8[v9].alpha = 100;
          v8[v9].filters = null;
        }
        this.player.filters = null;
      case 88:
        this.player.unstick();
        this.player.scale(100);
      case 90:
        this.player.unstick();
      case 91:
        this.player.unstick();
      case 94:
        this.game.globalActives[id] = false;
      case 95:
        this.player.unstick();
      case 96:
      case 97:
      case 98:
        this.game.destroyList(Data.PERFECT_ITEM);
      case 99:
        this.player.fl_chourou = false;
        this.player.replayAnim();
        this.clearRec();
      case 100:
        this.game.fxMan.attachFx(this.player.sticker._x, this.player.sticker._y, "hammer_fx_pop");
        this.player.unstick();
        this.player.scale(100);
      case 114:
        this.player.unstick();
    }
  }

  public function onStrike(): Void {
    var v3;
    var v2 = this.game.getList(Data.BAD_CLEAR);
    if (v2.length == 0 || v2 == null) {
      return;
    }
    var v4 = 0;
    do {
      v3 = v2[v4];
      ++v4;
    } while (v3.fl_kill == true);
    if (v3.fl_kill == false) {
      var v5 = this.game.depthMan.attach("hammer_fx_strike", Data.FX.asInt());
      v5._x = Data.DOC_WIDTH / 2;
      v5._y = v3._y - Data.CASE_HEIGHT * 0.5;
      var v6 = HfStd.random(2) * 2 - 1;
      v5._xscale *= v6;
      v5._yscale = HfStd.random(50) + 50;
      this.game.fxMan.attachShine(v3.x, v3.y);
      v3.forceKill(v6 * (HfStd.random(10) + 15));
    }
  }

  public function onFireRain(): Void {
    var v2 = HfStd.random(Math.round(Data.GAME_WIDTH)) + 50;
    var v3 = hf.entity.shoot.FireRain.attach(this.game, v2, -HfStd.random(50));
    v3.moveToAng(95 + HfStd.random(30), v3.shootSpeed);
  }

  public function onPoT(): Void {
    this.player.getScore(this.player, 250);
  }

  public function main(): Void {
    var v2 = 0;
    while (v2 < this.recurring.length) {
      var v3 = this.recurring[v2];
      v3.timer -= Timer.tmod;
      if (v3.timer <= 0) {
        v3.func();
        if (v3.fl_repeat) {
          v3.timer = v3.baseTimer + v3.timer;
        } else {
          this.recurring.splice(v2, 1);
          --v2;
        }
      }
      ++v2;
    }
    if (this.phoneMC._name != null) {
      var v4 = new Date();
      var v5 = "" + v4.getHours();
      if (v5.length < 2) {
        v5 = "0" + v5;
      }
      v5 += ":";
      if (v4.getMinutes() < 10) {
        v5 = v5 + "0" + v4.getMinutes();
      } else {
        v5 += v4.getMinutes();
      }
      this.phoneMC.screen.field.text = v5;
    }
    var v6 = 0;
    while (v6 < this.tempList.length) {
      if (this.game.cycle >= this.tempList[v6].end) {
        this.interrupt(this.tempList[v6].id);
        this.tempList.splice(v6, 1);
        --v6;
      }
      ++v6;
    }
  }
}
