package etwin.flash;

@:native("ContextMenuItem")
extern class ContextMenuItem {
  /**
    A string that specifies the menu item caption (text) displayed in the context menu.
  **/
  public var caption: String;

  /**
    A Boolean value that indicates whether the specified menu item is enabled or disabled.
  **/
  public var enabled: Bool;

  /**
    A Boolean value that indicates whether the specified menu item is visible when the Flash Player context menu is displayed.
  **/
  public var visible: Bool;

  /**
    A Boolean value that indicates whether a separator bar should appear above the specified menu item.
  **/
  public var separatorBefore: Bool;

  /**
    Creates a new ContextMenuItem object that can be added to the `ContextMenu.customItems` array.
  **/
  public function new(caption: String, callb: Dynamic -> ContextMenuItem -> Void, ?separatorBefore: Bool, ?enabled: Bool, ?visible: Bool): Void;

  /**
    Creates and returns a copy of the specified ContextMenuItem object.
  **/
  public function copy(): ContextMenuItem;

  /**
    Invoked when the specified menu item is selected from the Flash Player context menu.
  **/
  public dynamic function onSelect(obj: Dynamic, menuItem: ContextMenuItem): Void;
}
