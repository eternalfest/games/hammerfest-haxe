package hf.levels;

import etwin.flash.MovieClip;
import etwin.flash.DynamicMovieClip;
import hf.Data in Data;

class TeleporterData {
  public var cx: Float;
  public var cy: Float;
  public var dir: Int;
  public var length: Float;
  public var fl_on: Bool;
  public var centerX: Float;
  public var centerY: Float;
  public var startX: Float;
  public var startY: Float;
  public var endX: Float;
  public var endY: Float;
  public var mc: Null<DynamicMovieClip /* TODO */>;
  public var podA: Null<MovieClip>;
  public var podB: Null<MovieClip>;

  public function new(x: Float, y: Float, len: Float, dir: Int): Void {
    this.cx = x;
    this.cy = y;
    this.dir = dir;
    this.length = len;
    this.fl_on = false;
    this.centerX = this.cx * Data.CASE_WIDTH + Data.CASE_WIDTH / 2;
    this.centerY = this.cy * Data.CASE_HEIGHT + Data.CASE_HEIGHT;
    this.startX = Entity.x_ctr(x);
    this.startY = Entity.y_ctr(y);
    if (dir == Data.HORIZONTAL) {
      this.centerX += (this.length / 2) * Data.CASE_WIDTH;
      this.startX -= Data.CASE_WIDTH * 0.5;
    }
    if (dir == Data.VERTICAL) {
      this.centerY += (this.length / 2) * Data.CASE_HEIGHT;
      this.startY -= Data.CASE_HEIGHT;
    }
    this.endX = this.startX;
    this.endY = this.startY;
    if (dir == Data.HORIZONTAL) {
      this.endX += this.length * Data.CASE_WIDTH;
    } else {
      this.endY += this.length * Data.CASE_HEIGHT;
    }
  }
}
