package hf.entity.bomb;

import hf.entity.Bad;
import hf.entity.Bomb;
import hf.entity.Player;
import hf.mode.GameMode;

class BadBomb extends Bomb {
  public var owner: Null<Bad>;

  public function new(): Void {
    super();
    this.fl_airKick = true;
  }

  public function setOwner(b: Bad): Void {
    this.owner = b;
  }

  public function getFrozen(uid: Int): Null<Bomb> {
    return null;
  }

  override public function initBomb(g: GameMode, x: Float, y: Float): Void {
    super.initBomb(g, x, y);
    if (this.game.fl_bombExpert) {
      this.radius *= 1.4;
    }
  }

  override public function init(g: GameMode): Void {
    super.init(g);
    this.register(Data.BAD_BOMB);
  }

  override public function onExplode(): Void {
    super.onExplode();
    if (this.game.getDynamicVar("$BAD_BOMB_TRIGGER") != null) {
      this.game.onExplode(this.x, this.y, this.radius);
    }
  }

  override public function onKick(p: Player): Void {
    super.onKick(p);
    this.dx *= 3;
  }
}
