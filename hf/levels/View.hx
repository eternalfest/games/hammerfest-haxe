package hf.levels;

import etwin.flash.filters.BitmapFilter;
import etwin.flash.MovieClip;
import etwin.flash.DynamicMovieClip;
import hf.DepthManager;
import hf.levels.Data in LevelData;
import hf.levels.GameMechanics;
import hf.native.BitmapData;
import hf.Data in Data;
using hf.compat.BitmapDataTools;

class View {
  /**
   * Reference to the game world.
   *
   * The engine really types it as `hf.levels.SetManager` but uses it in fact as
   * `hf.levels.GameMechanics`.
   */
  public var world: GameMechanics;
  public var depthMan: DepthManager;
  public var viewX: Null<Float>;
  public var viewY: Null<Float>;
  public var xOffset: Float;
  public var fl_attach: Bool;
  public var fl_shadow: Bool;
  public var fl_hideTiles: Bool;
  public var fl_hideBorders: Bool;
  public var tileList: Array<MovieClip>;
  public var gridList: Array<MovieClip>;
  public var _fieldMap: Array<Array<Bool>>;
  public var mcList: Array<MovieClip>;
  public var _sprite_top: MovieClip;
  public var _sprite_back: MovieClip;
  public var _tiles: Null<MovieClip>;
  public var _top: Null<DynamicMovieClip /* TODO */>;
  public var _back: Null<DynamicMovieClip /* TODO */>;
  public var _field: Null<MovieClip>;
  public var _leftBorder: Null<MovieClip>;
  public var _rightBorder: Null<MovieClip>;
  public var _sprite_top_dm: DepthManager;
  public var _sprite_back_dm: DepthManager;
  public var _back_dm: DepthManager;
  public var _top_dm: DepthManager;
  public var _field_dm: DepthManager;
  public var _bg: Null<MovieClip>;
  public var _specialBg: Null<DynamicMovieClip /* TODO */>;
  public var fl_cache: Bool;
  public var fl_fast: Bool;
  public var levelId: Null<Int>;
  public var data: Null<LevelData>;
  public var viewCache: Null<BitmapData>;
  public var tileCache: Null<BitmapData>;
  public var topCache: Null<BitmapData>;
  public var snapShot: Null<BitmapData>;

  public function new(world: GameMechanics, dm: DepthManager): Void {
    this.world = world;
    this.depthMan = dm;
    this.xOffset = 10;
    this.fl_attach = false;
    this.fl_shadow = true;
    this.fl_hideTiles = false;
    this.fl_hideBorders = false;
    this.tileList = new Array();
    this.gridList = new Array();
    this.mcList = new Array();
    this._sprite_top = this.depthMan.empty(Data.DP_SPRITE_TOP_LAYER);
    this._sprite_top._x -= this.xOffset;
    this._sprite_back = this.depthMan.empty(Data.DP_SPRITE_BACK_LAYER);
    this._sprite_back._x -= this.xOffset;
    this._sprite_top_dm = new DepthManager(this._sprite_top);
    this._sprite_back_dm = new DepthManager(this._sprite_back);
    this.fl_cache = world.manager.fl_flash8;
    this.fl_fast = false;
  }

  public function display(id: Int): Void {
    this.data = this.world.levels[id];
    this.levelId = id;
    if (this.data == null) {
      GameManager.warning("null view");
    }
    this.attach();
  }

  public function displayCurrent(): Void {
    this.display(this.world.currentId);
  }

  public function displayExternal(d: LevelData): Void {
    this.data = d;
    this.levelId = null;
    this.detachLevel();
    this.attach();
  }

  public function scale(ratio: Float): Void {
    var v3 = Math.round(ratio * 100);
    this._tiles._xscale = v3;
    this._tiles._yscale = v3;
    this._bg._xscale = v3;
    this._bg._yscale = v3;
    this._sprite_back._xscale = v3;
    this._sprite_back._yscale = v3;
    this._sprite_top._xscale = v3;
    this._sprite_top._yscale = v3;
    this._leftBorder._visible = ratio == 1;
    this._rightBorder._visible = ratio == 1;
    if (this.fl_cache) {
      this._top.bitmapMC._xscale = v3;
      this._top.bitmapMC._yscale = v3;
      this._back.bitmapMC._xscale = v3;
      this._back.bitmapMC._yscale = v3;
      if (ratio != 1) {
        this._field._visible = false;
      } else {
        this._field._visible = true;
      }
    }
  }

  public function removeShadows(): Void {
    this.fl_shadow = false;
  }

  public function isWall(cx: Int, cy: Int): Bool {
    return this.data.__dollar__map[cx][cy] > 0 && (this.data.__dollar__map[cx - 1][cy] <= 0 || this.data.__dollar__map[cx - 1][cy] == null) && (this.data.__dollar__map[cx + 1][cy] <= 0 || this.data.__dollar__map[cx + 1][cy] == null);
  }

  public static function getTileSkinId(id: Int): Int {
    if (id >= 100) {
      id -= Math.floor(id / 100) * 100;
      return id;
    }
    return id;
  }

  public static function getColumnSkinId(id: Int): Int {
    if (id >= 100) {
      id = Math.floor(id / 100);
    }
    return id;
  }

  public static function buildSkinId(tile: Int, column: Int): Int {
    if (column == tile) {
      return tile;
    }
    return column * 100 + tile;
  }

  public function attachTile(sx: Int, sy: Int, wid: Int, skin: Int): Void {
    skin = hf.levels.View.getTileSkinId(skin);
    if (this.fl_fast) {
      skin = 30;
    }
    var v6: etwin.flash.DynamicMovieClip = cast HfStd.attachMC(this._tiles, "tile", sy * Data.LEVEL_WIDTH + sx);
    v6._x = sx * Data.CASE_WIDTH;
    v6._y = sy * Data.CASE_HEIGHT;
    v6.maskTile._width = wid * Data.CASE_WIDTH;
    v6.endTile._x = wid * Data.CASE_WIDTH;
    v6.skin.gotoAndStop('' + (skin));
    v6.endTile.gotoAndStop('' + (skin));
    if (!this.fl_shadow || this.fl_fast) {
      v6.ombre._visible = false;
      v6.endTile.ombre._visible = false;
    }
    this.tileList.push(v6);
  }

  public function attachColumn(sx: Int, sy: Int, wid: Int, skin: Int): Void {
    skin = hf.levels.View.getColumnSkinId(skin);
    if (this.fl_fast) {
      skin = 30;
    }
    var v6: etwin.flash.DynamicMovieClip = cast HfStd.attachMC(this._tiles, "tile", sy * Data.LEVEL_WIDTH + sx);
    v6._yscale = -100;
    v6._rotation = 90;
    v6._x = sx * Data.CASE_WIDTH;
    v6._y = sy * Data.CASE_HEIGHT;
    v6.maskTile._width = wid * Data.CASE_WIDTH;
    v6.endTile._x = wid * Data.CASE_WIDTH;
    v6.skin.gotoAndStop('' + (skin));
    v6.endTile.gotoAndStop('' + (skin));
    if (!this.fl_shadow || this.fl_fast) {
      v6.ombre._visible = false;
      v6.endTile.ombre._visible = false;
    }
    this.tileList.push(v6);
  }

  public function attachField(sx: Int, sy: Int): Void {
    if (this.fl_fast) {
      return;
    }
    var v4 = false;
    var v6 = this.data.__dollar__map[sx][sy];
    var v7 = null;
    var v5: etwin.flash.DynamicMovieClip = cast this._field_dm.attach("field", 1);
    v5._x = sx * Data.CASE_WIDTH;
    v5._y = sy * Data.CASE_HEIGHT;
    if (this.data.__dollar__map[sx + 1][sy] == v6) {
      v5.gotoAndStop("2");
      var v8 = sx;
      while (this.data.__dollar__map[v8][sy] == v6) {
        this._fieldMap[v8][sy] = true;
        ++v8;
      }
      if (v6 == Data.FIELD_TELEPORT) {
        v7 = new hf.levels.TeleporterData(sx, sy, v8 - sx, Data.HORIZONTAL);
        v7.mc = v5;
      }
      v5._width = Data.CASE_WIDTH * (v8 - sx);
    } else {
      if (this.data.__dollar__map[sx][sy + 1] == v6) {
        v5.gotoAndStop("1");
        var v9 = sy;
        while (this.data.__dollar__map[sx][v9] == v6) {
          this._fieldMap[sx][v9] = true;
          ++v9;
        }
        if (v6 == Data.FIELD_TELEPORT) {
          v7 = new hf.levels.TeleporterData(sx, sy, v9 - sy, Data.VERTICAL);
          v7.mc = v5;
        }
        if (v6 == Data.FIELD_PORTAL) {
          if (this.data.__dollar__map[sx + 1][sy] > 0) {
            v4 = true;
          }
        }
        v5._height = Data.CASE_HEIGHT * (v9 - sy);
      } else {
        v5.gotoAndStop("2");
        v5._width = Data.CASE_WIDTH;
      }
    }
    v5.skin.gotoAndStop("" + Math.abs(v6));
    v5.skin.sub.stop();
    if (v4) {
      v5.skin.sub._xscale *= -1;
    }
    if (v6 == Data.FIELD_TELEPORT) {
      v7.podA = this._field_dm.attach("hammer_pod", Data.DP_INTERF);
      v7.podA._x = v7.startX;
      v7.podA._y = v7.startY;
      v7.podA.stop();
      v7.podB = this._field_dm.attach("hammer_pod", Data.DP_INTERF);
      v7.podB._x = v7.endX;
      v7.podB._y = v7.endY;
      v7.podB._rotation = 180;
      v7.podB.stop();
      v7.mc.stop();
      if (v7.dir == Data.HORIZONTAL) {
        v7.podA._y -= Data.CASE_HEIGHT * 0.5;
        v7.podB._y -= Data.CASE_HEIGHT * 0.5;
      } else {
        v7.podA._rotation += 90;
        v7.podB._rotation += 90;
      }
      this.world.teleporterList.push(v7);
    }
    if (v6 == Data.FIELD_PORTAL) {
      this.world.portalList.push(new hf.levels.PortalData(v5, sx, sy));
    }
  }

  public function attachBg(): Void {
    this._bg.removeMovieClip();
    this._bg = this._back_dm.attach("hammer_bg", 0);
    this._bg._x = this.xOffset;
    this._bg.gotoAndStop("" + this.data.__dollar__skinBg);
    if (this.world.fl_mirror) {
      this._bg._xscale *= -1;
      this._bg._x += Data.GAME_WIDTH;
    }
  }

  public function attachSpecialBg(id: Int, subId: Null<Int>): MovieClip {
    this._specialBg = ((cast this.depthMan.attach("hammer_special_bg", Data.DP_SPECIAL_BG)): etwin.flash.DynamicMovieClip);
    this._specialBg.gotoAndStop('' + (id + 1));
    if (subId != null) {
      this._specialBg.sub.gotoAndStop('' + (subId + 1));
    }
    if (this.fl_cache) {
      this._specialBg.cacheAsBitmap = true;
      this._back_dm.destroy();
      this._back.bitmapMC = this._back_dm.empty(0);
      this._back.bitmapMC.attachBitmap(this.tileCache, 0);
      return this._specialBg;
    }
    this._bg._visible = false;
    return this._specialBg;
  }

  public function detachSpecialBg(): Void {
    this._specialBg.removeMovieClip();
    if (this.fl_cache) {
      this._back.bitmapMC.attachBitmap(this.viewCache, 0);
    } else {
      this._bg._visible = true;
    }
  }

  public function attach(): Void {
    var v2 = 0;
    var v3 = 0;
    var v4 = false;
    this.world.teleporterList = new Array();
    this.world.portalList = new Array();
    this._top = ((cast this.depthMan.empty(Data.DP_TOP_LAYER)): DynamicMovieClip);
    this._field = this.depthMan.empty(Data.DP_FIELD_LAYER);
    this._back = ((cast this.depthMan.empty(Data.DP_BACK_LAYER)): DynamicMovieClip);
    this._top_dm = new DepthManager(this._top);
    this._field_dm = new DepthManager(this._field);
    this._back_dm = new DepthManager(this._back);
    this._top._x = this.xOffset;
    this._tiles = this._back_dm.empty(2);
    this._tiles._x = this.xOffset;
    this._tiles._visible = !this.fl_hideTiles;
    if (this.fl_cache) {
      this._tiles.cacheAsBitmap = true;
    }
    this._fieldMap = new Array();
    for (v5 in 0...Data.LEVEL_WIDTH) {
      this._fieldMap[v5] = new Array();
    }
    if (!this.fl_fast) {
      this.attachBg();
    }
    for (v6 in 0...Data.LEVEL_HEIGHT) {
      var v7 = 0;
      while (v7 <= Data.LEVEL_WIDTH) {
        if (!v4) {
          if (this.data.__dollar__map[v7][v6] > 0) {
            v2 = v7;
            v3 = v6;
            v4 = true;
          }
        }
        if (v4) {
          if (this.data.__dollar__map[v7][v6] <= 0 || v7 == Data.LEVEL_WIDTH) {
            var v8 = v7 - v2;
            if (v8 == 1 && this.isWall(v7 - 1, v6)) {
              var v9 = 0;
              var v10 = v7 - 1;
              var v11 = v6;
              if (!this.isWall(v10, v11 - 1)) {
                while (this.isWall(v10, v11)) {
                  ++v9;
                  ++v11;
                }
                if (v9 == 1) {
                  this.attachTile(v2, v3, 1, this.data.__dollar__skinTiles);
                } else {
                  this.attachColumn(v2, v3, v9, this.data.__dollar__skinTiles);
                }
              }
            } else {
              this.attachTile(v2, v3, v8, this.data.__dollar__skinTiles);
            }
            v4 = false;
          }
        }
        ++v7;
      }
    }
    for (v12 in 0...Data.LEVEL_HEIGHT) {
      for (v13 in 0...Data.LEVEL_WIDTH) {
        if (this.data.__dollar__map[v13][v12] < 0 && this._fieldMap[v13][v12] == null) {
          this.attachField(v13, v12);
        }
      }
    }
    if (!this.fl_fast) {
      this._leftBorder = this._top_dm.attach("hammer_sides", 2);
      this._leftBorder._x = 5;
      this._rightBorder = this._top_dm.attach("hammer_sides", 2);
      this._rightBorder._x = Data.GAME_WIDTH + 15;
      this._leftBorder._visible = !this.fl_hideBorders;
      this._rightBorder._visible = !this.fl_hideBorders;
    }
    if (this.fl_cache) {
      this.topCache.dispose();
      this.viewCache.dispose();
      this.tileCache.dispose();
      this.topCache = cast new etwin.flash.display.BitmapData(Data.DOC_WIDTH, Data.DOC_HEIGHT, true, 16711680);
      this.viewCache = cast new etwin.flash.display.BitmapData(Data.DOC_WIDTH, Data.DOC_HEIGHT, true, 16711680);
      this.tileCache = cast new etwin.flash.display.BitmapData(Data.DOC_WIDTH, Data.DOC_HEIGHT, true, 16711680);
      this.topCache.drawMC(this._top, 0, 0);
      this._bg._visible = false;
      this.tileCache.drawMC(this._back, 0, 0);
      this._bg._visible = true;
      this.viewCache.drawMC(this._back, 0, 0);
      this._top_dm.destroy();
      this._back_dm.destroy();
      this._top.bitmapMC = this._top_dm.empty(0);
      this._back.bitmapMC = this._back_dm.empty(0);
      this._top.bitmapMC.attachBitmap(this.topCache, 0);
      this._back.bitmapMC.attachBitmap(this.viewCache, 0);
      this._top_dm = new DepthManager(this._top);
    }
    if (this._specialBg._name != null) {
      this._back_dm.destroy();
      this._back.bitmapMC = this._back_dm.empty(0);
      this._back.bitmapMC.attachBitmap(this.tileCache, 0);
    }
    this.fl_attach = true;
  }

  public function attachBadSpots(): Void {
    for (v2 in 0...this.data.__dollar__badList.length) {
      var v3 = this.data.__dollar__badList[v2];
      var v4 = this._sprite_top_dm.attach("hammer_editor_bad", Data.DP_BADS);
      v4._x = Entity.x_ctr(v3.__dollar__x) + Data.CASE_WIDTH * 0.5;
      v4._y = Entity.y_ctr(v3.__dollar__y);
      v4.gotoAndStop("" + (v3.__dollar__id + 1));
    }
  }

  public function attachGrid(flag: Int, over: Bool): Void {
    var v4 = Data.DP_SPECIAL_BG;
    if (over) {
      v4 = Data.DP_INTERF;
    }
    for (v5 in 0...Data.LEVEL_WIDTH) {
      for (v6 in 0...Data.LEVEL_HEIGHT) {
        var v7 = this._top_dm.attach("debugGrid", v4);
        v7._x = v5 * Data.CASE_WIDTH + this.xOffset;
        v7._y = v6 * Data.CASE_HEIGHT;
        if ((this.world.flagMap[v5][v6] & flag) == 0) {
          v7.gotoAndStop("1");
        } else {
          v7.gotoAndStop("2");
        }
        this.gridList.push(v7);
      }
    }
  }

  public function detachGrid(): Void {
    for (v2 in 0...this.gridList.length) {
      this.gridList[v2].removeMovieClip();
    }
    this.gridList = new Array();
  }

  public function attachSprite(link: String, x: Float, y: Float, fl_back: Bool): MovieClip {
    if (fl_back) {
      var v6 = this._sprite_back_dm;
      var v7 = v6.attach(link, 10);
      v7._x = x;
      v7._y = y;
      this.mcList.push(v7);
      return v7;
    }
    var v6 = this._sprite_top_dm;
    var v7 = v6.attach(link, 10);
    v7._x = x;
    v7._y = y;
    this.mcList.push(v7);
    return v7;
  }

  public function detach(): Void {
    this.detachLevel();
    this.detachSprites();
    this.fl_attach = false;
    this.snapShot.dispose();
  }

  public function detachLevel(): Void {
    for (v2 in 0...this.tileList.length) {
      this.tileList[v2].removeMovieClip();
    }
    this.tileList = new Array();
    this.detachGrid();
    this.topCache.dispose();
    this.viewCache.dispose();
    this.tileCache.dispose();
    this._top.removeMovieClip();
    this._back.removeMovieClip();
    this._field.removeMovieClip();
  }

  public function detachSprites(): Void {
    for (v2 in 0...this.mcList.length) {
      this.mcList[v2].removeMovieClip();
    }
    this.mcList = new Array();
    this._sprite_back_dm.destroy();
    this._sprite_top_dm.destroy();
  }

  public function moveTo(x: Float, y: Float): Void {
    this.viewX = x;
    this.viewY = y;
    this._top._x = x - this.xOffset;
    this._top._y = y;
    this._back._x = this._top._x;
    this._back._y = this._top._y;
    this._field._x = this._top._x + this.xOffset;
    this._field._y = this._top._y;
    this._sprite_back._x = this._top._x;
    this._sprite_back._y = this._top._y;
    this._sprite_top._x = this._top._x;
    this._sprite_top._y = this._top._y;
  }

  public function setFilter(f: BitmapFilter): Void {
    this._top.filters = [f];
    this._back.filters = [f];
    this._field.filters = [f];
    this._sprite_back.filters = [f];
    this._sprite_top.filters = [f];
  }

  public function moveToPreviousPos(): Void {
    if (this.viewX != null) {
      this.moveTo(this.viewX, this.viewY);
    } else {
      this.moveTo(0, 0);
    }
  }

  public function getSnapShot(x: Float, y: Float): BitmapData {
    var v4: BitmapData = cast new etwin.flash.display.BitmapData(Data.DOC_WIDTH, Data.DOC_HEIGHT, true, 16711680);
    v4.drawMC(this._back, x, y);
    v4.drawMC(this._sprite_back, x, y);
    v4.drawMC(this._top, x, y);
    v4.drawMC(this._sprite_top, x, y);
    return v4;
  }

  public function updateSnapShot(): Void {
    if (this.fl_attach) {
      this.snapShot.dispose();
      this.snapShot = this.getSnapShot(0, 0);
    } else {
      GameManager.warning("WARNING: updateSnapShot while not attached");
    }
  }

  public function destroy(): Void {
    this.detach();
  }
}
