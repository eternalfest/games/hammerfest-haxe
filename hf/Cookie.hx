package hf;

import etwin.flash.SharedObject;
import hf.GameManager;
import etwin.flash.Date;
import etwin.flash.Key;

class Cookie {
  public static var NAME: String = "$hammerfest_data";
  public static var VERSION: Int = 4;

  public var manager: GameManager;
  public var cookie: SharedObject;
  public var data: Dynamic /*TODO*/;

  public function new(m: GameManager): Void {
    this.manager = m;
    this.cookie = SharedObject.getLocal(Cookie.NAME);
    this.data = this.cookie.data;
    this.checkVersion();
  }

  public function reset(): Void {
    this.cookie.clear();
    this.data = this.cookie.data;
    this.data.version = Cookie.VERSION;
    this.flush();
  }

  public function flush(): Void {
    this.data.lastModified = (new Date()).getTime();
    this.cookie.flush();
  }

  public function checkVersion(): Void {
    var v2 = this.data.version;
    if (v2 != Cookie.VERSION || Key.isDown(Key.CONTROL)) {
      if (v2 == null || Key.isDown(Key.CONTROL)) {
        if (this.manager.fl_debug) {
          GameManager.warning("Cookie initialized to version " + Cookie.VERSION);
        }
        this.reset();
      } else {
        GameManager.fatal("invalid cookie version (compiled=" + Cookie.VERSION + " local=" + v2 + ")");
        GameManager.fatal("note: Hold CTRL at start-up to clear cookie");
      }
    }
  }

  public function saveSet(name: String, raw: String): Void {
    HfStd.setVar(this.data, name, raw);
    this.flush();
  }

  public function readSet(name: String): String {
    return HfStd.getVar(this.data, name);
  }
}
