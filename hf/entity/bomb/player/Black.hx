package hf.entity.bomb.player;

import hf.entity.bomb.PlayerBomb;
import hf.mode.GameMode;

class Black extends PlayerBomb {
  public function new(): Void {
    super();
    this.duration = 100;
    this.power = 20;
    this.explodeSound = "sound_bomb_black";
  }

  public static function attach(g: GameMode, x: Float, y: Float): Black {
    var v5 = "hammer_bomb_black";
    var v6: Black = cast g.depthMan.attach(v5, Data.DP_BOMBS);
    v6.initBomb(g, x, y);
    return v6;
  }

  override public function duplicate(): Black {
    return hf.entity.bomb.player.Black.attach(this.game, this.x, this.y);
  }

  override public function onExplode(): Void {
    super.onExplode();
    var v3 = this.bombGetClose(Data.BAD);
    this.game.shake(10, 4);
    this.game.fxMan.attachExplodeZone(this.x, this.y, this.radius);
    for (v4 in 0...v3.length) {
      var v5 = v3[v4];
      v5.setCombo(this.uniqId);
      v5.killHit(0);
      this.shockWave(v5, this.radius, this.power);
      v5.dy = -10 - HfStd.random(20);
    }
  }
}
