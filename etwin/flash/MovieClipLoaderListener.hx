package etwin.flash;

typedef MovieClipLoaderListener = {
function onLoadInit(target: MovieClip): Void;

function onLoadStart(target: MovieClip): Void;

function onLoadProgress(target: MovieClip, loaded: Int, total: Int): Void;

function onLoadComplete(target: MovieClip, httpStatus: Int): Void;

function onLoadError(target: MovieClip, error: String, httpStatus: Int): Void;
}
