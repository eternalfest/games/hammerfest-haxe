package hf.levels;

import etwin.flash.MovieClip;

class PortalData {
  public var mc: MovieClip;
  public var cx: Int;
  public var cy: Int;
  public var x: Float;
  public var y: Float;
  public var cpt: Float;

  public function new(mc: MovieClip, cx: Int, cy: Int): Void {
    this.mc = mc;
    this.cx = cx;
    this.cy = cy;
    this.x = mc._x;
    this.y = mc._y;
    this.cpt = 0;
  }
}
