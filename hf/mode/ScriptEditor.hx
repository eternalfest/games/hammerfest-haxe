package hf.mode;

import hf.gui.Container;
import hf.Mode;
import etwin.flash.Key;
import etwin.flash.Selection;
import etwin.flash.XML;

class ScriptEditor extends Mode {
  public var fl_focus: Bool;
  public var caretPos: Int;
  public var original: String;
  public var field: Dynamic /*TODO*/;
  public var menuC: Container;
  public var styleC: Container;
  public var fl_lockNext: Bool;
  public var attributes: Dynamic /*TODO*/;
  public var field_path: Dynamic /*TODO*/;
  public var fl_lockPrev: Bool;

  public static var INDENT_STR: String = "    ";

  public function new(m: Dynamic /*TODO*/, script: String): Void {
    super(m);
    this._name = "$scriptEd";
    this.xOffset = 10;
    this.lock();
    this.fl_focus = false;
    this.caretPos = 0;
    var v5: etwin.flash.DynamicMovieClip = cast this.depthMan.attach("hammer_editor_script", Data.DP_INTERF);
    v5._x = 7;
    v5._y = 30;
    this.original = script;
    this.field = v5.field;
    this.field.text = this.original;
    this.field_path = this.field;
    this.focus(this.caretPos);
  }

  public function focus(pos: Int): Void {
    Selection.setFocus(this.field_path);
    Selection.setSelection(pos, pos);
  }

  public function cleanXml(s: String): String {
    var v3 = 0;
    var v4 = false;
    var v5 = false;
    var v6 = false;
    var v7 = "";
    var v8 = 0;
    s = Tools.replace(s, "/>", " />");
    while (v3 < s.length) {
      var v9 = s.substr(v3, 1);
      if (v9 == "<" && !v5) {
        v6 = true;
      }
      if (v9 == ">" && !v5) {
        v6 = false;
      }
      if (v5) {
        if (v7 == v9) {
          v5 = false;
        }
      } else {
        if (v6 && (v9 == "\"" || v9 == "'")) {
          v5 = true;
          v7 = v9;
        }
        if (!v6 && v9 == "'") {
          s = s.substr(0, v3 - 1) + "&apos;" + s.substr(v3 + 1, 999999);
        }
        if (!v6 && v9 == "\"") {
          s = s.substr(0, v3 - 1) + "&quot;" + s.substr(v3 + 1, 999999);
        }
        if (!v4 && v9 == " ") {
          v4 = true;
          v8 = v3;
        }
        if (v4 && v9 != " ") {
          v4 = false;
          if (v3 - v8 > 1) {
            s = s.substr(0, v8) + " " + s.substr(v3, 999999);
            v3 -= v3 - v8 - 1;
          }
        }
      }
      ++v3;
    }
    return s;
  }

  public function isValid(raw: String): Bool {
    var v3 = this.cleanXml(raw);
    var v4 = new XML(v3);
    if ((v4.toString()).length - v3.length != 0) {
      return false;
    }
    return true;
  }

  public function getAttributes(node: Dynamic /*TODO*/): Dynamic /*TODO*/ {
    this.attributes = new Array();
    node.attributesIter(function (k, v) {
      this.attributes.push(k + "=\"" + v + "\"");
    });
    if (this.attributes.length == 0) {
      return "";
    }
    return this.attributes.join(" ");
  }

  public function getIndent(level: Int): String {
    var v3 = "";
    for (v4 in 0...level) {
      v3 += hf.mode.ScriptEditor.INDENT_STR;
    }
    return v3;
  }

  public function reIndent(node: Dynamic /*TODO*/, level: Int): String {
    var v4 = "";
    while (node != null) {
      if (node.nodeName != null) {
        if (node.firstChild == null) {
          v4 += this.getIndent(level) + "<" + node.nodeName + " " + this.getAttributes(node) + "/>\n";
        } else {
          var v5 = this.getAttributes(node);
          if (v5 != "") {
            v4 += this.getIndent(level) + "<" + node.nodeName + " " + v5 + ">\n";
          } else {
            v4 += this.getIndent(level) + "<" + node.nodeName + ">\n";
          }
          v4 += this.reIndent(node.firstChild, level + 1);
          v4 += this.getIndent(level) + "</" + node.nodeName + ">\n";
          if (level == 0) {
            v4 += "\n";
          }
        }
      } else {
        var v6 = Data.cleanLeading(node.toString());
        if (v6.length > 1) {
          v4 += v6 + "\n";
        }
      }
      node = node.nextSibling;
    }
    return v4;
  }

  public function append(s: String): Void {
    var v3 = this.field.text.substr(0, this.caretPos);
    var v4 = this.field.text.substr(this.caretPos, 99999);
    this.field.text = v3 + "\n" + s + v4;
    var v5 = this.field.text.indexOf("#c#", 0);
    this.field.text = Tools.replace(this.field.text, "#c#", "");
    this.focus(v5);
  }

  public function onClear(): Void {
    this.field.text = "";
  }

  public function onReIndent(): Void {
    if (this.isValid(this.field.text)) {
      this.field.text = Tools.replace(this.field.text, String.fromCharCode(13), " ");
      var v2 = new XML(this.field.text);
      this.field.text = this.reIndent(v2.firstChild, 0);
    } else {
      GameManager.warning("invalid XML");
    }
  }

  public function onReload(): Void {
    this.field.text = this.original;
  }

  public function onTriggerTimer(): Void {
    this.append("<" + hf.levels.ScriptEngine.T_TIMER + " $t=\"#c#\">\n\n</" + hf.levels.ScriptEngine.T_TIMER + ">");
  }

  public function onTriggerPos(): Void {
    this.append("<" + hf.levels.ScriptEngine.T_POS + " $x=\"#c#\" $y=\"\" $d=\"\">\n\n</" + hf.levels.ScriptEngine.T_POS + ">");
  }

  public function onTriggerDo(): Void {
    this.append("<" + hf.levels.ScriptEngine.T_DO + ">\n#c#\n</" + hf.levels.ScriptEngine.T_DO + ">");
  }

  public function onTriggerEnd(): Void {
    this.append("<" + hf.levels.ScriptEngine.T_END + ">\n#c#\n</" + hf.levels.ScriptEngine.T_END + ">");
  }

  public function onTriggerBirth(): Void {
    this.append("<" + hf.levels.ScriptEngine.T_BIRTH + " $repeat=\"-1\">\n#c#\n</" + hf.levels.ScriptEngine.T_BIRTH + ">");
  }

  public function onTriggerExp(): Void {
    this.append("<" + hf.levels.ScriptEngine.T_EXPLODE + " $x=\"#c#\" $y=\"\">\n\n</" + hf.levels.ScriptEngine.T_EXPLODE + ">");
  }

  public function onTriggerAttach(): Void {
    this.append("<" + hf.levels.ScriptEngine.T_ATTACH + " $repeat=\"-1\">\n#c#\n</" + hf.levels.ScriptEngine.T_ATTACH + ">");
  }

  public function onTriggerEnter(): Void {
    this.append("<" + hf.levels.ScriptEngine.T_ENTER + " $x=\"#c#\" $y=\"\">\n\n</" + hf.levels.ScriptEngine.T_ENTER + ">");
  }

  public function onTriggerNightmare(): Void {
    this.append("<" + hf.levels.ScriptEngine.T_NIGHTMARE + ">\n#c#\n</" + hf.levels.ScriptEngine.T_NIGHTMARE + ">");
  }

  public function onTriggerMirror(): Void {
    this.append("<" + hf.levels.ScriptEngine.T_MIRROR + ">\n#c#\n</" + hf.levels.ScriptEngine.T_MIRROR + ">");
  }

  public function onTriggerMulti(): Void {
    this.append("<" + hf.levels.ScriptEngine.T_MULTI + ">\n#c#\n</" + hf.levels.ScriptEngine.T_MULTI + ">");
  }

  public function onTriggerNinja(): Void {
    this.append("<" + hf.levels.ScriptEngine.T_NINJA + ">\n#c#\n</" + hf.levels.ScriptEngine.T_NINJA + ">");
  }

  public function onEventBad(): Void {
    this.append("<" + hf.levels.ScriptEngine.E_BAD + " $i=\"#c#\" $x=\"\" $y=\"\" />");
  }

  public function onEventScoreItem(): Void {
    this.append("<" + hf.levels.ScriptEngine.E_SCORE + " $i=\"#c#\" $si=\"\" $x=\"\" $y=\"\" $inf=\"1\"/>");
  }

  public function onEventSpecItem(): Void {
    this.append("<" + hf.levels.ScriptEngine.E_SPECIAL + " $i=\"#c#\" $si=\"\" $x=\"\" $y=\"\" $inf=\"1\"/>");
  }

  public function onEventMsg(): Void {
    this.append("<" + hf.levels.ScriptEngine.E_MESSAGE + " $id=\"#c#\" />");
  }

  public function onEventTuto(): Void {
    this.append("<" + hf.levels.ScriptEngine.E_TUTORIAL + " $id=\"#c#\" />");
  }

  public function onEventKillMsg(): Void {
    this.append("<" + hf.levels.ScriptEngine.E_KILLMSG + "/>\n#c#");
  }

  public function onEventPointer(): Void {
    this.append("<" + hf.levels.ScriptEngine.E_POINTER + " $x=\"#c#\" $y=\"\"/>");
  }

  public function onEventKillPointer(): Void {
    this.append("<" + hf.levels.ScriptEngine.E_KILLPTR + "/>\n#c#");
  }

  public function onEventDecoration(): Void {
    this.append("<" + hf.levels.ScriptEngine.E_MC + " $n=\"#c#\" $x=\"\" $y=\"\" $p=\"0\" />");
  }

  public function onEventMusic(): Void {
    this.append("<" + hf.levels.ScriptEngine.E_MUSIC + " $id=\"#c#\"/>");
  }

  public function onEventKill(): Void {
    this.append("<" + hf.levels.ScriptEngine.E_KILL + " $sid=\"#c#\" />");
  }

  public function onEventAddTile(): Void {
    this.append("<" + hf.levels.ScriptEngine.E_ADDTILE + " $x1=\"#c#\" $y1=\"\" $x2=\"\" $y2=\"\" $type=\"0\" />");
  }

  public function onEventRemoveTile(): Void {
    this.append("<" + hf.levels.ScriptEngine.E_REMTILE + " $x1=\"#c#\" $y1=\"\" $x2=\"\" $y2=\"\" />");
  }

  public function onEventGoto(): Void {
    this.append("<" + hf.levels.ScriptEngine.E_GOTO + " $id=\"#c#\"/>");
  }

  public function onEventHide(): Void {
    this.append("<" + hf.levels.ScriptEngine.E_HIDE + " $borders=\"1#c#\" $tiles=\"1\" />");
  }

  public function onEventCodeTrigger(): Void {
    this.append("<" + hf.levels.ScriptEngine.E_CODETRIGGER + " $id=\"#c#\" />");
  }

  public function onEventPortal(): Void {
    this.append("<" + hf.levels.ScriptEngine.E_PORTAL + " $pid=\"0#c#\" />");
  }

  public function onEventItemLine(): Void {
    this.append("<" + hf.levels.ScriptEngine.E_ITEMLINE + " $i=\"#c#\" $si=\"\" $x1=\"\" $y=\"\" $x2=\"\" $t=\"10\" />");
  }

  public function onEventSetVar(): Void {
    this.append("<" + hf.levels.ScriptEngine.E_SETVAR + " $var=\"#c#\" $value=\"1\" />");
  }

  public function onEventOpenPortal(): Void {
    this.append("<" + hf.levels.ScriptEngine.E_OPENPORTAL + " $x=\"#c#\" $y=\"\" $pid=\"0\" />");
  }

  public function onEventDarkness(): Void {
    this.append("<" + hf.levels.ScriptEngine.E_DARKNESS + " $v=\"#c#\" />");
  }

  public function onEventFakeLID(): Void {
    this.append("<" + hf.levels.ScriptEngine.E_FAKELID + " $lid=\"#c#\" />");
  }

  override public function init(): Void {
    super.init();
    this.unlock();
    this.menuC = new hf.gui.Container(this, 5, 10, Data.DOC_WIDTH - 10);
    hf.gui.SimpleButton.attach(this.menuC, " X ", Key.ESCAPE, HfStd.callback(this, "endMode"));
    var v3 = hf.gui.SimpleButton.attach(this.menuC, " Reindent ", Key.ENTER, HfStd.callback(this, "onReIndent"));
    v3.setToggleKey(Key.SHIFT);
    v3 = hf.gui.SimpleButton.attach(this.menuC, " Clear ", null, HfStd.callback(this, "onClear"));
    v3 = hf.gui.SimpleButton.attach(this.menuC, " Reload ", null, HfStd.callback(this, "onReload"));
    this.styleC = new hf.gui.Container(this, 5, 390, Data.DOC_WIDTH - 10);
    hf.gui.Label.attach(this.styleC, " Triggers ");
    hf.gui.SimpleButton.attach(this.styleC, " tim ", null, HfStd.callback(this, "onTriggerTimer"));
    hf.gui.SimpleButton.attach(this.styleC, " pos ", null, HfStd.callback(this, "onTriggerPos"));
    hf.gui.SimpleButton.attach(this.styleC, " do ", null, HfStd.callback(this, "onTriggerDo"));
    hf.gui.SimpleButton.attach(this.styleC, " end ", null, HfStd.callback(this, "onTriggerEnd"));
    hf.gui.SimpleButton.attach(this.styleC, " birth ", null, HfStd.callback(this, "onTriggerBirth"));
    hf.gui.SimpleButton.attach(this.styleC, " exp ", null, HfStd.callback(this, "onTriggerExp"));
    hf.gui.SimpleButton.attach(this.styleC, " att ", null, HfStd.callback(this, "onTriggerAttach"));
    hf.gui.SimpleButton.attach(this.styleC, " ent ", null, HfStd.callback(this, "onTriggerEnter"));
    hf.gui.SimpleButton.attach(this.styleC, " nightm ", null, HfStd.callback(this, "onTriggerNightmare"));
    hf.gui.SimpleButton.attach(this.styleC, " mirr ", null, HfStd.callback(this, "onTriggerMirror"));
    hf.gui.SimpleButton.attach(this.styleC, " multi ", null, HfStd.callback(this, "onTriggerMulti"));
    hf.gui.SimpleButton.attach(this.styleC, " ninja ", null, HfStd.callback(this, "onTriggerNinja"));
    this.styleC.endLine();
    hf.gui.Label.attach(this.styleC, " Actions ");
    hf.gui.SimpleButton.attach(this.styleC, " bad ", null, HfStd.callback(this, "onEventBad"));
    hf.gui.SimpleButton.attach(this.styleC, " score ", null, HfStd.callback(this, "onEventScoreItem"));
    hf.gui.SimpleButton.attach(this.styleC, " spec ", null, HfStd.callback(this, "onEventSpecItem"));
    hf.gui.SimpleButton.attach(this.styleC, " kill ", null, HfStd.callback(this, "onEventKill"));
    hf.gui.SimpleButton.attach(this.styleC, " msg ", null, HfStd.callback(this, "onEventMsg"));
    hf.gui.SimpleButton.attach(this.styleC, " tuto ", null, HfStd.callback(this, "onEventTuto"));
    hf.gui.SimpleButton.attach(this.styleC, " killMsg ", null, HfStd.callback(this, "onEventKillMsg"));
    hf.gui.SimpleButton.attach(this.styleC, " ptr ", null, HfStd.callback(this, "onEventPointer"));
    hf.gui.SimpleButton.attach(this.styleC, " killPt", null, HfStd.callback(this, "onEventKillPointer"));
    hf.gui.SimpleButton.attach(this.styleC, " mc ", null, HfStd.callback(this, "onEventDecoration"));
    hf.gui.SimpleButton.attach(this.styleC, " music ", null, HfStd.callback(this, "onEventMusic"));
    hf.gui.SimpleButton.attach(this.styleC, " +tile ", null, HfStd.callback(this, "onEventAddTile"));
    hf.gui.SimpleButton.attach(this.styleC, " -tile ", null, HfStd.callback(this, "onEventRemoveTile"));
    hf.gui.SimpleButton.attach(this.styleC, " hide ", null, HfStd.callback(this, "onEventHide"));
    hf.gui.SimpleButton.attach(this.styleC, " ctrig ", null, HfStd.callback(this, "onEventCodeTrigger"));
    hf.gui.SimpleButton.attach(this.styleC, " goLvl ", null, HfStd.callback(this, "onEventGoto"));
    hf.gui.SimpleButton.attach(this.styleC, " portal ", null, HfStd.callback(this, "onEventPortal"));
    hf.gui.SimpleButton.attach(this.styleC, " itemLine ", null, HfStd.callback(this, "onEventItemLine"));
    hf.gui.SimpleButton.attach(this.styleC, " set ", null, HfStd.callback(this, "onEventSetVar"));
    hf.gui.SimpleButton.attach(this.styleC, " open ", null, HfStd.callback(this, "onEventOpenPortal"));
    hf.gui.SimpleButton.attach(this.styleC, " dark ", null, HfStd.callback(this, "onEventDarkness"));
    hf.gui.SimpleButton.attach(this.styleC, " fakeLID ", null, HfStd.callback(this, "onEventFakeLID"));
  }

  override public function getControls(): Void {
    if (!Key.isDown(Key.LEFT)) {
      this.fl_lockPrev = false;
    }
    if (Key.isDown(Key.LEFT) && Key.isDown(Key.CONTROL) && !this.fl_lockPrev) {
      this.fl_lockPrev = true;
      var v2 = this.field.text.lastIndexOf("=\"", this.caretPos - 3);
      if (v2 >= 0) {
        this.focus(v2 + 2);
      } else {
        this.focus(this.caretPos);
      }
    }
    if (!Key.isDown(Key.RIGHT)) {
      this.fl_lockNext = false;
    }
    if (Key.isDown(Key.RIGHT) && Key.isDown(Key.CONTROL) && !this.fl_lockNext) {
      this.fl_lockNext = true;
      var v3 = this.field.text.indexOf("=\"", this.caretPos);
      if (v3 >= 0) {
        this.focus(v3 + 2);
      } else {
        this.focus(this.caretPos);
      }
    }
  }

  override public function endMode(): Void {
    this.manager.stopChild(Tools.replace(this.field.text, String.fromCharCode(27), ""));
  }

  override public function main(): Void {
    super.main();
    if (Selection.getCaretIndex() >= 0) {
      this.caretPos = Selection.getCaretIndex();
    }
    if (this.fl_lock) {
      return;
    }
    this.getControls();
    this.menuC.update();
  }
}
