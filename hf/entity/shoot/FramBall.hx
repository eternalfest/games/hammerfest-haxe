package hf.entity.shoot;

import hf.entity.bad.walker.Framboise;
import hf.entity.Shoot;
import hf.Entity;
import hf.mode.GameMode;

class FramBall extends Shoot {
  public var turnSpeed: Float;
  public var fl_arrived: Bool;
  public var ang: Float;
  public var white: Float;
  public var owner: Null<Framboise>;

  public function new(): Void {
    super();
    this.shootSpeed = 5 + HfStd.random(3);
    this.turnSpeed = 0.03 + HfStd.random(10) / 100 + this.shootSpeed * 0.02;
    this._yOffset = -2;
    this.setLifeTimer(Data.SECOND * 4);
    this.fl_checkBounds = false;
    this.fl_blink = false;
    this.fl_arrived = false;
    this.fl_anim = false;
    this.gotoAndStop("1");
    this.ang = HfStd.random(314);
    this.white = 0;
  }

  public function setOwner(b: Framboise): Void {
    this.owner = b;
    var v3 = Math.atan2(this.owner.ty - this.y, this.owner.tx - this.x);
    this.ang = -v3 + (HfStd.random(168) / 100) * (HfStd.random(2) * 2 - 1);
    if (this.owner.anger > 0) {
      this.shootSpeed *= 1 + this.owner.anger * 0.5;
    }
  }

  public static function attach(g: GameMode, x: Float, y: Float): FramBall {
    var v5 = "hammer_shoot_framBall2";
    var v6: FramBall = cast g.depthMan.attach(v5, Data.DP_SHOTS);
    v6.initShoot(g, x, y);
    return v6;
  }

  public function adjustAngRad(a: Float): Float {
    if (a < -Math.PI) {
      return a + Math.PI * 2;
    }
    if (a > Math.PI) {
      return a - Math.PI * 2;
    }
    return a;
  }

  public function onArrived(): Void {
    if (this.fl_arrived) {
      return;
    }
    this.fl_arrived = true;
    this.owner.onArrived(this);
  }

  override public function endUpdate(): Void {
    super.endUpdate();
  }

  override public function hit(e: Entity): Void {
    var v3 = Data.PLAYER.downcast(e);
    if (v3 != null) {
      v3.killHit(this.dx);
    }
  }

  override public function infix(): Void {
    super.infix();
    var v3 = this.distance(this.owner.tx, this.owner.ty);
    if (v3 < 40) {
      this.turnSpeed *= 1.1;
    }
    if (v3 < 20) {
      this.shootSpeed *= 0.9;
      if (this.shootSpeed <= 5.8 || this.owner.anger > 0) {
        this.onArrived();
      }
      this.white += 0.1 * Timer.tmod;
      this.white = Math.min(1, this.white);
    }
    this.turnSpeed = Math.min(1, this.turnSpeed);
    this.shootSpeed = Math.max(2, this.shootSpeed);
  }

  override public function onLifeTimer(): Void {
    this.onArrived();
    super.onLifeTimer();
  }

  override public function update(): Void {
    var v3 = Math.atan2(this.owner.ty - this.y, this.owner.tx - this.x);
    if (this.adjustAngRad(v3 - this.ang) > 0) {
      this.ang += this.turnSpeed * Timer.tmod;
    }
    if (this.adjustAngRad(v3 - this.ang) < 0) {
      this.ang -= this.turnSpeed * Timer.tmod;
    }
    this.dx = Math.cos(this.ang) * this.shootSpeed * Timer.tmod;
    this.dy = Math.sin(this.ang) * this.shootSpeed * Timer.tmod;
    super.update();
  }
}
