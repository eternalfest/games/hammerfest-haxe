package hf.entity.bomb.player;

import hf.entity.bomb.PlayerBomb;
import hf.entity.Player;
import hf.mode.GameMode;

class SoccerBall extends PlayerBomb {
  public static var TOP_SPEED: Float = 4;
  public var lastPlayer: Null<Player>;
  public var burnTimer: Float;
  public var speed: Float;

  public function new(): Void {
    super();
    this.lastPlayer = null;
    this.duration = 999999;
    this.burnTimer = 0;
    this.bounceFactor = 0.8;
    this.fl_bounce = true;
    this.slideFriction = Data.FRICTION_SLIDE * 0.9;
  }

  public static function attach(g: GameMode, x: Float, y: Float): SoccerBall {
    var v5 = "hammer_bomb_soccer";
    var v6: SoccerBall = cast g.depthMan.attach(v5, Data.DP_BOMBS);
    v6.initBomb(g, x, y);
    return v6;
  }

  public function burn(): Void {
    this.burnTimer = Data.SECOND;
  }

  override public function duplicate(): SoccerBall {
    return null;
  }

  override public function endUpdate(): Void {
    super.endUpdate();
    this.sub._rotation += this.dx * 5;
    if (this.dx > 0) {
      this.sub._xscale = -Math.abs(this.sub._xscale);
    }
    if (this.dx < 0) {
      this.sub._xscale = Math.abs(this.sub._xscale);
    }
  }

  override public function infix(): Void {
    super.infix();
    var v3 = this.world.getCase({x: this.cx, y: this.cy});
    var game: hf.mode.Soccer = cast this.game;
    if (v3 == Data.FIELD_GOAL_1) {
      game.goal(1);
      this.destroy();
    }
    if (v3 == Data.FIELD_GOAL_2) {
      game.goal(0);
      this.destroy();
    }
  }

  override public function init(g: GameMode): Void {
    super.init(g);
    this.register(Data.SOCCERBALL);
    FxManager.addGlow(this, 8421504, 2);
    this.game.fxMan.attachShine(this.x, this.y - Data.CASE_HEIGHT * 0.5);
  }

  override public function onExplode(): Void {
  }

  override public function onHitWall(): Void {
    var v2 = Entity.x_rtc(this.oldX);
    var v3 = Entity.y_rtc(this.oldY);
    if (this.world.getCase({x: v2, y: v3}) != Data.GROUND) {
      this.dx = -this.dx;
      if (Math.abs(this.dx) > 7) {
        this.game.fxMan.inGameParticlesDir(Data.PARTICLE_DUST, this.x, this.y, HfStd.random(5) + 1, this.dx);
      }
    }
  }

  override public function onKick(p: Player): Void {
    super.onKick(p);
    this.lastPlayer = p;
    if (Math.abs(this.dx) < 10) {
      this.dx *= 3;
      this.dy *= 1.1;
    }
  }

  override public function update(): Void {
    this.speed = Math.sqrt(Math.pow(this.dx, 2) + Math.pow(this.dy, 2));
    this.animFactor = 0.5 * this.speed / hf.entity.bomb.player.SoccerBall.TOP_SPEED;
    this.fl_airKick = true;
    if (this.burnTimer > 0) {
      this.game.fxMan.inGameParticles(Data.PARTICLE_SPARK, this.x, this.y, HfStd.random(3));
      var v3 = this.game.fxMan.attachFx(this.x + HfStd.random(5) * (HfStd.random(2) * 2 - 1), this.y - HfStd.random(20), "hammer_fx_ballBurn");
      var v4 = Math.min(1, this.speed / hf.entity.bomb.player.SoccerBall.TOP_SPEED);
      v3.mc._xscale = 100 * v4;
      v3.mc._yscale = v3.mc._xscale;
      this.burnTimer -= Timer.tmod;
    }
    super.update();
  }

  override public function upgradeBomb(p: Player): Void {
  }
}
