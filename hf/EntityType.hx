package hf;

// Not present in the original code.
// This is a poor-man abstract, to ensure that no code is actually generated.
@:interface
extern class EntityType<E: Entity> {
  
  public static inline function fromUnchecked<E: Entity>(id: Int): EntityType<E> {
    return cast id;
  }

  public inline function asInt(): Int {
    return cast this;
  }

  public inline function downcast(e: Entity): Null<E> {
    return check(e) ? cast e : null;
  }

  public inline function check(e: Entity): Bool {
    // cast to avoid deprecation warning
    return ((cast e).types & asInt()) > 0;
  }

  public inline function register(e: E): Void {
    // cast to avoid deprecation warning
    (cast e).register(this);
  }

  public inline function unregister(e: E): Void {
    // cast to avoid deprecation warning
    (cast e).unregister(this);
  }
}
