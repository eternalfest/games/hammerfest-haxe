class Main {

  public static function main(): Void {
    // Expose the GameManager class so the loader can call the game entrypoint
    flash.Lib.current.GameManager = hf.GameManager;

    // Expose the Adventure class so the loader can patch it
    flash.Lib.current.mode = {
      Adventure: hf.mode.Adventure
    };
  }
}