package hf.levels;

import hf.GameManager;
import hf.levels.Data in LevelData;
import hf.levels.TeleporterData;
import hf.mode.GameMode;
import hf.Pos;
import hf.Data in Data;

class SetManager {
  public var manager: GameManager;
  public var fl_read: Array<Bool>;
  public var levels: Array<LevelData>;
  public var raw: Array<String>;
  public var fl_mirror: Bool;
  public var setName: String;
  public var teleporterList: Array<TeleporterData>;
  public var portalList: Array<PortalData>;
  public var csum: Int;
  public var _previous: Null<LevelData>;
  public var _previousId: Null<Int>;
  public var current: Null<LevelData>;
  public var currentId: Null<Int>;

  public function new(m: GameManager, s: String): Void {
    this.manager = m;
    this.fl_read = new Array();
    this.fl_mirror = false;
    this.setName = s;
    this.teleporterList = new Array();
    this.portalList = new Array();
    var v4 = HfStd.getVar(this.manager.root, this.setName);
    this.raw = v4.split(":");
    if (HfStd.getVar(this.manager.root, this.setName + "_back_xml") == null) {
      HfStd.setVar(this.manager.root, this.setName + "_back_xml", this.raw.join(":"));
    }
    this.csum = 0;
    var v5 = v4.split("0");
    var v6 = this.setName.charCodeAt(3);
    for (v7 in 0...v5.length) {
      var v8 = v5[v7];
      if (v8.length > 30) {
        this.csum += v8.charCodeAt(v6 % 5) + v8.charCodeAt(v6 % 9) * v8.charCodeAt(v6 % 15) + v8.charCodeAt(v6 % 19) + v8.charCodeAt(v6 % 22);
      }
    }
    if (GameManager.HH.get("$" + Md5.encode(this.setName)) != "$" + Md5.encode("" + this.csum)) {
      return;
    }
    this.importCookie();
    if (this.raw == null) {
      GameManager.fatal("Error reading " + this.setName + " (null value)");
      return;
    }
    this.levels = new Array();
    this.levels[this.raw.length - 1] = null;
  }

  public function destroy(): Void {
    this.suspend();
    this.levels = new Array();
    this.fl_read = new Array();
  }

  public function overwrite(sdata: String): Void {
    if (HfStd.getVar(this.manager.root, this.setName + "_back") == null) {
      HfStd.setVar(this.manager.root, this.setName + "_back", this.raw.join(":"));
    }
    this.raw = sdata.split(":");
    HfStd.setVar(this.manager.root, this.setName, sdata);
  }

  public function rollback(): Void {
    if (HfStd.getVar(this.manager.root, this.setName + "_back") != null) {
      var v2 = HfStd.getVar(this.manager.root, this.setName + "_back");
      HfStd.setVar(this.manager.root, this.setName, v2);
      this.raw = v2.split(":");
    }
  }

  public function rollback_xml(): Void {
    var v2 = HfStd.getVar(this.manager.root, this.setName + "_back_xml");
    HfStd.setVar(this.manager.root, this.setName, v2);
    this.raw = v2.split(":");
  }

  public function showField(td: TeleporterData): Void {
    if (td.fl_on) {
      return;
    }
    td.fl_on = true;
    td.mc.skin.sub.gotoAndStop("2");
    td.podA.gotoAndStop("2");
    td.podB.gotoAndStop("2");
  }

  public function hideField(td: TeleporterData): Void {
    if (!td.fl_on) {
      return;
    }
    td.fl_on = false;
    td.mc.skin.sub.gotoAndStop("1");
    td.podA.gotoAndStop("1");
    td.podB.gotoAndStop("1");
  }

  public function suspend(): Void {
  }

  public function restore(lid: Int): Void {
  }

  public function setCurrent(id: Int): Void {
    if (GameManager.HH.get("$" + Md5.encode(this.setName)) != "$" + Md5.encode("" + this.csum)) {
      return;
    }
    this._previous = this.current;
    this._previousId = this.currentId;
    this.current = this.levels[id];
    this.currentId = id;
  }

  public function isDataReady(): Bool {
    return this.fl_read[this.currentId];
  }

  public function checkDataReady(): Void {
    if (this.isDataReady()) {
      this.onDataReady();
    }
  }

  public function goto(id: Int): Void {
    this.teleporterList = new Array();
    if (id >= this.levels.length) {
      this.onEndOfSet();
      id = this.currentId;
      return;
    }
    if (!this.fl_read[id]) {
      this.levels[id] = this.unserialize(id);
    }
    this.setCurrent(id);
    this.onReadComplete();
  }

  public function next(): Void {
    if (GameManager.HH.get("$" + Md5.encode(this.setName)) != "$" + Md5.encode("" + this.csum)) {
      return;
    }
    this.goto(this.currentId + 1);
  }

  public function delete(id: Int): Void {
    if (id >= this.levels.length) {
      GameManager.fatal("delete after end");
    }
    this.raw.splice(id, 1);
    this.levels.splice(id, 1);
    this.fl_read.splice(id, 1);
  }

  public function insert(id: Int, data: LevelData): Void {
    this.raw.insert(id, this.serializeExternal(data));
    this.levels.insert(id, data);
    this.fl_read.insert(id, true);
  }

  public function push(data: LevelData): Void {
    this.raw.push(this.serializeExternal(data));
    this.levels.push(data);
    this.fl_read.push(true);
  }

  public function flip(l: LevelData): LevelData {
    if (!this.fl_mirror) {
      return l;
    }
    var v3 = new hf.levels.Data();
    v3.__dollar__playerX = Data.LEVEL_WIDTH - l.__dollar__playerX - 1;
    v3.__dollar__playerY = l.__dollar__playerY;
    v3.__dollar__skinTiles = l.__dollar__skinTiles;
    v3.__dollar__skinBg = l.__dollar__skinBg;
    v3.__dollar__script = l.__dollar__script;
    v3.__dollar__badList = l.__dollar__badList;
    v3.__dollar__specialSlots = l.__dollar__specialSlots;
    v3.__dollar__scoreSlots = l.__dollar__scoreSlots;
    v3.__dollar__map = new Array();
    for (v4 in 0...Data.LEVEL_WIDTH) {
      v3.__dollar__map[v4] = new Array();
      for (v5 in 0...Data.LEVEL_HEIGHT) {
        v3.__dollar__map[v4][v5] = l.__dollar__map[Data.LEVEL_WIDTH - v4 - 1][v5];
      }
    }
    return v3;
  }

  public function flipPortals(): Void {
    var v2 = new Array();
    var v3 = new Array();
    for (v4 in 0...this.portalList.length) {
      var v5 = this.portalList[v4];
      if (v2[v5.cy] == null) {
        v2[v5.cy] = new Array();
      }
      v2[v5.cy].push(v5);
    }
    for (v6 in 0...v2.length) {
      if (v2[v6] != null) {
        var v7 = v2[v6].length - 1;
        while (v7 >= 0) {
          var v8 = v2[v6][v7];
          v3.push(v8);
          --v7;
        }
      }
    }
    this.portalList = v3;
  }

  public function isEmptyLevel(id: Int, g: GameMode): Bool {
    var v6;
    if (id >= this.levels.length) {
      return true;
    }
    if (!this.fl_read[id]) {
      this.levels[id] = this.unserialize(id);
    }
    var v4 = this.levels[id];
    var v5 = new hf.levels.Data();
    var v7 = v5.__dollar__playerY;
    if (g == null) {
      v6 = v5.__dollar__playerX;
    } else {
      v6 = g.flipCoordCase(v5.__dollar__playerX);
    }
    return v4.__dollar__playerX == v6 && v4.__dollar__playerY == v7 && v4.__dollar__skinBg == v5.__dollar__skinBg && v4.__dollar__skinTiles == v5.__dollar__skinTiles && v4.__dollar__badList.length == 0 && v4.__dollar__specialSlots.length == 0 && v4.__dollar__scoreSlots.length == 0;
  }

  public function getCase(pt: Pos<Int>): Int {
    var v3 = pt.x;
    var v4 = pt.y;
    if (this.inBound(v3, v4)) {
      if (v4 == 0) {
        if (this.current.__dollar__map[v3][0] > 0) {
          return Data.WALL;
        }
        return 0;
      }
      return this.current.__dollar__map[v3][v4];
    }
    if (v4 < 0) {
      if (this.current.__dollar__map[v3][0] > 0) {
        return Data.WALL;
      }
      return 0;
    }
    return Data.OUT_WALL;
  }

  public function forceCase(cx: Int, cy: Int, t: Int): Void {
    if (this.inBound(cx, cy)) {
      if (t <= 0 && this.getCase({x: cx, y: cy}) > 0 && this.getCase({x: cx, y: cy + 1}) == Data.WALL) {
        this.forceCase(cx, cy + 1, Data.GROUND);
      }
      this.current.__dollar__map[cx][cy] = t;
    }
  }

  public function inBound(cx: Int, cy: Int): Bool {
    return cx >= 0 && cx < Data.LEVEL_WIDTH && cy >= 0 && cy < Data.LEVEL_HEIGHT;
  }

  public function shapeInBound(e: Entity): Bool {
    return e.x >= -e._width && e.x < Data.GAME_WIDTH && e.y >= -e._height && e.y < Data.GAME_HEIGHT;
  }

  public function getGround(cx: Int, cy: Int): Pos<Int> {
    var v5 = 0, v4 = cy;
    while (v5 <= Data.LEVEL_HEIGHT) {
      if (v4 > 0 && this.getCase({x: cx, y: v4}) == Data.GROUND) {
        return {x: cx, y: v4 - 1};
      }
      if (v4 >= Data.LEVEL_HEIGHT) {
        v4 = 0;
      }
      ++v5; ++v4;
    }
    return {x: cx, y: cy};
  }

  public function onReadComplete(): Void {
    this.checkDataReady();
  }

  public function onDataReady(): Void {
  }

  public function onEndOfSet(): Void {
  }

  public function onRestoreReady(): Void {
  }

  public function unserialize(id: Int): LevelData {
    if (GameManager.HH.get("$" + Md5.encode(this.setName)) != "$" + Md5.encode("" + this.csum)) {
      return null;
    }
    var v3 = (new PersistCodec()).decode(this.raw[id]);
    if (this.fl_mirror) {
      v3 = this.flip(v3);
    }
    this.convertWalls(v3);
    if (v3.__dollar__specialSlots == null || v3.__dollar__scoreSlots == null) {
      GameManager.warning("empty slot array found ! spec=" + v3.__dollar__specialSlots.length + " score=" + v3.__dollar__scoreSlots.length);
    }
    this.fl_read[id] = true;
    return v3;
  }

  public function serialize(id: Int): String {
    this.convertWalls(this.levels[id]);
    var v3 = (new PersistCodec()).encode(this.levels[id]);
    return v3;
  }

  public function serializeExternal(l: LevelData): String {
    this.convertWalls(l);
    return (new PersistCodec()).encode(l);
  }

  public function convertWalls(l: LevelData): Void {
    var v3 = l.__dollar__map;
    for (v4 in 0...Data.LEVEL_HEIGHT) {
      for (v5 in 0...Data.LEVEL_WIDTH) {
        if (v3[v5][v4] == Data.WALL) {
          v3[v5][v4] = Data.GROUND;
          GameManager.warning("found wall @ " + v5 + "," + v4);
        }
      }
    }
  }

  public function exportCookie(): Void {
    if (!this.manager.fl_cookie) {
      return;
    }
    this.manager.cookie.saveSet(this.setName, this.raw.join(":"));
  }

  public function importCookie(): Void {
    if (!this.manager.fl_cookie) {
      return;
    }
    var v2 = this.manager.cookie.readSet(this.setName);
    if (v2 != null) {
      this.raw = v2.split(":");
    } else {
      this.exportCookie();
    }
  }

  public function trace(id: Int): Void {
    Log.trace("Total size: " + this.levels.length + " level(s)");
    if (id != null) {
      Log.trace("Level " + id + ":");
      Log.trace("player: " + this.current.__dollar__playerX + "," + this.current.__dollar__playerY);
      Log.trace(this.current.__dollar__map);
    }
  }

  public function update(): Void {
  }
}
