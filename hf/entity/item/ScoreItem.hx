package hf.entity.item;

import hf.entity.Item;
import hf.mode.GameMode;

class ScoreItem extends Item {
  public function new(): Void {
    super();
  }

  public static function attach(g: GameMode, x: Float, y: Float, id: Int, subId: Null<Int>): ScoreItem {
    var v7: ScoreItem = cast g.depthMan.attach("hammer_item_score", Data.DP_ITEMS);
    if (id >= 1000) {
      id -= 1000;
    }
    v7.initItem(g, x, y, id, subId);
    return v7;
  }

  override public function execute(p: Player): Void {
    var v4 = Data.ITEM_VALUES[this.id + 1000];
    this.game.soundMan.playSound("sound_item_score", Data.CHAN_ITEM);
    if (v4 == 0 || v4 == null) {
      switch (this.id) {
        case 0:
          v4 = Data.getCrystalValue(this.subId);
        case Data.DIAMANT:
          v4 = 2000;
        case Data.CONVERT_DIAMANT:
          v4 = Math.round(Math.min(10000, 75 * Math.pow(this.subId + 1, 4)));
        default:
          GameManager.fatal("null value");
      }
    }
    p.getScore(this, v4);
    this.game.pickUpScore(this.id, this.subId);
    var v5 = null;
    var v6 = 0;
    var v7 = Data.SCORE_ITEM_FAMILIES;
    while (true) {
      if (!(v5 == null && v6 < v7.length)) break;
      var v8 = 0;
      while (true) {
        if (!(v5 == null && v8 < v7[v6].length)) break;
        if (v7[v6][v8].id == this.id + 1000) {
          v5 = v7[v6][v8].r;
        }
        ++v8;
      }
      ++v6;
    }
    if (v5 > 0) {
      this.game.attachItemName(Data.SCORE_ITEM_FAMILIES, this.id + 1000);
    }
    super.execute(p);
  }

  override public function init(g: GameMode): Void {
    super.init(g);
  }

  override public function initItem(g: GameMode, x: Float, y: Float, i: Int, si: Null<Int>): Void {
    super.initItem(g, x, y, i, si);
    if (this.id == Data.CONVERT_DIAMANT) {
      this.register(Data.PERFECT_ITEM);
    }
  }
}
