package hf.mode;

import etwin.flash.MovieClip;
import hf.gui.Container;
import hf.gui.SimpleButton;
import hf.levels.SetManager;
import hf.levels.View;
import hf.Mode;
import etwin.flash.Key;
import etwin.flash.Mouse;
import etwin.flash.System;
import etwin.flash.XML;
using hf.compat.XMLNodeTools;

class Editor extends Mode {
  public var fl_menu: Bool;
  public var fl_click: Bool;
  public var fl_modified: Array<Bool>;
  public var badList: Array<Dynamic /*TODO*/>;
  public var badId: Int;
  public var slotList: Array<Dynamic /*TODO*/>;
  public var triggerList: Array<Dynamic /*TODO*/>;
  public var fieldList: Array<Dynamic /*TODO*/>;
  public var fieldId: Int;
  public var tool: Int;
  public var resetConfirm: Float;
  public var firstLevel: Int;
  public var firstSet: Dynamic /*TODO*/;
  public var input: String;
  public var fields: MovieClip;
  public var header: Dynamic /*TODO*/;
  public var footer: Dynamic /*TODO*/;
  public var cursor: Null<Dynamic /*TODO*/>;
  public var borders: Null<Dynamic /*TODO*/>;
  public var lastPoint: Null<Dynamic /*TODO*/>;
  public var view: Null<View>;
  public var world: Null<SetManager>;
  public var menuC: Null<Container>;
  public var logMC: Null<Dynamic /*TODO*/>;
  public var log: Null<Dynamic /*TODO*/>;
  public var tileButton: Null<SimpleButton>;
  public var startButton: Null<SimpleButton>;
  public var badButton: Null<SimpleButton>;
  public var fieldButton: Null<SimpleButton>;
  public var specialSlotButton: Null<SimpleButton>;
  public var scoreSlotButton: Null<SimpleButton>;
  public var fl_save: Null<Bool>;
  public var startMc: Null<Dynamic /*TODO*/>;
  public var bList: Null<Dynamic /*TODO*/>;
  public var currentBad: Null<Dynamic /*TODO*/>;
  public var lastKey: Null<Dynamic /*TODO*/>;
  public var fl_lockFollow: Null<Bool>;
  public var save_currentId: Dynamic /*TODO*/;
  public var save_raw: Dynamic /*TODO*/;
  public var styleBuffer: Dynamic /*TODO*/;
  public var fl_lockToggle: Dynamic /*TODO*/;
  public var cx: Int;
  public var cy: Int;
  public var buffer: Dynamic /*TODO*/;
  public var setName: Dynamic /*TODO*/;
  public var dimensionId: Dynamic /*TODO*/;

  public static var DIMENSIONS: Array<Dynamic /*TODO*/> = [{name: "xml_adventure", id: 0}, {name: "xml_deepnight", id: 1}, {name: "xml_hiko", id: 2}, {name: "xml_ayame", id: 3}, {name: "xml_hk", id: 4}];

  public function new(m: Dynamic /*TODO*/, fset: Dynamic /*TODO*/, id: Int): Void {
    super(m);
    this._name = "$editor";
    this.xOffset = 10;
    this.fl_menu = true;
    this.fl_click = false;
    this.fl_modified = new Array();
    this.badList = new Array();
    this.badId = 0;
    this.slotList = new Array();
    this.triggerList = new Array();
    this.fieldList = new Array();
    this.fieldId = 1;
    this.tool = Data.TOOL_TILE;
    this.resetConfirm = 0;
    this.firstLevel = id;
    this.firstSet = fset;
    this.input = "";
    this.fields = this.depthMan.empty(Data.DP_INTERF);
    this.header = this.depthMan.attach("hammer_editor_footer", Data.DP_INTERF);
    this.header._x = Data.DOC_WIDTH * 0.5;
    this.header._y = 9;
    this.header.scriptIcon._visible = false;
    this.footer = this.depthMan.attach("hammer_editor_footer", Data.DP_INTERF);
    this.footer._x = Data.DOC_WIDTH * 0.5;
    this.footer._y = Data.DOC_HEIGHT - 9;
    this.lock();
  }

  override public function init(): Void {
    super.init();
    this.cursor = this.depthMan.attach("hammer_editor_cursor", Data.DP_INTERF);
    this.cursor._width = Data.CASE_WIDTH;
    this.cursor._height = Data.CASE_HEIGHT;
    this.updateCursor();
    this.root.onMouseDown = function () {
      this.mouseDown();
    };
    this.root.onMouseUp = function () {
      this.mouseUp();
    };
    this.borders = this.depthMan.attach("hammer_editor_borders", Data.DP_INTERF);
    this.borders._x -= 10;
    this.attachMenu();
    this.toggleMenu();
    this.loadSet(this.firstSet);
    this.redrawAll();
    this.unlock();
  }

  public function loadSet(n: String): Void {
    this.setName = n;
    this.dimensionId = null;
    for (v3 in 0...hf.mode.Editor.DIMENSIONS.length) {
      if (hf.mode.Editor.DIMENSIONS[v3].name == this.setName) {
        this.dimensionId = hf.mode.Editor.DIMENSIONS[v3].id;
      }
    }
    this.view.destroy();
    this.world.destroy();
    this.world = new hf.levels.SetManager(this.manager, this.setName);
    this.world.goto(Std.int(Math.min(this.firstLevel, this.world.levels.length - 1)));
    this.firstLevel = 0;
    this.lastPoint = null;
    this.fl_modified = new Array();
  }

  public function attachMenu(): Void {
    this.menuC = new hf.gui.Container(this, 5, 10, Data.DOC_WIDTH - 10);
    this.logMC = this.depthMan.attach("hammer_editor_log", Data.DP_INTERF);
    this.logMC._x = 6;
    this.logMC._y = 250;
    this.log = this.logMC.field;
    this.cls();
    hf.gui.SimpleButton.attach(this.menuC, " QUIT ", null, HfStd.callback(this, "onQuit"));
    hf.gui.SimpleButton.attach(this.menuC, " Test level", 84, HfStd.callback(this, "onTest"));
    hf.gui.SimpleButton.attach(this.menuC, " Browser", 222, HfStd.callback(this, "onBrowse"));
    hf.gui.SimpleButton.attach(this.menuC, " Script", Key.ENTER, HfStd.callback(this, "onScript"));
    hf.gui.SimpleButton.attach(this.menuC, " Items", 73, HfStd.callback(this, "onItemBrowser"));
    hf.gui.SimpleButton.attach(this.menuC, " Quests", 81, HfStd.callback(this, "onQuestBrowser"));
    hf.gui.SimpleButton.attach(this.menuC, " Game", 71, HfStd.callback(this, "onStartGame"));
    this.menuC.endLine();
    hf.gui.Label.attach(this.menuC, "disk:");
    var v2 = hf.gui.SimpleButton.attach(this.menuC, "load all", 76, HfStd.callback(this, "onLoadAll"));
    v2.setToggleKey(Key.SHIFT);
    v2 = hf.gui.SimpleButton.attach(this.menuC, "save all", 83, HfStd.callback(this, "onSaveAll"));
    v2.setToggleKey(Key.SHIFT);
    hf.gui.SimpleButton.attach(this.menuC, " reset cookie ", null, HfStd.callback(this, "onCookieReset"));
    this.menuC.endLine();
    hf.gui.Label.attach(this.menuC, "files:");
    hf.gui.SimpleButton.attach(this.menuC, " adv", null, HfStd.callback(this, "onLoadAdv"));
    hf.gui.SimpleButton.attach(this.menuC, " tuto", null, HfStd.callback(this, "onLoadTuto"));
    hf.gui.SimpleButton.attach(this.menuC, " sharew", null, HfStd.callback(this, "onLoadShare"));
    hf.gui.SimpleButton.attach(this.menuC, " dev", null, HfStd.callback(this, "onLoadDev"));
    hf.gui.SimpleButton.attach(this.menuC, " test", null, HfStd.callback(this, "onLoadTest"));
    hf.gui.SimpleButton.attach(this.menuC, " time", null, HfStd.callback(this, "onLoadTime"));
    hf.gui.SimpleButton.attach(this.menuC, " mtime", null, HfStd.callback(this, "onLoadMultiTime"));
    hf.gui.SimpleButton.attach(this.menuC, " soccer", null, HfStd.callback(this, "onLoadSoccer"));
    hf.gui.SimpleButton.attach(this.menuC, " HoF", null, HfStd.callback(this, "onLoadHof"));
    hf.gui.SimpleButton.attach(this.menuC, " FJV", null, HfStd.callback(this, "onLoadFjv"));
    hf.gui.SimpleButton.attach(this.menuC, " deep", null, HfStd.callback(this, "onLoadDeep"));
    hf.gui.SimpleButton.attach(this.menuC, " hiko", null, HfStd.callback(this, "onLoadHiko"));
    hf.gui.SimpleButton.attach(this.menuC, " ayame", null, HfStd.callback(this, "onLoadAyame"));
    hf.gui.SimpleButton.attach(this.menuC, " HK", null, HfStd.callback(this, "onLoadHk"));
    this.menuC.endLine();
    hf.gui.Label.attach(this.menuC, "nav:");
    hf.gui.SimpleButton.attach(this.menuC, "<", 33, HfStd.callback(this, "onPrevLevel"));
    hf.gui.SimpleButton.attach(this.menuC, ">", 34, HfStd.callback(this, "onNextLevel"));
    v2 = hf.gui.SimpleButton.attach(this.menuC, "<<", 33, HfStd.callback(this, "onPrevLevelFast"));
    v2.setToggleKey(Key.SHIFT);
    v2 = hf.gui.SimpleButton.attach(this.menuC, ">>", 34, HfStd.callback(this, "onNextLevelFast"));
    v2.setToggleKey(Key.SHIFT);
    v2 = hf.gui.SimpleButton.attach(this.menuC, " first ", Key.HOME, HfStd.callback(this, "onFirstLevel"));
    v2 = hf.gui.SimpleButton.attach(this.menuC, " last ", Key.END, HfStd.callback(this, "onLastLevel"));
    hf.gui.SimpleButton.attach(this.menuC, " CLEAR ", null, HfStd.callback(this, "onNew"));
    this.menuC.endLine();
    hf.gui.Label.attach(this.menuC, "buffer:");
    hf.gui.SimpleButton.attach(this.menuC, " copy ", 67, HfStd.callback(this, "onCopy"));
    hf.gui.SimpleButton.attach(this.menuC, " paste ", 86, HfStd.callback(this, "onPaste"));
    hf.gui.Label.attach(this.menuC, "style:");
    v2 = hf.gui.SimpleButton.attach(this.menuC, " copy ", 67, HfStd.callback(this, "onStyleCopy"));
    v2.setToggleKey(Key.SHIFT);
    v2 = hf.gui.SimpleButton.attach(this.menuC, " paste ", 86, HfStd.callback(this, "onStylePaste"));
    v2.setToggleKey(Key.SHIFT);
    this.menuC.endLine();
    hf.gui.Label.attach(this.menuC, "bg:");
    hf.gui.SimpleButton.attach(this.menuC, "<", null, HfStd.callback(this, "onPrevBg"));
    hf.gui.SimpleButton.attach(this.menuC, ">", null, HfStd.callback(this, "onNextBg"));
    hf.gui.Label.attach(this.menuC, "skin:");
    hf.gui.SimpleButton.attach(this.menuC, "<", null, HfStd.callback(this, "onPrevTiles"));
    hf.gui.SimpleButton.attach(this.menuC, ">", null, HfStd.callback(this, "onNextTiles"));
    hf.gui.Label.attach(this.menuC, "col:");
    hf.gui.SimpleButton.attach(this.menuC, "<", null, HfStd.callback(this, "onPrevColumn"));
    hf.gui.SimpleButton.attach(this.menuC, ">", null, HfStd.callback(this, "onNextColumn"));
    hf.gui.SimpleButton.attach(this.menuC, "==", null, HfStd.callback(this, "onResetColumn"));
    this.menuC.endLine();
    hf.gui.Label.attach(this.menuC, "tools:");
    this.tileButton = hf.gui.SimpleButton.attach(this.menuC, " tiles ", 49, HfStd.callback(this, "onSelectTile"));
    this.startButton = hf.gui.SimpleButton.attach(this.menuC, " S ", 50, HfStd.callback(this, "onSelectStart"));
    hf.gui.SimpleButton.attach(this.menuC, "<", null, HfStd.callback(this, "onPrevBad"));
    this.badButton = hf.gui.SimpleButton.attach(this.menuC, "      ", 51, HfStd.callback(this, "onSelectBad"));
    hf.gui.SimpleButton.attach(this.menuC, ">", null, HfStd.callback(this, "onNextBad"));
    hf.gui.SimpleButton.attach(this.menuC, "<", null, HfStd.callback(this, "onPrevField"));
    this.fieldButton = hf.gui.SimpleButton.attach(this.menuC, " field ", 52, HfStd.callback(this, "onSelectField"));
    hf.gui.SimpleButton.attach(this.menuC, ">", null, HfStd.callback(this, "onNextField"));
    this.specialSlotButton = hf.gui.SimpleButton.attach(this.menuC, " specialSlot ", 53, HfStd.callback(this, "onSelectSpecial"));
    this.scoreSlotButton = hf.gui.SimpleButton.attach(this.menuC, " scoreSlot ", 54, HfStd.callback(this, "onSelectScore"));
    hf.gui.Label.attach(this.menuC, "panning:");
    v2 = hf.gui.SimpleButton.attach(this.menuC, " left ", Key.LEFT, HfStd.callback(this, "onPanLeft"));
    v2.setToggleKey(Key.SHIFT);
    v2 = hf.gui.SimpleButton.attach(this.menuC, " right ", Key.RIGHT, HfStd.callback(this, "onPanRight"));
    v2.setToggleKey(Key.SHIFT);
    v2 = hf.gui.SimpleButton.attach(this.menuC, " up ", Key.UP, HfStd.callback(this, "onPanUp"));
    v2.setToggleKey(Key.SHIFT);
    v2 = hf.gui.SimpleButton.attach(this.menuC, " down ", Key.DOWN, HfStd.callback(this, "onPanDown"));
    v2.setToggleKey(Key.SHIFT);
  }

  public function toggleMenu(): Void {
    if (this.fl_save) {
      return;
    }
    this.fl_menu = !this.fl_menu;
    this.menuC.mc._visible = this.fl_menu;
    this.logMC._visible = this.fl_menu;
    this.currentBad._visible = this.fl_menu;
    this.cursor._visible = !this.fl_menu;
    if (this.fl_menu) {
      this.hideCaseCursor();
    } else {
      this.showCaseCursor();
    }
  }

  public function display(txt: String): Void {
    if (txt == null) {
      this.cls();
    } else {
      var v3 = '' + (this.log.text);
      v3 += "~ " + txt + "\n";
      this.log.text = v3;
      while (this.log.textHeight > this.log._height) {
        this.log.text = this.log.text.substr(this.log.text.indexOf("~", 2), 999999);
      }
    }
  }

  public function cls(): Void {
    this.log.text = "";
  }

  public function redrawAll(): Void {
    this.redrawView();
    this.redrawBads();
    this.redrawSlots();
    this.redrawScript();
  }

  public function redrawView(): Void {
    if (this.view.fl_attach) {
      this.view.detach();
      this.view.displayCurrent();
    } else {
      var world: hf.levels.GameMechanics = cast this.world;
      this.view = new hf.levels.View(world, this.depthMan);
      this.view.fl_hideBorders = true;
      this.view.xOffset = 0;
      this.view.detach();
      this.view.displayCurrent();
    }
    this.view.updateSnapShot();
    for (v2 in 0...this.fieldList.length) {
      this.fieldList[v2].removeTextField();
    }
    this.fieldList = new Array();
    for (v3 in 0...this.world.portalList.length) {
      var v4 = HfStd.createTextField(this.fields, this.manager.uniq++);
      var v5 = this.world.portalList[v3];
      v4._x = Entity.x_ctr(v5.cx) - Data.CASE_WIDTH * 0.2;
      v4._y = Entity.y_ctr(v5.cy) - Data.CASE_HEIGHT * 0.5;
      v4.textColor = 16777215;
      var v6 = Data.getLink(this.dimensionId, this.world.currentId, v3);
      v4.text = "" + v3;
      if (v6 == null) {
        v4.text += " > ??";
        v4.textColor = 16733440;
      } else {
        if (v6.to_did == this.dimensionId) {
          v4.text += " > " + v6.to_lid;
        } else {
          v4.text += " > " + v6.to_did + "," + v6.to_lid;
        }
      }
      if (v4._x + v4.textWidth >= Data.GAME_WIDTH) {
        v4._x = Data.GAME_WIDTH - v4.textWidth;
      }
      if (v4._y + v4.textHeight >= Data.GAME_HEIGHT) {
        v4._y = Data.GAME_HEIGHT - v4.textHeight;
      }
      v4.selectable = false;
      this.fieldList.push(v4);
    }
    this.startMc.removeMovieClip();
    this.startMc = this.depthMan.attach("hammer_editor_start", Data.DP_BADS);
    this.startMc._x = this.world.current.__dollar__playerX * Data.CASE_WIDTH;
    this.startMc._y = this.world.current.__dollar__playerY * Data.CASE_HEIGHT;
  }

  public function redrawBads(): Void {
    for (v2 in 0...this.bList.length) {
      this.bList[v2].removeMovieClip();
    }
    this.bList = new Array();
    var v3 = 1;
    for (v4 in 0...this.world.current.__dollar__badList.length) {
      var v5 = this.world.current.__dollar__badList[v4];
      var v7 = Entity.x_ctr(v5.__dollar__x);
      var v8 = Entity.y_ctr(v5.__dollar__y);
      var v6: etwin.flash.DynamicMovieClip = cast this.depthMan.attach("hammer_editor_bad_bg", Data.DP_BADS);
      v6._x = v7;
      v6._y = v8;
      if (v5.__dollar__id != 13 && v5.__dollar__id != 3 && v5.__dollar__id != 15) {
        v6.field.text = v3;
        ++v3;
      } else {
        v6.field.text = "";
      }
      this.bList.push(v6);
      v6 = cast this.depthMan.attach(Data.LINKAGES[v5.__dollar__id], Data.DP_BADS);
      v6._x = v7;
      v6._y = v8;
      if (v6._height > 1.2 * Data.CASE_HEIGHT) {
        var v9 = 1.2 * Data.CASE_HEIGHT / v6._height;
        v6._xscale = v9 * 100;
        v6._yscale = v9 * 100;
      }
      v6.stop();
      v6.sub.stop();
      this.bList.push(v6);
    }
    this.updateBad();
  }

  public function redrawSlots(): Void {
    for (v2 in 0...this.slotList.length) {
      this.slotList[v2].removeMovieClip();
    }
    this.slotList = new Array();
    for (v3 in 0...this.world.current.__dollar__scoreSlots.length) {
      var v4 = this.world.current.__dollar__scoreSlots[v3];
      var v5 = this.depthMan.attach("hammer_editor_slot_score", Data.DP_ITEMS);
      v5._x = Entity.x_ctr(v4.__dollar__x);
      v5._y = Entity.y_ctr(v4.__dollar__y);
      this.slotList.push(v5);
    }
    for (v6 in 0...this.world.current.__dollar__specialSlots.length) {
      var v7 = this.world.current.__dollar__specialSlots[v6];
      var v8 = this.depthMan.attach("hammer_editor_slot_special", Data.DP_ITEMS);
      v8._x = Entity.x_ctr(v7.__dollar__x);
      v8._y = Entity.y_ctr(v7.__dollar__y);
      this.slotList.push(v8);
    }
  }

  public function redrawScript(): Void {
    for (v2 in 0...this.triggerList.length) {
      this.triggerList[v2].removeMovieClip();
    }
    this.triggerList = new Array();
    var v3 = new XML(this.world.current.__dollar__script);
    var v4 = v3.firstChild;
    while (v4 != null) {
      var v5 = HfStd.parseInt(v4.get("$x"), 10);
      var v6 = HfStd.parseInt(v4.get("$y"), 10);
      if (!HfStd.isNaN(v5) && !HfStd.isNaN(v6)) {
        var v7: etwin.flash.DynamicMovieClip = cast this.depthMan.attach("hammer_editor_trigger", Data.DP_ITEMS);
        v7._x = Entity.x_ctr(v5) - Data.CASE_WIDTH * 0.5;
        v7._y = Entity.y_ctr(v6) - Data.CASE_HEIGHT;
        v7.field.text = v4.nodeName.substring(1);
        this.triggerList.push(v7);
      }
      v4 = v4.nextSibling;
    }
  }

  public function updateFooter(): Void {
    this.footer.field.text = "Set: " + this.setName + " | Level: " + this.world.currentId + "/" + (this.world.levels.length - 1);
    if (this.fl_modified[this.world.currentId]) {
      this.footer.field.text += " (MODIFIED)";
      this.footer.field.textColor = 16711680;
    } else {
      if (this.world.current.__dollar__specialSlots.length == 0 || this.world.current.__dollar__scoreSlots.length == 0) {
        this.footer.field.textColor = 5592575;
      } else {
        this.footer.field.textColor = 16777215;
      }
    }
    if (this.world.current.__dollar__script != "" && this.world.current.__dollar__script != null) {
      this.footer.scriptIcon._visible = true;
    } else {
      this.footer.scriptIcon._visible = false;
    }
  }

  public function goto(id: Int): Void {
    id = Std.int(Math.max(id, 0));
    id = Std.int(Math.min(id, this.world.levels.length - 1));
    this.display("Level " + id);
    this.view.detach();
    this.world.goto(id);
    this.lastPoint = null;
    this.redrawAll();
  }

  public function updateMenu(): Void {
    this.fieldButton.setLabel(Data.FIELDS[this.fieldId]);
    this.tileButton.rollOut();
    this.startButton.rollOut();
    this.badButton.rollOut();
    this.fieldButton.rollOut();
    this.specialSlotButton.rollOut();
    this.scoreSlotButton.rollOut();
    switch (this.tool) {
      case Data.TOOL_TILE:
        this.tileButton.rollOver();
      case Data.TOOL_BAD:
        this.badButton.rollOver();
      case Data.TOOL_FIELD:
        this.fieldButton.rollOver();
      case Data.TOOL_START:
        this.startButton.rollOver();
      case Data.TOOL_SPECIAL:
        this.specialSlotButton.rollOver();
      case Data.TOOL_SCORE:
        this.scoreSlotButton.rollOver();
    }
  }

  public function updateBad(): Void {
    this.currentBad.removeMovieClip();
    this.currentBad = this.depthMan.attach(Data.LINKAGES[this.badId], Data.DP_INTERF);
    this.currentBad.stop();
    this.currentBad.sub.stop();
    this.currentBad._x = this.badButton._x + 20;
    this.currentBad._y = this.badButton._y + 30;
    this.currentBad._xscale = 60;
    this.currentBad._yscale = this.currentBad._xscale;
    this.currentBad._visible = this.fl_menu;
    this.currentBad.onRelease = HfStd.callback(this, "onSelectBad");
  }

  public function square(cx1: Int, cy1: Int, cx2: Int, cy2: Int): Void {
    if (cx1 > cx2) {
      var v6 = cx1;
      cx1 = cx2;
      cx2 = v6;
    }
    if (cy1 > cy2) {
      var v7 = cy1;
      cy1 = cy2;
      cy2 = v7;
    }
    var v8 = cx1;
    while (v8 <= cx2) {
      var v9 = cy1;
      while (v9 <= cy2) {
        if (Key.isDown(Key.CONTROL)) {
          this.world.current.__dollar__map[v8][v9] = 0;
        } else {
          this.world.current.__dollar__map[v8][v9] = 1;
        }
        ++v9;
      }
      ++v8;
    }
  }

  public function paint(cx: Int, cy: Int): Void {
    if (this.menuC.fl_lock) {
      return;
    }
    switch (this.tool) {
      case Data.TOOL_TILE:
        if (Key.isDown(Key.SHIFT) && this.lastPoint != null) {
          this.square(this.lastPoint.cx, this.lastPoint.cy, cx, cy);
          this.lastPoint = null;
        } else {
          this.world.current.__dollar__map[cx][cy] = 1;
          this.lastPoint = {cx: cx, cy: cy};
        }
      case Data.TOOL_BAD:
        var v4 = false;
        var v5 = {__dollar__id: this.badId, __dollar__x: cx, __dollar__y: cy};
        this.world.current.__dollar__badList.push(v5);
        this.redrawBads();
      case Data.TOOL_FIELD:
        this.world.current.__dollar__map[cx][cy] = -this.fieldId;
      case Data.TOOL_START:
        this.world.current.__dollar__playerX = cx;
        this.world.current.__dollar__playerY = cy;
      case Data.TOOL_SPECIAL:
        if (this.world.current.__dollar__specialSlots == null) {
          GameManager.warning("warning: null special slots array !");
        }
        this.world.current.__dollar__specialSlots.push({__dollar__x: cx, __dollar__y: cy});
        this.redrawSlots();
      case Data.TOOL_SCORE:
        this.world.current.__dollar__scoreSlots.push({__dollar__x: cx, __dollar__y: cy});
        this.redrawSlots();
    }
    this.setModified(this.world.currentId);
  }

  public function remove(cx: Int, cy: Int): Void {
    var v4 = false;
    if (this.menuC.fl_lock) {
      return;
    }
    this.world.current.__dollar__map[cx][cy] = 0;
    for (v5 in 0...this.world.current.__dollar__badList.length) {
      if (v4) break;
      var v6 = this.world.current.__dollar__badList[v5];
      if (v6.__dollar__x == cx && v6.__dollar__y == cy) {
        this.world.current.__dollar__badList.splice(v5, 1);
        this.redrawBads();
        v4 = true;
      }
    }
    for (v7 in 0...this.world.current.__dollar__scoreSlots.length) {
      if (v4) break;
      var v8 = this.world.current.__dollar__scoreSlots[v7];
      if (v8.__dollar__x == cx && v8.__dollar__y == cy) {
        this.world.current.__dollar__scoreSlots.splice(v7, 1);
        this.redrawSlots();
        v4 = true;
      }
    }
    for (v9 in 0...this.world.current.__dollar__specialSlots.length) {
      if (v4) break;
      var v10 = this.world.current.__dollar__specialSlots[v9];
      if (v10.__dollar__x == cx && v10.__dollar__y == cy) {
        this.world.current.__dollar__specialSlots.splice(v9, 1);
        this.redrawSlots();
        v4 = true;
      }
    }
    this.setModified(this.world.currentId);
  }

  public function isEmpty(cx: Int, cy: Int): Bool {
    var v4 = this.world.current.__dollar__map[cx][cy] == 0;
    for (v5 in 0...this.world.current.__dollar__badList.length) {
      var v6 = this.world.current.__dollar__badList[v5];
      if (v6.__dollar__x == cx && v6.__dollar__y == cy) {
        v4 = false;
      }
    }
    for (v7 in 0...this.world.current.__dollar__scoreSlots.length) {
      var v8 = this.world.current.__dollar__scoreSlots[v7];
      if (v8.__dollar__x == cx && v8.__dollar__y == cy) {
        v4 = false;
      }
    }
    for (v9 in 0...this.world.current.__dollar__specialSlots.length) {
      var v10 = this.world.current.__dollar__specialSlots[v9];
      if (v10.__dollar__x == cx && v10.__dollar__y == cy) {
        v4 = false;
      }
    }
    return v4;
  }

  public function anyModified(): Bool {
    var v2 = false;
    for (v3 in 0...this.fl_modified.length) {
      v2 = this.fl_modified[v3] || v2;
    }
    return v2;
  }

  public function setModified(lid: Int): Void {
    this.fl_modified[lid] = true;
    if (this.view.levelId == lid) {
      this.view.updateSnapShot();
    }
  }

  public function updateCursor(): Void {
    this.cx = Math.floor((this.root._xmouse - this.xOffset) / Data.CASE_WIDTH);
    this.cy = Math.floor(this.root._ymouse / Data.CASE_HEIGHT);
    if (this.cx < 0) {
      this.cx = 0;
    }
    if (this.cx >= Data.LEVEL_WIDTH) {
      this.cx = Data.LEVEL_WIDTH - 1;
    }
    if (this.cy < 0) {
      this.cy = 0;
    }
    if (this.cy >= Data.LEVEL_HEIGHT) {
      this.cy = Data.LEVEL_HEIGHT - 1;
    }
    this.cursor._x = this.cx * Data.CASE_WIDTH;
    this.cursor._y = this.cy * Data.CASE_HEIGHT;
    this.cursor.gotoAndStop("" + (this.tool - Data.TOOL_TILE + 1));
    if (!this.fl_menu && !this.fl_lock) {
      if (Key.isDown(Key.SHIFT)) {
        this.header.field.text = "Real coords : " + this.root._xmouse + "," + this.root._ymouse;
      } else {
        this.showCaseCursor();
        this.header.field.text = "Case coords : " + this.cx + "," + this.cy;
      }
    }
  }

  public function showCaseCursor(): Void {
    Mouse.hide();
    this.cursor._visible = true;
  }

  public function hideCaseCursor(): Void {
    Mouse.show();
    this.cursor._visible = false;
  }

  public function followPortal(pid: Int): Void {
    var v3 = Data.getLink(this.dimensionId, this.world.currentId, pid);
    if (v3 == null) {
      return;
    }
    if (v3.to_did == this.dimensionId) {
      this.goto(v3.to_lid);
    } else {
      if (this.anyModified()) {
        this.display("All changes will be lost ! Reload all or save all to follow this portal.");
      } else {
        var v4 = null;
        for (v5 in 0...hf.mode.Editor.DIMENSIONS.length) {
          if (hf.mode.Editor.DIMENSIONS[v5].id == v3.to_did) {
            v4 = hf.mode.Editor.DIMENSIONS[v5].name;
          }
        }
        if (v4 != null) {
          this.firstLevel = v3.to_lid;
          this.loadSet(v4);
          this.redrawAll();
        }
      }
    }
  }

  public function onLoadComplete(): Void {
    this.goto(this.firstLevel);
    this.firstLevel = 1;
    this.redrawAll();
    this.updateBad();
  }

  public function mouseDown(): Void {
    this.fl_click = true;
  }

  public function mouseUp(): Void {
    this.fl_click = false;
  }

  public function none(): Void {
    this.display("not implemented...");
  }

  public function onNew(): Void {
    this.world.levels[this.world.currentId] = new hf.levels.Data();
    this.setModified(this.world.currentId);
    this.world.goto(this.world.currentId);
    this.redrawAll();
  }

  public function onSaveLevel(): Void {
    this.world.raw[this.world.currentId] = this.world.serialize(this.world.currentId);
    this.fl_modified[this.world.currentId] = false;
    this.display("Current level saved. Length=" + this.world.raw[this.world.currentId].length);
  }

  public function onLoadLevel(): Void {
    this.lastPoint = null;
    this.world.levels[this.world.currentId] = this.world.unserialize(this.world.currentId);
    this.world.goto(this.world.currentId);
    this.fl_modified[this.world.currentId] = false;
    this.display("Current level reloaded.");
    this.redrawAll();
  }

  public function onLoadAll(): Void {
    this.fl_modified = new Array();
    this.view.destroy();
    this.loadSet(this.setName);
    this.redrawAll();
    this.display("All reloaded.");
  }

  public function onSaveAll(): Void {
    this.save_currentId = 0;
    this.save_raw = new Array();
    if (this.fl_menu) {
      this.toggleMenu();
      this.hideCaseCursor();
    }
    this.fl_save = true;
  }

  public function onCookieReset(): Void {
    this.resetConfirm += Data.SECOND * 0.5;
    if (this.resetConfirm <= Data.SECOND * 0.7) {
      this.display("WARNING: this operation CANNOT be canceled ! Double-click to confirm.");
    } else {
      this.resetConfirm = 0;
      this.display("Rolled-back to compiled XML data");
      this.world.rollback_xml();
      this.manager.fl_cookie = false;
      this.onLoadAll();
      this.manager.fl_cookie = true;
      this.onSaveAll();
    }
  }

  public function onNextLevel(): Void {
    this.goto(this.world.currentId + 1);
  }

  public function onPrevLevel(): Void {
    this.goto(this.world.currentId - 1);
  }

  public function onNextLevelFast(): Void {
    this.goto(this.world.currentId + 10);
  }

  public function onPrevLevelFast(): Void {
    this.goto(this.world.currentId - 10);
  }

  public function onFirstLevel(): Void {
    this.goto(0);
  }

  public function onLastLevel(): Void {
    this.goto(this.world.levels.length - 1);
  }

  public function onPrevBg(): Void {
    this.world.current.__dollar__skinBg = Std.int(Math.max(1, this.world.current.__dollar__skinBg - 1));
    this.setModified(this.world.currentId);
    this.redrawView();
  }

  public function onNextBg(): Void {
    this.world.current.__dollar__skinBg = Std.int(Math.min(Data.MAX_BG, this.world.current.__dollar__skinBg + 1));
    this.setModified(this.world.currentId);
    this.redrawView();
  }

  public function onPrevTiles(): Void {
    var v2 = hf.levels.View.getTileSkinId(this.world.current.__dollar__skinTiles);
    var v3 = hf.levels.View.getColumnSkinId(this.world.current.__dollar__skinTiles);
    if (v2 == v3) {
      --v2;
      v3 = v2;
    } else {
      --v2;
    }
    v2 = Std.int(Math.max(1, v2));
    this.world.current.__dollar__skinTiles = hf.levels.View.buildSkinId(v2, v3);
    this.setModified(this.world.currentId);
    this.redrawView();
  }

  public function onNextTiles(): Void {
    var v2 = hf.levels.View.getTileSkinId(this.world.current.__dollar__skinTiles);
    var v3 = hf.levels.View.getColumnSkinId(this.world.current.__dollar__skinTiles);
    if (v2 == v3) {
      ++v2;
      v3 = v2;
    } else {
      ++v2;
    }
    v2 = Std.int(Math.max(1, v2));
    this.world.current.__dollar__skinTiles = hf.levels.View.buildSkinId(v2, v3);
    this.setModified(this.world.currentId);
    this.redrawView();
  }

  public function onPrevColumn(): Void {
    var v2 = hf.levels.View.getTileSkinId(this.world.current.__dollar__skinTiles);
    var v3 = hf.levels.View.getColumnSkinId(this.world.current.__dollar__skinTiles);
    v3 = Std.int(Math.max(1, v3 - 1));
    this.world.current.__dollar__skinTiles = hf.levels.View.buildSkinId(v2, v3);
    this.setModified(this.world.currentId);
    this.redrawView();
  }

  public function onNextColumn(): Void {
    var v2 = hf.levels.View.getTileSkinId(this.world.current.__dollar__skinTiles);
    var v3 = hf.levels.View.getColumnSkinId(this.world.current.__dollar__skinTiles);
    ++v3;
    this.world.current.__dollar__skinTiles = hf.levels.View.buildSkinId(v2, v3);
    this.setModified(this.world.currentId);
    this.redrawView();
  }

  public function onResetColumn(): Void {
    var v2 = hf.levels.View.getTileSkinId(this.world.current.__dollar__skinTiles);
    this.world.current.__dollar__skinTiles = hf.levels.View.buildSkinId(v2, v2);
    this.setModified(this.world.currentId);
    this.redrawView();
  }

  public function onQuit(): Void {
    if (this.fl_switch) {
      return;
    }
    if (this.fl_menu) {
      this.toggleMenu();
    }
    this.cursor._visible = false;
    this.hideCaseCursor();
    this.endMode();
  }

  public function onSelectTile(): Void {
    this.tool = Data.TOOL_TILE;
  }

  public function onSelectBad(): Void {
    this.tool = Data.TOOL_BAD;
  }

  public function onSelectField(): Void {
    this.tool = Data.TOOL_FIELD;
  }

  public function onSelectStart(): Void {
    this.tool = Data.TOOL_START;
  }

  public function onSelectSpecial(): Void {
    this.tool = Data.TOOL_SPECIAL;
  }

  public function onSelectScore(): Void {
    this.tool = Data.TOOL_SCORE;
  }

  public function onPrevField(): Void {
    this.fieldId = Std.int(Math.max(1, this.fieldId - 1));
    this.onSelectField();
  }

  public function onNextField(): Void {
    this.fieldId = Std.int(Math.min(Data.MAX_FIELDS, this.fieldId + 1));
    this.onSelectField();
  }

  public function onPrevBad(): Void {
    --this.badId;
    if (this.badId < 0) {
      this.badId = Data.MAX_BADS;
    }
    this.onSelectBad();
    this.updateBad();
  }

  public function onNextBad(): Void {
    ++this.badId;
    if (this.badId > Data.MAX_BADS) {
      this.badId = 0;
    }
    this.onSelectBad();
    this.updateBad();
  }

  public function onCopy(): Void {
    this.buffer = Data.duplicate(this.world.current);
    this.display("Level copied to buffer.");
  }

  public function onPaste(): Void {
    if (this.buffer == null) {
      this.display("Level buffer is empty");
      return;
    }
    this.world.levels[this.world.currentId] = Data.duplicate(this.buffer);
    this.world.goto(this.world.currentId);
    this.setModified(this.world.currentId);
    this.redrawAll();
    this.display("Paste level from buffer.");
  }

  public function onStyleCopy(): Void {
    this.styleBuffer = Data.duplicate(this.world.current);
    this.display("Style copied to buffer");
  }

  public function onStylePaste(): Void {
    if (this.styleBuffer == null) {
      this.display("Style buffer is empty");
      return;
    }
    this.world.current.__dollar__skinTiles = this.styleBuffer.__dollar__skinTiles;
    this.world.current.__dollar__skinBg = this.styleBuffer.__dollar__skinBg;
    this.setModified(this.world.currentId);
    this.redrawAll();
  }

  public function onLoadAdv(): Void {
    this.loadSet("xml_adventure");
    this.redrawAll();
  }

  public function onLoadMulti(): Void {
    this.loadSet("xml_multi");
    this.redrawAll();
  }

  public function onLoadTuto(): Void {
    this.loadSet("xml_tutorial");
    this.redrawAll();
  }

  public function onLoadShare(): Void {
    this.loadSet("xml_shareware");
    this.redrawAll();
  }

  public function onLoadFjv(): Void {
    this.loadSet("xml_fjv");
    this.redrawAll();
  }

  public function onLoadDev(): Void {
    this.loadSet("xml_dev");
    this.redrawAll();
  }

  public function onLoadTest(): Void {
    this.loadSet("xml_test");
    this.redrawAll();
  }

  public function onLoadTime(): Void {
    this.loadSet("xml_time");
    this.redrawAll();
  }

  public function onLoadMultiTime(): Void {
    this.loadSet("xml_multitime");
    this.redrawAll();
  }

  public function onLoadSoccer(): Void {
    this.loadSet("xml_soccer");
    this.redrawAll();
  }

  public function onLoadHof(): Void {
    this.loadSet("xml_hof");
    this.redrawAll();
  }

  public function onLoadDeep(): Void {
    this.loadSet("xml_deepnight");
    this.redrawAll();
  }

  public function onLoadHiko(): Void {
    this.loadSet("xml_hiko");
    this.redrawAll();
  }

  public function onLoadAyame(): Void {
    this.loadSet("xml_ayame");
    this.redrawAll();
  }

  public function onLoadHk(): Void {
    this.loadSet("xml_hk");
    this.redrawAll();
  }

  public function onBrowse(): Void {
    this.manager.startChild(new hf.mode.Browser(this.manager, this.world, this.dimensionId, this.fl_modified));
  }

  public function onTest(): Void {
    this.manager.startChild(new hf.mode.AdventureTest(this.manager, this.world.current));
  }

  public function onScript(): Void {
    this.manager.startChild(new hf.mode.ScriptEditor(this.manager, this.world.current.__dollar__script));
  }

  public function onItemBrowser(): Void {
    this.manager.startChild(new hf.mode.ItemBrowser(this.manager));
  }

  public function onQuestBrowser(): Void {
    this.manager.startChild(new hf.mode.QuestBrowser(this.manager));
  }

  public function onStartGame(): Void {
    if (this.anyModified()) {
      this.display("All changes will be lost ! Reload all or save all to start the game.");
    } else {
      this.manager.startMode(new hf.mode.Adventure(this.manager, this.world.currentId));
    }
  }

  public function onPanLeft(): Void {
    this.panMap(-1, 0);
  }

  public function onPanRight(): Void {
    this.panMap(1, 0);
  }

  public function onPanUp(): Void {
    this.panMap(0, -1);
  }

  public function onPanDown(): Void {
    this.panMap(0, 1);
  }

  public function panMap(offsetX: Int, offsetY: Int): Void {
    var v7;
    var v6;
    var v5;
    var v10;
    var v9;
    var v8;
    var v4 = this.world.current;
    if (offsetX == 0) {
      v5 = 0;
      v6 = Data.LEVEL_WIDTH;
      v7 = 1;
    } else {
      v7 = -offsetX;
      if (offsetX > 0) {
        v5 = Data.LEVEL_WIDTH - 2;
        v6 = -1;
      } else {
        v5 = 1;
        v6 = Data.LEVEL_WIDTH;
      }
    }
    if (offsetY == 0) {
      v8 = 0;
      v9 = Data.LEVEL_HEIGHT;
      v10 = 1;
    } else {
      v10 = -offsetY;
      if (offsetY > 0) {
        v8 = Data.LEVEL_HEIGHT - 2;
        v9 = -1;
      } else {
        v8 = 1;
        v9 = Data.LEVEL_HEIGHT;
      }
    }
    var v11 = v8;
    while (v11 != v9) {
      var v12 = v5;
      while (v12 != v6) {
        v4.__dollar__map[v12 + offsetX][v11 + offsetY] = v4.__dollar__map[v12][v11];
        v12 += v7;
      }
      v11 += v10;
    }
    if (offsetX != 0) {
      for (v13 in 0...Data.LEVEL_HEIGHT) {
        this.world.current.__dollar__map[v6 + offsetX][v13] = 0;
      }
    }
    if (offsetY != 0) {
      for (v14 in 0...Data.LEVEL_WIDTH) {
        this.world.current.__dollar__map[v14][v9 + offsetY] = 0;
      }
    }
    this.redrawAll();
    this.setModified(this.world.currentId);
  }

  public function clearColumn(x: Int): Void {
    for (v3 in 0...Data.LEVEL_HEIGHT) {
      this.world.current.__dollar__map[x][v3] = 0;
    }
  }

  override public function onSleep(): Void {
    this.hideCaseCursor();
    super.onSleep();
  }

  override public function onWakeUp(n: Dynamic /*TODO*/, data: Dynamic /*TODO*/): Void {
    super.onWakeUp(n, data);
    if (data != null) {
      if (n.indexOf("$scriptEd", 0) >= 0) {
        var v5 = this.world.current.__dollar__script;
        this.world.current.__dollar__script = data;
        if (v5 != this.world.current.__dollar__script) {
          this.setModified(this.world.currentId);
        }
        this.redrawScript();
      }
      if (n.indexOf("$levelBrowser", 0) >= 0) {
        var v6 = data;
        this.goto(v6);
      }
    }
    if (this.fl_menu) {
      this.hideCaseCursor();
    } else {
      this.showCaseCursor();
    }
  }

  override public function getControls(): Void {
    if (!Key.isDown(Key.ESCAPE)) {
      this.fl_lockToggle = false;
    }
    if (Key.isDown(Key.ESCAPE) && !this.fl_lockToggle) {
      this.toggleMenu();
      this.fl_lockToggle = true;
    }
    if (Key.isDown(Key.SHIFT) && Key.isDown(Key.CONTROL) && Key.isDown(67)) {
      this.manager.startMode(new hf.mode.Converter(this.manager));
    }
    if (!this.fl_lock) {
      if (!Key.isDown(70)) {
        this.fl_lockFollow = false;
      }
      if (Key.isDown(70) && !this.fl_lockFollow) {
        var v2 = null;
        var v3 = 5.0;
        for (v4 in 0...this.world.portalList.length) {
          var v5 = this.world.portalList[v4];
          var v6 = Math.sqrt(Math.pow(v5.cx - this.cx, 2) + Math.pow(v5.cy - this.cy, 2));
          if (v6 < v3) {
            v2 = v4;
            v3 = v6;
          }
        }
        if (v2 != null) {
          this.followPortal(v2);
        }
        this.fl_lockFollow = true;
      }
    }
    if (this.lastKey > 0 && !Key.isDown(this.lastKey)) {
      this.lastKey = 0;
    }
    if (!this.fl_lock) {
      for (v7 in 0...10) {
        if (Key.isDown(96 + v7) && this.lastKey != 96 + v7) {
          this.input += '' + (v7);
          this.lastKey = 96 + v7;
        }
      }
      if (this.input.length >= 3) {
        var v8 = HfStd.parseInt(this.input, 10);
        if (v8 < this.world.levels.length) {
          this.goto(v8);
        }
        this.input = "";
      }
    }
    if (Key.isDown(Key.BACKSPACE)) {
      this.input = "";
    }
  }

  override public function endMode(): Void {
    this.manager.startGameMode(new hf.mode.Adventure(this.manager, 1));
  }

  override public function destroy(): Void {
    this.hideCaseCursor();
    super.destroy();
  }

  override public function main(): Void {
    super.main();
    if (this.fl_lock) {
      return;
    }
    if (this.input.length > 0) {
      var v3 = this.input;
      while (v3.length < 3) {
        v3 += "$_".substring(1);
      }
      Log.print("GOTO: [ " + v3 + " ]");
    }
    if (this.resetConfirm >= 0) {
      this.resetConfirm -= Timer.tmod;
    }
    if (this.fl_save) {
      this.manager.progress(this.save_currentId / this.world.levels.length);
      if (this.world.fl_read[this.save_currentId]) {
        this.save_raw[this.save_currentId] = this.world.serialize(this.save_currentId);
      } else {
        this.save_raw[this.save_currentId] = this.world.raw[this.save_currentId];
      }
      ++this.save_currentId;
      if (this.save_currentId == this.world.levels.length) {
        var v4 = this.save_raw.join(":");
        this.world.overwrite(v4);
        this.world.exportCookie();
        this.fl_modified = new Array();
        this.display(this.save_raw.length + " levels saved");
        System.setClipboard(v4);
        this.display("Copied to system clipboard (length = " + v4.length + ")");
        this.fl_save = false;
        this.manager.progress(null);
      } else {
        return;
      }
    }
    if (this.fl_save) {
      return;
    }
    this.updateConstants();
    this.getDebugControls();
    this.updateFooter();
    this.world.update();
    this.getControls();
    this.menuC.update();
    this.updateCursor();
    this.updateMenu();
    if (this.fl_click && !this.fl_menu) {
      if (!Key.isDown(Key.CONTROL)) {
        if (this.isEmpty(this.cx, this.cy)) {
          this.paint(this.cx, this.cy);
          this.redrawView();
        }
      } else {
        if (!this.isEmpty(this.cx, this.cy)) {
          this.remove(this.cx, this.cy);
          this.redrawView();
        }
      }
    }
  }
}
