package hf.gui;

import etwin.flash.MovieClip;
import etwin.flash.DynamicMovieClip;
import hf.mode.GameMode;

class SoccerInterface {
  public static var GLOW_COLOR: Int = hf.gui.GameInterface.GLOW_COLOR;

  public var mc: DynamicMovieClip /* TODO */;
  public var game: GameMode;
  public var scores: Array<DynamicMovieClip /* TODO */>;
  public var time: DynamicMovieClip /* TODO */;

  public function new(game: GameMode): Void {
    this.game = game;
    this.init();
  }

  public function init(): Void {
    this.mc = ((cast this.game.depthMan.attach("hammer_interf_game", Data.DP_TOP)): DynamicMovieClip);
    this.mc._x = -this.game.xOffset;
    this.mc._y = Data.DOC_HEIGHT;
    this.mc.gotoAndStop("3");
    this.mc.cacheAsBitmap = true;
    this.scores = [this.mc.score0, this.mc.score1];
    this.time = this.mc.time;
    FxManager.addGlow(this.scores[0], hf.gui.SoccerInterface.GLOW_COLOR, 2);
    FxManager.addGlow(this.scores[1], hf.gui.SoccerInterface.GLOW_COLOR, 2);
    FxManager.addGlow(this.time, hf.gui.SoccerInterface.GLOW_COLOR, 2);
    this.setScore(0, 0);
    this.setScore(1, 0);
  }

  public function setScore(pid: Int, n: Int): Void {
    this.scores[pid].text = "" + n;
  }

  public function setTime(str: String): Void {
    this.time.text = str;
  }

  public function destroy(): Void {
    this.mc.removeMovieClip();
  }

  public function update(): Void {
  }
}
