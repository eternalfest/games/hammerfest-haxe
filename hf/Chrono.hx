package hf;

import etwin.flash.Date;

class Chrono {
  public var frameTimer: Int;
  public var gameTimer: Int;
  public var prevFrameTimer: Null<Int>;
  public var suspendTimer: Null<Int>;
  public var haltedTimer: Int;
  public var fl_stop: Bool;
  public var fl_init: Bool;

  public function new(): Void {
    this.suspendTimer = null;
    this.frameTimer = HfStd.getTimer();
    this.gameTimer = this.frameTimer;
    this.haltedTimer = this.get();
    this.reset();
    this.fl_stop = true;
    this.fl_init = false;
  }

  public function formatTime(t: Int): String {
    var v3 = new Date();
    v3.setTime(t);
    return Data.leadingZeros(v3.getMinutes() + (v3.getHours() - 1) * 60, 2) + "\" " + Data.leadingZeros(v3.getSeconds(), 2) + "' " + Data.leadingZeros(v3.getMilliseconds(), 3);
  }

  public function formatTimeShort(t: Int): String {
    var v3 = new Date();
    v3.setTime(t);
    var v4 = 0;
    if (v3.getMilliseconds() >= 500) {
      v4 = 1;
    }
    return Data.leadingZeros(v3.getMinutes() + (v3.getHours() - 1) * 60, 2) + "\" " + Data.leadingZeros(v3.getSeconds() + v4, 2);
  }

  public function get(): Int {
    if (this.fl_stop) {
      return this.haltedTimer;
    }
    return Math.floor(this.frameTimer - this.gameTimer);
  }

  public function getStr(): String {
    return this.formatTime(this.get());
  }

  public function getStrShort(): String {
    return this.formatTimeShort(this.get());
  }

  public function reset(): Void {
    this.fl_init = true;
    this.fl_stop = false;
    this.suspendTimer = null;
    this.gameTimer = this.frameTimer;
  }

  public function start(): Void {
    if (this.suspendTimer != null) {
      var v2 = this.frameTimer - this.suspendTimer;
      this.gameTimer += v2;
    }
    this.haltedTimer = null;
    this.fl_stop = false;
    this.suspendTimer = null;
  }

  public function stop(): Void {
    if (this.fl_stop) {
      return;
    }
    this.haltedTimer = this.get();
    this.fl_stop = true;
    this.suspendTimer = this.frameTimer;
  }

  public function timeShift(n: Int): Void {
    this.gameTimer = Std.int(Math.min(this.frameTimer, this.gameTimer + n * 1000));
  }

  public function update(): Void {
    this.prevFrameTimer = this.frameTimer;
    this.frameTimer = HfStd.getTimer();
  }
}
