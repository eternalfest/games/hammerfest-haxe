package hf.entity.bomb.player;

import hf.entity.bomb.PlayerBomb;
import hf.mode.GameMode;

class Blue extends PlayerBomb {
  public function new(): Void {
    super();
    this.duration = 45;
    this.power = 20;
    this.explodeSound = "sound_bomb_blue";
  }

  public static function attach(g: GameMode, x: Float, y: Float): Blue {
    var v5 = "hammer_bomb_blue";
    var v6: Blue = cast g.depthMan.attach(v5, Data.DP_BOMBS);
    v6.initBomb(g, x, y);
    return v6;
  }

  override public function duplicate(): Blue {
    return hf.entity.bomb.player.Blue.attach(this.game, this.x, this.y);
  }

  override public function onExplode(): Void {
    super.onExplode();
    var v3 = this.bombGetClose(Data.BAD);
    for (v4 in 0...v3.length) {
      var v5 = v3[v4];
      v5.setCombo(this.uniqId);
      v5.knock(Data.KNOCK_DURATION * 0.75);
      this.shockWave(v5, this.radius, this.power);
      v5.dx *= 0.3;
      v5.dy = -6;
      v5.yTrigger = v5.y + Data.CASE_HEIGHT * 1.5;
      v5.fl_hitGround = false;
    }
  }
}
