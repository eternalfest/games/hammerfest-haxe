package hf.entity.supa;

import hf.entity.Player;
import hf.entity.Supa;
import hf.mode.GameMode;

class SupaItem extends Supa {
  public var supaId: Int;

  public function new(): Void {
    super();
    this.radius = 50;
  }

  public static function attach(g: GameMode, id: Int): SupaItem {
    var v4 = "hammer_supa_item";
    var v5: SupaItem = cast g.depthMan.attach(v4, Data.DP_SUPA);
    v5.supaId = id;
    v5.initSupa(g, Data.GAME_WIDTH / 2, -50);
    return v5;
  }

  public function pick(pl: Player): Void {
    this.game.fxMan.inGameParticles(Data.PARTICLE_ICE, this.x, this.y, 15);
    this.game.fxMan.attachExplodeZone(this.x, this.y, 50);
    var v3 = Data.getCrystalValue(this.supaId) * 5;
    this.game.manager.logAction("$SU" + this.supaId);
    pl.getScore(this, v3);
    this.game.soundMan.playSound("SoccerTeams", Data.CHAN_ITEM);
    var v4 = this.game.getPlayerList();
    for (v5 in 0...v4.length) {
      if (v4[v5].uniqId != pl.uniqId) {
        v4[v5].setBaseAnims(Data.ANIM_PLAYER_WALK, Data.ANIM_PLAYER_STOP_L);
      }
    }
    this.destroy();
  }

  override public function initSupa(g: GameMode, x: Float, y: Float): Void {
    super.initSupa(g, x, y);
    this.scale(200);
    this.moveDown(5);
    this.gotoAndStop('' + (this.supaId + 1));
  }

  override public function onDeathLine(): Void {
    var v2 = this.game.getPlayerList();
    for (v3 in 0...v2.length) {
      v2[v3].setBaseAnims(Data.ANIM_PLAYER_WALK, Data.ANIM_PLAYER_STOP_L);
    }
    this.destroy();
  }

  override public function prefix(): Void {
    super.prefix();
    var v3 = this.game.getClose(Data.PLAYER, this.x, this.y + Data.CASE_HEIGHT * 1.5, this.radius, false);
    var v4 = false;
    for (v5 in 0...v3.length) {
      if (v4) break;
      var v6 = v3[v5];
      if (!v6.fl_kill) {
        this.pick(v6);
        v4 = true;
      }
    }
    if (this.y >= Data.GAME_HEIGHT + 50) {
      this.onDeathLine();
    }
  }
}
