package hf.entity.bad.ww;

import hf.entity.bad.WallWalker;
import hf.mode.GameMode;

class Crawler extends WallWalker {
  public static var SCALE_RECAL: Float = 0.2;
  public static var CRAWL_STRETCH: Float = 1.8;
  public static var COLOR: Int = 16748870;
  public static var COLOR_ALPHA: Float = 40;
  public static var SHOOT_SPEED: Float = 6;
  public static var CHANCE_ATTACK: Int = 10;
  public static var COOLDOWN: Float = Data.SECOND * 2;
  public static var ATTACK_TIMER: Float = Data.SECOND * 0.5;

  public var fl_attack: Bool;
  public var _tolerates: Float;
  public var attackCD: Float;
  public var attackTimer: Float;
  public var colorAlpha: Null<Float>;
  public var xscale: Float;
  public var yscale: Float;

  public function new(): Void {
    super();
    this.speed = 2;
    this.angerFactor = 0.5;
    this.fl_attack = false;
    this.attackCD = Data.PEACE_COOLDOWN;
    this._tolerates = 0;
  }

  public static function attach(g: GameMode, x: Float, y: Float): Crawler {
    var v5 = Data.LINKAGES[Data.BAD_CRAWLER];
    var v6: Crawler = cast g.depthMan.attach(v5, Data.DP_BADS);
    v6.initBad(g, x, y);
    return v6;
  }

  public function prepareAttack(): Void {
    this.dx = 0;
    this.dy = 0;
    this.fl_attack = true;
    this.fl_wallWalk = false;
    this.attackTimer = hf.entity.bad.ww.Crawler.ATTACK_TIMER;
    this.playAnim(Data.ANIM_BAD_SHOOT_START);
  }

  public function attack(): Void {
    var v2 = hf.entity.shoot.FireBall.attach(this.game, this.x, this.y);
    v2.moveTo(this.x, this.y);
    v2.dx = -this.cp.x * hf.entity.bad.ww.Crawler.SHOOT_SPEED;
    v2.dy = -this.cp.y * hf.entity.bad.ww.Crawler.SHOOT_SPEED;
    v2.scale(70);
    var v3 = HfStd.random(3) + 2;
    if (this.cp.x != 0) {
      this.game.fxMan.inGameParticlesDir(Data.PARTICLE_BLOB, this.x, this.y, v3, -this.cp.x);
    } else {
      this.game.fxMan.inGameParticles(Data.PARTICLE_BLOB, this.x, this.y, v3);
    }
    this.game.fxMan.attachExplosion(this.x, this.y, 20);
    this.sub._xscale = 150 + Math.abs(this.cp.x) * 150;
    this.sub._yscale = 150 + Math.abs(this.cp.y) * 150;
    this.colorAlpha = hf.entity.bad.ww.Crawler.COLOR_ALPHA;
    this.setColorHex(Math.round(this.colorAlpha), hf.entity.bad.ww.Crawler.COLOR);
    this.attackCD = hf.entity.bad.ww.Crawler.COOLDOWN;
    this.playAnim(Data.ANIM_BAD_SHOOT_END);
  }

  public function decideAttack(): Bool {
    if (this.fl_attack) {
      return false;
    }
    var v2 = false;
    var v3 = 1.0;
    if (this.cp.y != 0 && Math.abs(this.player.x - this.x) <= Data.CASE_WIDTH * 2) {
      if (this.cp.y > 0 && this.player.y < this.y) {
        v2 = true;
      }
      if (this.cp.y < 0 && this.player.y > this.y) {
        v2 = true;
      }
    }
    if (this.cp.x != 0 && Math.abs(this.player.y - this.y) <= Data.CASE_HEIGHT * 2) {
      if (this.cp.x > 0 && this.player.x < this.x) {
        v2 = true;
      }
      if (this.cp.x < 0 && this.player.x > this.x) {
        v2 = true;
      }
    }
    if (v2) {
      this.attackCD -= Timer.tmod * 4;
      v3 = 8;
    }
    return this.isReady() && this.isHealthy() && this.attackCD <= 0 && HfStd.random(1000) < hf.entity.bad.ww.Crawler.CHANCE_ATTACK * v3;
  }

  override public function endUpdate(): Void {
    super.endUpdate();
    if (this.fl_attack) {
      this._x += (HfStd.random(15) / 10) * (HfStd.random(2) * 2 - 1);
      this._y += (HfStd.random(15) / 10) * (HfStd.random(2) * 2 - 1);
      this.xscale = this.scaleFactor * 100 + HfStd.random(20) * (HfStd.random(2) * 2 - 1);
      this.yscale = this.scaleFactor * 100 + HfStd.random(20) * (HfStd.random(2) * 2 - 1);
    } else {
      this.xscale = this.scaleFactor * 100;
      this.yscale = this.scaleFactor * 100;
    }
    if (this.fl_wallWalk) {
      if (this.dx != 0) {
        this.xscale = 100 * this.scaleFactor * hf.entity.bad.ww.Crawler.CRAWL_STRETCH;
      }
      if (this.dy != 0) {
        this.yscale = 100 * this.scaleFactor * hf.entity.bad.ww.Crawler.CRAWL_STRETCH;
      }
    }
    if (this.isHealthy()) {
      this.xscale += 10 * Math.sin(this._tolerates);
      this.yscale += 10 * Math.cos(this._tolerates);
      this._tolerates += Timer.tmod * 0.1;
    }
    this.sub._xscale += hf.entity.bad.ww.Crawler.SCALE_RECAL * (this.xscale - this.sub._xscale);
    this.sub._yscale += hf.entity.bad.ww.Crawler.SCALE_RECAL * (this.yscale - this.sub._yscale);
    if (this.colorAlpha > 0) {
      this.colorAlpha -= Timer.tmod * 3;
      if (this.colorAlpha <= 0) {
        this.resetColor();
      } else {
        this.setColorHex(Math.round(this.colorAlpha), hf.entity.bad.ww.Crawler.COLOR);
      }
    }
  }

  override public function initBad(g: GameMode, x: Float, y: Float): Void {
    super.initBad(g, x, y);
    this.scale(90);
  }

  override public function isReady(): Bool {
    return super.isReady() && !this.fl_attack;
  }

  override public function killHit(dx: Null<Float>): Void {
    super.killHit(dx);
    this.fl_attack = false;
  }

  override public function onEndAnim(id: Int): Void {
    super.onEndAnim(id);
    if (id == Data.ANIM_BAD_SHOOT_END.id) {
      this.fl_attack = false;
      this.fl_wallWalk = true;
      this.moveToSafePos();
      this.updateSpeed();
      if (this.dx == 0 && this.dy == 0) {
        this.wallWalk();
      }
    }
  }

  override public function onFreeze(): Void {
    super.onFreeze();
    this.fl_attack = false;
  }

  override public function onHitGround(h: Float): Void {
    super.onHitGround(h);
    if (Math.abs(h) >= Data.CASE_HEIGHT * 3) {
      this.sub._xscale = 2 * 100 * this.scaleFactor;
      this.sub._yscale = 0.2 * 100 * this.scaleFactor;
      this.sub._y = this.ySubBase + 10;
      if (!this.fl_freeze) {
        this.game.fxMan.inGameParticles(Data.PARTICLE_BLOB, this.x, this.y, HfStd.random(3) + 2);
      }
    }
  }

  override public function onKnock(): Void {
    super.onKnock();
    this.fl_attack = false;
  }

  override public function update(): Void {
    if (this.fl_attack) {
      this.dx = 0;
      this.dy = 0;
    }
    super.update();
    if (this.attackCD > 0) {
      this.attackCD -= Timer.tmod;
    }
    if (this.decideAttack()) {
      if (this.world.getCase({x: this.cx + this.cp.x, y: this.cy + this.cp.y}) > 0) {
        this.prepareAttack();
      }
    }
    if (this.fl_attack && this.attackTimer > 0) {
      this.attackTimer -= Timer.tmod;
      if (this.attackTimer <= 0) {
        this.attack();
      }
    }
  }
}
