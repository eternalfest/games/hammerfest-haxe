package hf.levels;

import etwin.flash.MovieClip;
import hf.levels.SetManager;
import hf.levels.View;
import hf.native.BitmapData;
import hf.Data in Data;

class ViewManager extends SetManager {
  public var view: Null<View>;
  public var fake: MovieClip;
  public var depthMan: Null<DepthManager>;
  public var scrollCpt: Float;
  public var scrollDir: Int;
  public var fl_scrolling: Bool;
  public var fl_hscrolling: Bool;
  public var fl_fading: Bool;
  public var fl_hideTiles: Bool;
  public var fl_hideBorders: Bool;
  public var fl_shadow: Bool;
  public var fl_restoring: Bool;
  public var fl_fadeNextTransition: Bool;
  public var darknessFactor: Float;
  public var prevSnap: Null<BitmapData>;

  public function new(m: GameManager, setName: String): Void {
    super(m, setName);
    this.fl_scrolling = false;
    this.fl_hscrolling = false;
    this.fl_hideTiles = false;
    this.fl_hideBorders = false;
    this.fl_shadow = true;
    this.fl_restoring = false;
    this.fl_fading = false;
    this.darknessFactor = 0;
  }

  public function setDepthMan(d: DepthManager): Void {
    this.depthMan = d;
  }

  public function cloneView(v: View): Void {
    var v3 = v.getSnapShot(0, 0);
    this.createFake(v3);
  }

  public function createFake(snap: BitmapData): Void {
    this.fake.removeMovieClip();
    this.fake = this.depthMan.empty(Data.DP_SCROLLER);
    this.fake.blendMode = BlendMode.LAYER;
    var v3 = HfStd.createEmptyMC(this.fake, 0);
    v3._x = -10;
    v3.attachBitmap(snap, 0);
    this.fake._alpha = Math.max(0, 100 - this.darknessFactor);
  }

  public function onTransitionDone(): Void {
    this.view.moveTo(0, 0);
    this.fake.removeMovieClip();
    if (this.fl_mirror) {
      this.flipPortals();
    }
  }

  public function onScrollDone(): Void {
    this.fl_scrolling = false;
    this.onViewReady();
  }

  public function onHScrollDone(): Void {
    this.fl_hscrolling = false;
  }

  public function onFadeDone(): Void {
    this.fl_fading = false;
  }

  public function onViewReady(): Void {
  }

  public function createView(id: Int): View {
    var self: hf.levels.GameMechanics = cast this;
    var v3 = new hf.levels.View(self, this.depthMan);
    v3.fl_hideTiles = this.fl_hideTiles;
    v3.fl_hideBorders = this.fl_hideBorders;
    v3.detach();
    if (!this.fl_shadow) {
      v3.removeShadows();
    }
    v3.display(id);
    return v3;
  }

  public function restoreFrom(snap: BitmapData, lid: Int): Void {
    this.prevSnap = snap;
    this.createFake(this.prevSnap);
    this.view.moveTo(Data.GAME_WIDTH, 0);
    this.restore(lid);
  }

  public function getSnapShot(): BitmapData {
    return this.view.getSnapShot(0, 0);
  }

  override public function destroy(): Void {
    super.destroy();
    this.view.destroy();
    this.fake.removeMovieClip();
  }

  override public function onDataReady(): Void {
    super.onDataReady();
    if (!this.view.fl_attach) {
      this.view.destroy();
      this.view = this.createView(this.currentId);
      if (this.fl_restoring) {
        this.view.moveTo(Data.GAME_WIDTH, 0);
        this.onRestoreReady();
      } else {
        this.view.moveTo(0, 0);
        this.onViewReady();
      }
    } else {
      this.cloneView(this.view);
      this.teleporterList = new Array();
      this.view.destroy();
      this.view = this.createView(this.currentId);
      this.view.moveTo(0, Data.GAME_HEIGHT);
      this.scrollCpt = 0;
      this.fl_scrolling = true;
    }
  }

  override public function onRestoreReady(): Void {
    super.onRestoreReady();
    this.fl_restoring = false;
    if (this.fl_fadeNextTransition) {
      this.fl_fadeNextTransition = false;
      this.fl_fading = true;
      this.view.moveTo(0, 0);
    } else {
      this.fl_hscrolling = true;
      this.scrollCpt = 0;
    }
  }

  override public function restore(lid: Int): Void {
    super.restore(lid);
    this.fl_restoring = true;
    this.goto(lid);
  }

  override public function suspend(): Void {
    super.suspend();
    this.view.detach();
    this.fake.removeMovieClip();
  }

  override public function update(): Void {
    super.update();
    if (this.fl_scrolling) {
      this.scrollCpt += Data.SCROLL_SPEED * Timer.tmod;
      this.view.moveTo(0, Data.GAME_HEIGHT + Math.sin(this.scrollCpt) * -Data.GAME_HEIGHT);
      this.fake._x = 0;
      this.fake._y = -Math.sin(this.scrollCpt) * Data.GAME_HEIGHT;
      if (this.scrollCpt >= Math.PI / 2) {
        this.onTransitionDone();
        this.onScrollDone();
      }
    }
    if (this.fl_hscrolling) {
      this.scrollCpt += this.scrollDir * Data.SCROLL_SPEED * Timer.tmod;
      if (this.scrollDir > 0) {
        this.view.moveTo(20 + Data.GAME_WIDTH + Math.sin(this.scrollCpt) * (-Data.GAME_WIDTH - 20), 0);
      } else {
        this.view.moveTo(-Data.GAME_WIDTH - 20 - Math.sin(this.scrollCpt) * (Data.GAME_WIDTH + 20), 0);
      }
      this.fake._x = -Math.sin(this.scrollCpt) * (Data.GAME_WIDTH + 20);
      this.fake._y = 0;
      if (this.scrollCpt >= Math.PI / 2 || this.scrollCpt <= -Math.PI / 2) {
        this.onTransitionDone();
        this.onHScrollDone();
      }
    }
    if (this.fl_fading) {
      this.fake._alpha -= Timer.tmod * Data.FADE_SPEED;
      var v3 = new etwin.flash.filters.BlurFilter();
      v3.blurX = 100 - this.fake._alpha;
      v3.blurY = v3.blurX * 0.3;
      this.fake.filters = [v3];
      if (this.fake._alpha <= 0) {
        this.onTransitionDone();
        this.onFadeDone();
      }
    }
  }
}
