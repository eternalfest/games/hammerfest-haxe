package etwin.ds;

import etwin.Symbol;

/**
 * The WeakSet class allows to flag arbitrary objects.
 *
 * A WeakSet can be used to safely attach a flag to an object.
 */
class WeakSet<T/*: {}*/> {
  private var symbol(default, null): Symbol;

  public function new(): Void {
    this.symbol = new Symbol();
  }

  public function add(o: T): Void {
    Reflect.setField(o, this.symbol.toString(), null);
    #if flash8
    // Make the property non-enumerable
    untyped ASSetPropFlags(o, this.symbol.toString(), 1, 0);
    #end
  }

  public function exists(o: T): Bool {
    return Reflect.hasField(o, this.symbol.toString());
  }

  public function remove(o: T): Void {
    Reflect.deleteField(o, this.symbol.toString());
  }
}
