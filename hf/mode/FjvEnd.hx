package hf.mode;

import etwin.flash.MovieClip;
import etwin.flash.SharedObject;
import hf.mode.Adventure;
import etwin.flash.Key;
import etwin.flash.System;
import etwin.flash.Selection;

class FjvEnd extends Adventure {
  public var fl_win: Bool;
  public var fCookie: SharedObject;
  public var scr: Dynamic /*TODO*/;
  public var bg: Dynamic /*TODO*/;
  public var timer: Float;
  public var fxList: Array<MovieClip>;
  public var fl_name: Bool;

  public function new(m: Dynamic /*TODO*/, win: Bool): Void {
    super(m);
    var v5 = HfStd.parseInt(HfStd.getVar(HfStd.getRoot(), "$time"), 10);
    if (HfStd.isNaN(v5)) {
      v5 = 30;
    }
    this.fl_win = win;
    this.fCookie = SharedObject.getLocal("$fjv_data");
    this._name = "$fjv_end";
    this.scr = this.depthMan.attach("hammer_fjv_end", Data.DP_INTERF);
    this.bg = this.depthMan.attach("hammer_fjv_bg", Data.DP_SPECIAL_BG);
    if (this.fl_win) {
      this.scr.gotoAndStop("2");
    } else {
      this.scr.gotoAndStop("1");
    }
    this.fCookie = SharedObject.getLocal("$fjv_data");
    this.timer = HfStd.parseInt(HfStd.getVar(this.fCookie.data, "timer"), 10);
    var v6 = HfStd.getVar(this.fCookie.data, "list");
    var v7 = HfStd.getVar(this.fCookie.data, "name");
    if (v6 == null) {
      v6 = new Array();
    }
    if (v7 != null) {
      var v8 = HfStd.parseInt(HfStd.getVar(this.fCookie.data, "score"), 10);
      v6.push({name: v7, win: this.fl_win, score: v8});
      HfStd.setVar(this.fCookie.data, "name", null);
    }
    while (v6.length > 50) {
      v6.splice(0, 1);
    }
    HfStd.setVar(this.fCookie.data, "list", v6);
    var v9 = "";
    for (v10 in 0...v6.length) {
      v9 += v6[v10].name + " -> " + v6[v10].win + " (" + v6[v10].score + ")\r\n";
    }
    System.setClipboard(v9);
    if (this.timer <= 0 || this.timer == null || HfStd.isNaN(this.timer)) {
      this.timer = v5 * Data.SECOND;
    }
    if (this.fl_win) {
      this.timer = 2 * v5 * Data.SECOND;
    }
    this.fxList = new Array();
    this.fl_name = this.timer <= 0;
  }

  public function focus(): Void {
    var v2 = this.scr.field;
    Selection.setFocus(v2);
    Selection.setSelection(this.scr.field.text.length, this.scr.field.text.length);
  }

  override public function endMode(): Void {
    this.scr.removeMovieClip();
    this.bg.removeMovieClip();
    super.endMode();
  }

  override public function main(): Void {
    super.main();
    if (this.fl_win && HfStd.random(2) == 0 && this.fxList.length < 40) {
      var v3 = this.depthMan.attach("hammer_fx_fjv", Data.DP_FX);
      v3._x = HfStd.random(Data.DOC_WIDTH);
      v3._y = -15;
      v3._xscale = HfStd.random(50) + 10;
      v3._yscale = v3._xscale;
      this.fxList.push(v3);
    }
    var v4 = 0;
    while (v4 < this.fxList.length) {
      var v5 = this.fxList[v4];
      v5._y += Timer.tmod * (HfStd.random(20) / 10) + 3 * (100 - v5._alpha) / 100;
      v5._x += Timer.tmod * HfStd.random(3) * (HfStd.random(2) * 2 - 1);
      v5._alpha -= Timer.tmod * 2;
      if (v5._alpha <= 0) {
        v5.removeMovieClip();
        this.fxList.splice(v4, 1);
        --v4;
      }
      ++v4;
    }
    if (!this.fl_name) {
      this.timer -= Timer.tmod;
      HfStd.setVar(this.fCookie.data, "timer", this.timer);
      var v6 = Math.ceil(this.timer / Data.SECOND);
      if (v6 >= 60) {
        var v7 = Math.floor(v6 / 60);
        this.scr.field.text = v7 + ":" + Data.leadingZeros(v6 - v7 * 60, 2);
      } else {
        this.scr.field.text = "0:" + Data.leadingZeros(v6, 2);
      }
      if (this.timer <= 0) {
        this.fl_name = true;
        this.scr.gotoAndStop("3");
        this.focus();
      }
    } else {
      var v8 = Tools.replace(this.scr.field.text, "\r", "");
      v8 = Tools.replace(v8, "\n", "");
      v8 = Data.cleanLeading(v8);
      if (v8.length > 0 && Key.isDown(Key.ENTER)) {
        HfStd.setVar(this.fCookie.data, "name", v8);
        this.manager.startGameMode(new hf.mode.Fjv(this.manager, 0));
      }
    }
  }
}
