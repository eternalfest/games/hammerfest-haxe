package hf;

import etwin.flash.XMLSocket;

class FileServer {
  public var connected: Bool;
  public var commands: Array<String>;
  public var s: XMLSocket;
  public var onChange: Null<Void -> Void>;
  public var onLoad: Null<Null<String> -> Void>;

  public static var HOST: String = "localhost";
  public static var PATH: String = FileServer.initPath();
  public static var PORT: Int = 6666;

  public function new(key: Dynamic /*TODO*/): Void {
    this.connected = false;
    this.commands = new Array();
    this.connect();
  }

  public static function initPath(): String {
    var v2 = ((HfStd.getRoot())._url.split("\')).join("/");
    var v3 = v2.lastIndexOf("/", v2.length);
    return v2.substr(0, v3 + 1);
  }

  public function connect(): Void {
    if (this.connected) {
      this.connected = false;
      this.onChange();
    }
    this.s = new XMLSocket();
    this.s.onClose = HfStd.callback(this, "connect");
    this.s.onData = HfStd.callback(this, "onReceivedData");
    this.s.onConnect = HfStd.callback(this, "onConnectionResult");
    this.commands.unshift("<hello url=\"" + FileServer.PATH + "\"/>");
    this.s.connect(FileServer.HOST, FileServer.PORT);
  }

  public function close(): Void {
    this.s.onClose = null;
    this.s.close();
  }

  public function sendCommand(x: String): Void {
    if (!this.connected) {
      this.commands.push(x);
    } else {
      var v3 = '' + (x.length + 1);
      while (v3.length < 6) {
        v3 = "0" + v3;
      }
      this.s.send(v3 + x);
    }
  }

  public function onConnectionResult(b: Bool): Void {
    if (this.connected != b) {
      this.connected = b;
      this.onChange();
    }
    if (!b) {
      var id;
      id = (HfStd.getGlobal("setInterval"))(function () {
        this.s.connect(FileServer.HOST, FileServer.PORT);
        (HfStd.getGlobal("clearInterval"))(id);
      }, 40);
    } else {
      for (v3 in 0...this.commands.length) {
        this.sendCommand(this.commands[v3]);
      }
      this.commands = new Array();
    }
  }

  public function onReceivedData(d: Dynamic /*TODO*/): Void {
    var v3 = (new XML(d)).firstChild;
    if (v3.nodeName !== "load") {
    } else {
      this.onLoad(v3.firstChild.nodeValue);
    }
  }

  public function load(file: String): Void {
    this.sendCommand("<load file=\"" + file + "\"/>");
  }

  public function save(file: String, data: String): Void {
    this.sendCommand("<save file=\"" + file + "\">" + data + "</save>");
  }
}
