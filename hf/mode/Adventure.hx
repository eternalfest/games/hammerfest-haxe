package hf.mode;

import etwin.flash.MovieClip;
import hf.entity.Bad;
import hf.GameManager;
import hf.mode.GameMode;
import etwin.flash.Key;

class Adventure extends GameMode {
  public static var BUCKET_X: Int = 11;
  public static var BUCKET_Y: Int = 19;

  public var firstLevel: Int;
  public var perfectOrder: Array<Int>;
  public var perfectCount: Int;
  public var trackPos: Null<Float>;

  public function new(m: GameManager, ?id: Int): Void {
    super(m);
    this.firstLevel = id;
    this.fl_warpStart = false;
    this.fl_map = true;
    this._name = "$adventure";
  }

  override public function addLevelItems(): Void {
    var v4;
    var v3;
    super.addLevelItems();
    if (this.world.current.__dollar__specialSlots.length > 0) {
      this.statsMan.spreadExtend();
    }
    if (this.world.current.__dollar__specialSlots.length > 0) {
      v3 = HfStd.random(this.world.current.__dollar__specialSlots.length);
      v4 = this.world.current.__dollar__specialSlots[v3];
      this.world.scriptEngine.insertSpecialItem(this.randMan.draw(Data.RAND_ITEMS_ID), null, v4.__dollar__x, v4.__dollar__y, Data.SPECIAL_ITEM_TIMER, null, false, true);
    }
    if (this.world.current.__dollar__scoreSlots.length > 0) {
      v3 = HfStd.random(this.world.current.__dollar__scoreSlots.length);
      v4 = this.world.current.__dollar__scoreSlots[v3];
      this.world.scriptEngine.insertScoreItem(this.randMan.draw(Data.RAND_SCORES_ID), null, v4.__dollar__x, v4.__dollar__y, Data.SCORE_ITEM_TIMER, null, false, true);
      if (this.globalActives[94]) {
        var v5 = HfStd.random(Data.LEVEL_WIDTH);
        var v6 = HfStd.random(Data.LEVEL_HEIGHT - 5);
        var v7 = this.world.getGround(v5, v6);
        this.world.scriptEngine.insertScoreItem(this.randMan.draw(Data.RAND_SCORES_ID), null, v7.x, v7.y, Data.SCORE_ITEM_TIMER, null, false, true);
      }
    }
  }

  override public function attachBad(id: Int, x: Float, y: Float): Bad {
    var v6 = super.attachBad(id, x, y);
    if (Data.BAD_CLEAR.check(v6)) {
      this.perfectOrder.push(v6.uniqId);
      ++this.perfectCount;
    }
    return v6;
  }

  override public function destroy(): Void {
    super.destroy();
  }

  override public function endMode(): Void {
    this.stopMusic();
    this.manager.startMode(new hf.mode.Editor(this.manager, this.world.setName, this.world.currentId));
  }

  override public function getDebugControls(): Void {
    super.getDebugControls();
    if (Key.isDown(Key.SHIFT) && Key.isDown(Key.CONTROL) && Key.isDown(77)) {
      this.manager.startGameMode(new hf.mode.MultiCoop(this.manager, 0));
    }
  }

  override public function goto(id: Int): Void {
    this.perfectOrder = new Array();
    this.perfectCount = 0;
    super.goto(id);
  }

  override public function initGame(): Void {
    super.initGame();
    this.playMusic(0);
    this.world.goto(this.firstLevel);
    this.insertPlayer(this.world.current.__dollar__playerX, this.world.current.__dollar__playerY);
  }

  override public function init(): Void {
    super.init();
    this.initGame();
    var v3 = this.getPlayerList();
    for (v4 in 0...v3.length) {
      this.initPlayer(v3[v4]);
    }
    this.initInterface();
  }

  override public function initWorld(): Void {
    super.initWorld();
    this.addWorld("xml_adventure");
    this.addWorld("xml_deepnight");
    this.addWorld("xml_hiko");
    this.addWorld("xml_ayame");
    this.addWorld("xml_hk");
    if (this.manager.isDev()) {
      this.addWorld("xml_dev");
    }
  }

  override public function isBossLevel(id: Int): Bool {
    return (super.isBossLevel(id) || this.world.fl_mainWorld) && (id >= 30 && id % 10 == 0 || id == Data.BAT_LEVEL || id == Data.TUBERCULOZ_LEVEL);
  }

  override public function nextLevel(): Void {
    super.nextLevel();
    if (this.fl_warpStart) {
      this.world.currentId = 0;
      this.unlock();
      this.world.view.detach();
      this.forcedGoto(10);
    }
  }

  override public function onEndOfSet(): Void {
    super.onEndOfSet();
    this.exitGame();
  }

  override public function onExplode(x: Float, y: Float, radius: Float): Void {
    super.onExplode(x, y, radius);
    if (this.world.fl_mainWorld && this.world.currentId == Data.TUBERCULOZ_LEVEL) {
      ((cast this.getOne(Data.BOSS)): hf.entity.boss.Tuberculoz).onExplode(x, y, radius);
    }
  }

  override public function onGameOver(): Void {
    super.onGameOver();
    this.manager.logAction("$t=" + Math.round(this.duration / Data.SECOND));
    var v3 = (this.manager.history.join("|")).indexOf("!", 0) >= 0;
    this.manager.history = ["F=" + (HfStd.getRoot()).__dollar__version, "T=" + this.gameChrono.get()];
    if (v3) {
      this.manager.history.push("$illegal".substring(1));
    }
    this.stopMusic();
    this.saveScore();
  }

  override public function onHurryUp(): Null<MovieClip> {
    var v3 = super.onHurryUp();
    if (GameManager.CONFIG.hasMusic() && this.currentTrack == 0) {
      this.trackPos = this.manager.musics[this.currentTrack].position;
      this.playMusic(2);
    }
    return v3;
  }

  override public function onKillBad(b: Bad): Void {
    super.onKillBad(b);
    if (this.world.fl_mainWorld && this.world.currentId == Data.TUBERCULOZ_LEVEL) {
      ((cast this.getOne(Data.BOSS)): hf.entity.boss.Tuberculoz).onKillBad();
    }
    if (this.badCount > 1 && b.uniqId == this.perfectOrder[0]) {
      this.perfectOrder.splice(0, 1);
    }
  }

  override public function onLevelClear(): Void {
    super.onLevelClear();
    if (!this.world.isVisited() && this.perfectOrder.length == 0 && this.world.scriptEngine.cycle >= 10) {
      var v3 = this.getPlayerList();
      for (v4 in 0...v3.length) {
        v3[v4].setBaseAnims(Data.ANIM_PLAYER_WALK_V, Data.ANIM_PLAYER_STOP_V);
      }
      hf.entity.supa.SupaItem.attach(this, this.perfectCount - 1);
      this.statsMan.inc(Data.STAT_SUPAITEM, 1);
    }
  }

  override public function onLevelReady(): Void {
    super.onLevelReady();
    if (this.fl_warpStart) {
      if (this.world.fl_mainWorld) {
        var v3 = this.getOne(Data.PLAYER);
        this.world.view.attachSprite("$door_secret", Entity.x_ctr(this.world.current.__dollar__playerX), Entity.x_ctr(this.world.current.__dollar__playerY) + Data.CASE_HEIGHT * 0.5, true);
      }
      this.fl_warpStart = false;
    }
    if (!this.world.isVisited()) {
      this.fxMan.attachLevelPop(Lang.getLevelName(this.currentDim, this.world.currentId), this.world.currentId > 0);
    }
  }

  override public function resetHurry(): Void {
    super.resetHurry();
    if (this.currentTrack == 2) {
      this.playMusic(0);
    }
  }

  override public function saveScore(): Void {
    if (GameManager.HH.get("$" + Md5.encode(this.world.setName)) != "$" + Md5.encode("" + this.world.csum)) {
      return;
    }
    var v2 = "8d6fff6186db2e4f436852f16dcfbba8F1def3777b79eb80048ebf70a0ae83b77F38fe1fbe22565c2f6691f1f666a600d9Fa1a9405afb4576a3bceb75995ad17d09F8c49e07bc65be554538effb12eced2c2F0255841255b29dd91b88a57b4a27f422";
    var v3 = v2.split("$F".substr(1, 2));
    for (v4 in 0...this.dimensions.length) {
      var v5 = Md5.encode(this.dimensions[v4].setName);
      if (!v3.remove(v5)) {
        return;
      }
    }
    if (!this.manager.isDev()) {
      v3.splice(0, 1);
    }
    if (v3.length > 1) {
      return;
    }
    (HfStd.getGlobal("gameOver"))(this.savedScores[0], null, {__dollar__reachedLevel: this.dimensions[0].currentId, __dollar__item2: this.getPicks2(), __dollar__data: this.manager.history});
  }

  override public function startLevel(): Void {
    var v3 = this.getPlayerList();
    for (v4 in 0...v3.length) {
      v3[v4].setBaseAnims(Data.ANIM_PLAYER_WALK, Data.ANIM_PLAYER_STOP);
    }
    this.perfectOrder = new Array();
    this.perfectCount = 0;
    super.startLevel();
    if (this.world.fl_mainWorld && this.world.currentId == Data.BAT_LEVEL) {
      hf.entity.boss.Bat.attach(this);
      this.fl_clear = false;
    }
    if (this.world.fl_mainWorld && this.world.currentId == Data.TUBERCULOZ_LEVEL) {
      hf.entity.boss.Tuberculoz.attach(this);
      this.fl_clear = false;
    }
    if (this.world.fl_mainWorld && this.world.currentId == 0) {
      this.fxMan.detachExit();
    }
  }
}
