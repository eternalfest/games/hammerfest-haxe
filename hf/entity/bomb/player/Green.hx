package hf.entity.bomb.player;

import hf.entity.bomb.PlayerBomb;
import hf.mode.GameMode;

class Green extends PlayerBomb {
  public function new(): Void {
    super();
    this.duration = 200;
    this.power = 25;
    this.fl_blink = true;
    this.fl_alphaBlink = false;
    this.fl_unstable = true;
    this.explodeSound = "sound_bomb_green";
  }

  public static function attach(g: GameMode, x: Float, y: Float): Green {
    var v5 = "hammer_bomb_green";
    var v6: Green = cast g.depthMan.attach(v5, Data.DP_BOMBS);
    v6.initBomb(g, x, y);
    return v6;
  }

  override public function duplicate(): Green {
    return hf.entity.bomb.player.Green.attach(this.game, this.x, this.y);
  }

  override public function endUpdate(): Void {
    super.endUpdate();
    this.rotation = 0;
    this._rotation = this.rotation;
  }

  override public function onExplode(): Void {
    if (this.fl_explode) {
      return;
    }
    this.game.soundMan.playSound("sound_bomb", Data.CHAN_BOMB);
    super.onExplode();
    var v3 = this.bombGetClose(Data.BAD);
    for (v4 in 0...v3.length) {
      var v5 = v3[v4];
      v5.setCombo(this.uniqId);
      v5.freeze(Data.FREEZE_DURATION);
      this.shockWave(v5, this.radius, this.power);
    }
    this.game.fxMan.inGameParticles(Data.PARTICLE_ICE, this.x, this.y, HfStd.random(2) + 2);
    var v3 = this.bombGetClose(Data.BAD_BOMB);
    for (v6 in 0...v3.length) {
      var v7 = v3[v6];
      if (!v7.fl_explode) {
        var v8 = v7.getFrozen(this.uniqId);
        if (v8 != null) {
          this.shockWave(v8, this.radius, this.power);
          v7.destroy();
        }
      }
    }
  }
}
