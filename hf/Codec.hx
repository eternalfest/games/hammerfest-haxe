package hf;

class Codec {
  public var key: Dynamic /*TODO*/;
  public var ikey: Int;
  public var pkey: Null<Int>;
  public var crc: Null<Int>;
  public var str: Null<String>;

  public static var BASE64: String = ":_abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
  public static var IDCHARS: String = "$uasxIintfoj";

  public function new(key: Dynamic /*TODO*/): Void {
    this.key = key;
    this.ikey = 0;
    for (v3 in 0...key.length) {
      this.ikey *= 51;
      this.ikey += key.charCodeAt(v3);
      this.ikey &= -1;
    }
  }

  public function makeCrcString(): String {
    return Codec.BASE64.charAt(this.crc & 63) + Codec.BASE64.charAt(this.crc >> 6 & 63) + Codec.BASE64.charAt(this.crc >> 12 & 63) + Codec.BASE64.charAt(this.crc >> 18 & 63) + Codec.BASE64.charAt(this.crc >> 24 & 63);
  }

  public function encode(o: Dynamic): String {
    this.pkey = 0;
    this.crc = 0;
    this.str = "";
    this.encodeAny(o);
    this.str += this.makeCrcString();
    return this.str;
  }

  public function decode(s: String): Dynamic {
    this.pkey = 0;
    this.crc = 0;
    this.str = s;
    var v3 = this.decodeAny();
    if (this.str != this.makeCrcString()) {
      return null;
    }
    return v3;
  }

  public function writeStr(s: String): Void {
    for (v3 in 0...s.length) {
      this.writeChar(s.charAt(v3));
    }
  }

  public function writeChar(c: String): Void {
    var v3 = Codec.BASE64.indexOf(c, 0);
    if (v3 == -1) {
      this.str += c;
      return;
    }
    var v4 = v3 ^ this.ikey >> this.pkey & 63;
    this.crc *= 51;
    this.crc += v4;
    this.crc ^= -1;
    this.str += Codec.BASE64.charAt(v4);
    this.pkey += 6;
    if (this.pkey >= 28) {
      this.pkey -= 28;
    }
  }

  public function readChar(): String {
    var v2 = this.str.charAt(0);
    var v3 = Codec.BASE64.indexOf(v2, 0);
    this.str = this.str.substring(1);
    if (v3 == -1) {
      return v2;
    }
    this.crc *= 51;
    this.crc += v3;
    this.crc ^= -1;
    v3 ^= this.ikey >> this.pkey & 63;
    this.pkey += 6;
    if (this.pkey >= 28) {
      this.pkey -= 28;
    }
    return Codec.BASE64.charAt(v3);
  }

  public function readStr(): String {
    var v2 = "";
    while (true) {
      var v3 = this.readChar();
      if (v3 == null) {
        return null;
      }
      if (v3 == ":") {
        break;
      }
      v2 += v3;
    }
    return v2;
  }

  public function encodeArray(o: Array<Dynamic>): Void {
    this.writeStr('' + (o.length));
    this.writeStr(":");
    var v4 = 0;
    for (v3 in 0...o.length) {
      if (o[v3] == null) {
        ++v4;
      } else {
        if (v4 > 1) {
          this.writeStr(Codec.IDCHARS.charAt(11) + v4 + ":");
          v4 = 0;
        } else {
          if (v4 == 1) {
            this.encodeAny(null);
            v4 = 0;
          }
        }
        this.encodeAny(o[v3]);
      }
    }
    if (v4 > 0) {
      this.writeStr(Codec.IDCHARS.charAt(11) + v4 + ":");
    }
  }

  public function encodeObject(o: Dynamic): Void {
    HfStd.forin(o, function (k) {
      this.writeStr(k);
      this.writeStr(":");
      this.encodeAny(Reflect.field(o, k));
    });
    this.writeStr(":");
  }

  public function encodeAny(o: Dynamic): Void {
    if (o == null) {
      this.writeStr(Codec.IDCHARS.charAt(1));
    } else {
      if (HfStd._instanceof(o, Array)) {
        this.writeStr(Codec.IDCHARS.charAt(2));
        this.encodeArray(o);
      } else {
        switch (HfStd._typeof(o)) {
          case "string":
            this.writeStr(Codec.IDCHARS.charAt(3));
            this.writeStr(o);
            this.writeStr(":");
          case "number":
            var v3 = o;
            if (HfStd.isNaN(v3)) {
              this.writeStr(Codec.IDCHARS.charAt(4));
            } else {
              if (v3 == HfStd.infinity) {
                this.writeStr(Codec.IDCHARS.charAt(5));
              } else {
                if (v3 == -HfStd.infinity) {
                  this.writeStr(Codec.IDCHARS.charAt(6));
                } else {
                  v3 = Std.int(v3);
                  this.writeStr(Codec.IDCHARS.charAt(7));
                  if (v3 < 0) {
                    this.writeStr(Codec.IDCHARS.charAt(1));
                  }
                  this.writeStr('' + (Math.abs(v3)));
                  this.writeStr(":");
                }
              }
            }
          case "boolean":
            if (o == true) {
              this.writeStr(Codec.IDCHARS.charAt(8));
            } else {
              this.writeStr(Codec.IDCHARS.charAt(9));
            }
          default:
            this.writeStr(Codec.IDCHARS.charAt(10));
            this.encodeObject(o);
        }
      }
    }
  }

  public function decodeArray(): Array<Dynamic> {
    var v3 = (cast this.readStr()) | 0;
    var v4 = new Array();
    for (v2 in 0...v3) {
      v4.push(this.decodeAny());
    }
    return v4;
  }

  public function decodeObject(): Dynamic {
    var v2 = {};
    while (true) {
      var v3 = this.readStr();
      if (v3 == null) {
        return null;
      }
      if (v3 == "") {
        break;
      }
      var v4 = this.decodeAny();
      Reflect.setField(v2, v3, v4);
    }
    return v2;
  }

  public function decodeAny(): Dynamic {
    var v5;
    var v2 = this.readChar();
    if (v2 == Codec.IDCHARS.charAt(1)) {
        return null;
    } else if (v2 == Codec.IDCHARS.charAt(2)) {
        return this.decodeArray();
    } else if (v2 == Codec.IDCHARS.charAt(3)) {
        return this.readStr();
    } else if (v2 == Codec.IDCHARS.charAt(4)) {
        return null * 1;
    } else if (v2 == Codec.IDCHARS.charAt(5)) {
        return HfStd.infinity;
    } else if (v2 == Codec.IDCHARS.charAt(6)) {
        return -HfStd.infinity;
    } else if (v2 == Codec.IDCHARS.charAt(7)) {
        var v3 = this.readChar();
        var v4 = this.readStr();
        if (v3 == Codec.IDCHARS.charAt(1)) {
          v5 = -((cast v4) | 0);
        } else {
          v5 = (cast (v3 + v4)) | 0;
        }
        return v5;
    } else if (v2 == Codec.IDCHARS.charAt(8)) {
        return true;
    } else if (v2 == Codec.IDCHARS.charAt(9)) {
        return false;
    } else if (v2 == Codec.IDCHARS.charAt(10)) {
        return this.decodeObject();
    }
    return null;
  }
}
