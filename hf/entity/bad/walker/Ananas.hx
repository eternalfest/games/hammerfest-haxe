package hf.entity.bad.walker;

import hf.entity.bad.Jumper;
import hf.mode.GameMode;

class Ananas extends Jumper {
  public static var CHANCE_DASH: Int = 6;

  public var dashRadius: Float;
  public var dashPower: Float;
  public var fl_attack: Bool;

  public function new(): Void {
    super();
    this.setJumpH(100);
    this.speed *= 0.8;
    this.dashRadius = 100;
    this.dashPower = 30;
    this.fl_attack = false;
    this.slideFriction = 0.9;
    this.shockResistance = 2.0;
  }

  public static function attach(g: GameMode, x: Float, y: Float): Ananas {
    var v5 = Data.LINKAGES[Data.BAD_ANANAS];
    var v6: Ananas = cast g.depthMan.attach(v5, Data.DP_BADS);
    v6.initBad(g, x, y);
    return v6;
  }

  public function repel<E: Physics>(type: EntityType<E>, powerFactor: Float): Void {
    var v4 = this.game.getClose(type, this.x, this.y, this.dashRadius, false);
    for (v5 in 0...v4.length) {
      var v6 = v4[v5];
      this.shockWave(v6, this.dashRadius, this.dashPower * powerFactor);
      v6.dy -= 8;
      var p = Data.PLAYER.downcast(v6);
      if (p != null) {
        p.knock(Data.SECOND * 1.5);
      }
    }
  }

  public function vaporize<E: Entity>(type: EntityType<E>): Void {
    var v3 = this.game.getClose(type, this.x, this.y, this.dashRadius, false);
    for (v4 in 0...v3.length) {
      var v5 = v3[v4];
      this.game.fxMan.attachFx(v5.x, v5.y - Data.CASE_HEIGHT, "hammer_fx_pop");
      v5.destroy();
    }
  }

  public function startAttack(): Void {
    var v2 = true;
    var v3 = this.game.getPlayerList();
    for (v4 in 0...v3.length) {
      if (!v3[v4].fl_knock) {
        v2 = false;
      }
    }
    if (v2) {
      return;
    }
    this.halt();
    this.playAnim(Data.ANIM_BAD_THINK);
    this.forceLoop(true);
    this.setNext(0, -10, Data.SECOND * 0.9, Data.ACTION_MOVE);
    this.fl_attack = true;
    var v5 = this.game.depthMan.attach("curse", Data.DP_FX);
    v5.gotoAndStop("" + Data.CURSE_TAUNT);
    this.stick(v5, 0, -Data.CASE_HEIGHT * 2.5);
  }

  public function attack(): Void {
    var v2 = this.game.fxMan.attachExplodeZone(this.x, this.y, this.dashRadius);
    v2.mc._alpha = 20;
    this.game.shake(Data.SECOND * 0.5, 5);
    var v3 = this.game.getPlayerList();
    for (v4 in 0...v3.length) {
      var v5 = v3[v4];
      if (v5.fl_stable) {
        if (v5.fl_shield) {
          v5.dy = -8;
        } else {
          v5.knock(Data.PLAYER_KNOCK_DURATION);
        }
      }
    }
    this.repel(Data.BOMB, 1);
    this.repel(Data.PLAYER, 2);
    this.vaporize(Data.PLAYER_SHOOT);
    this.fl_attack = false;
    this.unstick();
  }

  override public function freeze(d: Float): Void {
    super.freeze(d);
    this.fallFactor *= 1.5;
    this.fl_attack = false;
    this.unstick();
  }

  override public function init(g: GameMode): Void {
    super.init(g);
    if (this.game.fl_bombExpert) {
      this.dashRadius *= 2;
    }
  }

  override public function isReady(): Bool {
    return !this.fl_attack && super.isReady();
  }

  override public function killHit(dx: Null<Float>): Void {
    super.killHit(dx);
    this.fl_attack = false;
  }

  override public function knock(d: Float): Void {
    super.knock(d);
    this.fallFactor *= 1.5;
    this.fl_attack = false;
    this.unstick();
  }

  override public function onEndAnim(id: Int): Void {
    super.onEndAnim(id);
    if (id == Data.ANIM_BAD_SHOOT_END.id) {
      this.walk();
    }
  }

  override public function onHitGround(h: Float): Void {
    super.onHitGround(h);
    if (this.fl_attack && this.isHealthy()) {
      this.attack();
      this.playAnim(Data.ANIM_BAD_SHOOT_END);
      this.halt();
    } else {
      this.game.shake(Data.SECOND * 0.2, 2);
    }
  }

  override public function onHitWall(): Void {
    if (!this.isHealthy()) {
      this.game.shake(5, 3);
    }
    super.onHitWall();
  }

  override public function prefix(): Void {
    if (this.isReady()) {
      if (this.fl_playerClose && HfStd.random(1000) <= hf.entity.bad.walker.Ananas.CHANCE_DASH * 2) {
        this.startAttack();
      }
      if (!this.fl_playerClose && HfStd.random(1000) <= hf.entity.bad.walker.Ananas.CHANCE_DASH) {
        this.startAttack();
      }
    }
    super.prefix();
  }
}
