package hf;

class Stat {
  public var current: Int;
  public var total: Int;

  public function new(): Void {
    this.current = 0;
    this.total = 0;
  }

  public function inc(n: Int): Void {
    this.current += n;
  }

  public function reset(): Void {
    this.total += this.current;
    this.current = 0;
  }
}
