package hf;

import etwin.flash.MovieClip;
import hf.GameManager;
import hf.Hash;

class GameParameters {
  public var root: MovieClip;
  public var manager: GameManager;
  public var options: Hash;
  public var optionList: Array<String>;
  public var families: Array<String>;
  public var scoreItemFamilies: Array<Int>;
  public var specialItemFamilies: Array<Int>;
  public var generalVolume: Float;
  public var soundVolume: Float;
  public var musicVolume: Float;
  public var fl_detail: Bool;
  public var fl_shaky: Bool;

  public function new(mc: MovieClip, man: GameManager, f: String, opt: String): Void {
    this.root = mc;
    this.manager = man;
    if (HfStd.isNaN(this.getInt("$volume"))) {
      GameManager.warning("missing parameters");
    }
    this.options = new Hash();
    this.optionList = new Array();
    if (opt.length > 0) {
      this.optionList = opt.split(",");
      for (v6 in 0...this.optionList.length) {
        this.options.set(this.optionList[v6], true);
      }
    }
    this.families = f.split(",");
    this.scoreItemFamilies = new Array();
    this.specialItemFamilies = new Array();
    for (v7 in 0...this.families.length) {
      var v8 = HfStd.parseInt(this.families[v7], 10);
      if (v8 >= 1000) {
        this.scoreItemFamilies.push(v8);
      } else {
        this.specialItemFamilies.push(v8);
      }
    }
    this.generalVolume = this.getInt("$volume") * 0.5 / 100;
    this.soundVolume = this.getInt("$sound") * this.generalVolume;
    this.musicVolume = this.getInt("$music") * this.generalVolume * 0.65;
    this.fl_detail = this.getBool("$detail");
    this.fl_shaky = this.getBool("$shake");
    if (!this.fl_detail) {
      this.setLowDetails();
    }
  }

  public function getStr(n: String): Dynamic {
    return HfStd.getVar(this.root, n);
  }

  public function getInt(n: String): Int {
    return HfStd.parseInt(HfStd.getVar(this.root, n), 10);
  }

  public function getBool(n: String): Bool {
    return HfStd.getVar(this.root, n) != "0" && HfStd.getVar(this.root, n) != null;
  }

  public function setLowDetails(): Void {
    this.fl_detail = false;
    (HfStd.getRoot())._quality = "$medium".substring(1);
    Data.MAX_FX = Math.ceil(Data.MAX_FX * 0.5);
  }

  public function hasFamily(id: Int): Bool {
    var v3 = false;
    for (v4 in 0...this.specialItemFamilies.length) {
      if (this.specialItemFamilies[v4] == id) {
        v3 = true;
      }
    }
    for (v5 in 0...this.scoreItemFamilies.length) {
      if (this.scoreItemFamilies[v5] == id) {
        v3 = true;
      }
    }
    return v3;
  }

  public function hasOption(oid: String): Bool {
    return this.options.get(oid) == true;
  }

  public function toString(): String {
    var v2 = "";
    v2 += "fam=" + this.families.join(", ") + "\n";
    v2 += "opt=" + this.optionList.join("\n  ") + "\n";
    v2 += "mus=" + this.musicVolume + "\n";
    v2 += "snd=" + this.soundVolume + "\n";
    v2 += "detail=" + this.fl_detail + "\n";
    v2 += "shaky =" + this.fl_shaky + "\n";
    return v2;
  }

  public function hasMusic(): Bool {
    return this.manager.musics[0] != null;
  }
}
