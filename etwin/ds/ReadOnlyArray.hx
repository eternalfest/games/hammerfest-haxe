package etwin.ds;

/**
 * Read-only array (cannot be mutated by the consumer, may still be change
 * if there are other references to the underlying array).
 */
@:forward(
  indexOf, iterator, join, lastIndexOf, toString,
  // These methods return a writable `Array` (but don't modify _this_ array).
  concat, copy, filter, map, slice
)
abstract ReadOnlyArray<T>(Array<T>) from Array<T> to Iterable<T> {
  /**
		The length of `this` Array.
	**/
  public var length(get, never): Int;

  private inline function get_length() {
    return this.length;
  }

  @:arrayAccess inline function get(i: Int): T {
    return this[i];
  }
}
