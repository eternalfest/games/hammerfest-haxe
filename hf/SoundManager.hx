package hf;

import etwin.flash.MovieClip;
import etwin.flash.Sound;

typedef SoundChannel = {
  var mc: MovieClip;
  var sounds: Hash;
  var vol: Float;
  var vol_ctrl: Sound;
  var nb: Int;
  var enabled: Bool;
}

class SoundManager {
  public var root_mc: MovieClip;
  public var depth: Int;
  public var channels: Array<SoundChannel>;
  public var flag_playing: Null<Bool>;
  public var fade_from: SoundChannel;
  public var fade_start: Float;
  public var fade_to: SoundChannel;
  public var fade_end: Float;
  public var last_time: Int;
  public var fade_pos: Float;
  public var fade_len: Float;

  public function new(mc: MovieClip, base_depth: Int): Void {
    this.root_mc = mc;
    this.depth = base_depth;
    this.channels = new Array();
    this.fade_pos = -1;
  }

  public static function do_stop_sound(k: String, s: Sound): Void {
    s.stop();
    Reflect.setField(s, "flag_playing", false);
  }

  public function on_sound_completed(): Void {
    this.flag_playing = false;
  }

  public function destroy(): Void {
    for (v2 in 0...this.channels.length) {
      var v3 = this.channels[v2];
      v3.sounds.iter(SoundManager.do_stop_sound);
      v3.mc.removeMovieClip();
    }
    this.channels = [];
  }

  public function getChannel(chan: Int): SoundChannel {
    var v3 = this.channels[chan];
    if (v3 == null) {
      var v4 = HfStd.createEmptyMC(this.root_mc, this.depth++);
      v3 = {mc: v4, sounds: new Hash(), vol: 100, vol_ctrl: new Sound(v4), nb: chan, enabled: true};
      this.channels[chan] = v3;
    }
    return v3;
  }

  public function getSound(name: String, chan: Int): Sound {
    var v4 = this.getChannel(chan);
    var v5 = v4.sounds.get(name);
    if (v5 == null) {
      v5 = new Sound(v4.mc);
      v5.attachSound(name);
      v5.onSoundComplete = this.on_sound_completed;
      Reflect.setField(v5, "flag_playing", false);
      v4.sounds.set(name, v5);
    }
    return v5;
  }

  public function playSound(name: String, chan: Int): Void {
    var v4 = this.getSound(name, chan);
    v4.start(0, 1);
    Reflect.setField(v4, "flag_playing", true);
  }

  public function play(name: String): Void {
    this.playSound(name, 0);
  }

  public function loop(name: String, chan: Int): Void {
    var v4 = this.getSound(name, chan);
    v4.start(0, 65535);
    Reflect.setField(v4, "flag_playing", true);
  }

  public function stopSound(name: String, chan: Int): Void {
    var v4 = this.getSound(name, chan);
    v4.stop();
    Reflect.setField(v4, "flag_playing", false);
  }

  public function fade(chan_from: Int, chan_to: Int, length: Float): Void {
    if (this.fade_pos != -1) {
      this.setVolume(this.fade_to.nb, this.fade_end);
      this.stop(this.fade_from.nb);
      this.setVolume(this.fade_from.nb, this.fade_end);
    }
    this.fade_from = this.getChannel(chan_from);
    this.fade_to = this.getChannel(chan_to);
    this.fade_start = this.fade_to.vol;
    this.fade_end = this.fade_from.vol;
    this.fade_pos = 0;
    this.fade_len = length;
    this.last_time = HfStd.getTimer();
  }

  public function main(): Void {
    if (this.fade_pos != -1) {
      var v2 = false;
      var v3 = HfStd.getTimer();
      this.fade_pos += (this.last_time - v3) / 1000 / this.fade_len;
      this.last_time = v3;
      if (this.fade_pos >= 1) {
        this.fade_pos = 1;
        v2 = true;
      }
      var v4 = (this.fade_end - this.fade_start) * this.fade_pos + this.fade_start;
      this.setVolume(this.fade_to.nb, v4);
      this.setVolume(this.fade_from.nb, this.fade_end - v4);
      if (v2) {
        this.fade_pos = -1;
        this.stop(this.fade_from.nb);
        this.setVolume(this.fade_from.nb, this.fade_end);
      }
    }
  }

  public function enable(chan: Int, flag: Bool): Void {
    var v4 = this.getChannel(chan);
    v4.enabled = flag;
    if (v4.enabled) {
      v4.vol_ctrl.setVolume(v4.vol);
    } else {
      v4.vol_ctrl.setVolume(0);
    }
  }

  public function stop(chan: Int): Void {
    var v3 = this.getChannel(chan);
    v3.sounds.iter(SoundManager.do_stop_sound);
  }

  public function isPlaying(name: String, chan: Int): Bool {
    var v4 = this.getSound(name, chan);
    return Reflect.field(v4, "flag_playing");
  }

  public function setVolume(chan: Int, volume: Float): Void {
    var v4 = this.getChannel(chan);
    v4.vol = volume;
    if (v4.enabled) {
      v4.vol_ctrl.setVolume(v4.vol);
    }
  }
}
