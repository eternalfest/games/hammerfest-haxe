package etwin.flash;

@:native("Date")
extern class Date {
  static public function UTC(year: Int, month: Int, date: Int, hour: Int, min: Int, sec: Int, ms: Int): Int;

  public function new(?year: Int, ?month: Int, ?date: Int, ?hour: Int, ?min: Int, ?sec: Int, ?ms: Int): Void;

  public function getFullYear(): Int;

  public function getYear(): Int;

  public function getMonth(): Int;

  public function getDate(): Int;

  public function getDay(): Int;

  public function getHours(): Int;

  public function getMinutes(): Int;

  public function getSeconds(): Int;

  public function getMilliseconds(): Int;

  public function getUTCFullYear(): Int;

  public function getUTCYear(): Int;

  public function getUTCMonth(): Int;

  public function getUTCDate(): Int;

  public function getUTCDay(): Int;

  public function getUTCHours(): Int;

  public function getUTCMinutes(): Int;

  public function getUTCSeconds(): Int;

  public function getUTCMilliseconds(): Int;

  public function setFullYear(value: Int): Void;

  public function setMonth(value: Int): Void;

  public function setDate(value: Int): Void;

  public function setHours(value: Int): Void;

  public function setMinutes(value: Int): Void;

  public function setSeconds(value: Int): Void;

  public function setMilliseconds(value: Int): Void;

  public function setUTCFullYear(value: Int): Void;

  public function setUTCMonth(value: Int): Void;

  public function setUTCDate(value: Int): Void;

  public function setUTCHours(value: Int): Void;

  public function setUTCMinutes(value: Int): Void;

  public function setUTCSeconds(value: Int): Void;

  public function setUTCMilliseconds(value: Int): Void;

  public function getTime(): Int;

  public function setTime(value: Int): Void;

  public function getTimezoneOffset(): Int;

  public function toString(): String;

  public function valueOf(): Int;

  public function setYear(value: Int): Void;
}
