package hf.gui;

import etwin.flash.MovieClip;
import hf.DepthManager;

class Container {
  public var mode: Dynamic /*TODO*/;
  public var mc: MovieClip;
  public var depthMan: DepthManager;
  public var width: Float;
  public var currentX: Float;
  public var currentY: Float;
  public var scale: Float;
  public var lineHeight: Float;
  public var list: Array<Dynamic /*TODO*/>;
  public var fl_lock: Bool;

  public static var MARGIN: Float = 5;
  public static var MIN_HEIGHT: Float = 20;

  public function new(m: Dynamic /*TODO*/, x: Float, y: Float, wid: Float): Void {
    this.mode = m;
    this.mc = this.mode.depthMan.empty(Data.DP_INTERF);
    this.depthMan = new DepthManager(this.mc);
    this.mc._x = x;
    this.mc._y = y;
    this.width = wid;
    this.currentX = 0;
    this.currentY = 0;
    this.scale = 1;
    this.lineHeight = hf.gui.Container.MIN_HEIGHT;
    this.list = new Array();
    this.unlock();
  }

  public function lock(): Void {
    this.fl_lock = true;
  }

  public function unlock(): Void {
    this.fl_lock = false;
  }

  public function insert(b: Dynamic /*TODO*/): Dynamic /*TODO*/ {
    b.scale(this.scale);
    var v3 = this.currentX + b.width * this.scale + hf.gui.Container.MARGIN;
    if (v3 > this.width) {
      this.endLine();
      v3 = b.width * this.scale + hf.gui.Container.MARGIN;
    }
    var v4 = {x: this.currentX, y: this.currentY};
    this.currentX = v3;
    this.lineHeight = Math.max(this.lineHeight, b._height);
    this.list.push(b);
    return v4;
  }

  public function endLine(): Void {
    this.currentX = 0;
    this.currentY += this.lineHeight + hf.gui.Container.MARGIN;
    this.lineHeight = hf.gui.Container.MIN_HEIGHT * this.scale;
  }

  public function setScale(ratio: Float): Void {
    this.scale = ratio;
  }

  public function update(): Void {
    for (v2 in 0...this.list.length) {
      this.list[v2].update();
    }
  }
}
