package hf;

import hf.entity.item.SpecialItem;
import hf.mode.GameMode;

class StatsManager {
  public var game: GameMode;
  public var stats: Array<Stat>;
  public var extendList: Null<Array<Int>>;

  public function new(g: GameMode): Void {
    this.game = g;
    this.stats = new Array();
    for (v3 in 0...50) {
      this.stats[v3] = new Stat();
    }
  }

  public function read(id: Int): Int {
    return this.stats[id].current;
  }

  public function getTotal(id: Int): Int {
    return this.stats[id].total;
  }

  public function write(id: Int, n: Int): Void {
    this.stats[id].current = n;
  }

  public function inc(id: Int, n: Int): Void {
    this.stats[id].inc(n);
  }

  public function reset(): Void {
    for (v2 in 0...this.stats.length) {
      this.stats[v2].reset();
    }
  }

  public function countExtend(): Int {
    var v2 = 1;
    if (this.read(Data.STAT_MAX_COMBO) >= 2) {
      v2 += this.read(Data.STAT_MAX_COMBO) - 1;
    }
    return cast Math.min(7, v2);
  }

  public function spreadExtend(): Void {
    var v5;
    var v2 = this.countExtend();
    if (v2 > 0) {
      this.game.world.scriptEngine.insertExtend();
      this.extendList = new Array();
      var v3 = new Array();
      for (v4 in 0...v2) {
        do {
          v5 = this.game.randMan.draw(Data.RAND_EXTENDS_ID);
        } while (v3[v5] == true);
        v3[v5] = true;
        this.extendList.push(v5);
      }
    }
  }

  public function attachExtend(): SpecialItem {
    if (this.game.fl_clear) {
      return null;
    }
    var v2 = this.game.world.getGround(HfStd.random(Data.LEVEL_WIDTH), HfStd.random(Data.LEVEL_HEIGHT));
    var v3 = Entity.x_ctr(v2.x);
    var v4 = Entity.y_ctr(v2.y);
    var v5 = this.extendList[HfStd.random(this.extendList.length)];
    var v6 = hf.entity.item.SpecialItem.attach(this.game, v3, v4, 0, v5);
    return v6;
  }
}
