package hf;

import etwin.flash.MovieClip;
import etwin.flash.TextField;

// Class renamed from Std to HfStd
@:native("Std") // Class provided by the loader
extern class HfStd {
  public static var icounter: Int;
  public static var infinity: Float;

  public static function attachMC(mc: MovieClip, link: String, depth: Int): MovieClip;

  public static function createEmptyMC(mc: MovieClip, depth: Int): MovieClip;

  public static function duplicateMC(mc: MovieClip, depth: Int): MovieClip;

  public static function getVar(mc: MovieClip, v: String): Dynamic;

  public static function setVar(mc: MovieClip, v: String, vval: Dynamic): Void;

  public static function getRoot(): Dynamic;

  public static function getGlobal(v: String): Dynamic;

  public static function setGlobal(v: String, vv: Dynamic): Void;

  public static function createTextField(mc: MovieClip, depth: Int): TextField;

  /**
   * Normalized to `cast` during obfuscation.
   */
  public static function _cast(x: Dynamic): Dynamic;

  public static function hitTest(mc1: MovieClip, mc2: MovieClip): Bool;

  public static function random(n: Int): Int;

  public static function xmouse(): Float;

  public static function ymouse(): Float;

  public static function escape(expression: String): String;

  // Despite the `Int` return value, if `expression` isn't a valid integer, this will return `NaN`.
  public static function parseInt(expression: String, ?radix: Int): Int;

  public static function toString(x: Dynamic): String;

  public static function toStringBase(x: Dynamic, n: Int): String;

  public static function isNaN(expression: Dynamic /*TODO*/): Bool;

  // TODO: Class<MovieClip>
  public static function registerClass(name: String, theClass: Class<Dynamic>): Bool;

  public static function copy(a: Array<Dynamic /*TODO*/>, b: Array<Dynamic /*TODO*/>): Array<Dynamic /*TODO*/>;

  public static function callback(o: Dynamic, f: Dynamic /*TODO*/, ?a1: Dynamic, ?a2: Dynamic, ?a3: Dynamic, ?a4: Dynamic): Dynamic;

  /**
   * Returns the number of milliseconds that have elapsed since the SWF file started playing.
   */
  public static function getTimer(): Int;

  public static function _typeof(o: Dynamic): String;

  public static function _instanceof(o: Dynamic /*TODO*/, k: Dynamic /*TODO*/): Bool;

  public static function forin(o: Dynamic /*TODO*/, f: Dynamic /*TODO*/): Void;

  public static function makeNew(o: Dynamic /*TODO*/, ?a1: Dynamic, ?a2: Dynamic, ?a3: Dynamic, ?a4: Dynamic): Dynamic /*TODO*/;

  public static function deleteField(o: Dynamic /*TODO*/, k: String): Void;

  public static function fscommand(c: String, a: Dynamic /*TODO*/): Void;

  /**
   * Normalized to `throw` during obfuscation.
   */
  public static function _throw(e: Dynamic): Void;
}
