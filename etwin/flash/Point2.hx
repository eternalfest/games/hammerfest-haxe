package etwin.flash;

typedef Point2 = {
  var x: Float;
  var y: Float;
}
