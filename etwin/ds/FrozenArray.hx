package etwin.ds;

import haxe.macro.Expr;

/**
 * An immutable array.
 */
@:forward(
  indexOf, iterator, join, lastIndexOf, toString,
  // These methods return a writable `Array` (but don't modify _this_ array).
  concat, copy, filter, map, slice
)
abstract FrozenArray<T>(Array<T>) to ReadOnlyArray<T> {
  private static var EMPTY(default, never): FrozenArray<Dynamic> = new FrozenArray([]);

  /**
   * Creates a new `FrozenArray` containing the elements of the given iterable.
   * Example: `FrozenArray.from([1, 5, 10])`
   */
  public static inline function from<T>(iterable: Iterable<T>): FrozenArray<T> {
    return new FrozenArray([for (item in iterable) item]);
  }

  /**
   * Converts the given `array` into a `FrozenArray`. No copy will be done; you
   * must ensure that `array` won't be further modified.
   */
  public static inline function uncheckedFrom<T>(array: Array<T>): FrozenArray<T> {
    return new FrozenArray(array);
  }

  /**
   * Creates a new `FrozenArray` containing the given elements.
   * Example: `FrozenArray.of(1, 5, 10)`
   */
  macro public static function of<T>(items: Array<ExprOf<T>>): ExprOf<FrozenArray<T>> {
    return macro new etwin.ds.FrozenArray([$a{items}]);
  }

  /**
   * Creates an empty `FrozenArray`.
   */
  public static inline function empty<T>(): FrozenArray<T> {
    return cast FrozenArray.EMPTY;
  }

  private inline function new(array: Array<T>): Void {
    this = array;
  }

  /**
	 * The length of `this` array.
	 */
  public var length(get, never): Int;

  private inline function get_length() {
    return this.length;
  }

  @:arrayAccess inline function get(i: Int): T {
    return this[i];
  }
}
