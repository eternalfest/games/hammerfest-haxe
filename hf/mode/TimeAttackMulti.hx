package hf.mode;

import hf.entity.Player;
import hf.GameManager;
import hf.mode.TimeAttack;
import etwin.flash.Key;

class TimeAttackMulti extends TimeAttack {
  public function new(m: GameManager, id: Int): Void {
    super(m, id);
    this._name = "$timeMulti";
  }

  override public function initGame(): Void {
    super.initGame();
    this.destroyList(Data.PLAYER);
    this.cleanKills();
    var v3 = this.world.current.__dollar__playerX;
    var v4 = this.world.current.__dollar__playerY;
    var v5 = this.insertPlayer(v3, v4);
    v5.ctrl.setKeys(Key.UP, Key.DOWN, Key.LEFT, Key.RIGHT, Key.ENTER);
    var v6 = this.insertPlayer(Data.LEVEL_WIDTH * 0.5 + (Data.LEVEL_WIDTH * 0.5 - v3), v4);
    v6.ctrl.setKeys(90, 83, 81, 68, 65);
    this.initializePlayers();
  }

  override public function initPlayer(p: Player): Void {
    super.initPlayer(p);
    p.baseColor = Data.BASE_COLORS[p.pid];
    if (p.pid == 1) {
      p.skin = 2;
      p.defaultHead = Data.HEAD_SANDY;
      p.head = p.defaultHead;
    }
  }

  override public function initRunId(): Void {
    if (GameManager.CONFIG.hasOption(Data.OPT_SET_MTA_0)) {
      this.runId = 0;
    }
    if (GameManager.CONFIG.hasOption(Data.OPT_SET_MTA_1)) {
      this.runId = 1;
    }
    if (GameManager.CONFIG.hasOption(Data.OPT_SET_MTA_2)) {
      this.runId = 2;
    }
  }

  override public function initWorld(): Void {
    this.addWorld("xml_multitime");
  }
}
