package hf;

typedef Delta = {
  var dx: Float;
  var dy: Float;
}
