package hf.entity.supa;

import hf.entity.Supa;
import hf.mode.GameMode;

class Smoke extends Supa {
  public function new(): Void {
    super();
    this.fl_blink = false;
  }

  public static function attach(g: GameMode): Smoke {
    var v3 = "hammer_supa_smoke";
    var v4: Smoke = cast g.depthMan.attach(v3, Data.DP_SUPA);
    v4.initSupa(g, Data.GAME_WIDTH / 2, Data.GAME_HEIGHT / 2);
    return v4;
  }

  override public function initSupa(g: GameMode, x: Float, y: Float): Void {
    super.initSupa(g, x, y);
    this.scale(265);
    this.setLifeTimer(Data.SECOND * 3);
  }
}
