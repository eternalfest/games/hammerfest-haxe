package hf.entity.supa;

import hf.entity.Supa;
import hf.mode.GameMode;

class Tons extends Supa {
  public function new(): Void {
    super();
    this.fl_gravity = true;
  }

  public static function attach(g: GameMode): Tons {
    var v3 = "hammer_supa_tons";
    var v4: Tons = cast g.depthMan.attach(v3, Data.DP_SUPA);
    v4.initSupa(g, Data.GAME_WIDTH, 0);
    return v4;
  }

  override public function initSupa(g: GameMode, x: Float, y: Float): Void {
    super.initSupa(g, x, y);
    this.speed = 0;
    this.radius = 40;
    this.moveTo(HfStd.random(Data.GAME_WIDTH), -40);
    this.fallFactor = 0.3;
  }

  override public function postfix(): Void {
    super.postfix();
    if (this.y >= Data.GAME_HEIGHT * 2) {
      this.moveTo(HfStd.random(Data.GAME_WIDTH), -40);
      this.dy = 0;
    }
  }

  override public function prefix(): Void {
    super.prefix();
    var v3 = this.game.getClose(Data.BAD, this.x, this.y, this.radius, false);
    for (v4 in 0...v3.length) {
      var v5 = v3[v4];
      if (!v5.fl_kill) {
        v5.killHit((HfStd.random(2) * 2 - 1) * HfStd.random(5));
        v5.dy = -25;
      }
    }
    var v3 = this.game.getClose(Data.PLAYER, this.x, this.y, this.radius, false);
    for (v6 in 0...v3.length) {
      var v7 = v3[v6];
      if (!v7.fl_kill) {
        v7.killHit((HfStd.random(2) * 2 - 1) * HfStd.random(5));
        v7.dy = -25;
      }
    }
  }
}
