package hf.entity.bad;

import hf.entity.Bad;
import hf.mode.GameMode;
import hf.Pos;

typedef MvtParams = {
  var x: Float;
  var y: Float;
  var cp: Pos<Int>;
  var xSpeed: Float;
  var ySpeed: Float;
}

class WallWalker extends Bad {
  public static var SUB_RECAL: Float = 0.2;

  public var speed: Float;
  public var subOffset: Float;
  public var fl_lost: Bool;
  public var xSub: Float;
  public var xSubBase: Float;
  public var ySub: Float;
  public var ySubBase: Float;
  public var fl_wallWalk: Null<Bool>;
  public var xSpeed: Float;
  public var ySpeed: Float;
  public var cp: Pos<Int>;
  public var lastSafe: Null<MvtParams>;

  public function new(): Void {
    super();
    this.speed = 3;
    this.angerFactor = 0.4;
    this.subOffset = 8;
    this.fl_intercept = false;
    this.fl_lost = false;
    this.fl_largeTrigger = true;
  }

  public function wallWalk(): Void {
    if (!this.isHealthy() || !this.isReady()) {
      return;
    }
    this.fl_wallWalk = true;
    this.fl_gravity = false;
    this.fl_friction = false;
    this.fl_hitWall = false;
    this.fl_hitGround = false;
    this.fl_hitBorder = false;
    var v2 = [{x: 1, y: 0, cpx: 0, cpy: 1}, {x: -1, y: 0, cpx: 0, cpy: 1}, {x: 1, y: 0, cpx: 0, cpy: -1}, {x: -1, y: 0, cpx: 0, cpy: -1}, {x: 0, y: 1, cpx: 1, cpy: 0}, {x: 0, y: -1, cpx: 1, cpy: 0}, {x: 0, y: 1, cpx: -1, cpy: 0}, {x: 0, y: -1, cpx: -1, cpy: 0}];
    var v3 = 0;
    while (v3 < v2.length) {
      var v4 = v2[v3];
      if (this.world.getCase({x: this.cx + v4.x, y: this.cy + v4.y}) > 0 || this.world.getCase({x: this.cx + v4.cpx, y: this.cy + v4.cpy}) <= 0) {
        v2.splice(v3, 1);
        --v3;
      }
      ++v3;
    }
    var v5 = v2[HfStd.random(v2.length)];
    if (v5 != null) {
      this.setDir(v5.x, v5.y);
      this.setCP(v5.cpx, v5.cpy);
      this.playAnim(Data.ANIM_BAD_WALK);
    } else {
      this.suicide();
    }
    this.fl_softRecal = false;
  }

  public function land(): Void {
    this.fl_wallWalk = false;
    this.fl_gravity = true;
    this.fl_friction = true;
    this.fl_hitWall = true;
    this.fl_hitGround = true;
    this.fl_hitBorder = true;
  }

  // xoff: -1 | 0 | 1
  // yoff: -1 | 0 | 1
  public function setDir(xoff: Int, yoff: Int): Void {
    this.xSpeed = this.speed * xoff;
    this.ySpeed = this.speed * yoff;
    this.updateSpeed();
    this.x = Entity.x_ctr(this.cx);
    this.y = Entity.y_ctr(this.cy);
    this.activateSoftRecal();
    this.setNext(this.dx, this.dy, 0, Data.ACTION_MOVE);
  }

  // xoff: -1 | 0 | 1
  // yoff: -1 | 0 | 1
  public function setCP(xoff: Int, yoff: Int): Void {
    this.cp = {x: xoff, y: yoff};
  }

  public function wallWalkIA(): Void {
    if (this.deathTimer > 0) {
      return;
    }
    var v2 = Math.round((this.dx != 0) ? this.dx / Math.abs(this.dx) : 0);
    var v3 = Math.round((this.dy != 0) ? this.dy / Math.abs(this.dy) : 0);
    if (this.cy == 0) {
      if (this.dy < 0) {
        this.setDir(-this.cp.x, 0);
        this.setCP(0, -1);
      } else {
        if (this.world.getCase({x: this.cx + v2, y: this.cy}) > 0) {
          this.setDir(0, 1);
          this.setCP(v2, 0);
        }
      }
    } else {
      if (this.world.getCase({x: this.cx + this.cp.x, y: this.cy + this.cp.y}) <= 0) {
        this.setDir(this.cp.x, this.cp.y);
        this.setCP(-v2, -v3);
      } else {
        if (this.world.getCase({x: this.cx + v2, y: this.cy + v3}) > 0) {
          this.setDir(-this.cp.x, -this.cp.y);
          this.setCP(v2, v3);
        }
      }
    }
    var v4 = true;
    var v5 = 0;
    while (true) {
      if (!(v4 && v5 < 4)) break;
      v2 = Math.round((this.dx != 0) ? this.dx / Math.abs(this.dx) : 0);
      v3 = Math.round((this.dy != 0) ? this.dy / Math.abs(this.dy) : 0);
      if (this.world.getCase({x: this.cx + v2, y: this.cy + v3}) > 0 && this.world.getCase({x: this.cx + this.cp.x, y: this.cy + this.cp.y}) > 0) {
        this.setDir(-this.cp.x, -this.cp.y);
        this.setCP(v2, v3);
      } else {
        v4 = false;
      }
      ++v5;
    }
    if (v4 && this.deathTimer <= 0) {
      this.suicide();
    }
  }

  public function suicide(): Void {
    if (this.deathTimer > 0) {
      return;
    }
    this.dx = 0;
    this.dy = 0;
    this.land();
    this.fl_lost = true;
    this.deathTimer = Data.SECOND * 3;
  }

  public function moveToSafePos(): Void {
    if (this.x != this.lastSafe.x || this.y != this.lastSafe.x) {
      this.x = this.lastSafe.x;
      this.y = this.lastSafe.y;
      this.cp = this.lastSafe.cp;
      this.xSpeed = this.lastSafe.xSpeed;
      this.ySpeed = this.lastSafe.ySpeed;
      this.updateCoords();
    }
  }

  override public function endUpdate(): Void {
    super.endUpdate();
    if (this.isHealthy()) {
      this.xSub = this.xSubBase + this.cp.x * this.subOffset;
      this.ySub = this.ySubBase + this.cp.y * this.subOffset;
      if (this.cp.y > 0) {
        this.ySub = this.ySubBase + this.subOffset * 0.5;
      }
    } else {
      this.xSub = this.xSubBase;
      if (this.fl_freeze) {
        this.ySub = this.ySubBase;
      } else {
        this.ySub = this.ySubBase + this.subOffset * 0.5;
      }
    }
    this.sub._x += hf.entity.bad.WallWalker.SUB_RECAL * (this.xSub - this.sub._x) * this.speedFactor;
    this.sub._y += hf.entity.bad.WallWalker.SUB_RECAL * (this.ySub - this.sub._y) * this.speedFactor;
  }

  override public function infix(): Void {
    super.infix();
    if (this.fl_wallWalk) {
      this.wallWalkIA();
    }
  }

  override public function initBad(g: GameMode, x: Float, y: Float): Void {
    super.initBad(g, x, y);
    this.xSub = this.sub._x;
    this.ySub = this.sub._y;
    this.xSubBase = this.xSub;
    this.ySubBase = this.ySub;
    this.wallWalk();
  }

  override public function isReady(): Bool {
    return this.isHealthy() && !this.fl_lost;
  }

  override public function killHit(dx: Null<Float>): Void {
    super.killHit(dx);
    this.land();
    this.fl_hitGround = false;
    this.fl_hitWall = false;
  }

  override public function onFreeze(): Void {
    super.onFreeze();
    if (this.fl_wallWalk) {
      this.fl_intercept = true;
    }
    this.land();
  }

  override public function onHitGround(h: Float): Void {
    super.onHitGround(h);
    this.fl_intercept = false;
    if (this.fl_lost) {
      this.fl_lost = false;
      this.deathTimer = null;
      this.wallWalk();
    }
  }

  override public function onHitWall(): Void {
    if (!this.fl_wallWalk) {
      if (this.world.getCase({x: this.cx, y: this.cy}) != Data.WALL) {
        this.dx = -this.dx;
      }
      return;
    }
  }

  override public function onKnock(): Void {
    super.onKnock();
    this.land();
  }

  override public function onMelt(): Void {
    super.onMelt();
    this.wallWalk();
  }

  override public function onWakeUp(): Void {
    super.onWakeUp();
    this.wallWalk();
  }

  override public function update(): Void {
    if ((this.game.getOne(Data.PLAYER)).y > this.y) {
      this.realRadius = Data.CASE_WIDTH * 1.4;
    } else {
      this.realRadius = Data.CASE_WIDTH;
    }
    super.update();
    if (this.lastSafe != null && this.world.getCase({x: Entity.x_rtc(this.lastSafe.x) + this.lastSafe.cp.x, y: Entity.y_rtc(this.lastSafe.y) + this.lastSafe.cp.y}) <= 0) {
      this.lastSafe = null;
      this.fl_gravity = true;
      this.fl_wallWalk = false;
      this.fl_hitGround = true;
      this.fl_friction = true;
      this.fl_lost = true;
    }
    if (this.world.getCase({x: this.cx + this.cp.x, y: this.cy + this.cp.y}) > 0) {
      this.lastSafe = {x: this.x, y: this.y, xSpeed: this.xSpeed, ySpeed: this.ySpeed, cp: {x: this.cp.x, y: this.cp.y}};
    }
  }

  override public function updateSpeed(): Void {
    super.updateSpeed();
    if (this.fl_wallWalk && this.isReady()) {
      this.dx = this.xSpeed * this.speedFactor;
      this.dy = this.ySpeed * this.speedFactor;
    }
  }
}
