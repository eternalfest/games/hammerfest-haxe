package hf.entity.shoot;

import hf.entity.Shoot;
import hf.Entity;
import hf.mode.GameMode;

class Pepin extends Shoot {
  public function new(): Void {
    super();
    this.shootSpeed = 5;
    this._yOffset = -2;
  }

  public static function attach(g: GameMode, x: Float, y: Float): Pepin {
    var v5 = "hammer_shoot_pepin";
    var v6: Pepin = cast g.depthMan.attach(v5, Data.DP_SHOTS);
    v6.initShoot(g, x, y);
    return v6;
  }

  override public function hit(e: Entity): Void {
    var v3 = Data.PLAYER.downcast(e);
    if (v3 != null) {
      v3.killHit(this.dx);
    }
  }
}
