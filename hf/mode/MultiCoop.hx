package hf.mode;

import hf.entity.Player;
import hf.GameManager;
import hf.mode.Adventure;
import etwin.flash.Key;

class MultiCoop extends Adventure {
  public function new(m: GameManager, id: Int): Void {
    super(m, id);
    this._name = "$multi";
    this.fl_bullet = false;
    this.fl_disguise = false;
    this.fl_nightmare = GameManager.CONFIG.hasOption(Data.OPT_NIGHTMARE_MULTI);
    this.fl_mirror = GameManager.CONFIG.hasOption(Data.OPT_MIRROR_MULTI);
    this.fl_map = true;
    this.fl_bombControl = GameManager.CONFIG.hasOption(Data.OPT_BOMB_CONTROL);
  }

  override public function initGame(): Void {
    super.initGame();
    this.destroyList(Data.PLAYER);
    this.cleanKills();
    var v3 = this.insertPlayer(5, -2);
    v3.ctrl.setKeys(Key.UP, Key.DOWN, Key.LEFT, Key.RIGHT, Key.ENTER);
    if (!this.fl_bombControl) {
      var v4 = this.insertPlayer(14, -2);
      v4.ctrl.setKeys(82, 70, 68, 71, 65);
    } else {
      v3.ctrl.setAlt(v3.ctrl.attack, Key.CONTROL);
    }
  }

  override public function initPlayer(p: Player): Void {
    super.initPlayer(p);
    p.baseColor = Data.BASE_COLORS[p.pid];
    p.darkColor = Data.DARK_COLORS[p.pid];
    p.lives = Math.ceil(p.lives * 0.5);
    if (p.pid == 1) {
      p.name = "$Sandy".substring(1);
      p.skin = 2;
      p.defaultHead = Data.HEAD_SANDY;
      p.head = p.defaultHead;
    }
  }

  override public function onPause(): Void {
    super.onPause();
    this.pauseMC.gotoAndStop("3");
    this.pauseMC.move.text = Lang.get(29);
    this.pauseMC.attack.text = Lang.get(42);
    this.pauseMC.click.text = "";
    var v3 = Lang.get(301 + this.tipId++);
    if (v3 == null) {
      this.tipId = 0;
      v3 = Lang.get(301 + this.tipId++);
    }
    this.pauseMC.tip.html = true;
    this.pauseMC.tip.htmlText = "<b>" + Lang.get(300) + "</b>" + v3;
  }

  override public function saveScore(): Void {
    var v2 = this.getPlayerList();
    (HfStd.getGlobal("gameOver"))(-1, null, {__dollar__reachedLevel: this.dimensions[0].currentId, __dollar__item2: this.getPicks2(), __dollar__data: {__dollar__s0: this.savedScores[0], __dollar__s1: this.savedScores[1]}});
  }
}
