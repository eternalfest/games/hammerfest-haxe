package hf.entity.boss;

import etwin.flash.filters.GlowFilter;
import hf.entity.Mover;
import hf.entity.shoot.BossFireBall;
import hf.mode.GameMode;
import etwin.flash.Key;

class Bat extends Mover {
  public static var GLOW_BASE: Float = 8;
  public static var GLOW_RANGE: Float = 4;
  public static var RADIUS: Float = Data.CASE_WIDTH * 1.5;
  public static var SPEED: Float = 6.5;
  public static var SHOOT_SPEED: Float = 8;
  public static var LIVES: Int = 3;
  public static var WAIT_TIME: Float = Data.SECOND * 3.5;
  public static var FLOAT_X: Float = 5;
  public static var FLOAT_Y: Float = 10;
  public static var MAX_FALL_ROTATION: Float = 80;

  public var fl_trap: Bool;
  public var fl_move: Bool;
  public var fl_wait: Bool;
  public var fl_shield: Bool;
  public var fl_immune: Bool;
  public var immuneTimer: Float;
  public var fl_anger: Bool;
  public var floatOffset: Float;
  public var fl_death: Bool;
  public var fl_deathUp: Bool;
  public var lives: Int;
  public var fbList: Array<BossFireBall>;
  public var glow: GlowFilter;
  public var tx: Float;
  public var ty: Float;
  public var glowCpt: Float;

  public function new(): Void {
    super();
    this.fl_hitGround = false;
    this.fl_hitCeil = false;
    this.fl_hitWall = false;
    this.fl_hitBorder = true;
    this.fl_gravity = false;
    this.fl_friction = false;
    this.fl_moveable = false;
    this.fl_alphaBlink = false;
    this.fl_trap = false;
    this.fl_move = false;
    this.fl_wait = false;
    this.fl_shield = false;
    this.fl_immune = false;
    this.immuneTimer = 0;
    this.fl_anger = false;
    this.floatOffset = 0;
    this.fl_death = false;
    this.fl_deathUp = false;
    this.dir = 1;
    this.lives = hf.entity.boss.Bat.LIVES;
    this.fbList = new Array();
    this.glow = new etwin.flash.filters.GlowFilter();
  }

  public static function attach(g: GameMode): Bat {
    var v3 = "hammer_boss_bat";
    var v4: Bat = cast g.depthMan.attach(v3, Data.DP_BADS);
    v4.initBoss(g);
    return v4;
  }

  public function initBoss(g: GameMode): Void {
    this.init(g);
    this.moveTo(Data.GAME_WIDTH * 0.5, 30);
    this.playAnim(Data.ANIM_BAT_INTRO);
    this._xscale = -this.scaleFactor * 100;
    this.endUpdate();
  }

  public function moveRandom(): Void {
    if (this.fl_death) {
      return;
    }
    var v2 = 0.8;
    do {
      this.tx = Data.GAME_WIDTH * (1 - v2) * 0.5 + HfStd.random(Math.round(Data.GAME_WIDTH * v2));
      this.ty = Data.GAME_HEIGHT * (1 - v2) * 0.5 + HfStd.random(Math.round(Data.GAME_HEIGHT * v2));
    } while(this.distance(this.tx, this.ty) < Data.BOSS_BAT_MIN_DIST || Math.abs(this.x - this.tx) < Data.BOSS_BAT_MIN_X_DIST);
    var v3 = Math.round((this.tx - this.x) / Math.abs(this.tx - this.x));
    if (v3 != this.dir) {
      this.flip();
    } else {
      this.playAnim(Data.ANIM_BAT_MOVE);
      this.fl_move = true;
    }
  }

  public function flip(): Void {
    this.halt();
    this.playAnim(Data.ANIM_BAT_SWITCH);
    this.dir = -this.dir;
    this._xscale = this.dir * this.scaleFactor * 100;
  }

  public function halt(): Void {
    this.fl_stopStepping = true;
    this.fl_move = false;
    this.dx = 0;
    this.dy = 0;
  }

  public function immune(): Void {
    this.fl_immune = true;
    this.immuneTimer = Data.SECOND * 3;
  }

  public function removeImmunity(): Void {
    this.fl_immune = false;
    this.stopBlink();
    this.shield();
  }

  public function wait(): Void {
    this.fl_wait = true;
  }

  public function stopWait(): Void {
    this.fl_wait = false;
    this.x = this._x;
    this.y = this._y;
  }

  public function shield(): Void {
    if (this.fl_shield || this.fl_death) {
      return;
    }
    this.fl_shield = true;
    this.filters = null;
    this.glowCpt = 0;
  }

  public function removeShield(): Void {
    this.fl_shield = false;
    this.glow.quality = 1;
    this.glow.color = 16766860;
    this.glow.strength = 180;
    this.glow.blurX = hf.entity.boss.Bat.GLOW_BASE;
    this.glow.blurY = hf.entity.boss.Bat.GLOW_BASE;
    this.glow.alpha = 1.0;
    this.filters = [this.glow];
  }

  public function freeze(d: Float): Void {
    if (this.fl_immune || this.fl_death) {
      return;
    }
    if (this.fl_shield) {
      this.removeShield();
      this.bossAnger();
    } else {
      this.stopWait();
      this.immune();
      this.shield();
      this.bossCalmDown();
      this.loseLife();
    }
  }

  public function knock(): Void {
  }

  public function onPlayerDeath(): Void {
    this.bossCalmDown();
    this.shield();
  }

  public function loseLife(): Void {
    if (this.fl_death) {
      return;
    }
    this.blinkColor = 16711680;
    this.blinkColorAlpha = 100;
    this.blink(Data.BLINK_DURATION);
    this.playAnim(Data.ANIM_BAT_KNOCK);
    this.game.fxMan.attachExplosion(this.x, this.y, 100);
    --this.lives;
    if (this.lives <= 0) {
      this.kill();
    } else {
      this.game.shake(Data.SECOND, 2);
    }
  }

  public function kill(): Void {
    this.removeShield();
    this.game.shake(Data.SECOND * 5, 3);
    this.playAnim(Data.ANIM_BAT_MOVE);
    this.blinkColorAlpha = 50;
    this.halt();
    this.filters = null;
    this.fl_wait = false;
    this.fl_death = true;
    this.fl_deathUp = true;
    this.rotation = -30 * this.dir;
    this.dy = -1.5;
    if (this.y >= Data.GAME_HEIGHT * 0.5) {
      this.dy *= 2;
    }
    this.floatOffset = 0;
    this.setNext(null, null, Data.SECOND * 9999, Data.ACTION_MOVE);
  }

  public function attachFireBall(ang: Float, distFactor: Float): BossFireBall {
    var v4 = hf.entity.shoot.BossFireBall.attach(this.game, this.x, this.y);
    v4.initBossShoot(this, ang);
    this.fbList.push(v4);
    v4.maxDist *= distFactor;
    v4.distSpeed *= distFactor;
    return v4;
  }

  public function bossAnger(): Void {
    this.stopWait();
    this.halt();
    this.playAnim(Data.ANIM_BAT_ANGER);
    this.game.fxMan.attachExplosion(this.x, this.y, 60);
    this.fl_anger = true;
    this.attachFireBall(0, 1.0);
    this.attachFireBall(180, 1.0);
    this.attachFireBall(0, 0.5);
    this.attachFireBall(180, 0.5);
    this.attachFireBall(90, 1.0);
    this.attachFireBall(270, 1.0);
    this.setNext(null, null, Data.SECOND * 15, Data.ACTION_MOVE);
  }

  public function bossCalmDown(): Void {
    for (v2 in 0...this.fbList.length) {
      this.fbList[v2].destroy();
    }
    if (this.fl_anger) {
      this.game.fxMan.attachExplosion(this.x, this.y, 50);
      this.playAnim(Data.ANIM_BAT_WAIT);
    }
    this.fbList = new Array();
    this.setNext(null, null, Data.SECOND, Data.ACTION_MOVE);
    this.fl_anger = false;
  }

  override public function destroy(): Void {
    super.destroy();
    this.game.fl_clear = true;
    this.game.fxMan.attachExit();
  }

  override public function endUpdate(): Void {
    super.endUpdate();
    if (this.fl_wait) {
      this.floatOffset -= 0.1 * this.dir;
      this._x = this.x + Math.sin(this.floatOffset) * hf.entity.boss.Bat.FLOAT_X;
      this._y = this.y + Math.cos(this.floatOffset) * hf.entity.boss.Bat.FLOAT_Y;
    }
    if (this.fl_death) {
      if (!this.fl_deathUp) {
        this._rotation = hf.entity.boss.Bat.MAX_FALL_ROTATION + Math.sin(this.floatOffset) * 7;
        this.floatOffset -= 0.5 * this.dir;
        this._x = this.x + Math.sin(this.floatOffset) * hf.entity.boss.Bat.FLOAT_X * 1.7;
      } else {
        this.floatOffset -= 0.2 * this.dir;
        this._x = this.x + Math.sin(this.floatOffset) * hf.entity.boss.Bat.FLOAT_X * 3;
      }
    }
  }

  override public function infix(): Void {
    super.infix();
  }

  override public function init(g: GameMode): Void {
    super.init(g);
    this.register(Data.BAD);
    this.register(Data.BOSS);
  }

  override public function killHit(dx: Null<Float>): Void {
  }

  override public function onEndAnim(id: Int): Void {
    super.onEndAnim(id);
    if (id == Data.ANIM_BAT_INTRO.id) {
      this._xscale = this.scaleFactor * 100;
      this.shield();
      this.moveRandom();
    }
    if (id == Data.ANIM_BAT_SWITCH.id) {
      this.playAnim(Data.ANIM_BAT_MOVE);
      this.fl_move = true;
    }
  }

  override public function onNext(): Void {
    if (this.next.action == Data.ACTION_MOVE) {
      if (this.fl_anger) {
        this.bossCalmDown();
        this.shield();
        return;
      }
      this.moveRandom();
      this.stopWait();
    }
    this.next = null;
  }

  override public function prefix(): Void {
    super.prefix();
    if (!this.fl_immune && !this.fl_death) {
      var v3 = this.game.getClose(Data.PLAYER, this.x, this.y + Data.CASE_HEIGHT * 0.5, hf.entity.boss.Bat.RADIUS, false);
      for (v4 in 0...v3.length) {
        var v5 = v3[v4];
        v5.killHit(this.dx);
      }
    }
  }

  override public function update(): Void {
    if (!this.fl_death) {
      this.game.huTimer = 0;
    } else {
      this.game.huTimer += Timer.tmod * 3;
    }
    super.update();
    if (this.fl_immune) {
      this.immuneTimer -= Timer.tmod;
      if (this.immuneTimer <= 0) {
        this.removeImmunity();
      }
    }
    if (this.game.manager.isDev() && Key.isDown(75) && !this.fl_death) {
      this.kill();
    }
    if (this.fl_move) {
      this.moveToPoint(this.tx, this.ty, hf.entity.boss.Bat.SPEED);
      this.glow.alpha = 0.5;
      if (this.distance(this.tx, this.ty) <= 10) {
        this.glow.alpha = 1.0;
        this.halt();
        this.wait();
        this.floatOffset = 0;
        this.playAnim(Data.ANIM_BAT_WAIT);
        this.setNext(null, null, hf.entity.boss.Bat.WAIT_TIME, Data.ACTION_MOVE);
      }
    }
    if (!this.fl_shield && !this.fl_death) {
      this.glowCpt += 0.3 * Timer.tmod;
      this.glow.blurX = hf.entity.boss.Bat.GLOW_BASE + hf.entity.boss.Bat.GLOW_RANGE * Math.sin(this.glowCpt);
      this.glow.blurY = this.glow.blurX;
      this.filters = [this.glow];
    }
    if (this.fl_death) {
      if (this.fl_deathUp && HfStd.random(6) == 0) {
        this.game.fxMan.inGameParticles(Data.PARTICLE_STONE, HfStd.random(Data.GAME_WIDTH), 0, HfStd.random(3) + 1);
        this.game.shake(Data.SECOND, 1);
        this.game.fxMan.attachExplodeZone(this.x + HfStd.random(20) * (HfStd.random(2) * 2 - 1), this.y + HfStd.random(20) * (HfStd.random(2) * 2 - 1), HfStd.random(40) + 10);
      }
      if (!this.fl_deathUp && HfStd.random(2) == 0) {
        var v3 = this.game.fxMan.attachFx(this.x + HfStd.random(20) * (HfStd.random(2) * 2 - 1), this.y - HfStd.random(40), "hammer_fx_pop");
        v3.mc._rotation = HfStd.random(360);
        v3.mc._xscale = HfStd.random(50) + 50;
        v3.mc._yscale = v3.mc._xscale;
      }
      if (this.y <= -50 && this.fl_deathUp) {
        this._xscale = this.scaleFactor * 100;
        this.playAnim(Data.ANIM_BAT_FINAL_DIVE);
        this.dy = 5;
        this.fl_deathUp = false;
      }
      if (!this.fl_deathUp) {
        this.rotation = Math.min(hf.entity.boss.Bat.MAX_FALL_ROTATION, this.rotation + 2.5 * Timer.tmod);
        this.dy += 0.1 * Timer.tmod;
        if (HfStd.random(2) == 0) {
          this.game.fxMan.inGameParticles(Data.PARTICLE_SPARK, this.x, this.y, HfStd.random(2) + 1);
        }
        if (this.y >= Data.DEATH_LINE + Data.GAME_HEIGHT * 1.5) {
          this.game.shake(Data.SECOND * 1.5, 5);
          this.destroy();
        }
      }
    }
  }
}
